#version 150

uniform mat4 MVP_Matrix;
uniform mat4 MV_Matrix;

in vec3 in_Position;

out float f_Height;

void main(void)
{
	f_Height = in_Position.y;
	gl_Position = MVP_Matrix * vec4(in_Position,1.0);
}
