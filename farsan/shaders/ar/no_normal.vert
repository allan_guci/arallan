#version 150// core   

// IN
in  vec3 in_Position;
out float g_Depth;

uniform mat4 MVP_Matrix;


void main(void)
{
	g_Depth			= abs(in_Position.z-1.5);
	gl_Position 	= MVP_Matrix*vec4(in_Position, 1.0); 	
}

