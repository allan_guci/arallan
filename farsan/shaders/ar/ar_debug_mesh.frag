#version 400


in float f_Height;

out vec4 out_Color;

void main(void)
{

	vec4 color = vec4(0.0,0.5,0.9,0.0)*vec4(f_Height+0.8);
	out_Color = (f_Height-2)*vec4(1.0); //(color)*0.8; 

}
