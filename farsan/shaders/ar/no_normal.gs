#version 150

layout (triangles) in;
layout(line_strip, max_vertices =3) out;
//layout(triangle_strip, max_vertices = 3) out;
in  float g_Depth[3];
out float f_Depth;
void main()
{

	for(int i = 0; i < 3; i++){
			gl_Position = gl_in[i].gl_Position;
			f_Depth = g_Depth[i];
			EmitVertex();
	}
	EndPrimitive();

}

