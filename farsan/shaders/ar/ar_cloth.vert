#version 150// core   

// IN
in  vec3 in_Position;
in  vec3 in_Normal;

// OUT

out vec3 f_Position;
out vec3 f_Normal;
out vec3 f_LightPosition;

uniform mat4 MV_Matrix;
uniform mat4 MVP_Matrix;
uniform mat3 Normal_Matrix;

void main(void)
{
	/* Hard coded light position, To be uniform */
	vec3 light = vec3(0.0, -2.0, 3.0); 	
	
	f_Normal   = Normal_Matrix*in_Normal;
	f_Position = vec3(MV_Matrix*vec4(in_Position,1.0));
	f_LightPosition = vec3(MV_Matrix*vec4(light,1.0));
	gl_Position 	= MVP_Matrix*vec4(in_Position, 1.0); 	
}

