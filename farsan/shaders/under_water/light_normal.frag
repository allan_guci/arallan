#version 400

out vec4 out_Color;

in vec3 f_Centroid;
in vec3 f_Position;
in float f_PositionThres;

void main(void)
{
	
	float att =   max(0.6-length(f_Position-f_Centroid),0.0);	
	vec4 color = vec4(att);
	color.a = 0;
	out_Color = vec4(color);
}
