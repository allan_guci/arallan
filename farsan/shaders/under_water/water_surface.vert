#version 150

// IN
in  vec3 in_Position;
in  vec3 in_Normal;
in vec2 in_TextureCoord;
// OUT

out vec3 f_Position;
out vec3 f_Normal;
out vec3 f_LightPosition;
out vec2 f_TextureCoord;
out vec3 f_NormalWorld_y;

uniform vec3 u_Light;

uniform mat4 MV_Matrix;
uniform mat4 MVP_Matrix;
uniform mat3 Normal_Matrix;

void main(void)
{

	f_TextureCoord = in_TextureCoord/10;


	f_NormalWorld_y = in_Normal;
	f_Normal   = Normal_Matrix*in_Normal;
	f_Position = vec3(MV_Matrix*vec4(in_Position,1.0));
	f_LightPosition = Normal_Matrix*u_Light;
	gl_Position 	= MVP_Matrix*vec4(in_Position, 1.0); 	
}
