#version 150

// IN
in  vec3 in_Position;
in  vec3 in_Normal;
in vec2 in_TextureCoord;

// OUT
/*
out float f_LightContribution;
out float f_LightDistance;
*/
out vec3 g_Position;
out float g_LightContribution;
out float g_LightDistance;


uniform vec3 u_Light;

uniform mat4 MVP_Matrix;

void main(void)
{

	float refractive_index = 1/1.333;
	float bottom_y_offset = -1;
	float reflection_tres =  0.659300324479335;
	vec3 hc_light = vec3(0.0,-4.0,0.0);
	
	/*
		g_Position = in_Position;
		g_Normal = in_Normal;
		g_LightDir = hc_light;
	*/
	
	vec3 nw  = normalize(-in_Normal);
	vec3 ldw = normalize(hc_light);
	vec3 pw  = in_Position;

	vec3 cross_nw_ldw = cross(nw,ldw); 
	float c0 = 1-pow(refractive_index,2.0)*dot(cross_nw_ldw,cross_nw_ldw);
	float s0 = sign(c0);

	float cos_thetha_i = dot(-ldw,nw);
	float sin_thetha_t = pow(refractive_index,2.0)*(1-pow(cos_thetha_i,2.0));
	vec3 refracted_dir = refractive_index*ldw + (refractive_index*cos_thetha_i  - sqrt(1-sin_thetha_t))*nw; // ;//(refractive_index*cross(nw,cross(-nw,ldw))-nw*sqrt(abs(c0)));
	refracted_dir /= abs(refracted_dir.y);

	float XZscale = 0.4;
	vec3 scale = vec3(XZscale, 1.0, XZscale);
	
	float deltaY = (pw.y-bottom_y_offset); 
	
	vec3 refracted_delta   = scale*deltaY*refracted_dir;
	vec3 intesection_point = pw + refracted_delta;
	
	float reflection_att = abs(dot(nw,-ldw));
	float cutoff = 0.506;
	float slope = 0.2;
	reflection_att =  clamp((reflection_att-cutoff)*2.0,0.0,1.0);
	
	g_LightDistance = length(refracted_delta);
	g_LightContribution =  1.0-4*length(refracted_dir.xz); 
	g_Position = intesection_point;

	
	gl_Position 	= MVP_Matrix*vec4(intesection_point, 1.0); 	    
}
