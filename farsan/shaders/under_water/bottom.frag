#version 400


uniform sampler2D u_TextureSand;
uniform sampler2D u_RefractionMap;

in vec2 f_TextureCoord;
in vec2 f_RefractionTextureCoord;

out vec4 out_Color;

void main(void)
{

	vec4 sand_color = texture(u_TextureSand,f_TextureCoord*2.0);
	
	float w = 0.0009765625;
	vec4 light = vec4(0.0);
	int fsize = 2;
	
	const float weights[25] ={	
			0.0144188183624608,	0.0280840233563492,	0.0350727008055935	,0.0280840233563492	,0.0144188183624608,
			0.0280840233563492,	0.0547002083009359,	0.0683122932707802	,0.0547002083009359	,0.0280840233563492,
			0.0350727008055935,	0.0683122932707802,	0.0853117301901251	,0.0683122932707802	,0.0350727008055935,
			0.0280840233563492,	0.0547002083009359,	0.0683122932707802	,0.0547002083009359	,0.0280840233563492,
			0.0144188183624608,	0.0280840233563492,	0.0350727008055935	,0.0280840233563492	,0.0144188183624608
			};
	int i_weights =  0;
	for(int y = -fsize; y <= fsize; y++)
		for(int x = -fsize; x <= fsize; x++){
			vec2 offset = w*vec2(float(x),float(y));
			float weight = weights[i_weights++];
			light += weight*texture(u_RefractionMap,f_RefractionTextureCoord+offset);
		}
	vec4 light_color = 0.9*vec4(2.8,1.4,0.8,1.0);
	light_color.a  = 1.0;
	float light_blend = texture(u_RefractionMap,f_RefractionTextureCoord).x;
	//light_blend = pow(abs(light_blend),4.0); 
	//light = texture(u_RefractionMap,f_TextureCoord);
	
	out_Color = light_color*min(0.1*light_blend,0.7)+sand_color; //mix(sand_color,light_color,light_blend); //min(light_blend,0.99)); 
}
