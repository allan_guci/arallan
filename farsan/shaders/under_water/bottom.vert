#version 150

// IN
in  vec3 in_Position;
in 	vec2 in_TextureCoord;

// OUT
out vec2 f_TextureCoord;
out vec2 f_RefractionTextureCoord;

uniform vec3 u_CamPos;

uniform mat4 MVP_Matrix;
uniform mat4 TextureMatrix;

void main(void)
{

	vec3 p = in_Position*5.0;
	
	vec4 projectedCoordinates = TextureMatrix * vec4(p,1.0);
	projectedCoordinates /= projectedCoordinates.w;
	
	f_RefractionTextureCoord = projectedCoordinates.st; 
 	f_TextureCoord = in_TextureCoord;
	
	gl_Position 	= MVP_Matrix*vec4(p, 1.0); 	
}
