#version 150

// IN
in  vec3 in_Position;
in vec2 in_TextureCoord;
// OUT
out vec2 f_TextureCoord;

// UNIFORM
uniform mat4 MVP_Matrix;

void main(void)
{

	f_TextureCoord = in_TextureCoord;
	gl_Position 	= MVP_Matrix*vec4(in_Position, 1.0); 	
}
