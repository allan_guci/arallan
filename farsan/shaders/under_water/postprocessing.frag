#version 400
uniform sampler2D u_Texture;
uniform sampler2D u_TextureDepth;

in vec2 f_TexCoord;
out vec4 out_Color;

float readDepth( in vec2 coord )
{
	float zNear = 0.2;
	float zFar = 40.0;
	float z_from_depth_texture = texture(u_TextureDepth, coord).x;
	float z_sb = 2.0 * z_from_depth_texture - 1.0; 
	float z_world = 2.0 * zNear * zFar / (zFar + zNear - z_sb * (zFar - zNear)); 
	return z_world;
}

void main(){
	
	//depth = clamp(pow(0.8*depth,2.0),0.0,1.0);
		/*
		float light = 0.0;
		int fsize = 2;
		float w = 0.0009765625;
		const float weights[25] ={	
		0.0144188183624608,	0.0280840233563492,	0.0350727008055935	,0.0280840233563492	,0.0144188183624608,
		0.0280840233563492,	0.0547002083009359,	0.0683122932707802	,0.0547002083009359	,0.0280840233563492,
		0.0350727008055935,	0.0683122932707802,	0.0853117301901251	,0.0683122932707802	,0.0350727008055935,
		0.0280840233563492,	0.0547002083009359,	0.0683122932707802	,0.0547002083009359	,0.0280840233563492,
		0.0144188183624608,	0.0280840233563492,	0.0350727008055935	,0.0280840233563492	,0.0144188183624608
		};
		int i_weights =  0;
		for(int y = -fsize; y <= fsize; y++)
		for(int x = -fsize; x <= fsize; x++){
		vec2 offset = w*vec2(float(x),float(y));
			float weight = weights[i_weights++];
			light += weight*texture(u_Texture,f_TexCoord+offset).x;
		}
	
	light*= 0.24; */

	vec4 sceneColor = texture(u_Texture,f_TexCoord);
	
	out_Color = sceneColor; // mix(sceneColor,depthColor,depthWorld); //pow(sceneColor*0.4,vec4(0.8)); // mix(depthColor,sceneColor,sceneColor.a);
}

