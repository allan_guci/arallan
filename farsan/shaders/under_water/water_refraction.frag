#version 150

out vec4 out_Color;

in vec2 f_TextureCoord;
in float f_ampl;
void main(void)
{
	float c  = 0.9;
//	c = pow(c, 0.1);
	float alpha = 1.0-length(0.5-f_TextureCoord)*2.0;
	
	vec4 color = 0.2*f_ampl*vec4(c);
	color.a *= alpha;
	out_Color = vec4(color);
}
