#version 150


in  vec3 in_Position;
in  vec2 in_TextureCoord;

in 	vec4 in_Instanced0;
in  vec4 in_Instanced1;

out vec2 f_TextureCoord;


uniform mat4 V_Matrix;
uniform mat3 Normal_Matrix;
uniform mat4 MVP_Matrix;

out float f_ampl;

void main(void)
{

	vec3 word_position = in_Instanced0.xyz  + 0.02*-in_Position.xzy+  0*length(in_TextureCoord) + 0*length(in_Instanced1);

	
	float refractive_index = 1/1.333;
	float bottom_y_offset = -1;
	float reflection_tres =  0.659300324479335;
	vec3 hc_light = vec3(0.0,-4.0,0.0);

	
	vec3 nw  = normalize(-in_Instanced1.xyz);
	vec3 ldw = normalize(hc_light);
	vec3 pw  = word_position;

	vec3 cross_nw_ldw = cross(nw,ldw); 
	float c0 = 1-pow(refractive_index,2.0)*dot(cross_nw_ldw,cross_nw_ldw);
	float s0 = sign(c0);

	float cos_thetha_i = dot(-ldw,nw);
	float sin_thetha_t = pow(refractive_index,2.0)*(1-pow(cos_thetha_i,2.0));
	vec3 refracted_dir = refractive_index*ldw + (refractive_index*cos_thetha_i  - sqrt(1-sin_thetha_t))*nw; // ;//(refractive_index*cross(nw,cross(-nw,ldw))-nw*sqrt(abs(c0)));
	refracted_dir /= abs(refracted_dir.y);

	float XZscale = 0.7;
	vec3 scale = vec3(XZscale, 1.0, XZscale);
	
	float deltaY = (pw.y-bottom_y_offset); 
	
	vec3 refracted_delta   = scale*deltaY*refracted_dir;
	vec3 intesection_point = pw + refracted_delta;
	
	f_ampl = 1.0-4*length(refracted_dir.xz);

	f_TextureCoord = in_TextureCoord;	
	gl_Position = MVP_Matrix * vec4(intesection_point.xyz, 1.0); 
}

