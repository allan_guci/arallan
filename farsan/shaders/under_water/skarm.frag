#version 400

uniform sampler2D u_Texture;



in vec2 f_TextureCoord;


out vec4 out_Color;


void main(void)
{
	out_Color = texture(u_Texture,vec2(20,10.0)*f_TextureCoord);
}
