#version 400

uniform sampler2D u_TextureSky;
uniform sampler2D u_TextureBottom;


in vec2 f_TextureCoord;


out vec4 out_Color;

in vec3 f_Normal;
in vec3 f_NormalWorld_y;
in vec3 f_Position;
in vec3 f_LightPosition;


float Fresnel(float NdotL, float fresnelBias, float fresnelPow){
  float facing = (1.0 - NdotL);
  return max(fresnelBias +(1.0 - fresnelBias) * pow(facing, fresnelPow), 0.0);
}

void main(void)
{


    
	vec3 normal         = f_Normal;
	vec3 position 	    = f_Position;
	vec3 lightVector 	= normalize(f_LightPosition); 
	
	vec3 l = normalize(lightVector);
	vec3 e = normalize(-position);
	vec3 n = normalize(normal);
	vec3 r = reflect(-l,n);
	
	vec2 screenSize = vec2(1024,1024);
	vec2 screenTexCoord =  vec2(0.5+gl_FragCoord.xy)/screenSize;
	
	
	
	
	float specular = max(dot(r,-e),0.0);
	specular = pow(specular,80.0);
	
	float diffuse = max(dot(n,l),0.0);
	float ambient = 0.2;
	float eDOTn = dot(-e,n);
	float fresnel = Fresnel(eDOTn,0.8, 5.0);
	
	vec3 waterReflection  = normalize(reflect(-e,n));
	
	/** Estimate a value of the angle **/
	float incident = abs(dot(e,n));
	float distortionAmplitude =  clamp(1.0-abs(f_NormalWorld_y.y),0.0,1.0); 
	
	/** Distort the reflected or refracted image **/
	vec2 distRefl = waterReflection.xy * 0.01*distortionAmplitude;
	vec2 distRefr = waterReflection.xy * 0.06*distortionAmplitude;
	

	vec2 texRefr = clamp(screenTexCoord+distRefr,0.0,1.0);
	vec2 texRefl = clamp(f_TextureCoord+10*distRefl,0.0,1.0);

	/** Sample refraction and reflection image **/
	vec4 colorRefraction = 0.7*texture(u_TextureBottom,texRefr);
	vec4 colorReflection = texture(u_TextureSky,texRefl); //vec4(0.0,0.2,0.6,1.0);
	
	/** Mix refraction and reflection color depending of the value of the incident angle **/
	vec4 color = mix(colorReflection,colorRefraction,incident); //+fresnel*colorReflection; //

	vec4 shading  = vec4(min(fresnel+diffuse,1.0));
	vec4 c = shading*color+specular; //+specular; //*color + (specular+fresnel)*0.4*vec4(1.0,0.9,0.4,0.0);
	
	out_Color = c;
}
