#version 150

layout (triangles) in;
layout(triangle_strip, max_vertices = 3) out;

uniform mat4 MVP_Matrix;

in vec3 g_Position[3];
in float g_LightContribution[3];
in float g_LightDistance[3];


out vec3 f_Position;
out vec3 f_Centroid;
out float f_PositionThres;




void main()
{

	int len = gl_in.length();

	vec3 A  = g_Position[0];
	vec3 B  = g_Position[1];
	vec3 C  = g_Position[2];	
	
	vec3 triCentriod = (A+B+C)/3.0;
	float minlen = (length(B-triCentriod)+length(C-triCentriod)+length(A-triCentriod))/3.0;
	
	
	
	
	
	for(int i = 0; i < len; i++){
			
	
		f_Position = g_Position[i];
		f_PositionThres = minlen+g_LightContribution[i]*0 + g_LightDistance[i]*0;
		f_Centroid = triCentriod;
	
		vec4 correctedPos = vec4(g_Position[i],1.0);
		gl_Position = MVP_Matrix*correctedPos; //  gl_in[i].gl_Position; 
		EmitVertex();
	}
	EndPrimitive();

}
