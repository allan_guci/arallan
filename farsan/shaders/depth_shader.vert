#version 150

uniform mat4 MVP_Matrix;

in vec3 in_Position;
out vec3 g_Position;
void main(void)
{
	g_Position = in_Position;
	gl_Position = MVP_Matrix * vec4(in_Position,1.0);
}
