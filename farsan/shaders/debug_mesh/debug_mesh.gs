#version 150

layout (triangles) in;
layout(triangle_strip, max_vertices = 3) out;


in vec3 g_Position[3];
in vec2 g_TextureCoord[3];
//in vec4 g_Color[3];

out vec3 f_Position;
out vec2 f_TextureCoord;
out vec3 f_Normal;

float meadianOfThree(float a, float b, float c)
{
	bool a_ind = ((a>b) && (a<c)) || ((a<b) && (a>c));
	if(a_ind)
		return a;

	bool b_ind = ((b>a) && (b<c)) || ((b<a) && (b>c));

	if(b_ind)
		return b;
	else
		return c;
}

void main()
{
	int len = gl_in.length();	

	vec3 v1 = g_Position[0];
	vec3 v2 = g_Position[1];
	vec3 v3 = g_Position[2];
	bool is_zero = false;
	if(v1.z < 0.1f)
	{
		is_zero = true;

	}
	if(v2.z < 0.1f)
	{
		is_zero = true;

	}
	if(v3.z < 0.1f)
	{
		is_zero = true;

	}

	float median_z = meadianOfThree(v1.z,v2.z,v3.z); 
	bool outlajjer  = false;
	float med_coeff = 0.02f;
	if(!((abs(v1.z-v2.z)<median_z*med_coeff && abs(v1.z-v3.z)<median_z*med_coeff&& abs(v2.z-v3.z)<median_z*med_coeff) && !is_zero)) {
		outlajjer  = true;
	}

  for(int i = 0; i < len; i++){
		
			f_TextureCoord = g_TextureCoord[i];

			if(outlajjer)
				gl_Position = vec4(gl_in[i].gl_Position.xyz,-1.0f);
			else
				gl_Position = gl_in[i].gl_Position;

			EmitVertex();
	}
	EndPrimitive();

}

