#version 400

//in vec4 f_Color;
in vec2 f_TextureCoord;

uniform sampler2D u_ColorTexture;
out vec4 out_Color;

void main(void)
{
	if(f_TextureCoord.x < 0.0 || f_TextureCoord.x > 1.0 || f_TextureCoord.y < 0.0 || f_TextureCoord.y>1.0)
	{
		out_Color = vec4(0.5);
		return;
	}

	//vec4 color = texture(u_ColorTexture,f_TextureCoord.xy);
    vec4 color = vec4(1.0);
 	out_Color = color;//vec4(0.1,0.5,0.5,1.0);


}
