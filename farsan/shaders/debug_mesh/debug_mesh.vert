#version 150



uniform mat4 MVP_Matrix;
uniform mat4 MV_Matrix;

in vec4 in_Position;
in vec2 in_TextureCoord;

out vec3 g_Position;
out vec2 g_TextureCoord;
//out vec4 g_Color;



void main(void)
{
	gl_Position = MVP_Matrix * vec4(in_Position.xyz,1.0);
	g_Position = in_Position.xyz;

	g_TextureCoord = in_TextureCoord;
	/*if(in_TextureCoord.x < 0 || in_TextureCoord.x > 1929 || in_TextureCoord.y < 0 || in_TextureCoord.y>1079)
	{
		g_Color = vec4(0.5);
	}
	else
	{
		g_Color = texture(u_ColorTexture,in_TextureCoord);
	}*/
}
