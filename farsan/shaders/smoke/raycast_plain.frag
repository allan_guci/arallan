#version 420

uniform sampler3D u_Texture;
uniform sampler2D u_Depth;

uniform float u_ScreenHeight;
uniform float u_ScreenWidth;

uniform float u_CameraNear;
uniform float u_CameraFar;

out vec4 out_Color;

in vec3 f_ModelPosition;
in vec3 f_ModelEye;

in vec3 f_ViewPosition;
in vec3 f_ViewEye;

#define TC_BOUND_UP 1.0
int checkBoxBounds(vec3 tc){
	if(tc.x > TC_BOUND_UP || tc.x < 0.0)
		return 0;
	if(tc.y > TC_BOUND_UP || tc.y < 0.0)
		return 0;
	if(tc.z > TC_BOUND_UP || tc.z < 0.0)
		return 0;
	
	return 1;
}

float getDensity(vec3 tc){
	return  texture3D(u_Texture,tc).x;

}

float readSceneDepth( in vec2 coord )
{
	float zNear = u_CameraNear;
	float zFar = u_CameraFar;
	float z_from_depth_texture = texture(u_Depth, coord).x;
	float z_sb = 2.0 * z_from_depth_texture - 1.0; 
	float z_world = 2.0 * zNear * zFar / (zFar + zNear - z_sb * (zFar - zNear)); 
	return z_world;
}

bool rayBlocked(float depth,float view_z){
	return (depth < abs(view_z));	
}

#define NUM_SAMPLES 96


//#define DEBUG
void main(void)
{

	const float maxDist = sqrt(6.0);
	float scale =  maxDist/NUM_SAMPLES;

	vec2 tc_depth = gl_FragCoord.xy / vec2(u_ScreenWidth, u_ScreenHeight);
	float depth   = readSceneDepth(tc_depth);

	
	vec3 pos 		= f_ModelPosition; 
	vec3 step_dir   = normalize(pos-f_ModelEye);
	vec3 step 		= scale*step_dir;

	vec3 pos_view 		= f_ViewPosition;
	vec3 step_view 		= normalize(pos_view-f_ViewEye)*scale;
	
	float strength = 0.0; 
	float alpha_res = 0.0;
	float s = 0.0;
	float alpha = 0.0;

	pos 	 += NUM_SAMPLES*step;
	pos_view += NUM_SAMPLES*step_view;
	
	
	vec3 tc;
	
	for( int i=0; i<NUM_SAMPLES; i++ )
	{
	/*
		if(rayBlocked(depth,pos_view.z))
			continue;
	*/	
		pos_view += step_view;
	
		tc = (pos+1.0)*0.5;	
		
		pos -= step;
		
		s = getDensity( tc );
		
		alpha  = s; 
		strength  = (1 - alpha*0.4) * strength + alpha * s;
		alpha_res = (1 - alpha*0.9) * alpha_res + alpha; // * s;
		
	}
	alpha_res = pow(alpha_res*pow(strength,0.2),2.0);
	strength = pow(0.4*strength,0.4); //*abs(0.5*strength);
	vec3 clr = vec3(0.6)*strength;
	//0.8*strength,0.6*strength,0.3*strength;
	out_Color = vec4(clr,alpha_res); //*pow(strength*0.4,2.0)+0.07; //4.6)*pow(strength+0.1,2.0);

	
}

