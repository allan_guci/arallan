#version 150
in  vec4 in_Position;
out vec3 f_Position;
uniform mat4 VP_Matrix;

void main(void)
{
	
	vec3 pos =in_Position.xyz ;; ////(in_Position.xyz+1.0)*0.5; //  in_Position.xyz; 
	
	f_Position 	= pos;
	//pos.x *=2.0;
	gl_Position = VP_Matrix * vec4(pos, 1.0); 
}

