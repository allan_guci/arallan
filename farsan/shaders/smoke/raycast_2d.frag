#version 420

uniform sampler2D u_Texture;
uniform vec3 u_Eye;
out vec4 out_Color;
in vec3 f_Position;

#define TC_BOUND_UP 1.0
int checkBoxBounds(vec3 tc){
	if(tc.x > TC_BOUND_UP || tc.x < 0.0)
		return 0;
	if(tc.y > TC_BOUND_UP || tc.y < 0.0)
		return 0;
	if(tc.z > TC_BOUND_UP || tc.z < 0.0)
		return 0;
	
	return 1;
}

float getDensity(vec3 tc){	
/*
	int X = int(ceil(tc.x*18));
	int Y = int(ceil(tc.y*18));
	
	
	int OX = Z % 6;
	int OY = Z / 6;
	
	
	int IX = X + OX * 18;
	int IY = Y + OY * 18;
	*/
	
	int Z = int(ceil(tc.z*66.0));
	int OX = Z % 6;
	int OY = Z / 6;
	
	float ix = tc.x + float(OX);
	float iy = tc.y + float(OY);
	
	vec2 t;
	t.x = ix/6.0;
	t.y = iy/3.0;
	return  texture(u_Texture,t).x;
}

#define NUM_SAMPLES 96

//#define DEBUG
void main(void)
{

	const float maxDist = sqrt(3.0);
	float scale =  maxDist/NUM_SAMPLES;

	
	vec3 eye 		= u_Eye; 
	vec3 start_pos  = f_Position;
	vec3 step_dir   = normalize(start_pos - eye);
	vec3 step_start = start_pos;
	vec3 step 		= scale*step_dir;
	
	scale *=10.0;
	
	vec3 pos =start_pos; 
	
	// Result stuff
	float T = 1.0;
	vec3 Lo = vec3(0.0);
	
	//Light stuff
	const int numLightSamples = 16;
	const float lscale =  maxDist / numLightSamples;
	const vec3 lightPos = vec3(0.0,-1.0,0.0);
	const vec3 lightIntensity = vec3(8.0);
	
	const float absorption = 8.0;

	#ifndef DEBUG
	vec3 tc;
	for(int t = 0; t < NUM_SAMPLES; t++){
		pos += step;
		tc = (pos+1.0)*0.5;
		if(checkBoxBounds(tc) == 0)
			break;
			
		float density = scale*getDensity(tc);
		
		if(density > 0.0){
			T *= 1.0-density*absorption;
			if(T <= 0.01)
				break;
			vec3 lightDir = normalize(lightPos-pos)*lscale;
			vec3 lpos = pos + lightDir;
			
			float Tl = 1.0;
			
			for(int s=0;s < numLightSamples; s++){
				tc = (lpos+1.0)*0.5;
				if(checkBoxBounds(tc) == 0)
					break;
				
				float ld = getDensity(tc);
				ld += 0.1*clamp((0.1-ld)/0.1,0.0,0.3);
				Tl *= 1.0-absorption*lscale*ld;
 
				if (Tl <= 0.08)
					break;
 
				lpos += lightDir;
			}
			
			vec3 Li = lightIntensity*Tl;
	
			Lo += Li*T*density;
		}
		
		
	}
	vec3 color = 2.0*pow(Lo.x,4.0)*vec3(0.652,0.324,0);
	
	out_Color = vec4(color,1.0-T);
	/*if(T < 0.99){
			out_Color = vec4(0.5);
	}*/
#else

	vec3 tc = (pos+0.95)*0.5;
	float I = getDensity(tc);	
	out_Color = vec4(I);
	
#endif
	
}