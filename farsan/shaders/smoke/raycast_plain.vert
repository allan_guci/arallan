#version 150
in  vec4 in_Position;

out vec3 f_ModelPosition;
out vec3 f_ModelEye;

out vec3 f_ViewPosition;
out vec3 f_ViewEye;

uniform mat4 MVP_Matrix;
uniform mat4 MV_Matrix;
uniform mat4 M_Matrix;

uniform vec3 u_Eye;

void main(void)
{
	vec3 pos 				= in_Position.xyz;
	f_ModelPosition 		= pos;
	f_ModelEye 				= (inverse(M_Matrix)*vec4(u_Eye,1.0)).xyz;

	f_ViewPosition 			= (MV_Matrix*vec4(pos,1.0)).xyz;
	f_ViewEye				= (MV_Matrix*vec4(u_Eye,1.0)).xyz;
	
	
	gl_Position = MVP_Matrix * vec4(pos, 1.0); 
}

