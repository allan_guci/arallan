#version 420

uniform sampler3D u_Texture;
uniform sampler2D u_Depth;

uniform float u_ScreenHeight;
uniform float u_ScreenWidth;

uniform float u_CameraNear;
uniform float u_CameraFar;

out vec4 out_Color;

in vec3 f_ModelPosition;
in vec3 f_ModelEye;

in vec3 f_ViewPosition;
in vec3 f_ViewEye;

#define TC_BOUND_UP 1.0
int checkBoxBounds(vec3 tc){
	if(tc.x > TC_BOUND_UP || tc.x < 0.0)
		return 0;
	if(tc.y > TC_BOUND_UP || tc.y < 0.0)
		return 0;
	if(tc.z > TC_BOUND_UP || tc.z < 0.0)
		return 0;
	
	return 1;
}

float getDensity(vec3 tc){
	return  texture3D(u_Texture,tc).x;

}

float readSceneDepth( in vec2 coord )
{
	float zNear = u_CameraNear;
	float zFar = u_CameraFar;
	float z_from_depth_texture = texture(u_Depth, coord).x;
	float z_sb = 2.0 * z_from_depth_texture - 1.0; 
	float z_world = 2.0 * zNear * zFar / (zFar + zNear - z_sb * (zFar - zNear)); 
	return z_world;
}

bool rayBlocked(float depth,vec3 pos_view){
	return (depth < pos_view.z);	
}

#define NUM_SAMPLES 96


//#define DEBUG
void main(void)
{

	const float maxDist = sqrt(3.0);
	float scale =  maxDist/NUM_SAMPLES;

	vec2 tc_depth = gl_FragCoord.xy / vec2(u_ScreenWidth, u_ScreenHeight);
	float depth   = readSceneDepth(tc_depth);

	
	vec3 pos 		= f_ModelPosition; 
	vec3 step_dir   = normalize(pos-f_ModelEye);
	vec3 step 		= scale*step_dir;

	vec3 pos_view 		= f_ViewPosition;
	vec3 step_view 		= normalize(pos_view-f_ViewEye)*scale;
	
	
	
	scale *=10.0;
	
	
	
	// Result stuff
	float T = 1.0;
	vec3 Lo = vec3(0.0);
	
	//Light stuff
	const int numLightSamples = 32;
	const float lscale =  maxDist / numLightSamples;
	const vec3 lightPos = 2*vec3(0.0,0.0,1.0);
	const vec3 lightIntensity = vec3(24.0);
	
	const float absorption = 4.0;

	#ifndef DEBUG
	vec3 tc;
	for(int t = 0; t < NUM_SAMPLES; t++){
	
		/*if(rayBlocked(depth,-pos_view))
			break;
		pos_view += step_view;
	*/
		pos += step;
		tc = (pos+1.0)*0.5;
		if(checkBoxBounds(tc) == 0)
			break;
			
		float density = scale*getDensity(tc);
		
		if(density > 0.0){
			T *= 1.0-density*absorption;
			if(T <= 0.01)
				break;
			vec3 lightDir = normalize(lightPos-pos)*lscale;
			vec3 lpos = pos + lightDir;
			
			float Tl = 1.0;
			
			for(int s=0;s < numLightSamples; s++){
				tc = (lpos+1.0)*0.5;
				if(checkBoxBounds(tc) == 0)
					break;
				
				float ld = texture3D(u_Texture, tc).x;
				ld += 0.2*clamp((0.2-ld)/0.2,0.0,0.3);
				Tl *= 1.0-absorption*lscale; //*ld;
 
				if (Tl <= 0.08)
					break;
 
				lpos += lightDir;
			}
			
			vec3 Li = lightIntensity*Tl;
	
			Lo += Li*T*density;
		}
		
		
	}
	vec3 color = pow(Lo.x*3,2.0)*vec3(0.5,0.5,0.5);
	
	out_Color = vec4(color,1.0-T);; //vec4(color,pow(1.0-T,2));
	#else
	vec3 tc = (pos+0.95)*0.5;
	float I = getDensity(tc); 
	out_Color = vec4(I);
	#endif
	
}

