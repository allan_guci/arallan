#version 400


in vec3 f_LightPosition;
in vec3 f_Position;

out vec4 out_Color;


/* InScatter routine as an approximation of the light contribution
*  over a line segment. This function is Copied from 
*  http://blog.mmacklin.com/2010/05/29/in-scattering-demo/
*/
float InScatter(vec3 dir, vec3 lightPos, float d)
{
	// light to ray origin
	vec3 q = -lightPos;
	 
	// coefficients
	float b = dot(dir, q);
	float c = dot(q, q);
	 
	// evaluate integral
	float s = 1.0f / sqrt(c - b*b);
	float l = s * (atan( (d + b) * s) - atan( b*s ));
	return l;

}

void main(void)
{

	/* Distance from eye to current position in the light frustum */
    float distance = length(f_Position);

	/* Direction to from the eye to the current position in the light frustum */
    vec3 direction = normalize(f_Position);

	
    float res = 40.0*InScatter(direction,f_LightPosition,distance);

    if (gl_FrontFacing) {
        res*=-1.0;
    }
	vec4 color = vec4(1.0);
    out_Color = color * res;

}
