#version 400
uniform sampler2D u_Texture;
uniform sampler2D u_Noise;

in vec2 f_TexCoord;
out vec4 out_Color;

void main(){
	
		
		//out_Color = vec4(1.0);
		float color = pow(texture(u_Texture,f_TexCoord).x,2.0);
		float noise = texture(u_Noise,f_TexCoord).x;
		out_Color  = vec4(1.0,0.86,0.6,1.0)*vec4(color); 	
		out_Color.a = 0.1*noise;
		out_Color.rgb *=1.5*noise;

}

