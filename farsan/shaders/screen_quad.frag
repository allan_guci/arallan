#version 400
uniform sampler2D u_Texture;

in vec2 f_TexCoord;
out vec4 out_Color;

void main(){
		out_Color = texture(u_Texture,f_TexCoord);
}

