#version 150


in  vec3 in_Position;
in  vec3 in_Normal;
in 	vec4 in_Instanced;


out vec3 f_Position;
out vec3 f_Normal;
out vec3 f_LightDir;

uniform mat4 V_Matrix;
uniform mat3 Normal_Matrix;
uniform mat4 VP_Matrix;

uniform float u_Dim;

void main(void)
{

	vec3 word_position = /*u_Dim**/in_Instanced.xyz + 10.0*in_Position/u_Dim;

	f_Position = (V_Matrix * vec4(word_position.xyz,1.0)).xyz;
	f_LightDir = Normal_Matrix * vec3(0.0,1.0,0.0);
	f_Normal   = Normal_Matrix * in_Normal.xyz;
	
	gl_Position = VP_Matrix * vec4(word_position.xyz, 1.0); 
}

