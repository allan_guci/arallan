#ifndef _GUI_DB_H
#define _GUI_DB_H
#include "f_gui.h"
class GUIDB {
public:
	GUIDB();
    void startGUI();
    void createProgramSelection();
private:
    QDialog  * dialog;
    Ui_Dialog gui;

};

#endif
