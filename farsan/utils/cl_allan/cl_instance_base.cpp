#include "cl_instance_base.h"


CLInstanceBase::CLInstanceBase()
: mTimer("")
{

}


void CLInstanceBase::setup(cl::Context context){
    cl_int res = CL_SUCCESS;

    mCLContext = context;

    mDevices = mCLContext.getInfo<CL_CONTEXT_DEVICES>();

    cl_command_queue_properties commandQueueProperties = 0;

    mCommandQueue = clCreateCommandQueue(mCLContext(), mDevices[0](), commandQueueProperties, &res);
	setOptions("-I kernels ");
    CLHelper::checkCLError(res);

}

ARTimer& CLInstanceBase::getTimer(){
    return mTimer;
}

CLKernel * CLInstanceBase::CreateKernel(const char* kernelName,ARSize globalThreadSize,ARSize localThreadSize){
    cl_int result;
    cl_kernel kernel = clCreateKernel(mSelectedProgram,kernelName,&result);
    CL_ERR(result);

    ARSize gs;
    if(globalThreadSize.valid())
        gs = globalThreadSize;
    else if(mNumGlobalThreads.valid())
        gs = mNumGlobalThreads;
    else{
        printf("Invalid Number of threads Thread - %s",kernelName);
        exit(-1);
    }

    ARSize ls;
    if(localThreadSize.valid())
        ls = localThreadSize;
    else if(mNumLocalThreads.valid())
        ls = mNumLocalThreads;
    else{
        printf("Invalid Number of threads Thread - %s",kernelName);
        exit(-1);
    }

    CLKernel* newKernel = new CLKernel(kernel,mCommandQueue,kernelName);
    newKernel->threadDimension(gs,ls);

    if(mKernels.count(kernelName) == 0){
        mKernels[kernelName] = newKernel;
        mTimer << newKernel->getTimer();
    }
    return newKernel;
}

void CLInstanceBase::AddKernel(CLKernel * kernel, std::string key){
    int turn = mKernels.size();
    std::string theKey = kernel->getLabel();

    if(key.compare("DEFAULT")){
        theKey = key;
    }
    mKernelOrder[turn] = theKey;
    mKernels[theKey]   = kernel;
}

void CLInstanceBase::RunKernels(bool finish){
    bindGLMem();

    for( std::map<int, std::string>::iterator it = mKernelOrder.begin(); it != mKernelOrder.end(); it++ ){
        std::string strKey = it->second;
        mKernels[strKey]->run(finish);
    }

    releaseGLMem();
    sync();
}

void CLInstanceBase::RunKernel(CLKernel * kern){
    bindGLMem();
    kern->run();
    releaseGLMem();
    sync();
}

CLKernel * CLInstanceBase::GetKernel(std::string key){
    if(mKernels.count(key) == 0){
        printf("Number of kernels: %d\n", mKernels.size());
        printf("Kernel: %s is not present.\n Present kernels are:\n",key.c_str());

        for(auto kernPair : mKernels)
            printf("%s",kernPair.first.c_str());
        exit(-1);
    }
    return mKernels[key];
}

CLMemory CLInstanceBase::CreateImage(ImageOptions options,void * hostPtr){
        CLMemory mem;
        mem.initialize(mCLContext);
        mem.createImage<char>(options,hostPtr);
        int s = mAllBuffers.size();
        mem.id(s);
        mAllBuffers.push_back(mem);
        return mem;
}

CLMemory CLInstanceBase::CreateImageFromGL(GLuint texId, ImageOptions options){
        CLMemory mem;
        mem.initialize(mCLContext);
        mem.GLTexture<char>(texId,options);
        int s = mAllBuffers.size();
        mem.id(s);
        mAllBuffers.push_back(mem);
        return mem;
}

CLMemory CLInstanceBase::CreateBufferFromGL(GLHelper::Buffer buffer,GLBufferItem item,cl_mem_flags flags){
        CLMemory mem;
        mem.initialize(mCLContext);
        mem.GLBuffer(buffer,item,flags);
        int s = mAllBuffers.size();
        mem.id(s);
        mAllBuffers.push_back(mem);
        return mem;
}


void CLInstanceBase::sync(){
    clFinish(mCommandQueue);
}


void CLInstanceBase::bindGLMem(){
    for(size_t i = 0; i < mAllBuffers.size(); i++)
            mAllBuffers[i].bind();
    mCLInput.bind();
    mCLOutput.bind();
    mCollidable.bind();
}

void CLInstanceBase::releaseGLMem(){
    for(size_t i = 0; i < mAllBuffers.size(); i++)
            mAllBuffers[i].release();
    mCLInput.release();
    mCLOutput.release();
    mCollidable.release();
}

void CLInstanceBase::setCLOutput(CLMemory m){
    mCLOutput = m;
}

CLMemory CLInstanceBase::getCLOutput(){
    return mCLOutput;
}

void CLInstanceBase::inputMem(CLMemory m, std::string key){
    mInputMemory[key] = m;
}
CLMemory CLInstanceBase::inputMem(std::string key){
    return mInputMemory[key];
}

void CLInstanceBase::setCLInput(CLMemory m){
    mCLInput = m;
}

CLMemory CLInstanceBase::getCLInput(){
        return mCLInput;
}

void CLInstanceBase::setCollidable(CollidableType collidable){
    mCollidable = collidable;
}

CollidableType CLInstanceBase::getCollidable(){
    return mCollidable;
}

void CLInstanceBase::setOutputMesh(MeshType mesh){
    mOutMesh = mesh;
}

MeshType CLInstanceBase::getInputMesh(){
    return mInMesh;
}

void CLInstanceBase::setInputMesh(MeshType mesh){
    mInMesh = mesh;
}

MeshType CLInstanceBase::getOutputMesh(){
    return mOutMesh;
}

