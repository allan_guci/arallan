#include "cl_memory.h"

void PERR(cl_int r,const char * errStr){
    CL_ERR_S(r,errStr);
}


cl_mem  glTextToClImg(cl_context context, ImageOptions opt, GLuint textId){

   GLenum target;
   if(opt.mSize.multiplicity() == 2)
        target = GL_TEXTURE_2D;
    else if(opt.mSize.multiplicity() == 3)
        target = GL_TEXTURE_3D;
    else{
        printf("CreateImage - Invalid data dimension");
        exit(-1);
    }
    cl_int res;
    cl_mem mem = clCreateFromGLTexture(context, opt.flags, target, 0, textId, &res);
    PERR(res,"Create CL Image from GL texture");
    return mem;
}

size_t  getImageSize(ImageOptions opt){

   size_t size = opt.mSize.dSize();
   if(opt.format.image_channel_order == CL_R)
        return size;

    if(opt.format.image_channel_order == CL_RG)
        return 2*size;

    if(opt.format.image_channel_order == CL_RGB)
        return 3*size;

    if(opt.format.image_channel_order == CL_RGBA)
        return 4*size;

    if( size == 0)
        PERR(-1,"CL image size not set");

    return size;
}

size_t  getChannelSize(ImageOptions options){
     size_t  sizeElem = 0;
     switch(options.format.image_channel_data_type){
            case CL_HALF_FLOAT:
                sizeElem = 2;
            break;
            case CL_FLOAT:
                sizeElem = 4;
            break;
            case CL_SIGNED_INT32:
            case CL_UNSIGNED_INT32:
                sizeElem = 4;
            break;
            case CL_SIGNED_INT16:
            case CL_UNSIGNED_INT16:
                sizeElem = 2;
            break;
            case CL_SIGNED_INT8:
            case CL_UNSIGNED_INT8:
                sizeElem = 1;
            break;

            default:
                printf("CreateImage: unknown data format...");
                exit(-1);
            break;

        }
     return sizeElem;
}


cl_image_desc getDesctriptor(ImageOptions opt,void * hostPtr){

            size_t elemSize = getChannelSize(opt);
            cl_image_desc desc;
            desc.image_height = 0;
            desc.image_depth  = 0;
			desc.num_mip_levels = 0;
			desc.num_samples = 0;
			desc.buffer = 0;
			size_t sx = opt.mSize.dx();
			size_t sy = opt.mSize.dy();
			size_t sz = opt.mSize.dz();

            switch(opt.mSize.multiplicity()){
            case 1:
                desc.image_type   = CL_MEM_OBJECT_IMAGE1D;
                desc.image_width  = sx;
                if(hostPtr != 0){
                    desc.image_array_size  = elemSize*sx;
                    desc.image_row_pitch   = 0;
                    desc.image_slice_pitch = 0;
                }else{
                    desc.image_array_size = 0;
                    desc.image_row_pitch = 0;
                    desc.image_slice_pitch = 0;
                }
                break;
            case 2:
                desc.image_type   = CL_MEM_OBJECT_IMAGE2D;
                desc.image_width  = sx;
                desc.image_height = sy;
                if(hostPtr != 0){
                    desc.image_array_size  = elemSize*sx*sy;
                    desc.image_row_pitch   = elemSize*sy;
                    desc.image_slice_pitch = 0;
                }else{
                    desc.image_array_size = 0;
                    desc.image_row_pitch = 0;
                    desc.image_slice_pitch = 0;
                }
                break;
            case 3:
				desc.image_type   = CL_MEM_OBJECT_IMAGE3D;
				desc.image_width  = sx;
				desc.image_height = sy;
				desc.image_depth  = sz;
				if(hostPtr != 0){
					desc.image_array_size  = elemSize*sx*sy*sz;
					desc.image_row_pitch   = elemSize*sx;
					desc.image_slice_pitch = elemSize*sx*sy;
				}else{
					desc.image_array_size = 0;
					desc.image_row_pitch = 0;
					desc.image_slice_pitch = 0;
				}
                break;
            default:
                    PERR(-1,"Create Image  - Invalid image options");
            }
			return desc;
}

cl_image_format getFormat(ImageOptions opt){

	cl_image_format format;
    format.image_channel_order = opt.format.image_channel_order;
    format.image_channel_data_type = opt.format.image_channel_data_type;

	return format;
}


void CLMemory::bind(){
    if(!mIsAcquired && mGLSharing){
        mIsAcquired = true;
        cl_event event = NULL;
        cl_int err = clEnqueueAcquireGLObjects(mCommandQueue, 1,&mCLMemoryPtr, 0, 0, &event);
        if(event != NULL){
             clReleaseEvent(event);
            // delete event;
        }
        PERR(err,"CLMemory - Acquire GL memory");
    }
}

void CLMemory::release(){
    if(mIsAcquired && mGLSharing){
        mIsAcquired = false;
        cl_event event = NULL;
        cl_int err = clEnqueueReleaseGLObjects(mCommandQueue, 1, &mCLMemoryPtr, 0, 0, &event);
        if(event != NULL){
             clReleaseEvent(event);
            // delete event;
        }
        PERR(err,"CLMemory - Release GL memory");
    }
}
