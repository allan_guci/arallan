#ifndef CL_HELPER_H
#define CL_HELPER_H

#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <fstream>

#include <sys/timeb.h>
#include <time.h>
#include <unistd.h>

#include "GL_utilities.h"
#include <CL/cl_gl.h>
#include <CL/cl.hpp>

#define CL_ERR(r)  CLHelper::checkCLError(r)
#define CL_ERR_S(r,s)  CLHelper::checkCLError(r,s)




namespace CLHelper{

const char *ERR(cl_int error);
void checkCLError(cl_int err, const char * s = "");

}
#endif // PROGRAM

