#include "cl_program.h"


CLProgram::~CLProgram(){
    deleteProgram();
}


void CLProgram::setMacroValue(std::string s,int val){
    /** Convert int to ascii **/
    char v[10];
    memset(v,0,10);
    snprintf(v,sizeof(v),"%d",val);
    setMacroValue(s,v);
}

void CLProgram::setMacroValue(std::string s,const char *v){

    mOptions.append("-D");
    mOptions.append(s);
    mOptions.append("=");
    mOptions.append(v);
    mOptions.append(" ");

}

void CLProgram::setMacroValue(std::string s){
    mOptions.append(s);
}

void CLProgram::deleteProgram(){
	clReleaseProgram(mProgram);

}

/** \brief Create OpenCL kernel
 *
 * \param kernelName std::string
 * \return void
 *
 */

char* readFile(char *file)
{
	FILE *fptr = NULL;
	long length;
	char *buf;

	fptr = fopen(file, "rb"); /* Open file for reading */
    //int errNum;
	//errNum = fopen_s(&fptr, file, "rb"); /* Open file for reading */
	if (!fptr) /* Return NULL on failure */
		return NULL;
	fseek(fptr, 0, SEEK_END); /* Seek to the end of the file */
	length = ftell(fptr); /* Find out how many bytes into the file we are */
	buf = (char*)malloc(length+1); /* Allocate a buffer for the entire length of the file and a null terminator */
	fseek(fptr, 0, SEEK_SET); /* Go back to the beginning of the file */
	fread(buf, length, 1, fptr); /* Read the contents of the file in to the buffer */
	fclose(fptr); /* Close the file */
	buf[length] = 0; /* Null terminator */

	return buf; /* Return the buffer */
}

void CLProgram::createAndCompileProgram(cl_context c,cl_command_queue q, const char* filePath,std::vector<cl::Device> devices){

    size_t* stringLengths = NULL;
    cl_int result;

    char * kernelFile = readFile((char *)filePath);

    /** Create program **/
		cl_program program = clCreateProgramWithSource(c,1,(const char**)&kernelFile,stringLengths,&result);
    CL_ERR(result);

    cl_device_id deviceList[2];
    deviceList[0] = devices[0]();

    /** Compile program **/
    result = clBuildProgram(program,1,deviceList,mOptions.c_str(),NULL,NULL);
    if(result != CL_SUCCESS){

        char cBuildLog[16384];
        size_t logSize = sizeof(cBuildLog);

        result = clGetProgramBuildInfo(program, deviceList[0], CL_PROGRAM_BUILD_LOG,logSize, cBuildLog, NULL );

        printf("Log:\n%s\n\n", (char *)cBuildLog);

        printf("CL_error: could not compile the program\n");
				CL_ERR(result);
        exit(0);
    }
    mProgram = program;
}


