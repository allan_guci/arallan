#ifndef _AR_TIMER_H
#define _AR_TIMER_H

#include <string>
#include <vector>
#include <map>

#include <sys/timeb.h>
#include <time.h>
#include <unistd.h>
class ARTimer;
class ARTimer
{
    struct LapType{
            std::string label;
            clock_t     time;
            size_t      lvl;
            size_t      numupdates;
            LapType& operator+=(const LapType& other){
                //time += other.time;
                lvl += other.lvl;
                return *this;
            }
            LapType operator+(const LapType& other){
                LapType tmp(label, other.time, lvl + other.lvl);
                return tmp;
            }

            size_t numTabs(){
                return 4 - label.size()/6;
            }
            void update(clock_t delta){
                time += delta;
                numupdates ++;
            }

            clock_t mean(){
                float meanfloat = (float)time/(float)numupdates;
                return (clock_t)meanfloat;

            }


            LapType(const std::string& lab, clock_t t, size_t l = 1) : label(lab), time(t), lvl(l), numupdates(1) {}
    };

    public:

	ARTimer(const std::string& label);

    void start();
    void lap(const std::string& label = std::string("NO_LABEL"));
    void stop(const std::string& label);
    void print();
    void dump(const char* filename);
    void fill(std::vector<LapType>& laps) const;
    void increment(const LapType& lap) const;
    clock_t sum();
    std::string label() const;
    std::string format(size_t lvl);
    size_t size();

    ARTimer& operator+=(const ARTimer& other);
    ARTimer operator+(const ARTimer& other);
    ARTimer& operator<<(ARTimer& other);

	private:

	    std::string mLabel;
        clock_t mStartTime;
        std::map<std::string,int > mLapOrder;
        std::vector<LapType> mLapVector;
        std::vector<ARTimer*> mChildren;


};

#endif
