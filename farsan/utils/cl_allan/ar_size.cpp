#include "ar_size.h"
template<class T>
inline void set(T d[], size_t n,T val){
    for(size_t i = 0; i < n; i++)
        d[i] = val;
}

template<class T>
inline void set(T d[], size_t n, T src[]){
    for(size_t i = 0; i < n; i++)
        d[i] = src[i];
}

template<class T>
inline void set(T d[],T d0,T d1,T d2){
        d[0] = d0;
        d[1] = d1;
        d[2] = d2;
}

template<typename T>
inline size_t sum(T * d, size_t n){
    size_t r = 0;
    for(size_t i = 0; i < n; i++)
        r += d[i];

    return r;

}
template<typename T>
inline size_t setDim(T t0,T t1,T t2){
    size_t d = 0;
    if(t0 != 0) d++;
    if(t1 != 0) d++;
    if(t2 != 0) d++;
    return d;
}


ARSize::ARSize(size_t d0,size_t d1,size_t d2){
    dim(d0,d1,d2);
}
ARSize::ARSize(int d0,int d1,int d2){
    dim(d0,d1,d2);
}

ARSize::ARSize(float d0,float d1,float d2,float isfloat){
    scale(d0,d1,d2);
}
ARSize::ARSize(size_t numDim,size_t* d){
    dim(numDim,d);
}
ARSize::ARSize(size_t numDim,float* s){
    scale(numDim,s);
}

size_t ARSize::multiplicity(){
    return mMultiplicity;
}

void   ARSize::dim(size_t s0, size_t s1, size_t s2){
    mMultiplicity = setDim(s0,s1,s2);
    set(mSize,s0,s1,s2);
    isSet = true;
}
void  ARSize::scale(float s0, float s1, float s2){
    mMultiplicity = setDim(s0,s1,s2);
    set(mScale,s0,s1,s2);
    isSet = true;
}

void   ARSize::dim(size_t nd, size_t* s){
    mMultiplicity = nd;
    set(mSize,nd,s);
    isSet = true;
}
void  ARSize::scale(size_t nd, float * s){
    mMultiplicity = nd;
    set(mScale,nd,s);
    isSet = true;
}


#define CHECK_MULT(X,M) \
if((M+1) > mMultiplicity){              \
    printf("ARSize: out of bounds\n");    \
    exit(-1);                           \
}                                       \
return X[M];

#define RET_VAL(X,M)    \
if(mMultiplicity > M  ) \
    return X[M];        \
else                    \
    return 1


size_t ARSize::dx(){
    RET_VAL(mSize,0);
}
size_t ARSize::dy(){
    RET_VAL(mSize,1);
}
size_t ARSize::dz(){
    RET_VAL(mSize,2);
}

size_t ARSize::dSize(){
    size_t tot = 1;
    if(mMultiplicity > 0)
        tot *= dx();
    if(mMultiplicity > 1)
        tot *= dy();
    if(mMultiplicity > 2)
        tot *= dz();
    return tot;
}

float ARSize::sx(){
    CHECK_MULT(mScale,0);
}
float ARSize::sy(){
    CHECK_MULT(mScale,1);
}
float ARSize::sz(){
    CHECK_MULT(mScale,2);
}
