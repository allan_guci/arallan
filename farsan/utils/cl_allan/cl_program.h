#ifndef CL_PROGRAM_H
#define CL_PROGRAM_H

#include <stdlib.h>
#include <stdio.h>

#include <string.h>
#include <string>
#include "GL_utilities.h"
#include <CL/cl_gl.h>
#include "cl_helper.h"




class CLProgram{


public:

    ~CLProgram();
    void deleteProgram();

    void setMacroValue(std::string s,int val);;
    void setMacroValue(std::string s,const char *v);
    void setMacroValue(std::string s);

    void createAndCompileProgram(cl_context c, cl_command_queue q, const char * filePath,std::vector<cl::Device> devices);

    cl_program get(){return mProgram;}

protected:

private:

    /** Program Level **/
    cl_program           mProgram;
    cl_event*            mEventQueue;

    /** Build options **/
    std::string mOptions;


};
#endif // PROGRAM

