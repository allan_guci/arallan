#ifndef CL_KERNEL_H
#define CL_KERNEL_H
#include <stdlib.h>
#include <stdio.h>
#include "cl_types.h"
#include "cl_helper.h"
#include "GL_utilities.h"
#include <CL/cl_gl.h>
#include <sys/timeb.h>
#include <time.h>
#include <unistd.h>
#include "cl_memory.h"
#include "ar_timer.h"

class CLMemory;

class CLKernel{

public:
    CLKernel(cl_kernel kernel,cl_command_queue commandQueue, std::string name);
    ~CLKernel();

    void setArgument(int argIndex, CLMemory buffer);
    void setArgument(int argIndex, cl_uint);
    void setArgument(int argIndex, cl_float);


    template<class T>
    void arg(T& arg_){
        setArgument(numArg(),arg_);
    }
    void arg(void * arg, size_t sizeArg,int argIndex = -1);
    void arg(cl_float f);
    void arg(cl_uint ui);


    CLKernel* operator<<(CLMemory& m);

    void incNumArg();
    cl_uint numArg();

    void threadDimension(ARSize global,ARSize local);

    void createPingPong(CLKernel* kernel);
    void createPingPong(CLKernel* kernel,CLMemory buf1,CLMemory buf2);

    void run();
    void run(bool finish);

    std::string getLabel();
    void setLabel(std::string newLabel);
    void setPingPongLabel(std::string newLabel);

    void release();

    ARTimer& getTimer();

protected:

private:

    void printContent();

    /** CL-kernel handle **/
    cl_kernel mKernel;

    /** Pointer to the parent CL-Program instance **/
    cl_command_queue mCommandQueue;

    /** For ping ponging **/
    CLKernel* mPingPong;
    int mCtr;

    /** Number of arguments **/
    cl_uint  mNumArgs;

    /** Number of work-items in local work group **/
    size_t mLocalSize[3];

    /** Total number of work-items **/
    size_t mGlobalSize[3];

    /** Number of dimensions **/
    size_t mNumDimensions;



    std::string mLabel;

    /** Timer **/
    ARTimer mTimer;


};


#endif // CL_KERNEL_H

