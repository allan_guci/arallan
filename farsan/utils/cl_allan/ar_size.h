#ifndef AR_SIZE_H
#define AR_SIZE_H
#include <stdlib.h>
#include <stdio.h>


class ARSize{


    public :
    ARSize(){isSet = false;};
    ARSize(size_t d0,size_t d1 = 0,size_t d2 = 0);
    ARSize(int d0,int d1 = 0,int d2 = 0);
    ARSize(float d0,float d1 = 0,float d2 = 0,float isfloat = 1);
    ARSize(size_t numDim,size_t* d1);
    ARSize(size_t numDim,float* s);

    void dim(size_t d0,size_t d1,size_t d2);
    void scale(float s0, float s1, float s2);
    void dim(size_t nd, size_t* s);
    void scale(size_t nd, float * s);


    size_t multiplicity();
    size_t dSize();
    size_t dx();
    size_t dy();
    size_t dz();
    size_t * dataI(){return mSize;}
    float  * dataF(){return mScale;}

    float sx();
    float sy();
    float sz();

    bool valid(){return isSet;}

    static const int UNDEF;

    private:
    size_t mMultiplicity;
    size_t mSize[4];
    float  mScale[4];
    bool isSet;


};
#endif // PROGRAM

