#ifndef CL_MEMORY_H
#define CL_MEMORY_H

#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <fstream>

#include "cl_helper.h"
#include "ar_size.h"

#include <CL/cl_gl.h>
#include <CL/cl.hpp>
#include "GLHelper.h"


struct ImageOptions{

    ARSize mSize;
    cl::ImageFormat format;
    cl_mem_flags flags;
    int numElem;
    ImageOptions(){};

    ImageOptions(cl::ImageFormat fo, ARSize size, cl_mem_flags fl = CL_MEM_READ_WRITE)
    {
        mSize  = size;
        flags  = fl;
        format = fo;
        switch(fo.image_channel_order)
        {
        case CL_R:
            numElem = 1;
            break;
        case CL_RG:
            numElem = 2;
            break;
        case CL_RGBA:
            numElem = 4;
            break;
        default:
            printf("Invalid image format\n");
            exit(-1);
            break;

        }
    }
};

typedef enum {
    Image,
    Buffer
}MemType;

typedef enum {
    VertexPosition,
    VertexNormal,
    VertexTexCoord,
    VertexIndex
}GLBufferItem;

void PERR(cl_int r,const char * errStr = "");

cl_image_desc   getDesctriptor(ImageOptions opt, void * hostPtr);

cl_image_format getFormat(ImageOptions opt);

cl_mem          glTextToClImg(cl_context context, ImageOptions opt, GLuint textId);

size_t          getImageSize(ImageOptions);
size_t          getChannelSize(ImageOptions);
class CLMemory{

    public :
        CLMemory():mCLMemoryPtr(0), mGLSharing(false),mIsAcquired(false),mDataPtr(NULL),mSize(0),mSizeBytes(0),mId(-1)
        {

        }

        void mem(cl_mem m){
            mCLMemoryPtr = m;
        }


        cl_mem operator()()
        {
            return mCLMemoryPtr;
        }

        void initialize(cl::Context context){

            if(active())
                return;

            cl_int res;

            mCLContext = context;
            mDevices = mCLContext.getInfo<CL_CONTEXT_DEVICES>();

            cl_command_queue_properties commandQueueProperties = 0;
            mCommandQueue = clCreateCommandQueue(mCLContext(), mDevices[0](), commandQueueProperties, &res);

            PERR(res,"CLMemory - Initialize");
        }

        template<class TTYPE>
        void createImage(ImageOptions options,void * hostPtr = NULL){

            mImageOptions = options;
            /// Set size
            mSize      = getImageSize(options);
            mSizeBytes = mSize*getChannelSize(options);

            /// Set type
			mMemType               = Image;

			/// Create image descriptor and format
			cl_image_desc desc     = getDesctriptor(options,hostPtr);
			cl_image_format format = getFormat(options);

			/// Create image
			cl_int result;
			mCLMemoryPtr = clCreateImage(mCLContext(), options.flags, &format,&desc,hostPtr,&result);
			PERR(result,"Create Image");
        }
        template<class TTYPE>
        void createBuffer(size_t numElem, cl_mem_flags flags = CL_MEM_READ_WRITE, void * hostptr = NULL){

            if(mCLMemoryPtr != 0)
                freeBuffer();

            /// Set Type
            mMemType = Buffer;

            /// Set size
            mSize      = numElem;
            mSizeBytes = mSize*sizeof(TTYPE);

            cl_int result;

            /// Allocate cl buffer
            mCLMemoryPtr = clCreateBuffer(mCLContext(), flags, mSizeBytes, hostptr, &result);
            PERR(result,"Create buffer");

            if(hostptr != NULL) {

               /// Upload data to cl buffer
               cl_event copyEvent;
               result = clEnqueueWriteBuffer(mCommandQueue, mCLMemoryPtr, CL_TRUE, 0, mSizeBytes, hostptr, 0, NULL, &copyEvent);
               PERR(result,"Create buffer - upload data");

               result = clWaitForEvents(1, &copyEvent);
               PERR(result,"Create buffer - wait upload ");

               clFinish(mCommandQueue);
            }
        }

        template<class TTYPE>
		void GLTexture(GLuint texId, ImageOptions options){

            mImageOptions = options;

		    /// Set type
            mMemType = Image;

		    /// Set size
		    mSize      = getImageSize(options);
            mSizeBytes = mSize*getChannelSize(options);

            /// Create cl texture from gl texture
            mCLMemoryPtr = glTextToClImg(mCLContext(),options,texId);
            mGLSharing = true;
		}

		void GLBuffer(GLHelper::Buffer buffer,GLBufferItem item,cl_mem_flags flags = CL_MEM_READ_WRITE){

		    size_t sizeData = 4;
		    size_t stride   = 0;
		    size_t numElem  = 0;
		    GLuint bufId    = 0;

		    switch(item) {
                case VertexPosition:
                    stride  = 4;
                    bufId   = buffer.sVBOPosition;
                    numElem = buffer.sNumVertices;

                    if(buffer.sPositionSize == 0)
                        return;

                    break;
                case VertexNormal:
                    stride = 4;
                    bufId  = buffer.sVBONormal;
                    numElem = buffer.sNumVertices;

                    if(buffer.sNormalSize == 0)
                        return;

                    break;
                case VertexTexCoord:
                    stride = 2;
                    bufId  = buffer.sVBOTextureCoord;
                    numElem = buffer.sNumVertices;

                    if(buffer.sTextureCoordSize == 0)
                        return;

                    break;
                case VertexIndex:
                    stride = 1;
                    bufId  = buffer.sIBO;
                    numElem = buffer.sNumIndices;
                    break;
		    }

            /// Set type
            mMemType = Buffer;

            /// Set size
            mSize      = numElem*stride;
            mSizeBytes = mSize*sizeData;

            /// Create cl memory from gl buffer
            int res;
            mCLMemoryPtr = clCreateFromGLBuffer(mCLContext(),flags,bufId,&res);
            PERR(res,"Create cl-buffer from gl-buffer");
            mGLSharing = true;
		}



        template<class TTYPE>
        void toFile(std::string filename){
            TTYPE* ptr = toHost<TTYPE>();
            std::ofstream out(filename.c_str());
            size_t N = mSizeBytes/sizeof(TTYPE);
            for(size_t i = 0; i < N; i++){
                    if(sizeof(TTYPE) == 1)
                    out << (int)ptr[i];
                        else
                    out << ptr[i];
                    if(i < (mSize-1))
                        out << ",";
            }
            out.close();
            printf("%s written, num elements: %d\n",filename.c_str(), N);
        }

        template<class TTYPE>
        TTYPE * toHost(size_t* nBytes = NULL, size_t* nElem = NULL){
            TTYPE* ptr = reinterpret_cast<TTYPE*>(mDataPtr);
            if(ptr != NULL)
                delete[] ptr;

            if(nBytes != NULL)
                *nBytes = mSizeBytes;

            if(nElem != NULL)
                *nElem = mSize;

            ptr         = new TTYPE[mSize];

            if(mMemType == Buffer){
                cl_int res = clEnqueueReadBuffer(mCommandQueue,mCLMemoryPtr,CL_TRUE,0,mSizeBytes,ptr,0,NULL,NULL);
                PERR(res);
            }else{

                size_t sx = mImageOptions.mSize.dx();
                size_t sy = mImageOptions.mSize.dy();
                size_t sz = mImageOptions.mSize.dz();

                size_t origin[] ={0,0,0};
                size_t region[] ={sx,sy,sz};

                size_t rowPitch = sx*mImageOptions.numElem*sizeof(TTYPE);
                size_t rowSlice = sx*sy*mImageOptions.numElem*sizeof(TTYPE);

                cl_int res = clEnqueueReadImage (mCommandQueue,mCLMemoryPtr,CL_TRUE,origin,region,rowPitch,rowSlice,ptr,0,NULL,NULL);
                PERR(res);
            }
            return ptr;
        }

        bool active(){
            return mCLMemoryPtr != 0;
        }

        int id(){
            return mId;
        }

        void id(int id){
            mId = id;
        }


        template<class TTYPE>
        void free(){
            TTYPE* ptr = reinterpret_cast<TTYPE*>(mDataPtr);
            cl_int res = clReleaseMemObject(mCLMemoryPtr);
            PERR(res,"CLMemory - Release cl_mem");
            if(ptr != NULL)
                delete[] ptr;
        }



        void bind();
        void release();

        void resize();

        bool GLSharing()   { return mGLSharing;};
        MemType type()     { return mMemType;}
        size_t size()      { return mSize;}
        size_t sizeBytes() { return mSizeBytes;}


private:

    void freeBuffer(){
        cl_int res =  clReleaseMemObject(mCLMemoryPtr);
        PERR(res);
    }

    cl_mem mCLMemoryPtr;

    bool            mGLSharing;
    bool            mIsAcquired;

    MemType  mMemType;
    void * mDataPtr;
    ImageOptions mImageOptions;


    ///
    cl_command_queue mCommandQueue;
    std::vector<cl::Device> mDevices;
	cl::Context mCLContext;

    size_t mSize;
    size_t mSizeBytes;

    int mId;

};


#endif
