#include "ar_timer.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdlib.h>

using namespace std;

ARTimer::ARTimer(const string& label)
: mLabel(label), mStartTime(0)
{

}
inline
void ASSERT_START(clock_t time){
    if(time == 0) {
        printf("Start timer before doing other stuff with it\n");
        exit(-1);
    }
}

void ARTimer::start(){
    mStartTime = clock();
}

void ARTimer::lap(const string& label){
    ASSERT_START(mStartTime);
    clock_t delta = clock() - mStartTime;
    if(mLapOrder.count(label)){
        int idx = mLapOrder[label];
        mLapVector.at(idx).update(delta);
    }else{
        mLapOrder[label] = mLapVector.size();
        mLapVector.push_back(LapType(label,delta));
    }
    mStartTime = clock();
}

void ARTimer::stop(const string&label){
    ASSERT_START(mStartTime);
    lap(label);
}

clock_t ARTimer::sum() {
    clock_t s = 0;
    for (auto & lap : mLapVector)
        s += lap.time;

    for(auto * child : mChildren)
        s += child->sum();

    return s;
}

void ARTimer::fill(vector<LapType>& laps) const {
    copy(mLapVector.begin(),mLapVector.end(),back_inserter(laps));
}

string ARTimer::label() const{
    return mLabel;
}

size_t ARTimer::size(){
    return mLapVector.size();
}

void ARTimer::print(){
    printf("%s", format(0).c_str());
}

void ARTimer::dump(const char* filename){
   std::ofstream out(filename);
   out << format(0);
   out.close();
}

inline string tab(int numtabs){
    stringstream ss;
    for(int i = 0; i < numtabs; i++) ss << "\t";
    return ss.str();
}

string ARTimer::format(size_t lvl){
    stringstream ss;
    ss << tab(lvl) << mLabel << ": " << sum() << "ms\n";
    for (auto & lap : mLapVector)
        if(lap.label.compare("NO_LABEL"))
            ss << tab(lvl+1) << lap.label << " - " << lap.time << "ms\n";

    for(auto * child : mChildren)
        ss << child->format(lvl+1);

    return ss.str();
}

ARTimer& ARTimer::operator+=(const ARTimer& other){
    other.fill(mLapVector);
    return *this;
}

ARTimer ARTimer::operator+(const ARTimer& other){
    *this += other;
    ARTimer newTimer(this->label());
    newTimer += *this;
    return newTimer;
}

ARTimer& ARTimer::operator<<(ARTimer& other){
    mChildren.push_back(&other);
    return *this;
}
