#include "cl_kernel.h"

using namespace std;

CLKernel::CLKernel(cl_kernel kernel,cl_command_queue commandQueue, string name)
: mKernel(kernel), mCommandQueue(commandQueue), mPingPong(NULL), mCtr(0), mNumArgs(0), mLabel(name),mTimer(name)
{

}

CLKernel::~CLKernel(){
    release();
}

std::string CLKernel::getLabel(){
    return mLabel;
}

void CLKernel::incNumArg(){
    mNumArgs++;
}
cl_uint CLKernel::numArg(){
    return mNumArgs;
}

void CLKernel::arg(void * a, size_t sizeArg,int argIndex){
    int na = argIndex;
    if( argIndex == -1){
        na = numArg();
    }

    cl_int result = clSetKernelArg(mKernel,na,sizeArg,a);
    CL_ERR_S(result,"Kernel arg");
    incNumArg();
    if(mPingPong != NULL)
        mPingPong->arg(a,sizeArg,na);
}

void CLKernel::setArgument(int argIndex, cl_float arg){
    cl_int result = clSetKernelArg(mKernel,argIndex,sizeof(cl_float),&arg);
    CL_ERR_S(result,"Kernel set arg");
    incNumArg();
    if(mPingPong != NULL)
        mPingPong->setArgument(argIndex,arg);

}

void CLKernel::setArgument(int argIndex, CLMemory m){

    int sizeMem = sizeof(cl_mem);
    cl_mem mem = m();
    cl_int result = clSetKernelArg(mKernel,argIndex,sizeMem,&mem);

    CL_ERR_S(result,"Kernel set arg mem");
    incNumArg();
    if(mPingPong != NULL)
        mPingPong->setArgument(argIndex,m);

}

void CLKernel::arg(cl_float arg_){
    setArgument(numArg(),arg_);
}

void CLKernel::arg(cl_uint arg_){
     setArgument(numArg(),arg_);
}


void CLKernel::setArgument(int argIndex, cl_uint arg){
    cl_int result = clSetKernelArg(mKernel,argIndex,sizeof(cl_uint),&arg);
    CL_ERR_S(result,"Kernel Set arg uint");
    incNumArg();
    if(mPingPong != NULL)
        mPingPong->setArgument(argIndex,arg);

}


void CLKernel::threadDimension(ARSize global,ARSize local){
    mNumDimensions = global.multiplicity();
    size_t * l = local.dataI();
    size_t * g = global.dataI();
    for(size_t i = 0; i < mNumDimensions; i++){
         mLocalSize[i]  = l[i];
         mGlobalSize[i] = g[i];
    }
    if(mPingPong != NULL)
        mPingPong->threadDimension(global,local);
}

void CLKernel::createPingPong(CLKernel* kernel,CLMemory buf1,CLMemory buf2){


    this->setArgument(mNumArgs,buf1);
    this->setArgument(mNumArgs,buf2);

    kernel->setArgument(kernel->numArg(),buf2);
    kernel->setArgument(kernel->numArg(),buf1);
    mPingPong = kernel;

}

void CLKernel::createPingPong(CLKernel* kernel){
    mPingPong = kernel;
}

//#define PRINT_STATS

/** \brief Run kernel. Dimension and arguments should have been
 * provided before calling this function.
 *
 * \return void
 *
 */
 void CLKernel::run(){
     run(false);
 }

void CLKernel::run(bool finish){

    if(mPingPong != NULL && mCtr % 2 == 1){
        mPingPong->run(finish);
        mCtr = 0;
        return;
    }
    mCtr++;
    cl_event  kernelExecEvent;
    if(!finish)
      kernelExecEvent = NULL;

    mTimer.start();

    /// Set dimension of the data
    size_t*   globalWorkOffset = NULL;

    /// Run kernel
    cl_int result;
    if(mLocalSize[0] == 0){
        result = clEnqueueNDRangeKernel(mCommandQueue, mKernel, mNumDimensions, globalWorkOffset, mGlobalSize,
                    NULL, 0, NULL, &kernelExecEvent);
    }else{
        result = clEnqueueNDRangeKernel(mCommandQueue, mKernel, mNumDimensions, globalWorkOffset, mGlobalSize,
                    mLocalSize,0, NULL, &kernelExecEvent);
    }
    CL_ERR_S(result,"Kernel Run");



    /// Wait for CL to execute all events
    if(finish){
        result = clWaitForEvents(1, &kernelExecEvent);
        clReleaseEvent(kernelExecEvent);
        CL_ERR_S(result,"Kernel Run - wait");
    }

    mTimer.lap("NO_LABEL");

    free(globalWorkOffset);

}

ARTimer& CLKernel::getTimer(){
    return mTimer;
}

/** \brief Delete OpenCL kernel
 *
 * \param kernelName std::string
 * \return void
 *
 */
void CLKernel::release(){
    cl_int res = clReleaseKernel(mKernel);
    CL_ERR(res);
}




