#ifndef CL_TYPES_H
#define CL_TYPES_H

#include "cl_memory.h"

struct MeshType{
    CLMemory sVertexPosition;
    CLMemory sVertexNormal;
    CLMemory sVertexTexCoord;
    CLMemory sVertexIndex;
    cl_uint sNumVertices;
    cl_uint sNumIndices;
    cl_uint* sNumVerticesPtr;
    cl_uint* sNumIndicesPtr;
    MeshType(){
        sNumVertices    = 0;
        sNumIndices    = 0;
        sNumVerticesPtr = NULL;
        sNumIndicesPtr  = NULL;
    }



    MeshType(GLHelper::Buffer buffers, cl::Context context){

        sNumVertices = buffers.sNumVertices;
        sNumIndices  = buffers.sNumIndices;

        sNumVerticesPtr = new cl_uint(sNumVertices);
        sNumIndicesPtr  = new cl_uint(sNumIndices);

        if(buffers.hasPositionBuffer()){
            sVertexPosition.initialize(context);
            sVertexPosition.GLBuffer(buffers,VertexPosition);
        }

        if(buffers.hasNormalBuffer()){
            sVertexNormal.initialize(context);
            sVertexNormal.GLBuffer(buffers,VertexNormal);
        }

        if(buffers.hasTexCoordBuffer()){
            sVertexTexCoord.initialize(context);
            sVertexTexCoord.GLBuffer(buffers,VertexTexCoord);
        }

        sVertexIndex.initialize(context);
        sVertexIndex.GLBuffer(buffers,VertexIndex);

    }

    void setMeta(cl_uint numIndices, cl_uint numVert){
        sNumVertices = numVert;
        sNumIndices  = numIndices;

        if(sNumVerticesPtr == NULL)
            sNumVerticesPtr = new cl_uint(numVert);
        else
            *sNumVerticesPtr  = numVert;

        if(sNumIndicesPtr == NULL)
            sNumIndicesPtr  = new cl_uint(numIndices);
        else
            *sNumIndicesPtr  = numIndices;
    }


    void bind(){
        sVertexPosition.bind();
        sVertexNormal.bind();
        sVertexTexCoord.bind();
        sVertexIndex.bind();
    }

    void release(){
        sVertexPosition.release();
        sVertexNormal.release();
        sVertexTexCoord.release();
        sVertexIndex.release();
    }

};

typedef struct{
    CLMemory sNodes;
    CLMemory sRoot;
    cl_uint sRootN;
    MeshType sMesh;

    void bind(){
        sNodes.bind();
        sRoot.bind();
        sMesh.bind();
    }

    void release(){
        sNodes.release();
        sRoot.release();
        sMesh.release();
    }
}CollidableType;



#endif //

