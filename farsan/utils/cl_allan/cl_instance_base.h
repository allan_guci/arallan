#ifndef _CL_INSTANCE_BASE_H
#define _CL_INSTANCE_BASE_H
#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <map>

#include "cl_helper.h"
#include "cl_kernel.h"
#include "cl_program.h"
#include "cl_memory.h"
#include "cl_types.h"
#define CL_MEM  CLMemory

#define setInput(name) inputMem(name,#name)
#define getInput(name) inputMem(name)

class CLInstanceBase
{


public:
	CLInstanceBase();
    virtual void initCL(cl::Context context) = 0;
    virtual void runCL() = 0;

    CLMemory getCLOutput();
    void setCLInput(CLMemory m);
    void inputMem(CLMemory m, std::string key);

    void setInputMesh(MeshType mesh);
    MeshType getOutputMesh();

    void setCollidable(CollidableType collidable);
    CollidableType getCollidable();

    ARTimer& getTimer();




protected:


    void setup(cl::Context context);


    /// Kernel
    CLKernel * CreateKernel(const char* kernelName,ARSize globalThreadSize = ARSize(),ARSize localThreadSize = ARSize());
    void  AddKernel(CLKernel * kernel, std::string key = "DEFAULT");
    void RunKernels(bool finish = false);
    void RunKernel(CLKernel* kern);
    CLKernel * GetKernel(std::string key);



    /// Memory

    void bindGLMem();

    void releaseGLMem();

    void sharedGLMem(CL_MEM& m){mSharedGLMem.push_back(m);};

    void sync();

    /// Program
    void CreateProgram(const char * a){
        mCLProgram.createAndCompileProgram(mCLContext(),mCommandQueue,a,mDevices);
        selectProgram(mCLProgram.get());
        mTimer = ARTimer(std::string(a));
    }
    void selectProgram(cl_program program){mSelectedProgram = program;};

    #define MEM_CREATE(OP) \
    CLMemory mem; \
    mem.initialize(mCLContext); \
    mem.OP; \
    if(mem.id() != -1){ \
        mAllBuffers[mem.id()] = mem; \
        return mem; \
    } \
    int s = mAllBuffers.size(); \
    mem.id(s); \
    mAllBuffers.push_back(mem); \
    return mem



    /// CREATE CL MEMORY OBJECTS
    template<class TTYPE>
    CLMemory CreateBuffer(size_t numElem, cl_mem_flags flags = CL_MEM_READ_WRITE, void * hostptr = NULL){
        MEM_CREATE(createBuffer<TTYPE>(numElem,flags,hostptr));
    }

    CLMemory CreateImage(ImageOptions options,void * hostPtr = NULL);

    CLMemory ResizeImage();

    CLMemory CreateImageFromGL(GLuint texId, ImageOptions options);
    CLMemory CreateBufferFromGL(GLHelper::Buffer buffer,GLBufferItem item,cl_mem_flags flags = CL_MEM_READ_WRITE);

    /// CL Input/Output
    CLMemory getCLInput();
    CLMemory inputMem(std::string key);
    void setCLOutput(CL_MEM m);

    /// CL Mesh Input/Output
    void setOutputMesh(MeshType mesh);
    MeshType getInputMesh();



    template<typename T>
    void addMacro(const char * s,const T& t){
        std::ostringstream ss;
        ss<<"-D";
        ss<<s;
        ss<< "=";
        ss<< t;
        if(std::is_floating_point<T>::value)
            ss<< "f";
        ss<< " ";
        mCLProgram.setMacroValue(ss.str());
    }

    void setOptions(std::string opt){
        mCLProgram.setMacroValue(opt);
    }



    ARSize NumLocalThreads(){ return mNumLocalThreads;}
    void   NumLocalThreads( ARSize nt) { mNumLocalThreads = nt;}

    ARSize NumGlobalThreads(){return mNumGlobalThreads;}
    void   NumGlobalThreads( ARSize nt) { mNumGlobalThreads = nt;}

    cl_command_queue mCommandQueue;
    cl::Context getContext(){ return mCLContext;}

    CollidableType mCollidable;

	private:

    std::vector<cl::Device> mDevices;
	cl::Context mCLContext;

    cl_program mSelectedProgram;
    CLProgram mCLProgram;

    std::vector<CL_MEM> mSharedGLMem;
    std::vector<CL_MEM> mAllBuffers;

    std::map<std::string,CLKernel *> mKernels;
    std::map<int,std::string> mKernelOrder;
    std::map<std::string, CLMemory> mInputMemory;
    ARSize mNumGlobalThreads;
    ARSize mNumLocalThreads;

    CL_MEM mCLInput;
    CL_MEM mCLOutput;

    MeshType mInMesh;
    MeshType mOutMesh;

    ARTimer mTimer;

};

#endif
