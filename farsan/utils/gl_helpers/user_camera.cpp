#include "user_camera.h"

UserCamera::UserCamera()
{

    cameraEye = vec3(-1.5f, 3.50f, -1.5f);
  //  cameraCenter = vec3(2.0f, 0.0f, 2.0f);
    cameraCenter = vec3(1.0f, 1.5f, 1.0f);
    cameraUp = vec3(0.0f, 1.0f, 0.0f);


    //updateViewMatrix();

    mFar  = 40.0f;
    mNear = 0.2f;
    xPrev = -1;
    yPrev = -1;
    mFov  = 90.0f;
}

void UserCamera::create(vec3 eye, vec3 center, float near , float far, int width,int height,float fov){

    cameraEye = eye;
    cameraCenter = center;

    mNear = near;
    mFar  = far;

    mScreenWidth = width;
    mScreenHeight = height;

    mFov = fov;

    updateProjectionMatrix();
    updateViewMatrix();

}

UserCamera::~UserCamera()
{
    //dtor
}

void UserCamera::init(){
  updateViewMatrix();
}

vec3 UserCamera::getEye(){

    return cameraEye;
}

void UserCamera::setScreenSize(GLuint w, GLuint h){
    mScreenWidth  = w;
    mScreenHeight = h;
    GLint curfbo;
    glGetIntegerv(GL_FRAMEBUFFER_BINDING, &curfbo);
    if(curfbo == 0){
        glViewport(0, 0, w, h);
    }
    updateProjectionMatrix();
	printError("Reshape");

}


mat4 UserCamera::getTextureMatrix(mat4 modelMatrix){
    /*vec3 lightUp  = vec3(0,1,0);
    vec3 look     = lookAtVec();
	vec3 right    = rightVec();

	GLfloat pi          = 3.14159265;
	GLfloat  angleY 	= pi-atan2(look.x,look.z);

	GLfloat  lengthXZ 	= sqrt(look.x*look.x + look.z*look.z);
	GLfloat  angleArb 	= atan2(look.y,lengthXZ);

    mat4 trans = T(0,-1,0);
	mat4 rot   = ArbRotate(look,angleArb)*Ry(angleY);
	mat4 model = trans*rot;*/

	mat4 textureMatrix = T(0.5, 0.5, 0.0)*S(0.5, 0.5, 1.0)*((mProjectionMatrix*mViewMatrix)*modelMatrix); //*trans); //*model);
	return textureMatrix;

}
void UserCamera::updateProjectionMatrix(){
    float s = mScreenWidth;
    float l = mScreenHeight;
	GLfloat ratio = s/l;
	mProjectionMatrix = perspective(mFov, ratio, mNear, mFar);
}

 GLfloat UserCamera::getFar(){
    return mFar;
 }
 GLfloat UserCamera::getNear(){
    return mNear;
 }



void UserCamera::keyPress(unsigned char key, int x, int y)
{
    float speed = 0.1f;

    vec3 lk = lookAtVec();
    vec3 right = rightVec();
    vec3 up    = upVec();
    float sphereSpeed = 0.02f;
    vec3 sphereDeltaPos = vec3(0,0,0);
    switch (key)
    {
        case 'w':
            cameraEye += speed*lk;
            cameraCenter += speed*lk;
        break;
        case 's':
            cameraEye -= speed*lk;
            cameraCenter -= speed*lk;
        break;
        case 'd':
            cameraEye += speed*right;
            cameraCenter += speed*right;
        break;
        case 'a':
            cameraEye -= speed*right;
            cameraCenter -= speed*right;
        break;
        case 'r':
            //mFluids->toggleSmookeSource();
        break;
        case 'W':
            sphereDeltaPos = sphereSpeed*lk;
        break;
        case 'S':
            sphereDeltaPos = -sphereSpeed*lk;
        break;
        case 'D':
            sphereDeltaPos = sphereSpeed*right;
        break;
        case 'A':
             sphereDeltaPos = -sphereSpeed*right;
        break;
        case 'Z':
            sphereDeltaPos = sphereSpeed*up;
        break;
        case 'X':
            sphereDeltaPos = -sphereSpeed*up;
        break;

    }

    updateViewMatrix();


}

void UserCamera::updateViewMatrix(){

 mViewMatrix =  lookAt(cameraEye.x, cameraEye.y, cameraEye.z,
                       cameraCenter.x, cameraCenter.y, cameraCenter.z,
                       cameraUp.x, cameraUp.y, cameraUp.z);
}

void UserCamera::mouseHandle(int x, int y)
{
    if (xPrev > 0 && yPrev > 0) {
        GLfloat mouseFactor = 0.01f;
        float dx = mouseFactor * (x - xPrev);
        float dy = mouseFactor * (y - yPrev);

        cameraCenter = cameraEye + (lookAtVec() + dx * rightVec() - dy * upVec());
        updateViewMatrix();
    }
    xPrev = x;
    yPrev = y;
   // print2Vec3((char*)"CAM EYE: ", *cameraEye);
   // print2Vec3((char*)"CAM CENTER: ", *cameraCenter);
}

mat4 UserCamera::getViewMatrix()
{
    return mViewMatrix;
}

mat4 UserCamera::getProjectionMatrix(){
    return mProjectionMatrix;
}

void UserCamera::mouseUp()
{
    xPrev = -1;
    yPrev = -1;
}

vec3 UserCamera::lookAtVec()
{
    return Normalize(cameraCenter - cameraEye);
}

vec3 UserCamera::rightVec()
{
    return Normalize(CrossProduct(lookAtVec(), cameraUp));
}

vec3 UserCamera::upVec()
{
    return CrossProduct(rightVec(), lookAtVec());
}
