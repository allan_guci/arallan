#ifndef KEYMOUSEHANDLER_H
#define KEYMOUSEHANDLER_H

#include "GL_utilities.h"
#include "VectorUtils3.h"

class UserCamera
{
    public:


        UserCamera();
        virtual ~UserCamera();

        void init();

        void create(vec3 eye, vec3 center, float near , float far, int width,int height,float fov = 90);

        void keyPress(unsigned char key, int x, int y);
        void mouseHandle(int x, int y);
        void setScreenSize(GLuint mWidth, GLuint mHeight);

        mat4 getViewMatrix();
        mat4 getProjectionMatrix();
        mat4 getTextureMatrix(mat4 modelMatrix);

        vec3 getEye();
        vec3 lookAtVec();

        GLfloat getFar();
        GLfloat getNear();

        void mouseUp();

        GLuint mScreenWidth;
        GLuint mScreenHeight;

    protected:
    private:

        void updateViewMatrix();
        void updateProjectionMatrix();


        mat4 mViewMatrix;
        mat4 mProjectionMatrix;

        vec3 cameraEye;
        vec3 cameraCenter;
        vec3 cameraUp;

        float mFar;
        float mNear;

        float mFov;

        // Previous mouse position
        GLfloat xPrev, yPrev;


        vec3 rightVec();
        vec3 upVec();

};

#endif // KEYMOUSEHANDLER_H
