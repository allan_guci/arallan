#ifndef DEBUG_MESH_H
#define DEBUG_MESH_H

#include "Renderable.h"
#include "user_camera.h"
#define DEBUG_MESH_SHADER 1

class DebugMesh  {


public :
	DebugMesh();

	void SetBuffers(int width, int height,GLHelper::Buffer& buf);
	void draw(UserCamera * uc);
	void setColorTexture(GLuint tex);
	void init();
private:
	Renderable * mRenderable;
};


#endif



