#include "gl_instance_base.h"


GLInstanceBase::GLInstanceBase(): mSelectedShader(0),mShaderCount(0),mNumRenderTargets(0),mCurrentRenderTarget(0)
{
    mGLScene = new Renderable();
    mBlendEnabled = false;
    mModelMatrix = IdentityMatrix();
}
GLuint GLInstanceBase::createShader(const char * vs,const char * fs,const char * gs,Tranform_Composition_Type type){
    GLuint shader;

    if(gs == NULL)
        shader = loadShaders(vs, fs);
    else
        shader = loadShadersG(vs, fs,gs);

    mSelectedShader = mShaderCount;
    mGLScene->setShader(shader,mShaderCount,type);
    MMSet();
    mShaderCount++;

    return mSelectedShader;
}

void GLInstanceBase::selectShader(GLuint shader){
    mSelectedShader = shader;
}

void GLInstanceBase::setTexture(GLuint texId,const char* textureName, GLenum target){
    if(target == GL_TEXTURE_3D)
        mGLScene->set3DTexture(mSelectedShader,texId,textureName);
    else if(target == GL_TEXTURE_2D)
        mGLScene->setTexture(mSelectedShader,texId,textureName);
    else{
        GL_ERR("Invalid Texture Target");
    }
}

GLuint GLInstanceBase::getDepthTexture(){
    return getRenderTarget().sDepthTexture;
}

GLuint GLInstanceBase::getColorTexture(){
    return getRenderTarget().sColorTexture;
}


GLHelper::Buffer GLInstanceBase::loadModel(const char * filePath, bool useNormal, bool useTexCoord, bool upload){
    Model * m  = LoadModelPlus((char *)filePath);
    if(!useNormal && m->normalArray != NULL){
        free(m->normalArray);
        m->normalArray = NULL;
    }
    if(!useTexCoord && m->texCoordArray != NULL){
        free(m->texCoordArray);
        m->texCoordArray = NULL;
    }
    GLHelper::Buffer buf  = GLHelper::Buffer::model2Buffer(m);
    if(upload)
        mGLScene->setBuffers(buf,mSelectedShader);
    return buf;

}

void GLInstanceBase::loadQuadModel(GLHelper::Buffer ibuf){
   GLfloat square[] = {
                    -1,-1,0,
                    -1,1, 0,
                    1,1, 0,
                    1,-1, 0};
    GLfloat squareTexCoord[] = {
                     0, 0,
                     0, 1,
                     1, 1,
                     1, 0};

    GLuint squareIndices[] = {0, 2, 1, 0, 3, 2};


    GLHelper::Buffer buf = GLHelper::Buffer::createBuffers(
                    square,
                    NULL,
                    squareTexCoord,
                    squareIndices,
                    4,
                    2*3);

    if(ibuf.isActive()){
        typedef GLHelper::Buffer::InstanceType BufPair;
        std::vector<BufPair> pairs;
        pairs.push_back(BufPair(ibuf.sVBOPosition,ibuf.sPositionSize));
        pairs.push_back(BufPair(ibuf.sVBONormal,ibuf.sNormalSize));
        buf.setInstanced(pairs);

        mGLScene->setDrawMethod(INSTANCED,mSelectedShader,ibuf.sNumVertices);
    }
    mGLScene->setBuffers(buf,mSelectedShader);
/*
    mGLScene->setBuffers(
        square,
        NULL,
        squareTexCoord,
        squareIndices,
        4,
        2*3,
        mSelectedShader);*/

}

void GLInstanceBase::setBuffer(GLHelper::Buffer buf){
    mGLScene->setBuffers(buf,mSelectedShader);

}
void GLInstanceBase::setBufferNumIndices(size_t n){
    mGLScene->setNumIndices(mSelectedShader,n);
}
void GLInstanceBase::MMUpload(){
    mGLScene->setUniformMatrix(mModelMatrix,mSelectedShader,"M_Matrix");
}

void GLInstanceBase::MMSet(){
    mGLScene->setTransform(mModelMatrix,mSelectedShader);
}

void GLInstanceBase::MMRotate(GLfloat x,GLfloat y,GLfloat z){
    mModelMatrix *= Rx(x);
    mModelMatrix *= Ry(y);
    mModelMatrix *= Rz(z);
    MMSet();
}
void GLInstanceBase::MMScale(GLfloat x,GLfloat y,GLfloat z){
    mModelMatrix = mModelMatrix*S(x,y,z);
    MMSet();
}
void GLInstanceBase::MMTranslate(GLfloat x,GLfloat y,GLfloat z){
    mModelMatrix = mModelMatrix*T(x,y,z);
    MMSet();
}
void GLInstanceBase::MMReset(){
    mModelMatrix = IdentityMatrix();
    MMSet();
}
void GLInstanceBase::enableRenderTarget(){
    if(mRenderTargets[mCurrentRenderTarget].sActive){
        glBindFramebuffer(GL_FRAMEBUFFER, mRenderTargets[mCurrentRenderTarget].sFBO);
        glViewport(0,0,mRenderTargets[mCurrentRenderTarget].sWidth, mRenderTargets[mCurrentRenderTarget].sHeight);
       // glClearColor(0.0, 0.0, 0.0, 0.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }
}
void GLInstanceBase::disableRenderTarget(ARSize defaultSize){
    if(mRenderTargets[mCurrentRenderTarget].sActive){
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glViewport(0,0,defaultSize.dx(),defaultSize.dy());
    }
}
void GLInstanceBase::draw(UserCamera* uc){
    if(mBlendEnabled){
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }

    enableRenderTarget();

    mGLScene->draw(uc->getProjectionMatrix(),uc->getViewMatrix());

    disableRenderTarget(ARSize(uc->mScreenWidth,uc->mScreenHeight));

    if(mBlendEnabled){
        glDisable(GL_BLEND);
    }
}

void GLInstanceBase::setUniformFloat(GLfloat* f,const char * uniformName,size_t s){
    if(s == 0)
        mGLScene->replaceUniformFloat(f,mSelectedShader,uniformName);
    else
        mGLScene->setUniformFloat(f,s,mSelectedShader,uniformName);
}

void GLInstanceBase::setUniformFloat(GLfloat f,const char * uniformName){
    mGLScene->setUniformFloat(f,mSelectedShader,uniformName);
}

void GLInstanceBase::setUniformMatrix(mat4 mat, const char* uniformName){
    mGLScene->setUniformMatrix(mat,mSelectedShader,uniformName);
}

void GLInstanceBase::setBlend(bool b){
    mBlendEnabled = b;
}

void GLInstanceBase::createRenderTarget(RenderTargetType type,ARSize size, GLuint internalFormat,GLuint format,int nunRenderTargets){
    mNumRenderTargets = nunRenderTargets;
    for(int i = 0; i < mNumRenderTargets; i ++){
        GLuint fbo;
        glGenFramebuffers(1, &fbo);
        glBindFramebuffer(GL_FRAMEBUFFER, fbo);

        if(type == COLOR ||type == COLOR_DEPTH){
            GLuint colorTex = CreateGLTexture(internalFormat,format,size);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, colorTex, 0);
            mRenderTargets[i].sColorTexture = colorTex;
        }

        if(type == DEPTH || type == COLOR_DEPTH){
              GLuint depthTex;
              GL_ERR("Create Render Target - Pre Depth Buffer Texture");
              glGenTextures(1, &depthTex);
              glBindTexture(GL_TEXTURE_2D, depthTex);

              glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, size.dx(), size.dy(), 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, 0L);
              GL_ERR("Create Render Target - Depth Buffer Texture");
              glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
              glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
              glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
              glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
              glBindTexture(GL_TEXTURE_2D, 0);
              glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthTex, 0);
              mRenderTargets[i].sDepthTexture = depthTex;
        }
        GLenum status;
        status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
        if (status != GL_FRAMEBUFFER_COMPLETE){
            printf("Error - Creating render target\n");
            exit(-1);
        }

        mRenderTargets[i].sWidth  = size.dx();
        mRenderTargets[i].sHeight = size.dy();

        GL_ERR("Create Render Target");
        mRenderTargets[i].sFBO = fbo;
        glBindFramebuffer(GL_FRAMEBUFFER, 0);

    }

}

void GLInstanceBase::useRenderTarget(bool b){
    for(int i = 0; i < mNumRenderTargets; i ++)
        mRenderTargets[i].sActive = b;
}

ARSize GLInstanceBase::getLight(){
    return mLight;
}

void GLInstanceBase::setLight(ARSize light){
    mLight = light;
}
void GLInstanceBase::uploadLight(){
    if(mLight.multiplicity() == 0){
        printf("GL Instance - upload light error: light not set");
        exit(-1);
    }
    setUniformFloat(mLight.dataF(),"u_Light",3);

}

GLuint GLInstanceBase::CreateGLTexture(GLuint internalFormat,GLuint format,ARSize size,void * data){
    GLuint tex;
    if(size.multiplicity() == 2)
        tex = GLHelper::Texture::create2DTexture(size.dx(),size.dy(),data,internalFormat,format);
    else if(size.multiplicity() == 3)
        tex = GLHelper::Texture::create3DTexture(size.dx(),size.dy(),size.dz(),data,internalFormat,format);
    else{
        printf("CreateImage - Invalid data dimension");
        exit(-1);
    }
    return tex;
}

