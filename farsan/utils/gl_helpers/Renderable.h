#ifndef _RENDERABLE_H
#define _RENDERABLE_H

#include <stdlib.h>
#include <string.h>
#include <vector>
#include <map>
#include <string>
#include "GL_utilities.h"
#include "loadobj.h"
#include "VectorUtils3.h"
#include "GLHelper.h"
#include "LoadTGA.h"
#include<stdio.h>

#define SELECT_CONFIG  if(shader->sDepthTest == NO_DEPTH_TEST){glDisable(GL_DEPTH_TEST);}
#define DESELECT_CONFIG if(shader->sDepthTest == NO_DEPTH_TEST){glEnable(GL_DEPTH_TEST);}

//#define MODEL_OBJECT_VERBOSE
#define CHAR_LEN 40

typedef enum{
	ELEMENTS,
	POINTSTEST,
	PATCHES,
	INSTANCED
}DrawMethod_Type;

typedef enum{
    //MPV = 0, DEFAUULT
    MVP = 1,
    VP,
    P,
    NONE
}Tranform_Composition_Type;

typedef enum{
    DEPTH_TEST,
    NO_DEPTH_TEST
}DepthTest_Type;

typedef struct{
    char sUniformName[40];
    GLuint sTextureId;
    GLuint sShaderId;
    void set(GLuint id){sTextureId = id;}
    GLuint get(){ return sTextureId;}
}Texture_Type;

typedef struct{
    char sUniformName[40];
    GLuint sSize;
    GLfloat sData[4];
    GLuint sShaderId;
}UniformFloat_Type;


typedef struct{
    mat4 sMatrix;
    GLuint sShaderId;
    char sUniformName[40];
}UniformMatrix_Type;

typedef struct{
    double s;
    int ms;
}Timestamp_Type;

typedef struct{

    /** Key value for the shader map **/
    GLuint sShaderId;

    /** Shader program **/
    GLuint sProgramHandle;

    /** Textures **/
    std::map<std::string,Texture_Type> sTextureMap;
    std::map<std::string,Texture_Type> sTexture3DMap;

    /** Uniform Lists*/
    std::map<std::string,UniformFloat_Type> sUniformFloatMap;
    std::map<std::string,UniformMatrix_Type> sUniformMatrixMap;

    /** GL flags **/
    DepthTest_Type sDepthTest;

    /** Model to world transform **/
    mat4 sTransform;
    Tranform_Composition_Type sComposition;

	/** Draw method type **/
	DrawMethod_Type sDrawMethod;

	/** Number of instances (uses if INSTANCE-draw method is selected **/
	GLuint sNumInstances;

    GLHelper::Buffer sBuffers;

}Shader_Type;

typedef std::map<GLuint,Shader_Type*>::iterator ShaderIterator;
typedef std::map<std::string,Texture_Type>::iterator TextureIterator;
typedef std::map<std::string,UniformFloat_Type>::iterator UniformFloatIterator;
typedef std::map<std::string,UniformMatrix_Type>::iterator UniformMatrixIterator;

class Renderable
{
    public:

        Renderable();
        virtual ~Renderable();

        /** Misc functions, used by Cloth-classes **/
        void LoadDataToModel(
			GLfloat *vertices,
			GLfloat *normals,
			GLfloat *texCoords,
			GLfloat *colors,
			GLuint *indices,
			int numVert,
			int numInd,
			GLuint shaderId);
        void BuildModelVAO2(Model *m);
        void uploadNewVertexData(GLfloat* dataBuffer,size_t bufferSize,GLuint shaderId);

        /** Draw Function **/
        void draw(mat4 projectionMatrix, mat4 viewMatrix);
        void drawBuffers(GLuint shaderId,GLuint numBuffers,GLuint * attachment);
        void draw(GLuint shaderId,mat4 projectionMatrix, mat4 viewMatrix);
        void clone(GLuint srcShaderId, GLuint dstShaderId, bool copyUniforms, bool copyTextures);
        /** Set functions **/
        //void setModel(Model * m,GLuint shaderId);
        void setBuffers(GLHelper::Buffer buffers, GLuint shaderId);
        void setBuffers(GLfloat *positions, GLfloat *normals, GLfloat *texCoords, GLuint *indices, int numVert, int numInd, GLuint shaderId);
        void setNumIndices(GLuint shaderId,size_t n);
        void setShader(GLuint handle,GLuint id,Tranform_Composition_Type  composition);
        void setShader(GLuint handle,GLuint id,Tranform_Composition_Type  composition,DepthTest_Type depthTest);
        void setShader(GLuint handle,GLuint id);
        void setTexture(GLuint handle,GLuint shaderId,const char* uniformName);
        void set3DTexture(GLuint shaderId,GLuint texId,const char* uniformName);
        void setUniformFloat(GLfloat* data, GLuint sizeData, GLuint shaderId, const char* uniformName);
        void setUniformMatrix(mat4 data, GLuint shaderId, const char* uniformName);
        void setUniformFloat(const GLfloat data, GLuint shaderId, const char* uniformName);
        void setTransform(mat4 transf,GLuint id);
        void setSpeedlinesInit(GLuint shaderId);
        void setSpeedModelsInit(GLuint shaderId);
        void setNoSpeedlines(GLuint shaderId);
        void registerTimeSpeedlines(GLuint shaderId);
		void setDrawMethod(DrawMethod_Type method, GLuint shaderId);
		void setDrawMethod(DrawMethod_Type method, GLuint shaderId,GLuint numInstances);
		void setNumInstances(GLuint numInstances, GLuint shaderId);
		void setInstances(GLuint vbo,GLuint shaderId);



		/* Get functions */
		DrawMethod_Type getDrawMethod(GLuint shaderId);
        int getSpeedlineMode(GLuint shaderId);


        void replaceTexture(GLuint handle,GLuint shaderId,const char* uniformName);
        void replace3DTexture(GLuint handle,GLuint shaderId,const char* uniformName);

        void replaceUniformFloat(GLfloat* handle,GLuint shaderId,const char* uniformName);
        void replaceUniformMatrix(mat4 matrix,GLuint shaderId,const char* uniformName);
        /** Misc functions **/
         void freeModelData(Model * m);
        mat4 * getTransform(GLuint ShaderId);
        void flipModels(GLuint shaderId);

    protected:
    private:

        /** Private help functions **/
        void   uploadUniformFloat(UniformFloat_Type* uniform);
        void   uploadUniformMatrix(UniformMatrix_Type* uniform);
        GLuint   selectTexture(Shader_Type * shader,GLuint ntext);
        GLuint   selectTexture3D(Shader_Type * shader,GLuint ntext);
        void   uploadTransform(Shader_Type * shader,mat4 projectionMatrix,mat4 viewMatrix);

        /** Private data containers **/
        std::map<GLuint,Shader_Type*> mShaderMap;

        /** Private draw functions **/
		void selectDrawMethod(Shader_Type * shader, mat4 projectionMatrix, mat4 viewMatrix);
        void drawModel(Shader_Type* shader);
		void drawPoints(Shader_Type *  shader,mat4 projectionMatrix, mat4 viewMatrix);
		void drawElements(Shader_Type *  shader,mat4 projectionMatrix, mat4 viewMatrix);
		void drawArrays(Shader_Type *  shader,mat4 projectionMatrix, mat4 viewMatrix);
		void drawInstanced(Shader_Type * shader,mat4 projectionMatrix, mat4 viewMatrix);


};

#endif // MODELOBJECT_H
