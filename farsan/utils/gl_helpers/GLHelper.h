#ifndef _GLHELPER_H
#define _GLHELPER_H
#include "GL_utilities.h"
#include "loadobj.h"

#include <GL/gl.h>
//#include <GL/glut.h>


#include <stdlib.h>
#include <stdio.h>
#include <utility>
#include <vector>
namespace GLHelper{

#define INDICES_PER_QUAD 6
#define VERTICES_PER_TRIANGLE 3






class Buffer {
    public:


        typedef std::pair<GLuint,size_t> InstanceType;

        static const GLuint UNUSED = 0xFFFF;

        GLuint sVAO;
        GLuint sIBO;

        GLuint sVBOPosition;
        size_t sPositionSize;
        GLuint sVBONormal;
        size_t sNormalSize;
        GLuint sVBOTextureCoord;
        size_t sTextureCoordSize;


        std::vector<InstanceType> sInstanceBuffers;

        size_t sNumVertices;
        size_t sNumIndices;


        size_t size(){return sNumVertices;};

        Buffer();

		void createVAO();
        void enableAttributes(GLuint program);
        void setInstanced(std::vector<InstanceType> &ibVec);
        bool isInstanced();
        void copyBuffer(Buffer& dst, bool useNormal = true,bool useTcoords = true);

		void createPositionBuffer(size_t sizeBytes,void * hostPtr,GLuint stride = 3);
		void createNormalBuffer(size_t sizeBytes,void * hostPtr,GLuint stride = 3);
		void createTexCoordBuffer(size_t sizeBytes,void * hostPtr,GLuint stride = 2);

        bool isActive();
		bool hasPositionBuffer();
		bool hasNormalBuffer();
		bool hasTexCoordBuffer();

		void createIndexBuffer(size_t sizeBytes, void * hostPtr);
		static GLuint createVertexBuffer(size_t sizeBytes, void * hostPtr);
		void setNumElements(GLuint numInd,GLuint numVert);

    void release();
    void bind();
    void unbind();
    static Buffer createBuffers(
                GLfloat *positions,
                size_t sizePositions,
                GLfloat *normals,
                size_t sizeNormals,
                GLfloat *texCoords,
                size_t sizeTexCoords,
                GLuint *indices,
                int numVert,
                int numInd);

    static Buffer createBuffers(
                GLfloat *positions,
                GLfloat *normals,
                GLfloat *texCoords,
                GLuint *indices,
                int numVert,
                int numInd);

    static Buffer model2Buffer(Model * m);
    static void freeModelData(Model * m);

    private:




};


typedef struct{

    Buffer sBuffers;
    GLuint sSizeDim[3];
    GLuint sNumDim;

}GLGrid;

class Grid {

   public:
    static GLGrid * create2DGrid(GLuint w, GLuint h,GLfloat* gridSize,bool normals);
    static GLuint create3DParticlesVBO(GLuint w, GLuint h,GLuint d);
   private:
    static GLuint createVAO();
    static GLuint create2DGridVBO(GLuint w,GLuint h,GLfloat* gridSize);
    static void create2DGridIBO(GLuint w,GLuint h,GLGrid * grid);

};

class Texture{

public:
    static GLuint create3DTexture(GLuint sx,GLuint sy,GLuint sz,void * data,GLuint internalFormat,GLuint format);
    static GLuint create2DTexture(GLuint sx,GLuint sy,void * data,GLuint internalFormat,GLuint format);
	static GLuint create2DTextureByte(GLuint sx,GLuint sy,void * data,GLuint internalFormat,GLuint format);
	static void update2DTextureByte(GLuint sx,GLuint sy, void * data,GLuint format, GLuint text);

};

}
#endif // GLHELPER_H
