#include "debug_mesh.h"
#include <iostream>

DebugMesh::DebugMesh(){
	init();
}


void DebugMesh::SetBuffers(int width, int height,GLHelper::Buffer& buf)
{
  int vertexCount = width * height;
  int triangleCount = (width-1) * (height-1) * 2;
  int x, z;
  GLuint *indexArray = (GLuint *)malloc(sizeof(GLuint) * triangleCount*3);

	for (x = 0; x < width-1; x++)
		{
		for (z = 0; z < height-1; z++)
				{


			// Triangle 1
			int vertexPosCurrent = (x + z * (width-1))*6;
			indexArray[vertexPosCurrent + 0] = x + z * width;
			indexArray[vertexPosCurrent + 1] = x + (z+1) * width;
			indexArray[vertexPosCurrent + 2] = x+1 + z * width;

			// Triangle 2
			indexArray[vertexPosCurrent + 3] = x+1 + z * width;
			indexArray[vertexPosCurrent + 4] = x + (z+1) * width;
			indexArray[vertexPosCurrent + 5] = x+1 + (z+1) * width;


		}
	}

    buf.createIndexBuffer(sizeof(GLuint) * triangleCount*3,indexArray);
	buf.setNumElements(triangleCount*3,vertexCount);

    mRenderable->setBuffers(buf,DEBUG_MESH_SHADER);

	free(indexArray);

}

void DebugMesh::setColorTexture(GLuint tex){

	std::cout<<"tex rgb debug mesh = "<<tex<<std::endl;
	mRenderable->setTexture(tex,DEBUG_MESH_SHADER,"u_ColorTexture");
}


void DebugMesh::init()
{

	mRenderable = new Renderable();

  // Load and compile shader
  GLuint program = loadShadersG("shaders/debug_mesh/debug_mesh.vert", "shaders/debug_mesh/debug_mesh.frag","shaders/debug_mesh/debug_mesh.gs");
  glUseProgram(program);
  printError("init shader");

	mRenderable->setShader(program,DEBUG_MESH_SHADER,MVP);
	mRenderable->setTransform(IdentityMatrix(),DEBUG_MESH_SHADER);


}

void DebugMesh::draw(UserCamera * uc){

    glDisable(GL_CULL_FACE);
	 mRenderable->draw(uc->getProjectionMatrix(),uc->getViewMatrix());
	 glEnable(GL_CULL_FACE);
}

