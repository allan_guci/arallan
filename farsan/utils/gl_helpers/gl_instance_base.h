#ifndef _GL_INSTANCE_BASE_H
#define _GL_INSTANCE_BASE_H
#include "Renderable.h"
#include "user_camera.h"
#include "ar_size.h"
#define MESH_SHADER 1

#define SIZE_X 512
#define SIZE_Y 512

#define GL_ERR(str) if(printError(str) == -1) \
                    exit(-1)

   enum RenderTargetType{
        COLOR,
        DEPTH,
        COLOR_DEPTH
    };

    struct RenderTarget{
        GLuint sFBO;
        GLuint sColorTexture;
        GLuint sDepthTexture;
        GLuint sWidth;
        GLuint sHeight;
        bool   sActive;
        RenderTarget() : sFBO(99),sColorTexture(99),sDepthTexture(99),sWidth(0),sHeight(0),sActive(false){};
    };


class GLInstanceBase
{


public:

	GLInstanceBase();
    virtual void initGL() = 0;
    virtual void drawGL(UserCamera* uc) = 0;


    void setGLInputBuffer(GLHelper::Buffer buf){
        mGLInputBuffer = buf;
    }
    void setGLInputTexture(GLuint tex){
        mGLInputTexture = tex;
    }

    void setGLInputTexture(GLuint tex,std::string key){
        mInputTextures[key] = tex;
    }


    GLHelper::Buffer getGLOutputBuffer(){
        return mGLOutputBuffer;
    }
    GLuint getGLOutputTexture(){
        return mGLOutputTexture;
    }

    void setExternalCamera(UserCamera* uc){
        mExternalCamera = uc;
    }

    void setLight(ARSize light);

    GLuint getDepthTexture();
    GLuint getColorTexture();

protected:


    GLuint createShader(const char * vs,const char * fs,const char * gs = NULL,Tranform_Composition_Type type = MVP);
    void   selectShader(GLuint shader);

    void setTexture(GLuint texId,const char* textureName, GLenum target);

    GLHelper::Buffer loadModel(const char * filePath, bool useNormal, bool useTexCoord, bool upload = true);
    void loadQuadModel(GLHelper::Buffer ibuf = GLHelper::Buffer());
    void setBuffer(GLHelper::Buffer buf);
    void setBufferNumIndices(size_t n);
    void setBlend(bool b);

    ARSize getLight();
    void uploadLight();

    void setUniformFloat(GLfloat* f,const char * uniformName,size_t s = 0);
    void setUniformFloat(GLfloat f,const char * uniformName);
    void setUniformMatrix(mat4 mat, const char* uniformName);

    void createRenderTarget(RenderTargetType type,ARSize size, GLuint internalFormat = GL_RGBA32F,GLuint format = GL_RGBA, int nunRenderTargets = 1);
    void useRenderTarget(bool b);
    void enableRenderTarget();
    void disableRenderTarget(ARSize defaultSize);
    void draw(UserCamera* uc);

    GLuint CreateGLTexture(GLuint internalFormat,GLuint format,ARSize size,void * data = NULL);


    void setGLOutputBuffer(GLHelper::Buffer buf){
        mGLOutputBuffer = buf;
    }
    void setGLOutputTexture(GLuint tex){
        mGLOutputTexture = tex;
    }
    GLHelper::Buffer getGLInputBuffer(){
        return mGLInputBuffer;
    }
    GLuint getGLInputTexture(){
        return mGLInputTexture;
    }

    GLuint getGLInputTexture(std::string key){
        return mInputTextures[key];
    }
    RenderTarget* getRenderTargets(){
        return mRenderTargets;
    }

    RenderTarget getRenderTarget(int index = -1){
        if(index == -1)
            return mRenderTargets[mCurrentRenderTarget];
        else
            return mRenderTargets[index];
    }

    void MMRotate(GLfloat x,GLfloat y,GLfloat z);
    void MMScale(GLfloat x,GLfloat y,GLfloat z);
    void MMTranslate(GLfloat x,GLfloat y,GLfloat z);

    void MMRotate(GLfloat * r){
        MMRotate(r[0],r[1],r[2]);
    }
    void MMScale(GLfloat * s){
        MMScale(s[0],s[1],s[2]);
    }
    void MMTranslate(GLfloat * t){
        MMTranslate(t[0],t[1],t[2]);
    }

    mat4 MMGet(){
        return mModelMatrix;
    }

    UserCamera * getExternalCamera(){
        return mExternalCamera;
    }

    void MMUpload();

    void MMReset();

	private:

	    void MMSet();

        GLuint mSelectedShader;
        GLuint mShaderCount;
        bool mBlendEnabled;

        Renderable * mGLScene;

        GLHelper::Buffer mGLOutputBuffer;
        GLHelper::Buffer mGLInputBuffer;
        std::map<std::string,GLuint > mInputTextures;
        GLuint mGLOutputTexture;
        GLuint mGLInputTexture;

        int mNumRenderTargets;
        int mCurrentRenderTarget;
        RenderTarget mRenderTargets[10];

        mat4 mModelMatrix;

        ARSize mLight;

        UserCamera * mExternalCamera;

};

#endif
