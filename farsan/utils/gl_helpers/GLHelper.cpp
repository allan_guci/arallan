#include "GLHelper.h"
#include <iostream>

namespace GLHelper{

/*************************************************************/
/********************* Buffer Helper *************************/
/*************************************************************/

Buffer::Buffer(){
        sVAO = UNUSED;
        sIBO = UNUSED;
        sVBOPosition = UNUSED;
        sPositionSize = 3;
        sVBONormal   = UNUSED;
        sNormalSize = 3;
        sVBOTextureCoord = UNUSED;
        sTextureCoordSize = 2;

}


void Buffer::createVAO(){
	glGenVertexArrays(1, &sVAO);
	printError("createVAO");
}

GLuint Buffer::createVertexBuffer(size_t sizeBytes,void * hostPtr){
		GLuint buf;
		glGenBuffers(1, &buf);
		glBindBuffer(GL_ARRAY_BUFFER,buf);
		glBufferData(GL_ARRAY_BUFFER,sizeBytes, hostPtr, GL_STATIC_DRAW);
		return buf;
		printError("create VBO");
}

void Buffer::createPositionBuffer(size_t sizeBytes,void * hostPtr, GLuint stride){
		glBindVertexArray(sVAO);
		sVBOPosition = createVertexBuffer(sizeBytes,hostPtr);
		sPositionSize = stride;
		glBindVertexArray(0);
		printError("create VBO POS");
}

void Buffer::createNormalBuffer(size_t sizeBytes,void * hostPtr,GLuint stride){
		glBindVertexArray(sVAO);
		sVBONormal = createVertexBuffer(sizeBytes,hostPtr);
		sNormalSize = stride;
		glBindVertexArray(0);
}

void Buffer::createTexCoordBuffer(size_t sizeBytes,void * hostPtr,GLuint stride){
		glBindVertexArray(sVAO);
		sVBOTextureCoord = createVertexBuffer(sizeBytes,hostPtr);
		sTextureCoordSize = stride;
		glBindVertexArray(0);
}

void Buffer::createIndexBuffer(size_t sizeBytes,void * hostPtr){
		glBindVertexArray(sVAO);
		glGenBuffers(1, &sIBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,sIBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeBytes, hostPtr, GL_STATIC_DRAW);
		glBindVertexArray(0);
		printError("create IBO POS");
}


void Buffer::setNumElements(GLuint numInd,GLuint numVert){
		sNumIndices = numInd;
        sNumVertices = numVert;
}


void Buffer::copyBuffer(Buffer& dst, bool useNormal,bool useTcoords){

    dst = *this;
    dst.createVAO();
    dst.bind();

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,dst.sIBO);

    if(this->sVBOPosition == UNUSED || this->sIBO == UNUSED){
        printf("GLBuffer - copy buffer invalid source");
        exit(-1);
    }
    if(!useNormal){
        dst.sVBONormal = UNUSED;
    }
    if(!useTcoords){
        dst.sVBOTextureCoord = UNUSED;
    }


}


void Buffer::enableAttributes(GLuint program){
    GLint loc;
    bool hasInstancedBuffer = sInstanceBuffers.size() > 0;
    /** Select VAO **/
    glBindVertexArray(sVAO);
    /** Select VBO Position **/
    glBindBuffer(GL_ARRAY_BUFFER, sVBOPosition);
    loc = glGetAttribLocation(program, "in_Position");
    if (loc >= 0){
        //printError("Buffer Enable 3");
        glEnableVertexAttribArray(loc);

        glVertexAttribPointer(loc, sPositionSize, GL_FLOAT, GL_FALSE, 0, 0);
  //printError("Buffer Enable 2");

        if(hasInstancedBuffer){
            glVertexAttribDivisor(loc, 0);
        }

    } else
        fprintf(stderr, "'%s' not declared in shader!\n", "in_Position");
   //printError("Buffer Enable 1");
    /** Select VBO Normal **/
    if (sVBONormal!=UNUSED){
        loc = glGetAttribLocation(program, "in_Normal");
        if (loc >= 0){
            glBindBuffer(GL_ARRAY_BUFFER, sVBONormal);
            glEnableVertexAttribArray(loc);
            glVertexAttribPointer(loc, sNormalSize, GL_FLOAT, GL_FALSE, 0, 0);

            if(hasInstancedBuffer){
                glVertexAttribDivisor(loc, 0);
            }
        }else
            fprintf(stderr, "DrawModel warning: '%s' not found in shader!\n", "in_Normal");
    }
    /** Select VBO TexCoord **/
    if (sVBOTextureCoord != UNUSED){
        loc = glGetAttribLocation(program, "in_TextureCoord");
        if (loc >= 0){
            glBindBuffer(GL_ARRAY_BUFFER, sVBOTextureCoord);
            glEnableVertexAttribArray(loc);
            glVertexAttribPointer(loc, sTextureCoordSize, GL_FLOAT, GL_FALSE, 0, 0);

            if(hasInstancedBuffer){
                glVertexAttribDivisor(loc, 0);
            }
        }
        else
            fprintf(stderr, "DrawModel warning: '%s' not found in shader!\n", "in_TextureCoord");
    }


    /**Select VBO for alt buffer **/

    for(int i = 0; i < sInstanceBuffers.size(); i++){
        std::string label = "in_Instanced";
        label = label + char(i+48);
        loc = glGetAttribLocation(program,label.c_str());
        InstanceType it = sInstanceBuffers[i];
        GLuint vbo = it.first;
        GLuint vboSize = it.second;
        if(loc  >= 0 ){
            //printError("Buffer Enable 0");
            glBindBuffer(GL_ARRAY_BUFFER, vbo);
            glEnableVertexAttribArray(loc);
            glVertexAttribPointer(loc, vboSize, GL_FLOAT, GL_FALSE, 0, 0);
            glVertexAttribDivisor(loc, 1);
        }
    }

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    //printError("Buffer Enable Attrib");



}

void Buffer::setInstanced(std::vector<InstanceType> &ibVec){
    for(int i = 0; i < ibVec.size(); i ++){
        sInstanceBuffers.push_back(ibVec[i]);
    }
}

bool Buffer::isInstanced(){
    return sInstanceBuffers.size() > 0;
}

void Buffer::release(){
        if(sVAO != UNUSED){
            glDeleteVertexArrays(1, &sVAO);
            sVAO = UNUSED;
        }
       //   printError("Clean up VAO");
        if(sIBO != UNUSED){
            glDeleteBuffers(1, &sIBO);
            sIBO = UNUSED;
        }
        //printError("Clean up IBO");
        if(sVBOPosition != UNUSED){
            glDeleteBuffers(1, &sVBOPosition);
            sVBOPosition = UNUSED;
        }
        //printError("Clean up vertex position buffer");
         if(sVBONormal != UNUSED){
            glDeleteBuffers(1, &sVBONormal);
            sVBONormal = UNUSED;
        }
        //printError("Clean up vertex normal buffer");
         if(sVBOTextureCoord != UNUSED){
            glDeleteBuffers(1, &sVBOTextureCoord);
            sVBOTextureCoord = UNUSED;
        }
       // printError("Clean up vertex texture buffer");
}

void Buffer::bind(){
      glBindVertexArray(sVAO);
}

void Buffer::unbind(){
      glBindVertexArray(0);
}

bool Buffer::isActive(){
    return sVAO != UNUSED;
}

bool Buffer::hasPositionBuffer(){
    return sVBOPosition != UNUSED;
}
bool Buffer::hasNormalBuffer(){
    return sVBONormal != UNUSED;
}
bool Buffer::hasTexCoordBuffer(){
    return sVBOTextureCoord!= UNUSED;
}



Buffer Buffer::createBuffers(
            GLfloat *positions,
            size_t sizePositions,
			GLfloat *normals,
			size_t sizeNormals,
			GLfloat *texCoords,
			size_t sizeTexCoords,
			GLuint *indices,
			int numVert,
			int numInd){

    Buffer buffers;
    /** Generate buffers **/
	glGenVertexArrays(1, &buffers.sVAO);
	glGenBuffers(1, &buffers.sVBOPosition);
	glGenBuffers(1, &buffers.sIBO);

	if(normals != NULL)
        glGenBuffers(1, &buffers.sVBONormal);

	if (texCoords != NULL)
		glGenBuffers(1, &buffers.sVBOTextureCoord);

    /** Bind VAO **/
	glBindVertexArray(buffers.sVAO);

    /** Buffer position data **/
	glBindBuffer(GL_ARRAY_BUFFER, buffers.sVBOPosition);
	glBufferData(GL_ARRAY_BUFFER, numVert*sizePositions*sizeof(GLfloat), positions, GL_STATIC_DRAW);
    buffers.sPositionSize = sizePositions;

    /** Buffer normal data **/
    if(normals!= NULL ){
        glBindBuffer(GL_ARRAY_BUFFER, buffers.sVBONormal);
        glBufferData(GL_ARRAY_BUFFER,numVert*sizeNormals*sizeof(GLfloat), normals, GL_STATIC_DRAW);
        buffers.sNormalSize = sizeNormals;
    }

    /** Buffer texture coordinate data **/
	if (texCoords != NULL){
		glBindBuffer(GL_ARRAY_BUFFER, buffers.sVBOTextureCoord);
		glBufferData(GL_ARRAY_BUFFER, numVert*sizeTexCoords*sizeof(GLfloat), texCoords, GL_STATIC_DRAW);
	}

    /** Buffer index data **/
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers.sIBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, numInd*sizeof(GLuint), indices, GL_STATIC_DRAW);

	/** Store #vertices and #indices **/
    buffers.sNumIndices = numInd;
    buffers.sNumVertices = numVert;

    return buffers;
}

Buffer Buffer::createBuffers(
            GLfloat *positions,
			GLfloat *normals,
			GLfloat *texCoords,
			GLuint *indices,
			int numVert,
			int numInd){

    return createBuffers(positions,3,normals,3,texCoords,2,indices,numVert,numInd);
}


Buffer Buffer::model2Buffer(Model * m){
        Buffer buffers;

        buffers.sVAO = m->vao;

        if(m->indexArray != NULL)
            buffers.sIBO = m->ib;

        if(m->normalArray != NULL)
            buffers.sVBONormal = m->nb;
        else
            buffers.sVBONormal = UNUSED;

        if(m->vertexArray != NULL)
            buffers.sVBOPosition= m->vb;
        else
            buffers.sVBOPosition= UNUSED;

        if(m->texCoordArray != NULL)
            buffers.sVBOTextureCoord = m->tb;
        else
            buffers.sVBOTextureCoord = UNUSED;

        buffers.sNumIndices = m->numIndices;
        buffers.sNumVertices = m->numVertices;

        freeModelData(m);

        return buffers;
}

/** \brief Free Model-instance (TSBK03-stuff)
 *
 * \param m Model*
 * \return void
 *
 */
void Buffer::freeModelData(Model * m){
        if(m->vertexArray!= NULL){
            free(m->vertexArray);

        }
        if(m->indexArray != NULL){
            free(m->indexArray);

        }
        if(m->texCoordArray != NULL){
            free(m->texCoordArray);

        }
        if(m->normalArray != NULL){
            free(m->normalArray);

        }
}



/*************************************************************/
/********************* Grid Helper ***************************/
/*************************************************************/

GLuint Grid::createVAO(){
    GLuint vao;
    glGenVertexArrays(1, &vao);
    return vao;
}

GLuint Grid::create2DGridVBO(GLuint w,GLuint h,GLfloat* gridSize){

    GLfloat resX = gridSize[2];
    GLfloat resY = gridSize[3];

    GLuint vertexCount = w*h;
    size_t arraySize = 4*vertexCount * sizeof(GLfloat);
    GLfloat* vertexArray = (float*)malloc(arraySize);

    GLuint stride = 4;
    float xf = 0.0f;
    float zf = 0.0f;
    for(GLuint z = 0; z < h; ++z){

        for(GLuint x = 0; x < w; ++x){

            GLfloat xPos = (GLfloat)xf*resX*gridSize[0];
            GLfloat yPos = 0;
            GLfloat zPos = (GLfloat)zf*resY*gridSize[1];

            GLuint startIndex = (x + z * h)*stride;

            vertexArray[startIndex + 0] = xPos;
            vertexArray[startIndex + 1] = yPos;
            vertexArray[startIndex + 2] = zPos;
            vertexArray[startIndex + 3] = 1.0;
        xf += 1.0;
        }
        zf += 1.0;
        xf = 0;
    }

    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, arraySize, vertexArray, GL_DYNAMIC_DRAW);
    free(vertexArray);

#ifdef NEVER
    int bufferSize = 0;
    glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &bufferSize);
    if(sizeof(vertexArray) != bufferSize)
    {

        printf("[createVBO()] Data size is mismatch with input array\n");
    }
#endif
    return vbo;

}

void Grid::create2DGridIBO(GLuint w,GLuint h, GLGrid * grid){

    GLuint triangleCount = (w-1) * (h-1)* 2;
    GLuint indexCount    = triangleCount*VERTICES_PER_TRIANGLE;
    size_t sizeIndices = indexCount*sizeof(GLuint);
    GLuint* indexArray = (GLuint *) malloc(sizeIndices);

    for (GLuint y = 0; y < h-1; y++)
        for (GLuint x = 0; x < w-1; x++)
        {

            indexArray[(x + y * (w-1))*INDICES_PER_QUAD + 0] = x + y * w;
            indexArray[(x + y * (w-1))*INDICES_PER_QUAD + 1] = x + (y+1) * w;
            indexArray[(x + y * (w-1))*INDICES_PER_QUAD + 2] = x+1 + y * w;

            indexArray[(x + y *  (w-1))*INDICES_PER_QUAD + 3] = x+1 + y * w;
            indexArray[(x + y *  (w-1))*INDICES_PER_QUAD + 4] = x + (y+1) * w;
            indexArray[(x + y *  (w-1))*INDICES_PER_QUAD + 5] = x+1 + (y+1) * w;

        }



    GLuint ibo;
    glGenBuffers(1, &ibo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sizeIndices), indexArray, GL_STATIC_DRAW);

    grid->sBuffers.sNumIndices = indexCount;
    grid->sBuffers.sIBO= ibo;

    free(indexArray);

}


GLGrid * Grid::create2DGrid(GLuint w, GLuint h,GLfloat* gridSize,bool normals){
    GLGrid * grid = new GLGrid();

    /** Create VAO **/
    grid->sBuffers.sVAO = createVAO();
    glBindVertexArray(grid->sBuffers.sVAO);

    /** VBO - Positions **/
    grid->sBuffers.sVBOPosition = create2DGridVBO(w,h,gridSize);

    /** VBO - Positions **/
    if(normals)
        grid->sBuffers.sVBONormal = create2DGridVBO(w,h,gridSize);
    else
        grid->sBuffers.sVBONormal = 0xFFFF;

    /** IBO **/
    create2DGridIBO(w,h,grid);



    grid->sSizeDim[0] = w;
    grid->sSizeDim[1] = h;
    grid->sNumDim = 2;
    grid->sBuffers.sNumVertices = w*h;


    grid->sBuffers.sPositionSize = 4;
    grid->sBuffers.sNormalSize   = 4;


    return grid;

}

GLuint Grid::create3DParticlesVBO(GLuint w, GLuint h,GLuint d){
    GLfloat resX = 1.0f/((GLfloat)w-1.0f);
    GLfloat resY = 1.0f/((GLfloat)h-1.0f);
    GLfloat resZ = 1.0f/((GLfloat)d-1.0f);

    GLuint vertexCount = w*h*d;
    GLfloat* vertexArray = new GLfloat[4 * vertexCount];

    GLuint stride = 4;

    for(GLuint z = 0; z < h; ++z)
      for(GLuint y = 0; y < d; ++y)
        for(GLuint x = 0; x < w; ++x){

            GLfloat xPos = (GLfloat)x*resX;
            GLfloat yPos = (GLfloat)y*resY;
            GLfloat zPos = (GLfloat)z*resZ;

            GLuint startIndex = (x + h*(y + z * d))*stride;

            vertexArray[startIndex + 0] = xPos;
            vertexArray[startIndex + 1] = yPos;
            vertexArray[startIndex + 2] = zPos;
            vertexArray[startIndex + 3] = 1.0;
    }

    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, vertexCount*4*sizeof(GLfloat), vertexArray, GL_DYNAMIC_DRAW);
#ifdef NEVER
    int bufferSize = 0;
    glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &bufferSize);
    if(vertexCount*4*sizeof(GLfloat) != bufferSize)
    {
        printf("[createVBO()] Data size is mismatch with input array\n");
    }
#endif
    delete vertexArray;
    return vbo;
}
/**
* Creates 32F 3D-Texture
**/
#define LINEAR_INTERP
//#define _REPEAT
GLuint GLHelper::Texture::create3DTexture(GLuint sx,GLuint sy,GLuint sz, void * data,GLuint internalFormat,GLuint format){
    GLuint text;
    glGenTextures(1,&text);
    glBindTexture(GL_TEXTURE_3D,text);
    #ifdef LINEAR_INTERP
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    #else
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    #endif //LINEAR_INTERP
    //printError("3D texture 2");
    #ifdef _REPEAT
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_REPEAT);
    #else
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP);

    #endif
    printError("Error setting TextParameteriri");
   // glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA32F, sx,sy,sz, 0, GL_RGBA, GL_FLOAT, data);
     GLuint l = GL_FLOAT;
     if(internalFormat == GL_R8UI){
        l = GL_UNSIGNED_BYTE;
    }

    glTexImage3D(GL_TEXTURE_3D, 0, internalFormat, sx,sy,sz, 0, format, l, data);
    printError("Error creating GL Texture");
//    glGenerateMipmap(GL_TEXTURE_3D);
    return text;
}

GLuint GLHelper::Texture::create2DTexture(GLuint sx,GLuint sy,void * data,GLuint internalFormat,GLuint format){
    GLuint text;
    glGenTextures(1,&text);
    glBindTexture(GL_TEXTURE_2D,text);
    #ifdef LINEAR_INTERP
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    #else
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    #endif //LINEAR_INTERP
    printError("3D texture 2");
    #ifdef _REPEAT
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    #else
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    #endif
    printError("3D texture 2.5");
   // glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA32F, sx,sy,sz, 0, GL_RGBA, GL_FLOAT, data);
    GLuint l = GL_FLOAT;
    if(internalFormat == GL_R8UI){
        l = GL_UNSIGNED_BYTE;
    }

    glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, sx,sy, 0, format, l, data);
    printError("3D texture 3");
//    glGenerateMipmap(GL_TEXTURE_3D);
    return text;

}


GLuint GLHelper::Texture::create2DTextureByte(GLuint sx,GLuint sy,void * data,GLuint internalFormat,GLuint format){
    GLuint text;
    glGenTextures(1,&text);
    glBindTexture(GL_TEXTURE_2D,text);
    #ifdef LINEAR_INTERP
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    #else
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    #endif //LINEAR_INTERP
    printError("2D texture 2");
    #ifdef _REPEAT
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    #else
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    #endif
    printError("2D texture 2.5");
    //glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA32F, sx,sy,sz, 0, GL_RGBA, GL_FLOAT, data);
    glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, sx,sy, 0, format, GL_UNSIGNED_BYTE, data);
    printError("2D texture 3");
    //glGenerateMipmap(GL_TEXTURE_3D);
    return text;
}

void GLHelper::Texture::update2DTextureByte(GLuint sx,GLuint sy, void * data, GLuint format, GLuint text){
    //glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D,text);
    #ifdef LINEAR_INTERP
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    #else
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    #endif //LINEAR_INTERP
    printError("2D texture 2");
    #ifdef _REPEAT
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    #else
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    #endif
    printError("2D texture 2.5");
   // glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA32F, sx,sy,sz, 0, GL_RGBA, GL_FLOAT, data);
    //glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, sx,sy, 0, format, GL_UNSIGNED_BYTE, data);
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, sx,sy, format, GL_UNSIGNED_BYTE, data);
    printError("2D texture 3");
//    glGenerateMipmap(GL_TEXTURE_3D);

}
}

