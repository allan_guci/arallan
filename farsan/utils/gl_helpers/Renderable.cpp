#include "Renderable.h"

Renderable::Renderable()
{


}

Renderable::~Renderable()
{

    glFinish();
    /** Clean up models **/
    Shader_Type* shader;
    for(ShaderIterator it = mShaderMap.begin(); it != mShaderMap.end(); ++it) {
        shader = it->second;

        /***** Delete Models ********/
        shader->sBuffers.release();
       // printError("Clean up buffers");
        /***** Delete Textures ********/

        Texture_Type  texture;
        for(TextureIterator itTexture = shader->sTextureMap.begin(); itTexture != shader->sTextureMap.end(); ++itTexture) {
            texture = itTexture->second;
            glDeleteTextures(1,&texture.sTextureId);
        }
        shader->sTextureMap.clear();
        //printError("Clean up textures");

        /** Delete Uniform**/
        UniformFloat_Type uniformFloat;
        for(UniformFloatIterator itUniform = shader->sUniformFloatMap.begin(); itUniform != shader->sUniformFloatMap.end(); ++itUniform) {
            uniformFloat = itUniform->second;
        }
        shader->sUniformFloatMap.clear();
        //printError("Uniforms vectors");

        /** Delete Matrix Uniform**/
        UniformMatrix_Type matrixUniform;
        for(UniformMatrixIterator itUniform = shader->sUniformMatrixMap.begin(); itUniform != shader->sUniformMatrixMap.end(); ++itUniform) {
            matrixUniform = itUniform->second;
            //delete matrixUniform;
        }
        shader->sUniformFloatMap.clear();
       // printError("Uniforms matrices");

    }

    #ifdef MODEL_OBJECT_VERBOSE
    //printError("Model Object Clean up");
    #endif
}
void Renderable::draw(mat4 projectionMatrix, mat4 viewMatrix){

    Shader_Type * shader;
    for(ShaderIterator it = mShaderMap.begin(); it != mShaderMap.end(); ++it) {
		shader = it->second;
   		selectDrawMethod(shader,projectionMatrix,viewMatrix);
    }
}

void Renderable::draw(GLuint shaderId,mat4 projectionMatrix, mat4 viewMatrix){
	selectDrawMethod(mShaderMap[shaderId],projectionMatrix,viewMatrix);

}



void Renderable::selectDrawMethod(Shader_Type * shader, mat4 projectionMatrix, mat4 viewMatrix){

    if(shader == NULL)
        printf("ERROR NULL SHADER\n");

	switch(shader->sDrawMethod){

		case POINTSTEST:
			drawPoints(shader,projectionMatrix,viewMatrix);
			break;
        case PATCHES:
		case ELEMENTS:
			drawElements(shader,projectionMatrix,viewMatrix);
			break;
		case INSTANCED:
			drawInstanced(shader,projectionMatrix,viewMatrix);
			break;
		default:
			printf("ERROR invalid draw Method\n");
		break;
	}
}



void Renderable::flipModels(GLuint shaderId){
    Shader_Type * shader = mShaderMap[shaderId];
    shader->sTransform = S(1,-1,1)*shader->sTransform;

    if(shader->sUniformFloatMap.find("u_Clip") != shader->sUniformFloatMap.end()){
        shader->sUniformFloatMap["u_Clip"].sData[0] *= -1;
        uploadUniformFloat(&shader->sUniformFloatMap["u_Clip"]);
    }

}


void Renderable::uploadTransform(Shader_Type * shader,mat4 projectionMatrix,mat4 viewMatrix){
    mat4 mvMatrix;
    mat4 mvpMatrix;
    mat4 vpMatrix;
    mat3 normalMatrix;
    GLuint program = shader->sProgramHandle;
    switch(shader->sComposition){
    case MVP:
        mvMatrix = Mult(viewMatrix,shader->sTransform);
        mvpMatrix = Mult(projectionMatrix,mvMatrix);
        glUniformMatrix4fv(glGetUniformLocation(program, "MV_Matrix"), 1, GL_TRUE, mvMatrix.m);
        if(glGetUniformLocation(program, "MVP_Matrix") < 0){
            printf("Unused or invalid uniform: %s\n", "MVP_Matrix");
        }
        glUniformMatrix4fv(glGetUniformLocation(program, "MVP_Matrix"), 1, GL_TRUE, mvpMatrix.m);
        normalMatrix = InverseTranspose(mvMatrix);
        glUniformMatrix3fv(glGetUniformLocation(program, "Normal_Matrix"), 1, GL_TRUE, normalMatrix.m);
        glUniformMatrix4fv(glGetUniformLocation(program, "P_Matrix"), 1, GL_TRUE, projectionMatrix.m);
        break;
    case VP:
        vpMatrix = Mult(projectionMatrix,viewMatrix);
        glUniformMatrix4fv(glGetUniformLocation(program, "V_Matrix"), 1, GL_TRUE, viewMatrix.m);
        glUniformMatrix4fv(glGetUniformLocation(program, "VP_Matrix"), 1, GL_TRUE, vpMatrix.m);
        glUniformMatrix4fv(glGetUniformLocation(program, "P_Matrix"), 1, GL_TRUE, projectionMatrix.m);
        normalMatrix = InverseTranspose(viewMatrix);
        glUniformMatrix3fv(glGetUniformLocation(program, "Normal_Matrix"), 1, GL_TRUE, normalMatrix.m);
        break;
    case P:
        glUniformMatrix4fv(glGetUniformLocation(program, "P_Matrix"), 1, GL_TRUE, projectionMatrix.m);
        break;
    case NONE:
        break;
    default:
        mvMatrix = Mult(viewMatrix,shader->sTransform);
        mvpMatrix = Mult(projectionMatrix,mvMatrix);

        glUniformMatrix4fv(glGetUniformLocation(program, "MV_Matrix"), 1, GL_TRUE, mvMatrix.m);
        glUniformMatrix4fv(glGetUniformLocation(program, "MVP_Matrix"), 1, GL_TRUE, mvpMatrix.m);
        break;
    }
    #ifdef MODEL_OBJECT_VERBOSE
    printError("Shader Transform Matrices");
    #endif

}

GLuint Renderable::selectTexture(Shader_Type * shader,GLuint ntext){
    Texture_Type texture;
    GLuint numActiveTextures = ntext;
    GLuint program = shader->sProgramHandle;
    for(TextureIterator it = shader->sTextureMap.begin(); it != shader->sTextureMap.end(); ++it) {
        texture = it->second;
        glActiveTexture(GL_TEXTURE0+numActiveTextures);
        glBindTexture(GL_TEXTURE_2D, texture.sTextureId);
        glUniform1i(glGetUniformLocation(program, texture.sUniformName), numActiveTextures);
        numActiveTextures++;
        #ifdef MODEL_OBJECT_VERBOSE
        printError(texture.sUniformName);
        #endif
    }
    return numActiveTextures;
}

GLuint Renderable::selectTexture3D(Shader_Type * shader,GLuint ntext){
    Texture_Type texture;
    GLuint numActiveTextures = ntext;
    GLuint program = shader->sProgramHandle;
    for(TextureIterator it = shader->sTexture3DMap.begin(); it != shader->sTexture3DMap.end(); ++it) {
        texture = it->second;
        glActiveTexture(GL_TEXTURE0+numActiveTextures);
        glBindTexture(GL_TEXTURE_3D, texture.get());
        glUniform1i(glGetUniformLocation(program, texture.sUniformName), numActiveTextures);
        numActiveTextures++;
        #ifdef MODEL_OBJECT_VERBOSE
        printError(texture.sUniformName);
        #endif
    }
    return numActiveTextures;
}
void Renderable::uploadUniformFloat(UniformFloat_Type* uniform){
    Shader_Type * shader = mShaderMap[uniform->sShaderId];
    GLuint program = shader->sProgramHandle;
    glUseProgram(program);
    int loc = glGetUniformLocation(program, uniform->sUniformName);
    #ifdef MODEL_OBJECT_VERBOSE
    if(loc < 0 ){
        printf("Unused or invalid uniform: %s\n", uniform->sUniformName);
    }
    printError(uniform->sUniformName);
    #endif
    switch(uniform->sSize){
        case 1:
            glUniform1f(loc, uniform->sData[0]);
        break;
        case 2:
            glUniform2f(loc, uniform->sData[0],uniform->sData[1]);
        break;
        case 3:
            glUniform3f(loc, uniform->sData[0],uniform->sData[1],uniform->sData[2]);
        break;
        case 4:
            glUniform4f(loc, uniform->sData[0],uniform->sData[1],uniform->sData[2],uniform->sData[3]);
        break;
        default:
        break;
    }
    glUseProgram(0);
    #ifdef MODEL_OBJECT_VERBOSE
    printError("Upload Uniform");
    #endif
}

void Renderable::uploadUniformMatrix(UniformMatrix_Type* uniformMatrix){
    Shader_Type * shader = mShaderMap[uniformMatrix->sShaderId];
    GLuint program = shader->sProgramHandle;
    glUseProgram(program);
    //int loc = glGetUniformLocation(program, uniformMatrix->sUniformName);
    #ifdef MODEL_OBJECT_VERBOSE


    printError(uniformMatrix->sUniformName);
    #endif
    glUniformMatrix4fv(glGetUniformLocation(program, uniformMatrix->sUniformName), 1, GL_TRUE,  uniformMatrix->sMatrix.m);
    //glUniformMatrix4fv(loc, 1, GL_TRUE, uniformMatrix->sMatrix.m);
    glUseProgram(0);
    #ifdef MODEL_OBJECT_VERBOSE
    printError("Upload Uniform");
    #endif
}

void Renderable::setUniformFloat(GLfloat * data, GLuint sizeData, GLuint shaderId, const char * uniformName){
    UniformFloat_Type uniform;
    for(GLuint i=0;i < sizeData; i++){
        uniform.sData[i] = data[i];
    }
    uniform.sSize = sizeData;
    uniform.sShaderId = shaderId;
    memset(uniform.sUniformName,0,CHAR_LEN*sizeof(char));
    strcpy(uniform.sUniformName,uniformName);
    mShaderMap[shaderId]->sUniformFloatMap[uniformName] = uniform;
    uploadUniformFloat(&uniform);

}

void Renderable::setUniformMatrix(mat4 matrix, GLuint shaderId, const char * uniformName){
    UniformMatrix_Type uniform;
    uniform.sMatrix = matrix;
    uniform.sShaderId = shaderId;
    memset(uniform.sUniformName,0,CHAR_LEN*sizeof(char));
    strcpy(uniform.sUniformName,uniformName);
    mShaderMap[shaderId]->sUniformMatrixMap[uniformName] = uniform;
    uploadUniformMatrix(&uniform);

}


void Renderable::setUniformFloat(const GLfloat data,GLuint shaderId, const char * uniformName){
    UniformFloat_Type uniform;
    uniform.sData[0] = data;
    uniform.sSize = 1;
    uniform.sShaderId = shaderId;
    memset(uniform.sUniformName,0,CHAR_LEN*sizeof(char));
    strcpy(uniform.sUniformName,uniformName);
    mShaderMap[shaderId]->sUniformFloatMap[uniformName] = uniform;
    uploadUniformFloat(&uniform);
}

void Renderable::setShader(GLuint handleGPU, GLuint shaderId,Tranform_Composition_Type  composition){
    Shader_Type * shader = new Shader_Type();
    shader->sProgramHandle = handleGPU;
    shader->sShaderId        = shaderId;
    shader->sComposition     = composition;
    shader->sDepthTest       = DEPTH_TEST;
	shader->sDrawMethod 	 = ELEMENTS;
    mShaderMap[shaderId] 	 = shader;
}

void Renderable::setShader(GLuint handleGPU, GLuint shaderId,Tranform_Composition_Type  composition,DepthTest_Type depthTest){
    Shader_Type * shader     = new Shader_Type();
    shader->sProgramHandle   = handleGPU;
    shader->sShaderId        = shaderId;
    shader->sComposition     = composition;
    shader->sDepthTest       = depthTest;
	shader->sDrawMethod 	 = ELEMENTS;
    mShaderMap[shaderId] 	 = shader;
}

void Renderable::setShader(GLuint handleGPU, GLuint shaderId){
    Shader_Type * shader 	 = new Shader_Type();
    shader->sProgramHandle   = handleGPU;
    shader->sShaderId        = shaderId;
    shader->sComposition     = MVP;
    shader->sDepthTest       = DEPTH_TEST;
	shader->sDrawMethod 	 = ELEMENTS;
    mShaderMap[shaderId] 	 = shader;
}

void Renderable::setNumInstances(GLuint numInstances, GLuint shaderId){
	mShaderMap[shaderId]->sNumInstances = numInstances;
}

void Renderable::setTexture(GLuint shaderId,GLuint handle,const char* uniformName){

    Texture_Type newTexture;

    newTexture.sShaderId     = shaderId;
    newTexture.sTextureId    = handle;

    memset(newTexture.sUniformName,0,CHAR_LEN*sizeof(char));
    strcpy(newTexture.sUniformName,uniformName);

    mShaderMap[shaderId]->sTextureMap[uniformName] = newTexture;
}

void Renderable::set3DTexture(GLuint shaderId,GLuint texId,const char* uniformName){

    Texture_Type newTexture;

    newTexture.sShaderId     = shaderId;
    newTexture.sTextureId    = texId;

    memset(newTexture.sUniformName,0,CHAR_LEN*sizeof(char));
    strcpy(newTexture.sUniformName,uniformName);

    mShaderMap[shaderId]->sTexture3DMap[uniformName] = newTexture;
}

void Renderable::setBuffers(GLHelper::Buffer buffers, GLuint shaderId){
    /** Enable vertex attributes **/
    buffers.enableAttributes(mShaderMap[shaderId]->sProgramHandle);

    /** Store buffer **/
    mShaderMap[shaderId]->sBuffers = buffers;
}
void Renderable::setNumIndices(GLuint shaderId,size_t n){
    mShaderMap[shaderId]->sBuffers.sNumIndices = n;
}
void Renderable::setBuffers(
                GLfloat *positions,
                GLfloat *normals,
                GLfloat *texCoords,
                GLuint *indices,
                int numVert,
                int numInd,
                GLuint shaderId){

    /** Create buffer object **/
    GLHelper::Buffer buffers = GLHelper::Buffer::createBuffers(positions,
                                                                  normals,
                                                                  texCoords,
                                                                  indices,
                                                                  numVert,
                                                                  numInd);
    setBuffers(buffers,shaderId);

}


void Renderable::setTransform(mat4 transf,GLuint shaderId){
     Shader_Type * shader = mShaderMap[shaderId];
  //   shader->sComposition = MVP;
    mShaderMap[shaderId]->sTransform = transf;

}

void Renderable::setDrawMethod(DrawMethod_Type method, GLuint shaderId, GLuint numInstances ){
    mShaderMap[shaderId]->sDrawMethod   = method;
    mShaderMap[shaderId]->sNumInstances = numInstances;
}

void Renderable::setDrawMethod(DrawMethod_Type method,GLuint shaderId){
	mShaderMap[shaderId]->sDrawMethod = method;
}

DrawMethod_Type Renderable::getDrawMethod(GLuint shaderId){
	return mShaderMap[shaderId]->sDrawMethod;
}

void Renderable::replace3DTexture(GLuint shaderId,GLuint handle,const char* uniformName){
    Shader_Type * sh =  mShaderMap[shaderId];
    sh->sTexture3DMap[uniformName].set(handle);
}

void Renderable::replaceTexture(GLuint shaderId,GLuint handle,const char* uniformName){
     Shader_Type * sh =  mShaderMap[shaderId];
    sh->sTextureMap[uniformName].set(handle);
}

void Renderable::replaceUniformFloat(GLfloat* data,GLuint shaderId,const char* uniformName){
    UniformFloat_Type uniform = mShaderMap[shaderId]->sUniformFloatMap[uniformName];

    for(GLuint i = 0;i < uniform.sSize; i++)
        uniform.sData[i] = data[i];
    uploadUniformFloat(&uniform);

    #ifdef MODEL_OBJECT_VERBOSE
    printError("Replace Uniform Float");
    #endif

}

void Renderable::replaceUniformMatrix(mat4 matrix,GLuint shaderId,const char* uniformName){
    UniformMatrix_Type uniform = mShaderMap[shaderId]->sUniformMatrixMap[uniformName];
    uniform.sMatrix = matrix;
    uploadUniformMatrix(&uniform);
    #ifdef MODEL_OBJECT_VERBOSE
    printError("Replace Uniform Matrix");
    #endif


}
void Renderable::clone(GLuint srcShaderId, GLuint dstShaderId, bool copyUniforms, bool copyTextures){

    Shader_Type* srcShader = mShaderMap[srcShaderId];

    /**Copy Uniforms **/
    if(copyUniforms){
        for(UniformFloatIterator it = srcShader->sUniformFloatMap.begin(); it != srcShader->sUniformFloatMap.end(); ++it) {
            mShaderMap[dstShaderId]->sUniformFloatMap[it->first] = it->second;
        }
        for(UniformMatrixIterator it = srcShader->sUniformMatrixMap.begin(); it != srcShader->sUniformMatrixMap.end(); ++it) {
            mShaderMap[dstShaderId]->sUniformMatrixMap[it->first] = it->second;
        }
    }
    /** Copy textures **/
    if(copyTextures){
        for(TextureIterator it = srcShader->sTextureMap.begin(); it != srcShader->sTextureMap.end(); ++it) {
                mShaderMap[dstShaderId]->sTextureMap[it->first] = it->second;
        }
    }

    /** Copy handles to buffers **/
    mShaderMap[dstShaderId]->sBuffers = srcShader->sBuffers;

}


void Renderable::drawPoints(Shader_Type * shader,mat4 projectionMatrix, mat4 viewMatrix){

	GLuint program  = shader->sProgramHandle;

    glUseProgram(program);

    uploadTransform(shader,projectionMatrix,viewMatrix);
	selectTexture(shader,0);

    shader->sBuffers.enableAttributes(program);
    glDrawArrays(GL_POINTS, 0, 1);

    #ifdef MODEL_OBJECT_VERBOSE
    printError("Draw Points");
    #endif

}

void Renderable::drawInstanced(Shader_Type * shader,mat4 projectionMatrix, mat4 viewMatrix){

	GLuint program  = shader->sProgramHandle;

    glUseProgram(program);

    uploadTransform(shader,projectionMatrix,viewMatrix);

	selectTexture(shader,0);

    shader->sBuffers.bind();

   	glDrawElementsInstanced(GL_TRIANGLES, shader->sBuffers.sNumIndices, GL_UNSIGNED_INT,0L, shader->sNumInstances);

    #ifdef MODEL_OBJECT_VERBOSE
    printError("Draw Instanced");
    #endif

     shader->sBuffers.unbind();

}

void Renderable::drawElements(Shader_Type * shader,mat4 projectionMatrix,mat4 viewMatrix){

    GLuint program = shader->sProgramHandle;
    glUseProgram(program);

    uploadTransform(shader,projectionMatrix,viewMatrix);

    GLuint ntext = selectTexture(shader,0);
    selectTexture3D(shader,ntext);

    shader->sBuffers.bind();

    GLsizei numIndices = shader->sBuffers.sNumIndices;
    GLenum mode = GL_TRIANGLES;

    /** Is drawing patches set mode and parameter **/
/*    if(shader->sDrawMethod == PATCHES){
        glPatchParameteri(GL_PATCH_VERTICES, 3);
        #ifdef MODEL_OBJECT_VERBOSE
        printError("Draw Patches Parameter");
        #endif
        mode = GL_PATCHES;
    }*/
    SELECT_CONFIG
    glDrawElements(mode , numIndices, GL_UNSIGNED_INT, 0L);
    DESELECT_CONFIG
    shader->sBuffers.unbind();
}



mat4 * Renderable::getTransform(GLuint shaderId){
    return &(mShaderMap[shaderId]->sTransform);
}


