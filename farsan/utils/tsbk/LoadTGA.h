#ifndef __TGA_LOADER__
#define __TGA_LOADER__

#ifdef __cplusplus
extern "C" {
#endif

#ifdef WIN32
	#include "GL_utilities.h"
#else
    #include <GL/gl.h>

#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef true
#define true 1
#endif

#ifndef false
#define false 0
#endif

#ifndef bool
#define bool char
#endif

typedef struct TextureData		// Create A Structure for .tga loading.
{
	GLubyte	*imageData;			// Image Data (Up To 32 Bits)
	GLuint	bpp;				// Image Color Depth In Bits Per Pixel.
	GLuint	width;				// Image Width
	GLuint	height;				// Image Height
	GLuint	w;				// Image Width "raw"
	GLuint	h;				// Image Height "raw"
	GLuint	texID;				// Texture ID Used To Select A Texture
	GLfloat	texWidth, texHeight;
} TextureData, *TextureDataPtr;					// Structure Name

int LoadTGATexture(const char *filename, TextureData *texture);
void LoadTGATextureSimple(const char *filename, GLuint *tex);
void LoadTGASetMipmapping(int active);
int LoadTGATextureData(const char *filename, TextureData *texture);

#ifdef __cplusplus
}
#endif

#endif

