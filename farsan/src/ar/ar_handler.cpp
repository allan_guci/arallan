#include "ar_handler.h"



ARHandler::ARHandler(){

}


ARProgram * ARHandler::selectProgram(ProgramType p,cl::Context& context){
    ARProgram * program;
    switch(p){

        case CLOTH_PROGRAM:
            program = new ARProgramCloth();
        break;

        case SMOKE_PROGRAM:
            program = new ARProgramSmoke();
        break;

        case DEBUG_PROGRAM:
            program = new ARProgramDebug();
        break;
        case UNDER_WATER_PROGRAM:
            program = new ARProgramUnderWater();
        break;

    }

    program->init(context);
    return program;

}

void  ARHandler::init(cl::Context& context){
    mCurrentProgram = selectProgram(DEBUG_PROGRAM,context);
}

void ARHandler::runCL()
{
    mCurrentProgram->runCL();
}

void ARHandler::drawGL(UserCamera* uc){
    mCurrentProgram->drawGL(uc);
}
