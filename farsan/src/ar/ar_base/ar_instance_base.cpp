#include "ar_instance_base.h"


ARInstanceBase::ARInstanceBase() : GLInstanceBase(), CLInstanceBase()
{

}

void ARInstanceBase::prepareGLMesh(MeshType mesh){
    size_t bufSize = mesh.sNumVertices*4*4;

    mGLMesh.createVAO();
    if(mesh.sVertexPosition.size() > 0)
        mGLMesh.createPositionBuffer(bufSize,NULL,4);

    if(mesh.sVertexNormal.size() > 0)
        mGLMesh.createNormalBuffer(bufSize,NULL,4);

    if(mesh.sVertexTexCoord.size() > 0)
        mGLMesh.createTexCoordBuffer(bufSize/2,NULL,2);

    if(mesh.sVertexIndex.size() > 0)
        mGLMesh.createIndexBuffer(mesh.sNumIndices*4,NULL);

    mGLMesh.setNumElements(mesh.sNumIndices,mesh.sNumVertices);
}
