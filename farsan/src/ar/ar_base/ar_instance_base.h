#ifndef _AR_INSTANCE_BASE_H
#define _AR_INSTANCE_BASE_H

#include "cl_instance_base.h"
#include "gl_instance_base.h"

class ARInstanceBase :  public GLInstanceBase, public CLInstanceBase
{


public:
        ARInstanceBase();
        virtual void initAR(cl::Context context){
            initCL(context);
            printf("initAR - IMPLEMENT!\n");
        }; // = 0;
        virtual void initCL(cl::Context context) {
            context  = 1;
        }; //= 0;
        virtual void initGL(){
            printf("initGL - IMPLEMENT!\n");
        }; // = 0;
        virtual void runCL(){

        }; //  = 0;
        virtual void drawGL(UserCamera* uc){
            uc->getEye();
        }; // = 0;


        void prepareGLMesh(MeshType mesh);


    protected:

        GLHelper::Buffer mGLMesh;






	private:



};

#endif
