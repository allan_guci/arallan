#ifndef _GRID_INSTANCE_BASE_H
#define _GRID_INSTANCE_BASE_H

#define UNDEFINED_DIMENSION 90000
#include <stdlib.h>
#include <stdio.h>
#include "ar_size.h"
class GridInstanceBase
{

public:

	GridInstanceBase();
    void setGridDimension(size_t w, size_t h, size_t d);
    void setGridScale(float sx, float sy, float sz);
    void setGridOffset(float ox, float oy, float oz);

    void setGridDimension(ARSize d);
    void setGridScale(ARSize d);
    void setGridOffset(ARSize d);

    protected:


    ARSize getGridDim(){
        return mGridDim;
    }

    ARSize getGridScale(){
        return mGridScale;
    }

    ARSize getGridOffset(){
        return mGridOffset;
    }

    size_t * getGridDimMem(){
        return mGridDim.dataI();
    }

    float * getGridScaleMem(){
        return mGridScale.dataF();
    }

    float * getGridOffsetMem(){
        return mGridOffset.dataF();
    }

    ARSize  mGridDim;
    ARSize  mGridScale;
    ARSize  mGridOffset;

	private:

};

#endif
