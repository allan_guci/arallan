#include "grid_instance_base.h"




GridInstanceBase::GridInstanceBase(){

}

void GridInstanceBase::setGridDimension(size_t w, size_t h, size_t d){
    mGridDim = ARSize(w,h,d);
}

void GridInstanceBase::setGridScale(float sx, float sy, float sz){
    mGridScale= ARSize(sx,sy,sz);
}

void GridInstanceBase::setGridOffset(float ox, float oy, float oz){
     mGridOffset = ARSize(ox,oy,oz);
}


void GridInstanceBase::setGridDimension(ARSize d){
    mGridDim = d;
}

void GridInstanceBase::setGridScale(ARSize d){
    mGridScale= d;
}

void GridInstanceBase::setGridOffset(ARSize d){
     mGridOffset = d;
}



