#ifndef _AR_WATER_SURFACE_H
#define _AR_WATER_SURFACE_H

#include "ar_instance_base.h"
#include "loadTGA.h"
#include "ar_meshconverter.h"
#include "grid_instance_base.h"

class ARWaterSurface: public ARInstanceBase, public GridInstanceBase
{
public:
	ARWaterSurface();
    void initAR(cl::Context context);
    void initCL(cl::Context context);
    void initGL();

    void runCL();
    void drawGL(UserCamera * uc);
	private:

     struct SurfacePropertyType{
        cl_float4 offset;
        cl_float4 scale;
        cl_float4 camoffset;
        cl_float frequency;
        cl_float amplitude;
        cl_float ncompontents;
        cl_float time;

    void setOffset(cl_float v1,cl_float v2,cl_float v3){
        offset.s[0] = v1;
        offset.s[1] = v2;
        offset.s[2] = v3;
    }

      void setScale(cl_float v1,cl_float v2,cl_float v3){
        scale.s[0] = v1;
        scale.s[1] = v2;
        scale.s[2] = v3;
    }

    void setCamOffset(cl_float v1,cl_float v2,cl_float v3){
        camoffset.s[0] = v1;
        camoffset.s[1] = v2;
        camoffset.s[2] = v3;
    }


    void setFrequency(cl_float f){
        frequency = f;
    }

    void setAmplitude(cl_float a){
        amplitude = a;
    }

    void setNumComponents(cl_float n){
        ncompontents = n;
    }
    void setTime(cl_float t){
        time = t;
    }

};

    float mTime;
    CLKernel* mKernel;
    MeshType mMesh;
    GLHelper::Buffer mGLBuffer;
    GLHelper::Buffer mGLBuffer4;
    CLMemory mTmpMemory1;
    CLMemory mTmpMemory2;
    CLKernel * mInitPos;
    CLKernel * mUpdate;
    CLKernel * mCommitUpdate;
    CLKernel * mComputeNormal;
    void genereateGLBuffers(GLHelper::Buffer& buffer);
    SurfacePropertyType mProperties;
};

#endif
