#include "ar_water_surface_refraction.h"

ARWaterSurfaceRefraction::ARWaterSurfaceRefraction(){
     mLightCamera = new UserCamera();
}

void ARWaterSurfaceRefraction::initAR(cl::Context context){
    initGL();
}

UserCamera * ARWaterSurfaceRefraction::getLightCamera(){
    return mLightCamera;
}


void ARWaterSurfaceRefraction::initGL(){

    createShader("shaders/under_water/water_refraction.vert","shaders/under_water/water_refraction.frag");
    GLHelper::Buffer buf = getGLInputBuffer();
    loadQuadModel(buf);

    createRenderTarget(RenderTargetType::COLOR_DEPTH,ARSize(getGridDim().dx(),getGridDim().dy()));
    useRenderTarget(true);
    mLightCamera->create(vec3(1,0,0),vec3(0,0,0),1.2,20.0f,getGridDim().dx(),getGridDim().dy());
}

void ARWaterSurfaceRefraction::drawGL(UserCamera* uc){

    bool blend = true;

    vec3 eye     = vec3(-0.05,4.0,-0.05); //-lightMem[0],-lightMem[1],-lightMem[2]);
    vec3 center  = vec3(0.0,0.0,0.0);

    mLightCamera->create(eye,center,1.2,40.0f,getGridDim().dx(),getGridDim().dy());
    glCullFace(GL_FRONT);

    if(blend){
        glDisable(GL_DEPTH_TEST);
        //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glBlendFunc(GL_ONE, GL_ONE);
        glEnable(GL_BLEND);


    }
    draw(mLightCamera);

    if(blend){
        glEnable(GL_DEPTH_TEST);
        glDisable(GL_BLEND);
    }
    glCullFace(GL_BACK);
}


