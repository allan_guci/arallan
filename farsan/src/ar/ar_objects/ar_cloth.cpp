#include "ar_cloth.h"


ARCloth::ARCloth(){

}

void ARCloth::initAR(cl::Context context){
    initGL();
    initCL(context);
}

void ARCloth::initBuffers(){

    size_t s1 = mGLBuffer.sNumVertices;
    size_t s4 = 4*mGLBuffer.sNumVertices;

    mClothNextPosition = CreateBufferFromGL(mGLBuffer,VertexPosition);
    mClothNormal       = CreateBufferFromGL(mGLBuffer,VertexNormal);

    mClothPosition        = CreateBuffer<cl_float>(s4);
    mClothPreviousPostion = CreateBuffer<cl_float>(s4);
    mClothSpring          = CreateBuffer<cl_short>(s1);



}

void ARCloth::initCL(cl::Context context){
	setup(context);

    CreateProgram("kernels/cloth.cl");

    initBuffers();

    MeshType mesh(mGLBuffer,context);
    setOutputMesh(mesh);

    configureKernels();

    bindGLMem();
	mKernelClothInit->run();
    releaseGLMem();
    sync();

}

void ARCloth::initGL(){
    createShader("shaders/ar/ar_cloth.vert", "shaders/ar/ar_cloth.frag");
    genereateGLBuffers(mGLBuffer);
    setBuffer(mGLBuffer);

}

void ARCloth::drawGL(UserCamera * uc){
    glDisable(GL_CULL_FACE);
    draw(uc);
    glEnable(GL_CULL_FACE);
}

void ARCloth::runCL()
{
    bindGLMem();
	for( int i = 0; i < 4; i++){
        mKernelClothUpdate1->run();
        mKernelClothCollision1->run();
        mKernelClothUpdate2->run();
        mKernelClothCollision2->run();
        mKernelClothUpdate3->run();
        mKernelClothCollision3->run();
    }



	mKernelClothComptuteNormals->run();

	sync();
    releaseGLMem();

}

void ARCloth::configureKernels(){
    ARSize clothSize(CLOTH_DIM,CLOTH_DIM);


    NumGlobalThreads(clothSize);
    NumLocalThreads(ARSize(0));

	mKernelClothInit = CreateKernel("init");
	mKernelClothInit->arg(mClothNextPosition);
	mKernelClothInit->arg(mClothPosition);
	mKernelClothInit->arg(mClothPreviousPostion);
	mKernelClothInit->arg(mClothSpring);
	cl_float4 offset = {0.0,0,0.0,0};
	mKernelClothInit->arg(&offset,sizeof(cl_float4),4);
	mKernelClothInit->arg((cl_uint)CLOTH_DIM);

    mKernelClothUpdate1 = CreateKernel("update");
    mKernelClothUpdate1->arg(mClothPreviousPostion); // next
	mKernelClothUpdate1->arg(mClothNextPosition);    // cur
	mKernelClothUpdate1->arg(mClothPosition);        // prev
	mKernelClothUpdate1->arg(mClothSpring);
	mKernelClothUpdate1->arg((cl_uint)CLOTH_DIM);

	mKernelClothUpdate2 = CreateKernel("update");
    mKernelClothUpdate2->arg(mClothPosition);
	mKernelClothUpdate2->arg(mClothPreviousPostion);
	mKernelClothUpdate2->arg(mClothNextPosition);
	mKernelClothUpdate2->arg(mClothSpring);
	mKernelClothUpdate2->arg((cl_uint)CLOTH_DIM);

	mKernelClothUpdate3 = CreateKernel("update");
    mKernelClothUpdate3->arg(mClothNextPosition);
	mKernelClothUpdate3->arg(mClothPosition);
	mKernelClothUpdate3->arg(mClothPreviousPostion);
	mKernelClothUpdate3->arg(mClothSpring);
	mKernelClothUpdate3->arg((cl_uint)CLOTH_DIM);

    mKernelClothComptuteNormals = CreateKernel("compute_normal");
    mKernelClothComptuteNormals->arg(mClothNextPosition);
    mKernelClothComptuteNormals->arg(mClothNormal);
    mKernelClothComptuteNormals->arg((cl_uint)CLOTH_DIM);

    mKernelClothCollision1 = CreateKernel("collision",ARSize(CLOTH_DIM*CLOTH_DIM),ARSize(0));
	mKernelClothCollision1->arg(mClothPreviousPostion);
	mKernelClothCollision1->arg(mClothNextPosition);
	mKernelClothCollision1->arg(mCollidable.sMesh.sVertexPosition);
	mKernelClothCollision1->arg(mCollidable.sMesh.sVertexIndex);
	mKernelClothCollision1->arg(mCollidable.sNodes);
	mKernelClothCollision1->arg(mCollidable.sRoot);

	mKernelClothCollision2 = CreateKernel("collision",ARSize(CLOTH_DIM*CLOTH_DIM),ARSize(0));
	mKernelClothCollision2->arg(mClothPosition);
	mKernelClothCollision2->arg(mClothPreviousPostion);
	mKernelClothCollision2->arg(mCollidable.sMesh.sVertexPosition);
	mKernelClothCollision2->arg(mCollidable.sMesh.sVertexIndex);
	mKernelClothCollision2->arg(mCollidable.sNodes);
	mKernelClothCollision2->arg(mCollidable.sRoot);

	mKernelClothCollision3 = CreateKernel("collision",ARSize(CLOTH_DIM*CLOTH_DIM),ARSize(0));
	mKernelClothCollision3->arg(mClothNextPosition);
	mKernelClothCollision3->arg(mClothPosition);
	mKernelClothCollision3->arg(mCollidable.sMesh.sVertexPosition);
	mKernelClothCollision3->arg(mCollidable.sMesh.sVertexIndex);
	mKernelClothCollision3->arg(mCollidable.sNodes);
	mKernelClothCollision3->arg(mCollidable.sRoot);


}

void ARCloth::genereateGLBuffers(GLHelper::Buffer& buffer){
    float* vpos  =  new float[4*CLOTH_DIM*CLOTH_DIM];
    float* vnorm =  new float[4*CLOTH_DIM*CLOTH_DIM];
    int width = CLOTH_DIM;
    int height = CLOTH_DIM;
    int vertexCount = width * height;
    int triangleCount = (width-1) * (height-1) * 2;
    int x, z;
    GLuint *indexArray = (GLuint *)malloc(sizeof(GLuint) * triangleCount*3);
    for (z = 0; z < height-1; z++)
        {
        for (x = 0; x < width-1; x++)
        {

            // Triangle 1
            int vertexPosCurrent = (x + z * (width-1))*6;
            indexArray[vertexPosCurrent + 0] = x + z * width;
            indexArray[vertexPosCurrent + 1] = x+1 + z * width;
            indexArray[vertexPosCurrent + 2] = x + (z+1) * width;
            // Triangle 2
            indexArray[vertexPosCurrent + 3] = x+1 + z * width;
            indexArray[vertexPosCurrent + 4] = x+1 + (z+1) * width;
            indexArray[vertexPosCurrent + 5] = x + (z+1) * width;

        }
    }
    buffer = GLHelper::Buffer::createBuffers(vpos,4,vnorm,4,NULL,2,indexArray,vertexCount,triangleCount*3);
}
