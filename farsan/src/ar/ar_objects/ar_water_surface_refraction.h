#ifndef _AR_WATER_SURFACE_REFRACTION_H
#define _AR_WATER_SURFACE_REFRACTION_H

#include "ar_instance_base.h"
#include "grid_instance_base.h"

class ARWaterSurfaceRefraction: public ARInstanceBase, public GridInstanceBase
{

public:

	ARWaterSurfaceRefraction();

	void initGL();

	void drawGL(UserCamera* uc);

	void initAR(cl::Context context);

    UserCamera *getLightCamera();
protected:

private:
  UserCamera *mLightCamera;

};
#endif
