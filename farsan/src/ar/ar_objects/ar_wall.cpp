#include "ar_wall.h"

ARWall::ARWall(){
}


void ARWall::initAR(cl::Context context){
    initGL();
}

void ARWall::initGL(){

    createShader("shaders/under_water/skarm.vert","shaders/under_water/skarm.frag");

    loadModel("models/skarm.obj", true, true);

    GLuint tex;
    LoadTGATextureSimple("textures/wall.tga",&tex);
    setTexture(tex,"u_Texture",GL_TEXTURE_2D);
}

void ARWall::drawGL(UserCamera* uc){

    draw(uc);
}


