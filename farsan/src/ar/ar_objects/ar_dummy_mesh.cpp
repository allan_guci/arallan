#include "ar_dummy_mesh.h"


ARDummyMesh::ARDummyMesh(){

}

void ARDummyMesh::initAR(cl::Context context){
    initGL();
    initCL(context);
}

void ARDummyMesh::initCL(cl::Context context){

	setup(context);

    mCLMemory = CreateBufferFromGL(mGLBuffer,VertexPosition);

    CreateProgram("kernels/ar_debug_mesh.cl");

    ARSize globalThreadDim(SIZE_X,SIZE_Y);
    ARSize localThreadDim(8,8);

    mKernel = CreateKernel("main",globalThreadDim,localThreadDim);
    mKernel->arg(mCLMemory);
    mKernel->arg((cl_uint)SIZE_X);
    mKernel->arg((cl_uint)SIZE_Y);
    mKernel->arg(0.5f);
    mKernel->arg(1.0f);

    mTime = 0.0f;
    mKernel->arg(mTime);

    setCLOutput(mCLMemory);
}

void ARDummyMesh::initGL(){
    createShader("shaders/ar/ar_debug_mesh.vert", "shaders/ar/ar_debug_mesh.frag");

    genereateGLBuffers(mGLBuffer);
    setBuffer(mGLBuffer);

    setGLOutputBuffer(mGLBuffer);

}

void ARDummyMesh::drawGL(UserCamera * uc){
    draw(uc);
}

void ARDummyMesh::runCL()
{
    mTime += 0.01f;
    if(mTime > 2)
        mTime = 0;
    bindGLMem();
    mKernel->setArgument(5,mTime);
    mKernel->run();
    sync();
    releaseGLMem();
}

void ARDummyMesh::genereateGLBuffers(GLHelper::Buffer& buffer){
  int width = SIZE_X;
  int height = SIZE_Y;
  int vertexCount = width * height;
  int triangleCount = (width-1) * (height-1) * 2;
  int x, z;
  GLuint *indexArray = (GLuint *)malloc(sizeof(GLuint) * triangleCount*3);
	for (z = 0; z < height-1; z++)
		{
	for (x = 0; x < width-1; x++)
	{

			// Triangle 1
			int vertexPosCurrent = (x + z * (width-1))*6;
			indexArray[vertexPosCurrent + 0] = x + z * width;
			indexArray[vertexPosCurrent + 1] = x+1 + z * width;
            indexArray[vertexPosCurrent + 2] = x + (z+1) * width;
			// Triangle 2
			indexArray[vertexPosCurrent + 3] = x+1 + z * width;
			indexArray[vertexPosCurrent + 4] = x+1 + (z+1) * width;
            indexArray[vertexPosCurrent + 5] = x + (z+1) * width;

		}
	}
    buffer.createVAO();
    buffer.createPositionBuffer(SIZE_X*SIZE_Y*sizeof(float)*4,NULL,4);
    buffer.createIndexBuffer(sizeof(GLuint) * triangleCount*3,indexArray);
    buffer.setNumElements(triangleCount*3,vertexCount);

}
