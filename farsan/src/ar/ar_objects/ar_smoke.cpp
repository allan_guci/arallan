#include "ar_smoke.h"

#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

#define COLLISION


ARSmoke::ARSmoke() : ctr(0) ,mUniformSet(false)
{

}


ARSmoke::~ARSmoke()
{
   // mGLScene->~ModelObject();
   // mCLProgram->~CLProgram();
}
void ARSmoke::initAR(cl::Context context){

    initCL(context);
    initGL();

    texId = 0;
}

void ARSmoke::initCL(cl::Context context){
    setup(context);

    size_t* gridDim = getGridDimMem();
    addMacro("_G_SIZE_X",(int)gridDim[0]);
    addMacro("_G_SIZE_Y",(int)gridDim[1]);
    addMacro("_G_SIZE_Z",(int)gridDim[2]);
    addMacro("EDGE_MAX",1);
    addMacro("EDGE_MIN",1);
    addMacro("_DT",0.1f);
    addMacro("_RESCALE_OBS",0.333333f); //1.000001f); //.6734693f);
    addMacro("SOURCE_RADIUS_SCALE",2.0f);
    addMacro("SOURCE_RADIUS_MIN",0.1f);

    CreateProgram("kernels/fluidv7.cl");

    initBuffers();
    initKernels();

}

void ARSmoke::initBuffers(){

    ARSize dataSize = getGridDim();

    mGLSourceBuf1   = CreateGLTexture(GL_RG16F,GL_RG,dataSize);
    mGLSourceBuf2   = CreateGLTexture(GL_RG16F,GL_RG,dataSize);

    ImageOptions optionsR(cl::ImageFormat(CL_R,CL_HALF_FLOAT),dataSize);
    ImageOptions optionsRG(cl::ImageFormat(CL_RG,CL_HALF_FLOAT),dataSize);
    ImageOptions optionsRGBA(cl::ImageFormat(CL_RGBA,CL_HALF_FLOAT),dataSize);

    mCLSourceBuf1 = CreateImageFromGL(mGLSourceBuf1,optionsRG);
    mCLSourceBuf2 = CreateImageFromGL(mGLSourceBuf2,optionsRG);

    mCLVelocityBuf1 = CreateImage(optionsRGBA);
    mCLVelocityBuf2 = CreateImage(optionsRGBA);

    mCLSourceMC1 = CreateImageFromGL(mGLSourceBuf1,optionsRG);
    mCLSourceMC2 = CreateImageFromGL(mGLSourceBuf2,optionsRG);

    mCLVelocityMC1 = CreateImage(optionsRGBA);
    mCLVelocityMC2 = CreateImage(optionsRGBA);

    mOmegaBuf = CreateImage(optionsRGBA);
    mCurlBuf  = CreateImage(optionsR);

    mDivBuf   = CreateImage(optionsR);

    mPresBuf1 = CreateImage(optionsR);
    mPresBuf2 = CreateImage(optionsR);

}

CLKernel* ARSmoke::initAdvectionKernelPhase1(CLMemory memSrc1,CLMemory memSrc2,CLMemory memMC){

    CLKernel * kern      = CreateKernel("advect_phase1");
    CLKernel * kernTmp = CreateKernel("advect_phase1");

    kern->arg(mCLVelocityBuf1);
    kern->arg(memSrc1);

    kernTmp->arg(mCLVelocityBuf2);
    kernTmp->arg(memSrc2);

    kern->createPingPong(kernTmp);
    kern->arg(memMC);
    return kern;
}

CLKernel* ARSmoke::initAdvectionKernelPhase2(CLMemory memSrc1,CLMemory memSrc2,CLMemory memMCIn,CLMemory memMCOut){

    CLKernel * kern      = CreateKernel("advect_phase2");
    CLKernel * kernTmp = CreateKernel("advect_phase2");

    kern->arg(mCLVelocityBuf1);
    kern->arg(memSrc1);

    kernTmp->arg(mCLVelocityBuf2);
    kernTmp->arg(memSrc2);

    kern->createPingPong(kernTmp);
    kern->arg(memMCIn);
    kern->arg(memMCOut);

    return kern;
}


void ARSmoke::initKernels(){

    CL_MEM mInputMem = getCLInput();

    ARSize gridDim = getGridDim();

    NumGlobalThreads(ARSize(gridDim.dx()-2,gridDim.dy()-2,gridDim.dz()-2));
    NumLocalThreads(ARSize(0,0,0));


    mAdvectVelocityMC1 = initAdvectionKernelPhase1(mCLVelocityBuf1,mCLVelocityBuf2,mCLVelocityMC1);
    mAdvectVelocityMC2 = initAdvectionKernelPhase2(mCLVelocityBuf1,mCLVelocityBuf2,mCLVelocityMC1,mCLVelocityMC2);

    mAdvectSourceMC1 = initAdvectionKernelPhase1(mCLSourceBuf1,mCLSourceBuf2,mCLSourceMC1);
    mAdvectSourceMC2 = initAdvectionKernelPhase2(mCLSourceBuf1,mCLSourceBuf2,mCLSourceMC1,mCLSourceMC2);

    /// Add velocity (apply buoyancy)
    mAddVelocity      = CreateKernel("add_velocity");
    CLKernel * kernAV = CreateKernel("add_velocity");

    mAddVelocity->arg(mCLVelocityMC2);
    mAddVelocity->arg(mCLVelocityBuf2);
    mAddVelocity->arg(mCLSourceBuf2);

    kernAV->arg(mCLVelocityMC2);
    kernAV->arg(mCLVelocityBuf1);
    kernAV->arg(mCLSourceBuf1);
    mAddVelocity->createPingPong(kernAV);
    mAddVelocity->arg(mInputMem);

    mVelocityMetaData.setDissipation(0.995f);
    mAddVelocity->arg((void*)&mVelocityMetaData,sizeof(mVelocityMetaData));

    /// Add properties kernel ( density temperature)
    mAddProperties      = CreateKernel("add_properties");
    CLKernel * kernAP = CreateKernel("add_properties");

    mAddProperties->arg(mCLSourceMC2);
    mAddProperties->arg(mCLSourceBuf2);

    kernAP->arg(mCLSourceMC2);
    kernAP->arg(mCLSourceBuf1);

    mAddProperties->createPingPong(kernAP);
    mAddProperties->arg(mInputMem);

    mPropertiesMetaData.setValue(2.5f,5.8);
    mPropertiesMetaData.setDissipation(0.995f,0.986);
    mPropertiesMetaData.setPosition(gridDim.dx()/2.0f,gridDim.dy()/2.0f,gridDim.dz()/8.0f);
    mPropertiesMetaData.setRadius(12.1);
    mAddProperties->arg((void*)&mPropertiesMetaData,sizeof(mPropertiesMetaData));


    /// Vortify
    mVortKernel = CreateKernel("vortify");
    CLKernel * ppV = CreateKernel("vortify");

    mVortKernel->arg(mCLVelocityBuf2);
    ppV->arg(mCLVelocityBuf1);

    mVortKernel->createPingPong(ppV);
    mVortKernel->arg(mOmegaBuf);
    mVortKernel->arg(mCurlBuf);


    /// Confine
    mConKernel = CreateKernel("confine");
    CLKernel * ppC = CreateKernel("confine");

    mConKernel->createPingPong(ppC,mCLVelocityBuf2,mCLVelocityBuf1);
    mConKernel->arg(mOmegaBuf);
    mConKernel->arg(mCurlBuf);


    /// Divergence
    mDivKernel = CreateKernel("fluid_divergence");
    CLKernel * ppDiv = CreateKernel("fluid_divergence");

    mDivKernel->arg(mCLVelocityBuf1);
    ppDiv->arg(mCLVelocityBuf2);

    mDivKernel->createPingPong(ppDiv);
    mDivKernel->arg(mDivBuf);
    mDivKernel->arg(mInputMem);

    ///  Jacobi
    mJacKernel = CreateKernel("fluid_jacobi");
    CLKernel * ppJac = CreateKernel("fluid_jacobi");

    mJacKernel->createPingPong(ppJac,mPresBuf1,mPresBuf2);
    mJacKernel->arg(mDivBuf);
    mJacKernel->arg(mInputMem);

    /// Projection / Gradient subtraction
    mProjKernel       = CreateKernel("fluid_projection");
    CLKernel * ppProj = CreateKernel("fluid_projection");

    mProjKernel->arg(mPresBuf2);
    ppProj->arg(mPresBuf1);

    mProjKernel->createPingPong(ppProj,mCLVelocityBuf1,mCLVelocityBuf2);
    mProjKernel->arg(mInputMem);
}


void ARSmoke::initGL(){

    createShader("shaders/smoke/raycast_plain.vert", "shaders/smoke/raycast_plain.frag");

    loadModel("models/cube2.obj",false,true);

    GLfloat eye[] = {0,1,0};
    setUniformFloat(eye,"u_Eye",3);

    MMTranslate(getGridOffsetMem());
    MMScale(getGridScaleMem());
    MMUpload();

    setTexture(mGLSourceBuf1,"u_Texture",GL_TEXTURE_3D);
    setTexture(getGLInputTexture(),"u_Depth",GL_TEXTURE_2D);

    setBlend(true);

}

void ARSmoke::setShaderUniforms(UserCamera * uc){
    GLfloat camPos[] = {uc->getEye().x,uc->getEye().y,uc->getEye().z};
    setUniformFloat(camPos,"u_Eye");

    if(!mUniformSet){
        setUniformFloat((GLfloat)uc->mScreenWidth,"u_ScreenWidth");
        setUniformFloat((GLfloat)uc->mScreenHeight,"u_ScreenHeight");
        setUniformFloat(uc->getNear(),"u_CameraNear");
        setUniformFloat(uc->getFar(),"u_CameraFar");
        mUniformSet = true;
    }

}

void ARSmoke::runCL(){

    bindGLMem();

    mAdvectVelocityMC1->run(true);
    mAdvectVelocityMC2->run(true);

    mAdvectSourceMC1->run(true);
    mAdvectSourceMC2->run(true);

    mAddProperties->run(true);
    mAddVelocity->run(true);

    mVortKernel->run(true);
    mConKernel->run(true);
    mDivKernel->run(true);

    for(int i = 0; i < 31; i++)
        mJacKernel->run(true);
    mProjKernel->run(true);

    sync();
    releaseGLMem();


    ctr = (ctr+1) % 10000;


    ARSize gd = getGridDim();
    cl_float xOffset = (cos((cl_float)ctr/7.0f)+1.0)*0.5;
    xOffset = xOffset*xOffset;
    mPropertiesMetaData.setPosition((float)gd.dx()/2,(float)gd.dy()/2-10,(float)gd.dz()/4);//+xOffset*20);
    mPropertiesMetaData.setRadius(static_cast<float>(3.0f*fabs(xOffset*2)+ 2.5f));
    mAddProperties->arg((void*)&mPropertiesMetaData,sizeof(mPropertiesMetaData),3);

}

void ARSmoke::drawGL(UserCamera * uc){

    setShaderUniforms(uc);

    texId = (texId+1) % 2;
    if(texId == 0)
        setTexture(mGLSourceBuf1,"u_Texture",GL_TEXTURE_3D);
    else
        setTexture(mGLSourceBuf2,"u_Texture",GL_TEXTURE_3D);

    draw(uc);
}



