#ifndef FLUIDS_H
#define FLUIDS_H

#include "ar_instance_base.h"
#include "grid_instance_base.h"
#include "cl_program.h"
#include <sys/timeb.h>
#include <time.h>
#include <unistd.h>


#define VELOCITY_TYPE 0.0f
#define PROPERTY_TYPE 1.0f

class ARSmokePlain : public ARInstanceBase, public GridInstanceBase
{


    struct SourceMetaType{
        cl_float2 value;
        cl_float4 position;
        cl_float  radius;
        cl_float4 dissipation;

        void setValue(cl_float v1,cl_float v2){
            value.s[0] = v1;
            value.s[1] = v2;
        }

        void setPosition(cl_float px,cl_float py,cl_float pz){
            position.s[0] = px;
            position.s[1] = py;
            position.s[2] = pz;
            position.s[3] = 0;
        }

        void setRadius(cl_float r){
            radius = r;
        }

        void setDissipation(cl_float d1,cl_float d2){
            dissipation.s[0] = d1;
            dissipation.s[1] = d2;
            dissipation.s[2] = 0;
            dissipation.s[3] = 1.0f;
        }
        void setDissipation(cl_float d1){
            dissipation.s[0] = d1;
            dissipation.s[1] = d1;
            dissipation.s[2] = d1;
            dissipation.s[3] = 0.0f;
        }
    };

    public:


        ARSmokePlain();
        virtual ~ARSmokePlain();
        void initAR(cl::Context context);
        void initCL(cl::Context context);
        void initGL();
        void runCL();
        void drawGL(UserCamera * uc);

    protected:


    private:
        CLKernel*  initAdvectionKernelPhase1(CLMemory memSrc1,CLMemory memSrc2,CLMemory memMC);
        CLKernel*  initAdvectionKernelPhase2(CLMemory memSrc1,CLMemory memSrc2,CLMemory memMCIn,CLMemory memMCOut);
        CLKernel*  initAddSource(CLMemory memInSrc,CLMemory memProps,CLMemory memOutSource1,CLMemory memOutSrc2, cl_int type);
        CLKernel*  initAdvectionMCKernelVeloctiy(CLMemory memVel1,CLMemory memVel2,CLMemory memSrc1,CLMemory memSrc2,CLMemory memMC1,CLMemory memMC2, CLMemory props);
        void setShaderUniforms(UserCamera * uc);

        void initBuffers();
        void initKernels();

        CLKernel * mAddVelocity;
        CLKernel * mAddProperties;

        CLKernel * mAdvectVelocityMC1;
        CLKernel * mAdvectSourceMC1;
        CLKernel * mAdvectVelocityMC2;
        CLKernel * mAdvectSourceMC2;

        CLKernel * mVortKernel;
        CLKernel * mConKernel;

        CLKernel * mClearKernel;

        CLKernel * mDivKernel;
        CLKernel * mJacKernel;
        CLKernel * mClearPresKernel;
        CLKernel * mProjKernel;

        GLuint mGLSourceBuf1;
        GLuint mGLSourceBuf2;

        CLMemory mCLSourceBuf1;
        CLMemory mCLSourceBuf2;

        CLMemory mCLVelocityBuf1;
        CLMemory mCLVelocityBuf2;

        CLMemory mCLSourceMC1;
        CLMemory mCLSourceMC2;

        CLMemory mCLVelocityMC1;
        CLMemory mCLVelocityMC2;

        CLMemory mOmegaBuf;
        CLMemory mCurlBuf;

        CLMemory mDivBuf;

        CLMemory mPresBuf1;
        CLMemory mPresBuf2;

        SourceMetaType   mVelocityMetaData;
        SourceMetaType   mPropertiesMetaData;


        GLuint texId;

        cl_int ctr;

        bool mUniformSet;


};

#endif // FLUIDS_H




