#ifndef _AR_DUMMY_MESH_H
#define _AR_DUMMY_MESH_H

#include "ar_instance_base.h"

#define MESH_SHADER 1

#define SIZE_X 512
#define SIZE_Y 512

class ARDummyMesh : public ARInstanceBase
{
public:
	ARDummyMesh();
    void initAR(cl::Context context);
    void initCL(cl::Context context);
    void initGL();

    void runCL();
    void drawGL(UserCamera * uc);

    void genereateGLBuffers(GLHelper::Buffer& buffer);

	private:

    float mTime;
    CLKernel* mKernel;
    CLMemory mCLMemory;
    GLHelper::Buffer mGLBuffer;


};

#endif
