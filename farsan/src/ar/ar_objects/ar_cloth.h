#ifndef _AR_CLOTH_H
#define _AR_CLOTH_H

#include "ar_instance_base.h"
#include "ar_felja.h"
#include <math.h>
#define CLOTH_DIM 128

class ARCloth : public ARInstanceBase
{
public:
	ARCloth();
    void initAR(cl::Context context);
    void initBuffers();
    void initCL(cl::Context context);
    void initGL();

    void runCL();
    void drawGL(UserCamera * uc);
    void genereateGLBuffers(GLHelper::Buffer& buffer);

	private:
        void configureKernels();
	    int intersectBox(cl_float4 r_o, cl_float4 r_d, cl_float4 boxmin, cl_float4 boxmax, float *tnear, float *tfar);
        bool collideAABB(const BVHNode* node, cl_float4 pos, cl_float4 vel);


    CLKernel* mKernelClothInit;
    CLKernel* mKernelClothUpdate1;
    CLKernel* mKernelClothUpdate2;
    CLKernel* mKernelClothUpdate3;
    CLKernel* mKernelClothCollision1;
    CLKernel* mKernelClothCollision2;
    CLKernel* mKernelClothCollision3;
    CLKernel* mKernelClothComptuteNormals;

    std::vector<CLMemory> mClothPositions;

    GLHelper::Buffer mGLBuffer;

    CL_MEM mClothNextPosition;
    CL_MEM mClothPosition;
    CL_MEM mClothPreviousPostion;
    CL_MEM mClothNormal;
    CL_MEM mClothSpring;

    CLMemory mTestMem;
};

#endif
