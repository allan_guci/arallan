#ifndef _AR_MONKEY_H
#define _AR_MONKEY_H

#include "ar_instance_base.h"


class ARMonkey: public ARInstanceBase
{

public:

	void initAR(cl::Context context);

	void initGL();

	void drawGL(UserCamera* uc);

	ARMonkey();

protected:

private:

};
#endif