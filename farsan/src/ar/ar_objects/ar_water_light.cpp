#include "ar_water_light.h"


ARWaterLight::ARWaterLight(){
    mScaleFar = 0.2f;
    mScaleNear = 1.00f;
    mOffsetFar = FAR_LIGHT_GR;
    mOffsetNear = 1.0f;
}

void ARWaterLight::initAR(cl::Context context){
    initGL();
    initCL(context);
}

void ARWaterLight::initGL(){
    createShader("shaders/under_water/water_surface.vert", "shaders/under_water/water_surface.frag");

    uploadLight();
}

void ARWaterLight::drawGL(UserCamera * uc){
    draw(uc);
}

GLHelper::Buffer ARWaterLight::generateFrustumMesh(GLfloat farVal, GLfloat nearVal, GLuint dimension){


	// TODO SCALE FAR- AND FEAR FACES


	GLuint vertexCount = 2*dimension*dimension;
	int triangleCount = (dimension-1) * (dimension-1) * 2 * 2 + 4*2*(dimension-1);
	GLuint x, y;


  //  GLfloat *vertexArray = (GLfloat*)malloc(sizeof(GLfloat) * 3 * vertexCount);
	///GLuint *indexArray = (GLuint*)malloc(sizeof(GLuint) * triangleCount*3);
	GLfloat vertexArray [4 * vertexCount];
	GLuint indexArray[triangleCount*3];
	GLfloat res = dimension-1;

	GLuint N;
	GLuint SideOffset  =  6*(dimension-1);
	GLuint PlaneOffset = SideOffset*(dimension-1);
	GLuint SideIndexStep = dimension;
	GLuint PlaneIndexStep = dimension*dimension;
	GLuint TrianglesPerQuad = 6;

	/* Near face vertices */
	GLfloat xVal = 0.0f;
	GLfloat yVal = 0.0f;
	GLfloat zVal = 0.0f;
	for (x = 0; x < dimension; x++)
		for (y = 0; y < dimension; y++)
		{

            xVal = mScaleNear*(x/res - 0.5)*2.0;
            yVal = mScaleNear*(y/res - 0.5)*2.0;
            zVal = mOffsetNear;
            GLuint startIndex = (x + y * dimension)*3;
			vertexArray[startIndex + 0] = xVal;
			vertexArray[startIndex + 1] = yVal;
			vertexArray[startIndex + 2] = zVal;

		}


	/* Indices for the the near face */
	for (x = 0; x < dimension-1; x++)
		for (y = 0; y < dimension-1; y++)
		{

			GLuint startIndex = (x + y * (dimension-1))*6;
			indexArray[startIndex + 0] = x + y * dimension;
			indexArray[startIndex + 1] = x + (y+1) * dimension;
			indexArray[startIndex + 2] = x+1 + y * dimension;

			indexArray[startIndex + 3] = x+1 + y * dimension;
			indexArray[startIndex + 4] = x + (y+1) * dimension;
			indexArray[startIndex + 5] = x+1 + (y+1) * dimension;
		}

	/* Far face vertices */
	for (x = 0; x < dimension; x++)
		for (y = 0; y < dimension; y++)
		{

            xVal = mScaleFar*farVal*(x/res - 0.5f)*2.0f;
            yVal = mScaleFar*farVal*(y/res - 0.5f)*2.0f;
            zVal = mOffsetFar;

			GLuint startIndex = (x + y * dimension)*3 + 3*dimension*dimension;
			vertexArray[startIndex + 0] = xVal;
			vertexArray[startIndex + 1] = yVal;
			vertexArray[startIndex + 2] = zVal;

		}

	/* Indices for the the far face */
	for (x = 0; x < dimension-1; x++)
		for (y = 0; y < dimension-1; y++)
		{


			GLuint startIndex = (x + y * (dimension-1))*6 + PlaneOffset;
			indexArray[startIndex + 0] = x + y * dimension + PlaneIndexStep;
			indexArray[startIndex + 2] = x + (y+1) * dimension + PlaneIndexStep;
			indexArray[startIndex + 1] = x+1 + y * dimension + PlaneIndexStep;


			indexArray[startIndex + 3] = x+1 + y * dimension + PlaneIndexStep;
			indexArray[startIndex + 5] = x + (y+1) * dimension + PlaneIndexStep;
            indexArray[startIndex + 4] = x+1 + (y+1) * dimension + PlaneIndexStep;
		}
 	/* Indices for the faces connecting far and near plane */
	/* BOTTOM: */

	y = 0;

	GLuint BottomOffset = 2*PlaneOffset;
	for(x = 0; x < dimension-1; x++){
		N = BottomOffset + x*TrianglesPerQuad;
		//Triangle 1
		//  ____
		//  |  /
		//  | /
		//  |/

		indexArray[N + 0] = x;
		indexArray[N + 1] = x + PlaneIndexStep+1;
		indexArray[N + 2] = x + PlaneIndexStep;
		//    /|
		//   / |
		//  /__|

		indexArray[N + 3] = x;
		indexArray[N + 4] = x + 1;
		indexArray[N + 5] = x + PlaneIndexStep+1;

	}

	/*  RIGHT: */
	x = SideIndexStep-1;

	GLuint RightOffset = 2*PlaneOffset+SideOffset;
	for(y = 0; y < dimension-1; y++){
		N = RightOffset + y*TrianglesPerQuad;
		//Triangle 1
		//  ____
		//  |  /
		//  | /
		//  |/

		indexArray[N + 0] = x + (y + 0)*SideIndexStep;
		indexArray[N + 1] = x + (y + 1)*SideIndexStep+PlaneIndexStep;
		indexArray[N + 2] = x + (y + 0)*SideIndexStep+PlaneIndexStep;

		// Triangle 2
		//    /|
		//   / |
		//  /__|

		indexArray[N + 3] = x + (y + 0)*SideIndexStep;
		indexArray[N + 4] = x + (y + 1)*SideIndexStep;
		indexArray[N + 5] = x + (y + 1)*SideIndexStep+PlaneIndexStep;

	}

	/* Left: */
	x = 0;
	GLuint LeftOffset = 2*PlaneOffset+2*SideOffset;
	for(y = 0; y < dimension-1; y++){
		N = LeftOffset + y*TrianglesPerQuad;
		//Triangle 1
		//  ____
		//  |  /
		//  | /
		//  |/

		indexArray[N + 0] = x + (y + 0)*SideIndexStep;
		indexArray[N + 1] = x + (y + 0)*SideIndexStep+PlaneIndexStep;
		indexArray[N + 2] = x + (y + 1)*SideIndexStep+PlaneIndexStep;
		// Triangle 2
		//    /|
		//   / |
		//  /__|

		indexArray[N + 3] = x + (y + 0)*SideIndexStep;
		indexArray[N + 4] = x + (y + 1)*SideIndexStep+PlaneIndexStep;
		indexArray[N + 5] = x + (y + 1)*SideIndexStep;

	}
	/* TOP: */
	y = SideIndexStep -1;

	GLuint TopOffset = 2*PlaneOffset+3*SideOffset;
	for(x = 0; x < dimension-1; x++){
		N = TopOffset + x*TrianglesPerQuad;
		//Triangle 1
		//  ____
		//  |  /
		//  | /vertexArray
		//  |/

		indexArray[N + 0] = x + (y + 0)*SideIndexStep;
		indexArray[N + 1] = x + (y + 0)*SideIndexStep + PlaneIndexStep;
		indexArray[N + 2] = x + (y + 0)*SideIndexStep + PlaneIndexStep+1;
		//    /|
		//   / |
		//  /__|

		indexArray[N + 3] = x + (y + 0)*SideIndexStep;
		indexArray[N + 4] = x + (y + 0)*SideIndexStep + PlaneIndexStep+1;
		indexArray[N + 5] = x + (y + 0)*SideIndexStep + 1;

	}

	// End of Light frustum generation
	// Create Model and upload to GPU:
	GLHelper::Buffer buffer;
    buffer.createVAO();
    buffer.createPositionBuffer(SIZE_X*SIZE_Y*sizeof(float)*4,vertexArray,4);
    buffer.createIndexBuffer(sizeof(GLuint) * triangleCount*3,indexArray);
    buffer.setNumElements(triangleCount*3,vertexCount);
    return buffer;
    /** Clean up **/
    free(vertexArray);
    free(indexArray);
	printError("PostProcessing Buffer");

}
