#include "ar_water_surface.h"


ARWaterSurface::ARWaterSurface(){

}

void ARWaterSurface::initAR(cl::Context context){
    initGL();

    initCL(context);
}

void ARWaterSurface::initCL(cl::Context context){
	setup(context);

	mMesh = MeshType(mGLBuffer,context);

    GLuint tex;
    LoadTGATextureSimple("textures/fft_disp.tga",&tex);
    ImageOptions optionsRGBA(cl::ImageFormat(CL_RGBA,CL_UNSIGNED_INT8),ARSize(512,512));
    CLMemory dispMem = CreateImageFromGL(tex,optionsRGBA);

    mTmpMemory1 = CreateBuffer<float>(2*getGridDim().dSize());
    mTmpMemory2 = CreateBuffer<float>(2*getGridDim().dSize());

    addMacro("SURF_SIZE_X",getGridDim().dx());
    CreateProgram("kernels/watersurface.cl");

    ARSize scale =  getGridScale();
    ARSize offset = getGridOffset();

    mProperties.setScale(scale.sx(),1.0,scale.sz());
    mProperties.setOffset(offset.sx(),2.0,offset.sz());
    mProperties.setAmplitude(0);
    mProperties.setFrequency(0);
    mProperties.setNumComponents(1);
    mProperties.setTime(0);

    mInitPos      = CreateKernel("init_pos",ARSize(mMesh.sNumVertices),ARSize(0));
    mInitPos->arg(mMesh.sVertexPosition);
    mInitPos->arg(mMesh.sVertexTexCoord);
    mInitPos->arg(mTmpMemory1);
    mInitPos->arg(mTmpMemory2);
    mInitPos->arg((void*)&mProperties,sizeof(SurfacePropertyType));

    mUpdate = CreateKernel("update",getGridDim(),ARSize(0));
    CLKernel* ppKern = CreateKernel("update",getGridDim(),ARSize(0));
    mUpdate->createPingPong(ppKern,mTmpMemory1,mTmpMemory2);
    mUpdate->arg(mMesh.sVertexPosition);
    mUpdate->arg((void*)&mProperties,sizeof(SurfacePropertyType));

    mComputeNormal = CreateKernel("compute_normal",getGridDim(),ARSize(0));
    mComputeNormal->arg(mMesh.sVertexPosition);
    mComputeNormal->arg(mMesh.sVertexNormal);

    bindGLMem();
    mInitPos->run();
    sync();
    releaseGLMem();

    setOutputMesh(mMesh);


}


void ARWaterSurface::runCL()
{

    bindGLMem();
    for(int i = 0; i < 10; i++)
        mUpdate->run();
    mComputeNormal->run();

    sync();
    releaseGLMem();

    /*
    mTmpMemory2.toFile<float>("sobel.csv");
    exit(-1);*/



    mProperties.time +=0.015;
    if(mProperties.time > 100.0)
        mProperties.time = 0;
    mUpdate->arg((void*)&mProperties,sizeof(SurfacePropertyType),3);
}

void ARWaterSurface::initGL(){

    createShader("shaders/under_water/water_surface.vert", "shaders/under_water/water_surface.frag");

    //mGLBuffer = loadModel("models/surface.obj",true,true,false);

    genereateGLBuffers(mGLBuffer);
    setBuffer(mGLBuffer);
    setGLOutputBuffer(mGLBuffer);

    GLuint tex;
    LoadTGATextureSimple("textures/skydome.tga",&tex);
    setTexture(tex,"u_TextureSky",GL_TEXTURE_2D);

    setTexture(getGLInputTexture("COLOR"),"u_TextureBottom" ,GL_TEXTURE_2D);

    uploadLight();

}

void ARWaterSurface::drawGL(UserCamera * uc){

    vec3 eye    = uc->getEye();
    vec3 lookAt = uc->lookAtVec();
    float angle = atan2(lookAt.x,lookAt.z);
    MMReset();
   // MMTranslate(eye.x,eye.y,eye.z);
    mProperties.setCamOffset(eye.x,eye.y,eye.z);

    glCullFace(GL_FRONT);
    glDisable(GL_BLEND);
    draw(uc);
        glCullFace(GL_BACK);

}


void ARWaterSurface::genereateGLBuffers(GLHelper::Buffer& buffer){
  ARSize dim = getGridDim();
  int width = dim.dx();
  int height = dim.dy();
  int vertexCount = width * height;
  int triangleCount = (width-1) * (height-1) * 2;
  int x, z;
  GLuint *indexArray = (GLuint *)malloc(sizeof(GLuint) * triangleCount*3);
	for (z = 0; z < height-1; z++)
		{
	for (x = 0; x < width-1; x++)
	{

			// Triangle 1
			int vertexPosCurrent = (x + z * (width-1))*6;
			indexArray[vertexPosCurrent + 0] = x + z * width;
			indexArray[vertexPosCurrent + 1] = x+1 + z * width;
            indexArray[vertexPosCurrent + 2] = x + (z+1) * width;
			// Triangle 2
			indexArray[vertexPosCurrent + 3] = x+1 + z * width;
			indexArray[vertexPosCurrent + 4] = x+1 + (z+1) * width;
            indexArray[vertexPosCurrent + 5] = x + (z+1) * width;

		}
	}
    buffer.createVAO();
    buffer.createPositionBuffer(width*height*sizeof(float)*4,NULL,4);
    buffer.createNormalBuffer(width*height*sizeof(float)*4,NULL,4);
    buffer.createTexCoordBuffer(width*height*sizeof(float)*2,NULL,2);
    buffer.createIndexBuffer(sizeof(GLuint) * triangleCount*3,indexArray);
    buffer.setNumElements(triangleCount*3,vertexCount);

}
