#ifndef _AR_FELJA_H
#define _AR_FELJA_H

#include "ar_instance_base.h"
#include "ar_collision.h"
#include "grid_instance_base.h"
class ARFelja : public ARInstanceBase, public GridInstanceBase
{
public:
	ARFelja();
    void initAR(cl::Context context);
    void initGL();
    void drawGL(UserCamera * uc);
    void runCL();
    void initCL(cl::Context context);

	private:
    void genereateGLBuffers(GLHelper::Buffer& buffer);
    GLuint mHPTex;

    CLMemory mOutImage;
    CLMemory mInImage;


};

#endif
