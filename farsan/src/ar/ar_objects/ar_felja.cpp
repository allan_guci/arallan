#include "ar_felja.h"


ARFelja::ARFelja(){

}

void ARFelja::initAR(cl::Context context){
    initGL();
    initCL(context);
}
int n;
void ARFelja::initCL(cl::Context context){

    setup(context);

    CreateProgram("kernels/bottom.cl");

    ARSize imgSize = getGridDim();
    ARSize locSize(16,16);

    ImageOptions optRGBA(cl::ImageFormat(CL_RGBA,CL_FLOAT),imgSize);

    GLuint tex = getGLInputTexture();
    mInImage   = CreateImageFromGL(tex,optRGBA);
    mOutImage  = CreateImageFromGL(mHPTex,optRGBA);

    CLMemory summedLight = CreateBuffer<int>(getGridDim().dx()*getGridDim().dy());

    CLKernel* clearKernel = CreateKernel("clear",imgSize,locSize);
    clearKernel->arg(mOutImage);
    clearKernel->arg(summedLight);
    AddKernel(clearKernel);
/*
    CLKernel* sumKernel = CreateKernel("sum_light",imgSize,locSize);
    sumKernel->arg(mInImage);
    sumKernel->arg(summedLight);
    AddKernel(sumKernel);

    CLKernel* commitKernel = CreateKernel("commit",imgSize,locSize);
    commitKernel->arg(summedLight);
    commitKernel->arg(mOutImage);
    AddKernel(commitKernel);


    CLKernel* copyKernel = CreateKernel("debug",imgSize,locSize);
    copyKernel->arg(mInImage);
    copyKernel->arg(mOutImage);
    AddKernel(copyKernel);

*/

    n = 0;
}

void ARFelja::runCL(){
  //  RunKernels();
    /*if(n == 200){
        mInImage.toFile<cl_float>("sobel.csv");
        exit(-1);
    }
    n++;*/

}

void ARFelja::initGL(){
    createShader("shaders/under_water/bottom.vert", "shaders/under_water/bottom.frag");
    GLHelper::Buffer buf = loadModel("models/bottom.obj",false,true);
    setBuffer(buf);
    GLuint tex;

    LoadTGATextureSimple("textures/heightmap.tga",&tex);
    setTexture(tex,"u_HeightMap",GL_TEXTURE_2D);

    LoadTGATextureSimple("textures/sand.tga",&tex);
    setTexture(tex,"u_TextureSand",GL_TEXTURE_2D);

    LoadTGATextureSimple("textures/light_cookie.tga",&tex);
    setTexture(tex,"u_TextureCookie",GL_TEXTURE_2D);

    mHPTex = CreateGLTexture(GL_RGBA32F,GL_RGBA,getGridDim());
    setTexture(getGLInputTexture() ,"u_RefractionMap",GL_TEXTURE_2D);

    setGLOutputTexture(mHPTex);


    uploadLight();

}

void ARFelja::drawGL(UserCamera * uc){
    vec3 eyeVec  = uc->getEye();
    GLfloat eye[] = {eyeVec.x,eyeVec.y,eyeVec.z};
    setUniformFloat(eye,"u_CamPos");
    setUniformMatrix(getExternalCamera()->getTextureMatrix(T(0.0,-1.0,0.0)),"TextureMatrix");

    MMReset();
  //  MMTranslate(eyeVec.x,eyeVec.y-2,eyeVec.z);


    draw(uc);

}


