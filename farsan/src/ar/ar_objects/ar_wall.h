#ifndef _AR_WALL_H
#define _AR_WALL_H

#include "ar_instance_base.h"


class ARWall: public ARInstanceBase
{

public:

	ARWall();

	void initGL();

	void drawGL(UserCamera* uc);

	void initAR(cl::Context context);

protected:

private:

};
#endif