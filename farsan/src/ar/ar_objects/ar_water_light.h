#ifndef _AR_WATER_LIGHT_H
#define _AR_WATER_LIGHT_H

#include "ar_instance_base.h"

#define FAR_LIGHT_GR 18
class ARWaterLight: public ARInstanceBase
{
public:
	ARWaterLight();
    void initAR(cl::Context context);
    void initGL();
    void drawGL(UserCamera * uc);


	private:
    GLHelper::Buffer generateFrustumMesh(GLfloat farVal, GLfloat nearVal, GLuint dimension);


    GLfloat mScaleFar;
    GLfloat mScaleNear;
    GLfloat mOffsetFar;
    GLfloat mOffsetNear;
};

#endif
