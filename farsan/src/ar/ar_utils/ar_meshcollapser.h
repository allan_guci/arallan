#ifndef _AR_MESHCOLLAPSER_H
#define _AR_MESHCOLLAPSER_H

#include "ar_instance_base.h"

class ARMeshCollapser : public ARInstanceBase
{
public:
	ARMeshCollapser();
    void initAR(cl::Context context);
    void initCL(cl::Context context);
    void runCL();
    void setOriginalDimension(ARSize d);

private:
    void initBuffers();
    MeshType mCollapsedMesh;
    ARSize mOriginalDimension;

    cl_int mCollapseFactor;

    CLKernel * mKernelCollapsePosition;
    CLKernel * mKernelCollapseNormal;
    CLKernel * mKernelCollapseTexCoord;
    CLKernel * mKernelCollapseIndex;

};

#endif
