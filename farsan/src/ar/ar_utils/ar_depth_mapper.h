#ifndef _AR_DEPTH_MAPPER_H
#define _AR_DEPTH_MAPPER_H

#include "ar_instance_base.h"

class ARDepthMapper : public ARInstanceBase
{
public:
	ARDepthMapper();

	void initAR(cl::Context context);
	void initCL(cl::Context context){};
	void initGL();

	void runCL(){};
	void drawGL(UserCamera * uc);


	private:



};

#endif
