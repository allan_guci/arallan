#include "ar_meshconverter.h"


ARMeshConverter::ARMeshConverter(){

}

void ARMeshConverter::initAR(cl::Context context){
    initCL(context);
}

void ARMeshConverter::upload(size_t numVertices, size_t stride, GLHelper::Buffer bufferGL, CLMemory& bufferCL,GLBufferItem item){



    if(stride == 4){
        /// No copy. Just attach gl-buffer to cl-buffers
        bufferCL = CreateBufferFromGL(bufferGL,item);
    }else if( stride == 3) { /// Stride 3
        /// Attach Copy gl-buffer to cl buffer and then to cl-buffer, the stride is extended from 3 to 4.

        /// Attach vertex buffer to tmp cl memory
        CLMemory tmp = CreateBufferFromGL(bufferGL,item);

        /// Allocate vertex buffer with the stride of 4
        bufferCL = CreateBuffer<cl_float>(4*numVertices);


        /// Copy and extend data
        mKernelCopy->threadDimension(ARSize(numVertices),ARSize(0));
        mKernelCopy->setArgument(0,tmp);
        mKernelCopy->setArgument(1,bufferCL);

        bindGLMem();
        mKernelCopy->run(true);
        releaseGLMem();
    }else{
        printf("Mesh converter - Invalid stride");
        exit(-1);
    }


}


void ARMeshConverter::uploadCpy(size_t numVertices, size_t stride, GLHelper::Buffer bufferGL, CLMemory& bufferCL,GLBufferItem item){



    if(stride == 4){
            printf("NO REASON FOR THIS copy gl/cl copy \n");
            exit(-1);
    }else if( stride == 3) { /// Stride 3
        /// Attach Copy gl-buffer to cl buffer and then to cl-buffer, the stride is extended from 3 to 4.

        /// Attach vertex buffer to tmp cl memory
        CLMemory tmp = CreateBufferFromGL(bufferGL,item);

        /// Copy and extend data
        mKernelCopy->threadDimension(ARSize(numVertices),ARSize(0));
        mKernelCopy->setArgument(0,tmp);
        mKernelCopy->setArgument(1,bufferCL);

        bindGLMem();
        mKernelCopy->run(true);
        releaseGLMem();
    }else{
        printf("Mesh converter - Invalid stride");
        exit(-1);
    }


}

void ARMeshConverter::initCL(cl::Context context){

	setup(context);

    CreateProgram("kernels/convert.cl");
    mKernelCopy = CreateKernel("convert_f3_to_f4",ARSize(0),ARSize(0));

    GLHelper::Buffer buf = loadModel("models/felja_low.obj",false,false,false); //getGLInputBuffer();
    setGLOutputBuffer(buf);
    if(getInputMesh().sNumVertices == 0){

        mMesh.setMeta(buf.sNumIndices,buf.sNumVertices);
        if(buf.sVBOPosition != GLHelper::Buffer::UNUSED){
            upload(buf.sNumVertices,buf.sPositionSize,buf,mMesh.sVertexPosition,VertexPosition);
        }

        if(buf.sVBONormal != GLHelper::Buffer::UNUSED){
            upload(buf.sNumVertices,buf.sNormalSize,buf,mMesh.sVertexNormal,VertexNormal);
        }

        if(buf.sVBOTextureCoord != GLHelper::Buffer::UNUSED){
            mMesh.sVertexTexCoord = CreateBufferFromGL(buf,VertexTexCoord);
        }
        mMesh.sVertexIndex = CreateBufferFromGL(buf,VertexIndex);
    }else{
        mMesh = getInputMesh();
        if(buf.sVBOPosition != GLHelper::Buffer::UNUSED){
            uploadCpy(buf.sNumVertices,buf.sPositionSize,buf,mMesh.sVertexPosition,VertexPosition);
        }

        if(buf.sVBONormal != GLHelper::Buffer::UNUSED){
            uploadCpy(buf.sNumVertices,buf.sNormalSize,buf,mMesh.sVertexNormal,VertexNormal);
        }
    }







    setOutputMesh(mMesh);

}


