#ifndef _AR_TEXTURE_TO_SCREEN_H
#define _AR_TEXTURE_TO_SCREEN_H

#include "ar_instance_base.h"


class ARTextureToScreen : public ARInstanceBase
{
public:
	ARTextureToScreen();

	void initAR(cl::Context context);
	void initGL();
	void drawGL(UserCamera * uc);


	private:

};

#endif
