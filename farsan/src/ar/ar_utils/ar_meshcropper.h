#ifndef _AR_MESHIFIER_H
#define _AR_MESHIFIER_H

#include "ar_instance_base.h"
#include "ar_collision.h"
#define CL_LOCAL_SIZE (cl_uint)256
#define CL_LOCAL_MEM_SIZE (CL_LOCAL_SIZE+1)
#define BLOCKED_RUN false
class ARMeshCropper : public ARInstanceBase
{
public:
	ARMeshCropper();

	void initAR(cl::Context context);
	void initCL(cl::Context context);
	void runCL();


	private:


	    struct TraverseType{
            size_t sNumLevels;
            std::vector<size_t> sLevelSizes;
            std::vector<size_t> sLevelThreads;
            std::vector<CLMemory> sLevelMem;
	    };

	    void initBuffers();
	    void configureKernels();
	    void updateMeshData();
	    void initTraverseType(TraverseType& trav, size_t initSize);
        void travUpDown(TraverseType trav);

        TraverseType mTraverseVert;
        TraverseType mTraverseTri;

        CLMemory mMemTriangleNew;
        CLMemory mMemVertexPositionNew;
        CLMemory mMemVertexNormalNew;
        CLMemory mMemVertexTexCoordNew;

        CLMemory mMemMask;
        CLMemory mMemMaskTri;

        CLKernel * mConvertKernel;
        CLKernel * mCreateMaskKernel;
        CLKernel * mCreateTriangleMaskKernel;

        CLKernel * mTravUpKernel;
        CLKernel * mTravDownKernel;

        CLKernel * mMapVertexPositionKernel;
        CLKernel * mMapVertexNormalKernel;
        CLKernel * mMapVertexTexCoordKernel;
        CLKernel * mMapTriangleKernel;

        MeshType mCroppedMesh;
};

#endif
