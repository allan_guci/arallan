#include "ar_collision.h"


ARCollision::ARCollision(): isUsed(false),isStaticAlloc(false)
{

}


void ARCollision::initKernels(){
    ARSize def(20);

    NumGlobalThreads(def);
    NumLocalThreads(def);

    /// BVH
    mKernelAABB         = CreateKernel("clAABB");
    mKernelMorton       = CreateKernel("clMorton");
    mKernelCreateNodes  = CreateKernel("clCreateNodes");
    mKernelLinkNodes    = CreateKernel("clLinkNodes");
    mKernelCreateLeaves = CreateKernel("clCreateLeaves");
    mKernelComputeAABB  = CreateKernel("clComputeAABBs");

    /// Radix Sort
    mKernelBlockSort    = CreateKernel("clBlockSort");
    mKernelBlockScan    = CreateKernel("clBlockScan");
    mKernelBlockPrefix  = CreateKernel("clBlockPrefix");
    mKernelBlockReorder = CreateKernel("clReorder");

}

void ARCollision::initCL(cl::Context context){
	setup(context);
	CreateProgram("kernels/sort.cl");
	initKernels();
}

void ARCollision::staticAlloc(){
    MeshType mesh = getInputMesh();
    isStaticAlloc = true;

    mNumVertices  = mesh.sNumVertices;
    mNumIndices   = mesh.sNumIndices;
    mNumTriangles = mNumIndices/3;

    size_t blockSize  = 256;
    size_t blockCount = ceil((float)mNumTriangles/blockSize);

    mCLVertexPosition = mesh.sVertexPosition;
    mCLVertexIndex    = mesh.sVertexIndex;

    /// BVH
    mBufferAABB      = CreateBuffer<srtAABB>(1);
    mBufferMortonKey = CreateBuffer<cl_uint>(mNumTriangles);
    mBufferMortonVal = CreateBuffer<cl_uint>(mNumTriangles);
    mBufferBvhNode   = CreateBuffer<BVHNode>(2*mNumTriangles-1);
    mRootNode = 0;
    mBufferBvhRoot   = CreateBuffer<cl_uint>(1,CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR,&mRootNode);

    /// Radix Sort
    mBufferBlockScan   = CreateBuffer<cl_uint>(blockCount*(1<<4));
    mBufferBlockOffset = CreateBuffer<cl_uint>(blockCount*(1<<4));
    mBufferBlockSum    = CreateBuffer<cl_uint>(blockSize);

    mBufferTempKey     = CreateBuffer<cl_uint>(mNumTriangles);
    mBufferTempVal     = CreateBuffer<cl_uint>(mNumTriangles);

    /// Assign Tree to output
    mCollidable.sNodes = mBufferBvhNode;
    mCollidable.sRoot  = mBufferBvhRoot;
    cl_uint * r      = mBufferBvhRoot.toHost<cl_uint>();
    mCollidable.sRootN = *r;
    mCollidable.sMesh = mesh;

    configureKernels();

}

void ARCollision::buildBVH(){

    MeshType mesh = getInputMesh();
    mNumVertices  = *mesh.sNumVerticesPtr;
    mNumIndices   = *mesh.sNumIndicesPtr;
    mNumTriangles = mNumIndices/3;

    size_t blockSize  = 256;
    size_t blockCount = ceil((float)mNumTriangles/blockSize);

    mCLVertexPosition = mesh.sVertexPosition;
    mCLVertexIndex    = mesh.sVertexIndex;

    if(!isStaticAlloc){ /// Re-allocate buffer size
        /// BVH
        mBufferAABB      = CreateBuffer<srtAABB>(1);
        mBufferMortonKey = CreateBuffer<cl_uint>(mNumTriangles);
        mBufferMortonVal = CreateBuffer<cl_uint>(mNumTriangles);
        mBufferBvhNode   = CreateBuffer<BVHNode>(2*mNumTriangles-1);
        mRootNode = 0;
        mBufferBvhRoot   = CreateBuffer<cl_uint>(1,CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR,&mRootNode);

        /// Radix Sort
        mBufferBlockScan   = CreateBuffer<cl_uint>(blockCount*(1<<4));
        mBufferBlockOffset = CreateBuffer<cl_uint>(blockCount*(1<<4));
        mBufferBlockSum    = CreateBuffer<cl_uint>(blockSize);

        mBufferTempKey     = CreateBuffer<cl_uint>(mNumTriangles);
        mBufferTempVal     = CreateBuffer<cl_uint>(mNumTriangles);

        /// Set input to kernels
        configureKernels();
    }else{
        configureKernelsStatic();
    }



    /// Build BVH-tree
    build();

    /// Assign Tree to output
    mCollidable.sNodes = mBufferBvhNode;
    mCollidable.sRoot  = mBufferBvhRoot;
    cl_uint * r      = mBufferBvhRoot.toHost<cl_uint>();
    mCollidable.sRootN = *r;
    mCollidable.sMesh = mesh;
}

void ARCollision::build(){

    mKernelAABB->run(RUN_WAIT);
    mKernelMorton->run(RUN_WAIT);

    radixSort();

    mKernelCreateNodes->run(RUN_WAIT);
    mKernelLinkNodes->run(RUN_WAIT);
    mKernelCreateLeaves->run(RUN_WAIT);
    mKernelComputeAABB->run(RUN_WAIT);

    sync();
}

void ARCollision::radixSort(){
    int startBit = 0;
    int endBit   = 32;

    for(cl_int i = startBit; i < endBit ; i+=4){

        mKernelBlockSort->setArgument(4,(cl_uint)i);
        mKernelBlockSort->run(RUN_WAIT);
        mKernelBlockScan->run(RUN_WAIT);
        mKernelBlockPrefix->run(RUN_WAIT);
        mKernelBlockReorder->setArgument(6,(cl_uint)i);
        mKernelBlockReorder->run(RUN_WAIT);

    }
}
void ARCollision::configureKernelsStatic(){
   /// BVH

    size_t warpsize = 32;
    size_t batchsize = ceil(ceil(mNumVertices/8.0)/warpsize)*warpsize;

    mKernelAABB->threadDimension(ARSize(batchsize),ARSize(warpsize));
    mKernelAABB->setArgument(2,mNumVertices);

    mKernelMorton->threadDimension(ARSize(mNumTriangles),ARSize(0));

    size_t globalSize = mNumTriangles-1;
    mKernelCreateNodes->threadDimension(ARSize(globalSize),ARSize(0));

    mKernelLinkNodes->threadDimension(ARSize(globalSize),ARSize(0));

    mKernelCreateLeaves->threadDimension(ARSize(mNumTriangles),ARSize(0));

    mKernelComputeAABB->threadDimension(ARSize(mNumTriangles),ARSize(0));

    /// Radix Sort
    size_t blockSize  = 256;
    size_t blockCount = ceil((float)mNumTriangles/blockSize);
    globalSize = blockCount*blockSize;
    size_t scanCount  = blockCount*(1<<4)/4;
    size_t scanSize   = ceil((float)scanCount/blockSize)*blockSize;

    mKernelBlockSort->threadDimension(ARSize(globalSize),ARSize(blockSize));
    mKernelBlockSort->setArgument(7,mNumTriangles);

    mKernelBlockScan->threadDimension(ARSize(scanSize),ARSize(blockSize));
    mKernelBlockScan->setArgument(2,scanCount);

    mKernelBlockPrefix->threadDimension(ARSize(scanSize),ARSize(blockSize));
    mKernelBlockPrefix->setArgument(2,scanCount);

    mKernelBlockReorder->threadDimension(ARSize(globalSize),ARSize(blockSize));
    mKernelBlockReorder->setArgument(7,mNumTriangles);
}

void ARCollision::configureKernels(){

    /// BVH

    size_t warpsize = 32;
    size_t batchsize = ceil(ceil(mNumVertices/8.0)/warpsize)*warpsize;

    mKernelAABB->threadDimension(ARSize(batchsize),ARSize(warpsize));
    mKernelAABB->setArgument(0,mCLVertexPosition);
    mKernelAABB->setArgument(1,mBufferAABB);
    mKernelAABB->setArgument(2,mNumVertices);

    mKernelMorton->threadDimension(ARSize(mNumTriangles),ARSize(0));
    mKernelMorton->setArgument(0,mCLVertexIndex);
    mKernelMorton->setArgument(1,mCLVertexPosition);
    mKernelMorton->setArgument(2,mBufferMortonKey);
    mKernelMorton->setArgument(3,mBufferMortonVal);
    mKernelMorton->setArgument(4,mBufferAABB);

    size_t globalSize = mNumTriangles-1;
    mKernelCreateNodes->threadDimension(ARSize(globalSize),ARSize(0));
    mKernelCreateNodes->setArgument(0,mBufferMortonKey);
    mKernelCreateNodes->setArgument(1,mBufferMortonVal);
    mKernelCreateNodes->setArgument(2,mBufferBvhNode);

    mKernelLinkNodes->threadDimension(ARSize(globalSize),ARSize(0));
    mKernelLinkNodes->setArgument(0,mBufferMortonKey);
    mKernelLinkNodes->setArgument(1,mBufferBvhNode);
    mKernelLinkNodes->setArgument(2,mBufferBvhRoot);

    mKernelCreateLeaves->threadDimension(ARSize(mNumTriangles),ARSize(0));
    mKernelCreateLeaves->setArgument(0,mBufferMortonVal);
    mKernelCreateLeaves->setArgument(1,mBufferBvhNode);

    mKernelComputeAABB->threadDimension(ARSize(mNumTriangles),ARSize(0));
    mKernelComputeAABB->setArgument(0,mCLVertexIndex);
    mKernelComputeAABB->setArgument(1,mCLVertexPosition);
    mKernelComputeAABB->setArgument(2,mBufferBvhNode);
    mKernelComputeAABB->setArgument(3,mBufferBvhRoot);


    /// Radix Sort
    size_t blockSize  = 256;
    size_t blockCount = ceil((float)mNumTriangles/blockSize);
    globalSize = blockCount*blockSize;
    size_t scanCount  = blockCount*(1<<4)/4;
    size_t scanSize   = ceil((float)scanCount/blockSize)*blockSize;

    mKernelBlockSort->threadDimension(ARSize(globalSize),ARSize(blockSize));
    mKernelBlockSort->setArgument(0,mBufferMortonKey);
    mKernelBlockSort->setArgument(1,mBufferTempKey);
    mKernelBlockSort->setArgument(2,mBufferMortonVal);
    mKernelBlockSort->setArgument(3,mBufferTempVal);
    mKernelBlockSort->setArgument(4,(cl_uint)0);
    mKernelBlockSort->setArgument(5,mBufferBlockScan);
    mKernelBlockSort->setArgument(6,mBufferBlockOffset);
    mKernelBlockSort->setArgument(7,mNumTriangles);

    mKernelBlockScan->threadDimension(ARSize(scanSize),ARSize(blockSize));
    mKernelBlockScan->setArgument(0,mBufferBlockScan);
    mKernelBlockScan->setArgument(1,mBufferBlockSum);
    mKernelBlockScan->setArgument(2,scanCount);

    mKernelBlockPrefix->threadDimension(ARSize(scanSize),ARSize(blockSize));
    mKernelBlockPrefix->setArgument(0,mBufferBlockScan);
    mKernelBlockPrefix->setArgument(1,mBufferBlockSum);
    mKernelBlockPrefix->setArgument(2,scanCount);

    mKernelBlockReorder->threadDimension(ARSize(globalSize),ARSize(blockSize));
    mKernelBlockReorder->setArgument(0,mBufferTempKey);
    mKernelBlockReorder->setArgument(1,mBufferMortonKey);
    mKernelBlockReorder->setArgument(2,mBufferTempVal);
    mKernelBlockReorder->setArgument(3,mBufferMortonVal);
    mKernelBlockReorder->setArgument(4,mBufferBlockScan);
    mKernelBlockReorder->setArgument(5,mBufferBlockOffset);
    mKernelBlockReorder->setArgument(6,(cl_uint)0);
    mKernelBlockReorder->setArgument(7,mNumTriangles);
}




void ARCollision::runCL()
{
    buildBVH();
}



