#ifndef _AR_PREFIX_SUM_H
#define _AR_PREFIX_SUM_H

#include "ar_instance_base.h"
#include "grid_instance_base.h"

class ARPrefixSum: public ARInstanceBase ,public GridInstanceBase
{


    struct TraverseType{
            int sNumLevels;
            std::vector<size_t> sLevelSizes;
            std::vector<size_t> sLevelThreads;
            std::vector<CLMemory> sLevelMem;
    };


public:

    typedef enum{
        SumOffsets,
        SumSizes
    }SumType;

	void runCL();

	void initCL(cl::Context context);

	void initAR(cl::Context context);

	int getTotalSum();

    void setIsBinary(bool b);

	ARPrefixSum(CLMemory in, ARSize size, SumType type);

protected:

private:

    void initTraverseType(TraverseType& trav,size_t initSize);
    void travUpDown(TraverseType trav);

    void initBuffers();

    TraverseType mTraverse;

    SumType mType;

};
#endif
