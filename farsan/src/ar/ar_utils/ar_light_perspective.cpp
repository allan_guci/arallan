#include "ar_light_perspective.h"


ARLightPerspective::ARLightPerspective(){
    mLightCamera = new UserCamera();
}

void ARLightPerspective::initAR(cl::Context context){
    initGL();
}

UserCamera * ARLightPerspective::getLightCamera(){
    return mLightCamera;
}


void ARLightPerspective::initGL(){
    createShader("shaders/under_water/light_normal.vert", "shaders/under_water/light_normal.frag","shaders/under_water/light_normal.gs");

    setBuffer(getGLInputBuffer());
    createRenderTarget(RenderTargetType::COLOR_DEPTH,getGridDim());
    useRenderTarget(true);
    uploadLight();
    mLightCamera->create(vec3(1,0,0),vec3(0,0,0),1.2,20.0f,getGridDim().dx(),getGridDim().dy());
}

void ARLightPerspective::drawGL(UserCamera * uc){
    ARSize light = getLight();
    float * lightMem = light.dataF();
    vec3 eye     = vec3(-0.05,4.0,-0.05); //-lightMem[0],-lightMem[1],-lightMem[2]);
    vec3 center  = vec3(0.0,0.0,0.0);

    mLightCamera->create(eye,center,1.2,20.0f,getGridDim().dx(),getGridDim().dy());
    //glDisable(GL_CULL_FACE);

    bool blend = true;


    glCullFace(GL_FRONT);

    if(blend){
        glDisable(GL_DEPTH_TEST);
        glEnable(GL_BLEND);
       glDisable(GL_CULL_FACE);
        glBlendFunc(GL_ONE, GL_ONE);
    }
    draw(mLightCamera);

    if(blend){
        glEnable(GL_CULL_FACE);
        glEnable(GL_DEPTH_TEST);
        glDisable(GL_BLEND);
    }
    glCullFace(GL_BACK);
 //   glFinish();
   // glEnable(GL_CULL_FACE);

}

