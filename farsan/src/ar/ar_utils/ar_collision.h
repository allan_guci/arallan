#ifndef _AR_COLLISION_H
#define _AR_COLLISION_H

#include "ar_instance_base.h"
#include "math.h"
#define RUN_WAIT false
typedef struct
{
        cl_float4 bbMin;
        cl_float4 bbMax;
        cl_int flag;
}
srtAABB;

typedef struct
{
        cl_float4 bbMin;
        cl_float4 bbMax;
        cl_int left;
        cl_int right;
        cl_uint bit;
        cl_uint trav;
}BVHNode;

class ARCollision : public ARInstanceBase
{
public:

	ARCollision();
    void initCL(cl::Context context);
    void runCL();

    void staticAlloc();
    void buildBVH();

	private:

    void initBuffers();
    void initKernels();

    void configureKernelsStatic();
    void configureKernels();

    void build();
    void radixSort();

    bool isUsed;
    bool isStaticAlloc;

    cl_uint mRootNode;

    CLKernel * mKernelAABB;
    CLKernel * mKernelMorton;
    CLKernel * mKernelCreateNodes;
    CLKernel * mKernelLinkNodes;
    CLKernel * mKernelCreateLeaves;
    CLKernel * mKernelComputeAABB;

    CL_MEM mCLVertexPosition;
    CL_MEM mCLVertexIndex;

    CL_MEM mBufferAABB;
    CL_MEM mBufferMortonKey;
    CL_MEM mBufferMortonVal;
    CL_MEM mBufferBvhRoot;
    CL_MEM mBufferBvhNode;


    CLKernel * mKernelBlockSort;
    CLKernel * mKernelBlockScan;
    CLKernel * mKernelBlockPrefix;
    CLKernel * mKernelBlockReorder;

    CL_MEM mBufferBlockScan;
    CL_MEM mBufferBlockOffset;
    CL_MEM mBufferBlockSum;
    CL_MEM mBufferTempKey;
    CL_MEM mBufferTempVal;

    cl_uint mNumVertices;
    cl_uint mNumIndices;
    cl_uint mNumTriangles;

};

#endif
