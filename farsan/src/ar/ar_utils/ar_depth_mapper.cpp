#include "ar_depth_mapper.h"


ARDepthMapper::ARDepthMapper(){

}

void ARDepthMapper::initAR(cl::Context context){
    initGL();
}

void ARDepthMapper::initGL(){

    createShader("shaders/depth_shader.vert","shaders/depth_shader.frag","shaders/depth_shader.gs");

    GLHelper::Buffer buf;
    getGLInputBuffer().copyBuffer(buf,false,false);
    setBuffer(buf);

    size_t s = 512;
    ARSize dataSize(s,s);
    createRenderTarget(RenderTargetType::DEPTH,dataSize);
    useRenderTarget(true);

    setGLOutputTexture(getRenderTarget().sDepthTexture);

}


void ARDepthMapper::drawGL(UserCamera * uc){
    glDisable(GL_CULL_FACE);
    draw(uc);
    glEnable(GL_CULL_FACE);
}

