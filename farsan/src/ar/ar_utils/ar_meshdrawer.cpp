#include "ar_meshdrawer.h"


ARMeshDrawer::ARMeshDrawer(){

}

void ARMeshDrawer::initAR(cl::Context context){
    initGL();
}



void ARMeshDrawer::initGL(){
    //createShader("shaders/debug_mesh/debug_mesh.vert", "shaders/debug_mesh/debug_mesh.frag","shaders/debug_mesh/debug_mesh.gs");
    createShader(
                 "shaders/ar/ar_debug_mesh.vert",
                 "shaders/ar/ar_debug_mesh.frag"
              //   "shaders/ar/no_normal.gs"
                 );
    setBuffer(getGLInputBuffer());
    GLuint texId = getGLInputTexture();
    setTexture(getGLInputTexture(),"u_Texture",GL_TEXTURE_2D);

}

void ARMeshDrawer::drawGL(UserCamera * uc){
  //  glDisable(GL_CULL_FACE);
    setBufferNumIndices(*getInputMesh().sNumIndicesPtr);
    draw(uc);
  //  glEnable(GL_CULL_FACE);
}

