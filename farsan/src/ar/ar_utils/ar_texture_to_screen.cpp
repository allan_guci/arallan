#include "ar_texture_to_screen.h"


ARTextureToScreen::ARTextureToScreen(){

}

void ARTextureToScreen::initAR(cl::Context context){
    initGL();
}

void ARTextureToScreen::initGL(){

    createShader("shaders/under_water/postprocessing.vert","shaders/under_water/postprocessing.frag",NULL,NONE);
/**/
    setTexture(getGLInputTexture("COLOR"),"u_Texture",GL_TEXTURE_2D);
    setTexture(getGLInputTexture("DEPTH"),"u_TextureDepth",GL_TEXTURE_2D);

 //  createShader("shaders/screen_quad.vert","shaders/screen_quad.frag",NULL,NONE);
 //   setTexture(getGLInputTexture(),"u_Texture",GL_TEXTURE_2D);


  loadQuadModel();
}


void ARTextureToScreen::drawGL(UserCamera * uc){
    draw(uc);
}

