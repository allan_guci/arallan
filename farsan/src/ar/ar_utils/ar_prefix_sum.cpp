#include "ar_prefix_sum.h"
#include <string>       // std::string
#include <iostream>     // std::cout
#include <sstream>

inline
void createSettings(size_t numActiveThreads);

#define CL_LOCAL_SIZE 256

ARPrefixSum::ARPrefixSum(CLMemory in, ARSize size, SumType type){
    setGridDimension(size);
    setCLInput(in);
    mType = type;
    //createSettings(64);

}

void ARPrefixSum::initCL(cl::Context context){

    addMacro("LOCAL_SIZE",CL_LOCAL_SIZE);
    addMacro("LOCAL_MEM_SIZE",CL_LOCAL_SIZE+1);
    if(mType == SumSizes)
        addMacro("SIZE",1);
    else
        addMacro("OFFSET",1);

    setup(context);
    CreateProgram("kernels/prefix.cl");
    size_t numVertices = getCLInput().size();
    CLKernel* kernel_up = CreateKernel("trav_up",ARSize(numVertices),ARSize(0));
    CLKernel* kernel_down = CreateKernel("trav_down",ARSize(numVertices),ARSize(0));
    initBuffers();
}

void ARPrefixSum::initAR(cl::Context context){
    initCL(context);

    getTimer() = ARTimer("prefix sum");
}

int ARPrefixSum::getTotalSum(){
    int numLevels = mTraverse.sNumLevels;
    int* tmpPtr = mTraverse.sLevelMem[numLevels].toHost<int>();
    int res = *tmpPtr;
    delete tmpPtr;
    return res;
}

void ARPrefixSum::runCL(){
    travUpDown(mTraverse);
    sync();
}

void ARPrefixSum::initBuffers(){
    size_t numVertices = getCLInput().size();
    initTraverseType(mTraverse,numVertices);
    mTraverse.sLevelMem[0] = getCLInput();
}


void ARPrefixSum::initTraverseType(TraverseType& trav,size_t initSize){
    size_t curSize = initSize;
    size_t locSize = CL_LOCAL_SIZE;
    trav.sNumLevels = 0;


    for(int i = 0; i < 5; i ++){

        trav.sLevelSizes.push_back(curSize);
        trav.sLevelThreads.push_back(ceil((float)curSize/locSize)*locSize);

        CLMemory mem = CreateBuffer<cl_int>(curSize);
        trav.sLevelMem.push_back(mem);

        if(curSize == 1)
            break;

        curSize  = ceil((float)curSize/locSize);
        trav.sNumLevels++;
    }
}

void ARPrefixSum::travUpDown(TraverseType trav){
    bool prefixwait = true;
    int start = 0;
    getTimer().start();
    CLKernel* kernTravUp = GetKernel("trav_up");
    for(int i = start; i < trav.sNumLevels; i++){
        kernTravUp->threadDimension(ARSize(trav.sLevelThreads[i]),ARSize(CL_LOCAL_SIZE));
        kernTravUp->setArgument(0,trav.sLevelMem[i]);
        kernTravUp->setArgument(1,trav.sLevelMem[i+1]);
        kernTravUp->setArgument(2,trav.sLevelSizes[i]);
        kernTravUp->run(prefixwait);

    }
    getTimer().lap("traverse up");

    CLKernel* kernTravDown = GetKernel("trav_down");
    for(int i = trav.sNumLevels-2; i > -1; i--){
        kernTravDown->threadDimension(ARSize(trav.sLevelThreads[i]),ARSize(CL_LOCAL_SIZE));
        kernTravDown->setArgument(0,trav.sLevelMem[i]);
        kernTravDown->setArgument(1,trav.sLevelMem[i+1]);
        kernTravDown->setArgument(2,trav.sLevelSizes[i]);
        kernTravDown->run(prefixwait);
    }
    getTimer().lap("traverse down");
}



void INSERTARR(std::ofstream &stream,cl_uint * data,const char * nameData, size_t sizeData){
    stream << "#define " << nameData << " {";
    for(int i = 0; i < sizeData; i++){
        stream << data[i];
        if(i < sizeData - 1)
            stream << ",";
        else
            stream << "}\n";
    }
}

#define PSIZE 256
void dumpData(cl_uint *data, const char * filename){
    std::ofstream file(filename);
    for(int i = 0; i < PSIZE; i++){
        file << data[i]; \
        if ( i != PSIZE-1 ) file << ",";
    }
    file.close();
}


void INSERTELEM(std::ofstream &stream,cl_uint data,const char * nameData){
    stream << "#define " << nameData << " " << data << "\n";
}
inline
void createSettings(size_t numActiveThreads)
{
    cl_uint data[PSIZE];
    for(int i = 0; i < PSIZE; i++)
            data[i] = i;

    cl_uint resTrue[PSIZE];
    resTrue[0] = data[0];
    for(int i = 1; i < PSIZE; i++)
        resTrue[i] = resTrue[i-1] + data[i];


    dumpData(data,"in");
    dumpData(resTrue,"resultTrue");


    size_t LOCAL_SIZE = PSIZE;
    const cl_uint num_active_threads = numActiveThreads;
	const cl_uint num_active_threads_bits = log2(num_active_threads-1);
	const cl_uint size_bits  = log2(LOCAL_SIZE);
	const cl_uint data_red   = size_bits - num_active_threads_bits;
	const cl_uint num_iter   = size_bits/(data_red) + 1;

	const float divisor = (float)(1 << (data_red-1));
    const cl_uint data_size = (cl_uint)(LOCAL_SIZE/pow(divisor,num_iter)*pow(divisor,num_iter+1)/(divisor-1)  + 1);
    cl_uint* dataComp = new cl_uint[data_size];
    for ( int i = 0; i < PSIZE; i ++ ) dataComp[i] = data[i];

    cl_uint cur_num_active_threads[8];
    cl_uint stride[8];
    cl_uint partition_size[8];
    cl_uint partition_size_bits[8];
    cl_uint lvl_size[8];

	cur_num_active_threads[0] = num_active_threads;
	stride[0] = 0;
    lvl_size[0] = LOCAL_SIZE;
	partition_size[0] = LOCAL_SIZE >> (cl_uint)log2(num_active_threads);
	partition_size_bits[0] = (cl_uint) log2(partition_size[0]);

    for (int n = 1; n < (num_iter + 1); n++){
        cur_num_active_threads[n] = cur_num_active_threads[n-1] >> data_red;
        cur_num_active_threads[n] += cur_num_active_threads[n] == 0;
        partition_size[n] = cur_num_active_threads[n-1]/cur_num_active_threads[n];
        lvl_size[n] = cur_num_active_threads[n] * partition_size[n];
        stride[n] = stride[n-1] + lvl_size[n-1];
        partition_size_bits[n] = log2(partition_size[n]);

    }



    std::stringstream filename;
    filename << "kernels/prefix_settings.cl";
    std::ofstream out(filename.str());
    INSERTARR(out,partition_size_bits,"PARTITION_SIZE_BITS",(num_iter+1));
    INSERTARR(out,lvl_size,"LVL_SIZE",(num_iter+1));
    INSERTARR(out,cur_num_active_threads,"NUM_ACTIVE_THREADS",(num_iter+1));
    INSERTARR(out,stride,"STRIDE_DATA",(num_iter+1));
    INSERTELEM(out,num_iter,"NUM_ITER");
    INSERTELEM(out,num_iter+1,"NUM_ELEM");
    INSERTELEM(out,data_size,"DATA_SIZE");
    INSERTELEM(out,data_red,"DATA_RED");
    out.close();
    for (int u = 0; u < num_iter; u++){
        for(int lid = 0; lid < LOCAL_SIZE; lid ++){
            if(lid < cur_num_active_threads[u] ){

                cl_uint start    = stride[u] + (lid << partition_size_bits[u]) + 1;
                cl_uint stop     = stride[u] + ((lid+1) << partition_size_bits[u]) - 1;


                for(cl_uint n = start; n <= stop; n ++ )
                    dataComp[n] = dataComp[n] + dataComp[n-1];

                cl_uint next_start    = (stride[u+1] + lid);
                dataComp[next_start] = dataComp[stop];

            }

        }
	}
    for (int u = (num_iter-2); u > -1; u--){
        for(int lid = 0; lid < 256; lid ++){
            if(lid < lvl_size[u] ){
                cl_uint str = stride[u+1];
                cl_uint ps = lid >> partition_size_bits[u];
                cl_uint idx_offset = str + ps;
                cl_uint offset = dataComp[idx_offset-1]* (ps != 0);
                str = stride[u];
                cl_uint data_offset = str + lid * ( 1 << ((data_red-1) >> partition_size_bits[u]));
                dataComp[data_offset] += offset;
            }
        }
    }
    cl_int res = 0;
    for(int i = 1; i < PSIZE; i++)
        res += resTrue[i] - dataComp[i];

    printf("Reuslt: %d\n", res);
	dumpData(dataComp,"resultComp");
    if(res != 0){
        printf("invalid prefix parameter settings");
        exit(-1);
    }


}
