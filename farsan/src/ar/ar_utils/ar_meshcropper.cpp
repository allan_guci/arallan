#include "ar_meshcropper.h"

ARMeshCropper::ARMeshCropper()
{
}

void ARMeshCropper::initAR(cl::Context context){
    initCL(context);
}

void ARMeshCropper::initBuffers(){
    if(!mGLMesh.isActive())
        prepareGLMesh(getInputMesh());

    mCroppedMesh = MeshType(mGLMesh,getContext());

    initTraverseType(mTraverseVert,mGLMesh.sNumVertices);
    initTraverseType(mTraverseTri,mGLMesh.sNumIndices/3);

    mMemTriangleNew       = mCroppedMesh.sVertexIndex;
    mMemVertexPositionNew = mCroppedMesh.sVertexPosition;
    mMemVertexNormalNew   = mCroppedMesh.sVertexNormal;
    mMemVertexTexCoordNew = mCroppedMesh.sVertexTexCoord;

    mMemMask    = CreateBuffer<cl_char>(mGLMesh.sNumVertices);
    mMemMaskTri = CreateBuffer<cl_char>(mGLMesh.sNumIndices/3);

    setOutputMesh(mCroppedMesh);
    setCLOutput(mCroppedMesh.sVertexPosition);
    setGLOutputBuffer(mGLMesh);
}

void ARMeshCropper::configureKernels(){

    size_t numTriangles     = mGLMesh.sNumIndices/3;
    size_t numVertices      = mGLMesh.sNumVertices;

    MeshType inMesh = getInputMesh();

    mCreateMaskKernel = CreateKernel("create_mask",ARSize(numVertices),ARSize(0));
    mCreateMaskKernel->arg(inMesh.sVertexPosition);

    mCreateMaskKernel->arg(mTraverseVert.sLevelMem[0]);
    mCreateMaskKernel->arg(mMemMask);
    mCreateMaskKernel->arg(inMesh.sNumVertices);

    mCreateTriangleMaskKernel = CreateKernel("create_triangle_mask",ARSize(numTriangles),ARSize(0));
    mCreateTriangleMaskKernel->arg(mMemMask);
    mCreateTriangleMaskKernel->arg(mMemMaskTri);
    mCreateTriangleMaskKernel->arg(inMesh.sVertexIndex);
    mCreateTriangleMaskKernel->arg(mTraverseTri.sLevelMem[0]);

    mTravUpKernel = CreateKernel("trav_up",ARSize(numVertices),ARSize(0));
    mTravDownKernel = CreateKernel("trav_down",ARSize(numVertices),ARSize(0));

    mMapVertexPositionKernel = CreateKernel("map_vertex_buffer4",ARSize(inMesh.sNumVertices),ARSize(0));
    mMapVertexPositionKernel->arg(mMemMask);
    mMapVertexPositionKernel->arg(mTraverseVert.sLevelMem[0]);
    mMapVertexPositionKernel->arg(inMesh.sVertexPosition);
    mMapVertexPositionKernel->arg(mMemVertexPositionNew);

    if(inMesh.sVertexNormal.active()){
        mMapVertexNormalKernel = CreateKernel("map_vertex_buffer4",ARSize(inMesh.sNumVertices),ARSize(0));
        mMapVertexNormalKernel->arg(mMemMask);
        mMapVertexNormalKernel->arg(mTraverseVert.sLevelMem[0]);
        mMapVertexNormalKernel->arg(inMesh.sVertexNormal);
        mMapVertexNormalKernel->arg(mMemVertexNormalNew);
    }

    if(inMesh.sVertexTexCoord.active()){
        mMapVertexTexCoordKernel = CreateKernel("map_vertex_buffer2",ARSize(inMesh.sNumVertices),ARSize(0));
        mMapVertexTexCoordKernel->arg(mMemMask);
        mMapVertexTexCoordKernel->arg(mTraverseVert.sLevelMem[0]);
        mMapVertexTexCoordKernel->arg(inMesh.sVertexTexCoord);
        mMapVertexTexCoordKernel->arg(mMemVertexTexCoordNew);
    }

    mMapTriangleKernel = CreateKernel("map_triangle",ARSize(numTriangles),ARSize(0));
    mMapTriangleKernel->arg(mMemMaskTri);
    mMapTriangleKernel->arg(mTraverseVert.sLevelMem[0]);
    mMapTriangleKernel->arg(mTraverseTri.sLevelMem[0]);
    mMapTriangleKernel->arg(inMesh.sVertexIndex);
    mMapTriangleKernel->arg(mMemTriangleNew);

}

void ARMeshCropper::initCL(cl::Context context){

    setup(context);

    /// Set pre-defines LOCAL_SIZE used by traversal kernels
    addMacro("LOCAL_SIZE",CL_LOCAL_SIZE);
    addMacro("LOCAL_MEM_SIZE",CL_LOCAL_MEM_SIZE);

    CreateProgram("kernels/meshify.cl");
    initBuffers();
    configureKernels();

    runCL();


}

void ARMeshCropper::travUpDown(TraverseType trav){
    for(int i = 0; i < trav.sNumLevels; i++){
        mTravUpKernel->threadDimension(ARSize(trav.sLevelThreads[i]),ARSize(CL_LOCAL_SIZE));
        mTravUpKernel->setArgument(0,trav.sLevelMem[i]);
        mTravUpKernel->setArgument(1,trav.sLevelMem[i+1]);
        mTravUpKernel->setArgument(2,trav.sLevelSizes[i]);
        mTravUpKernel->run(BLOCKED_RUN);

    }

    for(int i = trav.sNumLevels-2; i > -1; i--){
        mTravDownKernel->threadDimension(ARSize(trav.sLevelThreads[i]),ARSize(CL_LOCAL_SIZE));
        mTravDownKernel->setArgument(0,trav.sLevelMem[i]);
        mTravDownKernel->setArgument(1,trav.sLevelMem[i+1]);
        mTravDownKernel->setArgument(2,trav.sLevelSizes[i]);
        mTravDownKernel->run(BLOCKED_RUN);
    }

}

void ARMeshCropper::runCL(){
    bindGLMem();

    mCreateMaskKernel->run(BLOCKED_RUN);
    mCreateTriangleMaskKernel->run(BLOCKED_RUN);

    travUpDown(mTraverseVert);
    mMapVertexPositionKernel->run(BLOCKED_RUN);
/*
    if(mMemVertexNormalNew.active())
        mMapVertexNormalKernel->run(BLOCKED_RUN);

    if(mMemVertexTexCoordNew.active())
        mMapVertexTexCoordKernel->run(BLOCKED_RUN);
*/
    travUpDown(mTraverseTri);
    mMapTriangleKernel->run(BLOCKED_RUN);

    sync();
    releaseGLMem();
    updateMeshData();


}

void ARMeshCropper::updateMeshData(){

    cl_int * numTriangles    = mTraverseTri.sLevelMem[mTraverseTri.sNumLevels].toHost<cl_int>();
    cl_int * numVerticesArr  = mTraverseVert.sLevelMem[mTraverseVert.sNumLevels].toHost<cl_int>();

    size_t numIndices  = numTriangles[0]*3;
    size_t numVertices = numVerticesArr[0];



    mCroppedMesh.sNumIndices  = numIndices;
    mCroppedMesh.sNumVertices = numVertices;
    *mCroppedMesh.sNumIndicesPtr  = numIndices;
    *mCroppedMesh.sNumVerticesPtr = numVertices;

}

void ARMeshCropper::initTraverseType(TraverseType& trav,size_t initSize){
    size_t curSize = initSize;
    size_t locSize = CL_LOCAL_SIZE;
    trav.sNumLevels = 0;
    for(int i = 0; i < 5; i ++){

        trav.sLevelSizes.push_back(curSize);
        trav.sLevelThreads.push_back(ceil((float)curSize/locSize)*locSize);

        CLMemory mem = CreateBuffer<cl_int>(curSize);
        trav.sLevelMem.push_back(mem);

        if(curSize == 1)
            break;

        curSize  = ceil((float)curSize/locSize);
        trav.sNumLevels++;
    }

}
