#ifndef _AR_LIGHT_VOLUME_H
#define _AR_LIGHT_VOLUME_H

#include "ar_instance_base.h"
#include "grid_instance_base.h"
class ARLightVolume : public ARInstanceBase, public GridInstanceBase
{
public:
	ARLightVolume();
    void initAR(cl::Context context);
    void initGL();
    void drawGL(UserCamera * uc);

	private:



};

#endif
