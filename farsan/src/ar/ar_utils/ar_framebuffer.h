#ifndef _AR_FRAMEBUFFER_H
#define _AR_FRAMEBUFFER_H

#include "ar_instance_base.h"

class ARFrameBuffer : public ARInstanceBase
{
public:
	ARFrameBuffer(RenderTargetType type,ARSize size);

	void initAR(cl::Context context);
	void initGL();


    void enable();
    void disable(UserCamera * uc);


	private:
    RenderTargetType mRenderTargetType;
    ARSize mRenderTargetSize;


};

#endif
