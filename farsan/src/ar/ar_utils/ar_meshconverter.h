#ifndef _AR_MESHCONVERTER_H
#define _AR_MESHCONVERTER_H

#include "ar_instance_base.h"

class ARMeshConverter : public ARInstanceBase
{
public:
	ARMeshConverter();
    void initAR(cl::Context context);
    void initCL(cl::Context context);

    void initGL(){};
    void runCL(){};
    void drawGL(UserCamera * uc){};

	private:
	    void upload(size_t numVertices, size_t stride, GLHelper::Buffer bufferGL, CLMemory& bufferCL,GLBufferItem item);
	    void uploadCpy(size_t numVertices, size_t stride, GLHelper::Buffer bufferGL, CLMemory& bufferCL,GLBufferItem item);
        CLKernel * mKernelCopy;
        MeshType mMesh;

};

#endif
