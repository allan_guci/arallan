#include "ar_gridifier.h"

ARGridifier::ARGridifier(): mDrawEnabled(false)
{

}


void ARGridifier::initAR(cl::Context context){
/*
    checkNonZero("Grid dimension",mGridDim,3);
    checkNonZero("Grid scale",mGridScale,3);
    checkNonZero("DepthMap size",mDepthMapDim,2);
*/
    initCL(context);
    initGL();

}

void ARGridifier::initCL(cl::Context context){

    setup(context);

    CreateProgram("kernels/gridify.cl");

    ARSize size = getGridDim();

    mGLGridMem = CreateGLTexture(GL_R8,GL_RED,size);

    ImageOptions opt(cl::ImageFormat(CL_R,CL_UNSIGNED_INT8),size);

    mGridMem = CreateImageFromGL(mGLGridMem,opt);

    ARSize gridDim         = getGridDim();
    ARSize globalThreadDim = gridDim;
    ARSize localThreadDim(0);


    mClearKernel = CreateKernel("clear_grid",globalThreadDim,localThreadDim);
    mClearKernel->arg(mGridMem);
    mClearKernel->arg(gridDim.dataI(),sizeof(size_t)*4);

    globalThreadDim  = ARSize(mDepthMapDim[0],mDepthMapDim[1]);

    mGridfyKernel = CreateKernel("gridify",globalThreadDim,localThreadDim);

    CL_MEM vboPosition = getCLInput();

    mGridfyKernel->arg(vboPosition);
    mGridfyKernel->arg(mGridMem);
    mGridfyKernel->arg(mDepthMapDim,sizeof(size_t)*2);

    mGridfyKernel->arg(getGridScaleMem(),sizeof(cl_float)*4);
    mGridfyKernel->arg(getGridOffsetMem(),sizeof(cl_float)*4);
    mGridfyKernel->arg(getGridDimMem(),sizeof(size_t)*4);

    setCLOutput(mGridMem);
}

void ARGridifier::runCL(){
    bindGLMem();
    mClearKernel->run();
    mGridfyKernel->run();
    releaseGLMem();
    sync();
  /*  memToFile("GRID.csv",mGridMem,mGridDim[0],mGridDim[1],mGridDim[2]);
    exit(-1);*/
}


void ARGridifier::initGL(){

    createShader("shaders/smoke/raycast.vert", "shaders/smoke/raycast.frag");

    loadModel("models/cube2.obj",false,false);

    GLfloat eye[] = {0,1,0};
    setUniformFloat(eye,"u_Eye",3);

    setTexture(mGLGridMem,"u_Texture",GL_TEXTURE_3D);
    setTexture(getGLInputTexture(),"u_Depth",GL_TEXTURE_2D);

    MMTranslate(getGridOffsetMem());
    MMScale(getGridScaleMem());

    MMUpload();

    setBlend(true);
}

void ARGridifier::enableDraw(bool b){
    mDrawEnabled = b;
}

void ARGridifier::drawGL(UserCamera* uc){
    if(mDrawEnabled){
        GLfloat eye[] = {uc->getEye().x,uc->getEye().y,uc->getEye().z};
        setUniformFloat(eye,"u_Eye",3);

        draw(uc);
    }
}
