#ifndef _AR_GRIDIFIER_H
#define _AR_GRIDIFIER_H

#include "ar_instance_base.h"
#include "grid_instance_base.h"
class ARGridifier : public ARInstanceBase, public GridInstanceBase
{
public:
	ARGridifier();

	void initAR(cl::Context context);
	void initCL(cl::Context context);
	void initGL(cl::Context context);

	void runCL();
	void drawGL(UserCamera* uc);

    void enableDraw(bool);

    void setDepthMapSize(size_t w,size_t h){
        mDepthMapDim[0] = w;
        mDepthMapDim[1] = h;
    }

	private:

    void initGL();

    CL_MEM mGridMem;
    GLuint mGLGridMem;

    CLKernel* mClearKernel;
    CLKernel* mGridfyKernel;

    bool mDrawEnabled;

    size_t mDepthMapDim[2];

};

#endif
