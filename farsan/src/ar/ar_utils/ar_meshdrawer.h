#ifndef _AR_MESHDRAWER_H
#define _AR_MESHDRAWER_H

#include "ar_instance_base.h"

class ARMeshDrawer : public ARInstanceBase
{
public:
	ARMeshDrawer();
    void initAR(cl::Context context);
    void initGL();
    void drawGL(UserCamera * uc);

	private:

};

#endif
