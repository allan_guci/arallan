#include "ar_framebuffer.h"


ARFrameBuffer::ARFrameBuffer(RenderTargetType type,ARSize size){
    mRenderTargetType = type;
    mRenderTargetSize = size;
}

void ARFrameBuffer::initAR(cl::Context context){
    initGL();
}

void ARFrameBuffer::enable(){
    enableRenderTarget();
}

void ARFrameBuffer::disable(UserCamera * uc){
    disableRenderTarget(ARSize(uc->mScreenWidth,uc->mScreenHeight));
}

void ARFrameBuffer::initGL(){
    createRenderTarget(mRenderTargetType,mRenderTargetSize);
    useRenderTarget(true);
}

