#ifndef _AR_LIGHT_PERSPECTIVE_H
#define _AR_LIGHT_PERSPECTIVE_H

#include "ar_instance_base.h"
#include "grid_instance_base.h"
class ARLightPerspective : public ARInstanceBase, public GridInstanceBase
{
public:
	ARLightPerspective();
    void initAR(cl::Context context);
    void initGL();
    void drawGL(UserCamera * uc);
    UserCamera * getLightCamera();

	private:

        UserCamera * mLightCamera;

};

#endif
