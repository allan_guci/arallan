#include "ar_meshcollapser.h"


ARMeshCollapser::ARMeshCollapser() : mKernelCollapsePosition(NULL),mKernelCollapseNormal(NULL),mKernelCollapseTexCoord(NULL),mKernelCollapseIndex(NULL)
{
    mCollapseFactor = 2;
}

void ARMeshCollapser::initAR(cl::Context context){
    initCL(context);
}

void ARMeshCollapser::initBuffers(){
    MeshType inMesh = getInputMesh();

    size_t numIndices   = inMesh.sNumIndices;
    size_t numVertices  = inMesh.sNumVertices;

    cl_uint xOrg = mOriginalDimension.dx();
    cl_uint yOrg = mOriginalDimension.dy();

    cl_uint xNew = xOrg/mCollapseFactor;
    cl_uint yNew = yOrg/mCollapseFactor;

    size_t numNewVertices  = xNew*yNew;
    size_t numNewIndices   = (yNew-1)*(xNew-1)*6;


    mCollapsedMesh.sVertexPosition = CreateBuffer<cl_float>(4*numNewVertices);

    if(inMesh.sVertexNormal.active())
        mCollapsedMesh.sVertexNormal = CreateBuffer<cl_float>(4*numNewVertices);

    if(inMesh.sVertexTexCoord.active())
        mCollapsedMesh.sVertexTexCoord = CreateBuffer<cl_float>(2*numNewVertices);


    mCollapsedMesh.sVertexIndex    = CreateBuffer<cl_int>(numNewIndices);

    mCollapsedMesh.setMeta(numNewIndices,numNewVertices);

    setOutputMesh(mCollapsedMesh);
}

void ARMeshCollapser::initCL(cl::Context context){
	setup(context);
	initBuffers();

    ARSize globalThreadDim = ARSize(mOriginalDimension.dx()/mCollapseFactor,mOriginalDimension.dy()/mCollapseFactor);
    MeshType inMesh = getInputMesh();

    CreateProgram("kernels/meshcollapse.cl");

    cl_int collapsedWidth = mOriginalDimension.dx()/mCollapseFactor;

    mKernelCollapsePosition = CreateKernel("collapse4",globalThreadDim,ARSize(0));
    mKernelCollapsePosition->arg(inMesh.sVertexPosition);
    mKernelCollapsePosition->arg(mCollapsedMesh.sVertexPosition);
    mKernelCollapsePosition->arg((cl_uint)collapsedWidth);
    mKernelCollapsePosition->arg((cl_uint)mCollapseFactor);

	if(mCollapsedMesh.sVertexNormal.active()){
        mKernelCollapseNormal = CreateKernel("collapse4",globalThreadDim,ARSize(0));
        mKernelCollapseNormal->arg(inMesh.sVertexNormal);
        mKernelCollapseNormal->arg(mCollapsedMesh.sVertexNormal);
        mKernelCollapseNormal->arg((cl_uint)collapsedWidth);
        mKernelCollapseNormal->arg((cl_uint)mCollapseFactor);
    }
    if(mCollapsedMesh.sVertexTexCoord.active()){
        mKernelCollapseTexCoord = CreateKernel("collapse2",globalThreadDim,ARSize(0));
        mKernelCollapseTexCoord->arg(inMesh.sVertexTexCoord);
        mKernelCollapseTexCoord->arg(mCollapsedMesh.sVertexTexCoord);
        mKernelCollapseTexCoord->arg((cl_uint)collapsedWidth);
        mKernelCollapseTexCoord->arg((cl_uint)mCollapseFactor);
    }
    ARSize globalThreadDim2 = ARSize(globalThreadDim.dx()-1,globalThreadDim.dy()-1);

    mKernelCollapseIndex = CreateKernel("collapse_tri",globalThreadDim2,ARSize(0));
    mKernelCollapseIndex->arg(mCollapsedMesh.sVertexIndex);
    mKernelCollapseIndex->arg((cl_uint)collapsedWidth);


}

void ARMeshCollapser::setOriginalDimension(ARSize d){
    mOriginalDimension = d;
}

void ARMeshCollapser::runCL(){
    bindGLMem();
    mKernelCollapsePosition->run();
    if(mKernelCollapseNormal != NULL)
        mKernelCollapseNormal->run();

    if(mKernelCollapseNormal != NULL)
        mKernelCollapseNormal->run();

    if(mKernelCollapseTexCoord!= NULL)
        mKernelCollapseTexCoord->run();

    mKernelCollapseIndex->run();

   sync();
   releaseGLMem();
}

