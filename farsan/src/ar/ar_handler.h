#ifndef _AR_HANDLER_H
#define _AR_HANDLER_H


#include "ar_program_cloth.h"
#include "ar_program_smoke.h"
#include "ar_program_debug.h"
#include "ar_program_underwater.h"


class ARHandler {
public:
	ARHandler();


	void init(cl::Context& context);
    void runCL();
	void drawGL(UserCamera* uc);

    void setGUI(GUIDB* gui){ mGui = gui; }

	private:

	    typedef enum{
            CLOTH_PROGRAM,
            SMOKE_PROGRAM,
            DEBUG_PROGRAM,
            UNDER_WATER_PROGRAM
	    }ProgramType;

    ARProgram * selectProgram(ProgramType p,cl::Context& context);

    ARProgram * mCurrentProgram;
    GUIDB * mGui;





};

#endif
