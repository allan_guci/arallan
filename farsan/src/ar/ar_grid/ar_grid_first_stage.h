#ifndef _AR_GRID_FIRST_STAGE_H
#define _AR_GRID_FIRST_STAGE_H

#include "ar_instance_base.h"
#include "grid_instance_base.h"
#include "ar_prefix_sum.h"
#include "ar_cluster_groups.h"


class ARGridFirstStage: public ARInstanceBase , public GridInstanceBase
{

public:

	ARGridFirstStage();

	void initAR(cl::Context context);

	void runCL();

	void initCL(cl::Context context);

	size_t numActiveLeaves();


protected:

private:

    ARPrefixSum * mPrefixSum;
    ARClusterGroups* mClusterGroups;
    cl_uint mNumActiveLeaves;

};
#endif
