#ifndef _AR_GRID_H
#define _AR_GRID_H

#include "ar_instance_base.h"
#include "grid_instance_base.h"
#include "ar_grid_first_stage.h"
#include "ar_grid_static_init.h"
#include "ar_grid_reorder.h"
#define SIZEGR 512
#define OUTTYPE float
    typedef struct{
            cl_uchar  is_leaf;
            cl_uchar  level;
            cl_uchar  parent;
            cl_uchar  center;
            cl_ushort  num_leaves;
            cl_ushort children[4];
            cl_ushort4  bbSquare;
            cl_float value;
    }NodeType;
class ARGrid: public ARInstanceBase, public GridInstanceBase
{
public:
	ARGrid();
    void initAR(cl::Context context);
    void initCL(cl::Context context);
    void runCL();

private:
	    void debugPsum();
	    float time;
};

#endif

