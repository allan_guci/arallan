#include "ar_grid.h"

#include <unistd.h>
#include <math.h>
#include "ar_timer.h"
static void checkResult(CLMemory metaMem, CLMemory treeMem, CLMemory img, CLMemory imgDebug, CLMemory dirLeaves);
ARGrid::ARGrid(){
    time = 0.0f;
}

void ARGrid::initAR(cl::Context context){
    initGL();
    initCL(context);
}

void ARGrid::initCL(cl::Context context){

	setup(context);

    CreateProgram("kernels/adaptive_grid.cl");

    size_t workGroupSize = 256;
    size_t localTreeSize = 341;
    size_t numGroups = getGridDim().dSize()/workGroupSize;
    size_t globalTreeSize = localTreeSize*numGroups;

    CLMemory treeMem     = CreateBuffer<NodeType>(globalTreeSize);
    CLMemory treeDirectLeaves  = CreateBuffer<cl_uint>(globalTreeSize);
    CLMemory treeLeavesSizes = CreateBuffer<int>(numGroups);

    ImageOptions optR(cl::ImageFormat(CL_R,CL_FLOAT),ARSize(SIZEGR,SIZEGR));
    CLMemory imgDebug = CreateImage(optR);

    ARGridStaticInit gridStaticInit;
    gridStaticInit.setGridDimension(getGridDim());
    gridStaticInit.setInput(treeMem);
    gridStaticInit.setInput(treeLeavesSizes);
    gridStaticInit.setInput(treeDirectLeaves);
    gridStaticInit.initAR(context);


    size_t workGroupSizeAdjusted = workGroupSize;
    if(numGroups < workGroupSizeAdjusted) workGroupSizeAdjusted = numGroups;
    ARGridFirstStage gridFirstStage;
    gridFirstStage.setInput(treeMem);
    gridFirstStage.setInput(treeLeavesSizes);
    gridFirstStage.setInput(treeDirectLeaves);
    gridFirstStage.setInput(imgDebug);
    gridFirstStage.setGridDimension(ARSize(numGroups,workGroupSizeAdjusted,workGroupSize)); // FIXME
    gridFirstStage.initAR(context);


    CLMemory mem1, mem2;
    gridStaticInit.gief(mem1,mem2);

    ARGridReorder gridReorder;
    gridReorder.setInput(treeMem);
    gridReorder.setInput(treeLeavesSizes);
    gridReorder.setInput(treeDirectLeaves);
    gridReorder.setInput(mem1);
    gridReorder.setInput(mem2);
    gridReorder.setGridDimension(getGridDim());
    gridReorder.initAR(context);

    getTimer() << gridStaticInit.getTimer();
    getTimer() << gridFirstStage.getTimer();
    getTimer() << gridReorder.getTimer();
    if(true){

        gridStaticInit.initalRun();
        gridFirstStage.runCL();
        gridStaticInit.runCL();
        //checkResult(treeDirectLeaves, treeMem, gridStaticInit.getCLOutput(),imgDebug,treeDirectLeaves);
        gridReorder.runCL();

         getTimer().print();
        exit(-1);
    }
}

void ARGrid::runCL()
{
    RunKernels();

    time += 0.001f;
    GetKernel("update")->setArgument(1,time);

}

static void writeNodeData(NodeType& node,OUTTYPE dataPtr[SIZEGR][SIZEGR]){
    cl_ushort4 bbSquare = node.bbSquare;

    int xstart = bbSquare.s[0];
    int xstop  = bbSquare.s[2];
    int ystart = bbSquare.s[1];
    int ystop  = bbSquare.s[3];
    for(int y = ystart; y < ystop; y++)
        for(int x = xstart; x < xstop; x++){
            dataPtr[y][x] = node.value;
        }

}


static void checkResult(CLMemory metaMem, CLMemory treeMem, CLMemory img, CLMemory imgDebug, CLMemory dirLeaves)
{
    bool writeToFile = true;

    if( img.size()/4 != (SIZEGR*SIZEGR) ){
        printf("checkResult: invalid image size");
        exit(-1);
    }

    auto * imgPtr = img.toHost<cl_float>();
    auto coverageImg = new OUTTYPE[SIZEGR][SIZEGR];
    for( int i = 0; i < (int)img.size(); i += 4){
        int  iflat = i / 4;
        int x = iflat % SIZEGR;
        int y = iflat / SIZEGR;
        coverageImg[y][x] = imgPtr[i];

    }

    auto * imgPtrDevice = imgDebug.toHost<cl_float>();
    auto coverageImgDebug = new OUTTYPE[SIZEGR][SIZEGR];
    for( int i = 0; i < (int)imgDebug.size(); i ++){
        int  iflat = i;
        int x = iflat % SIZEGR;
        int y = iflat / SIZEGR;
        coverageImgDebug[y][x] = imgPtrDevice[i];
    }

    auto coverage = new OUTTYPE[SIZEGR][SIZEGR];
    for(int y = 0; y < SIZEGR; y++)
        for(int x = 0; x < SIZEGR; x++)
            coverage[y][x] = 0;

    int* groupSizes = metaMem.toHost<int>();
    auto* treePtr = treeMem.toHost<NodeType>();
    int totNumLeaves = 0;
    int numGroups = SIZEGR*SIZEGR/256;

    auto* directLeaves = dirLeaves.toHost<cl_uint>();
    for(int group = 0; group < numGroups; group++){
        int o =  group*341;
        NodeType subRoot = treePtr[o];
        int numLeaves = subRoot.num_leaves;
        totNumLeaves += numLeaves;
        for(int n = 0; n < numLeaves; n++){
            cl_uint leaf_index = directLeaves[o+n];
            NodeType node = treePtr[leaf_index];
            writeNodeData(node,coverage);
        }
    }
    OUTTYPE errSum1 = 0;
    OUTTYPE errSum2 = 0;
    for(int y= 0; y < SIZEGR; y++)
        for(int x= 0; x < SIZEGR; x++){
            errSum1 += coverage[y][x] - coverageImg[y][x];
            errSum2 += coverageImgDebug[y][x] - coverageImg[y][x];

    }

    printf("check result: errsum host = %f\n",errSum1);
    printf("check result: errsum device = %f\n",errSum2);
    printf("Total number of leaves: %d\n",totNumLeaves);


    if(writeToFile){
        std::ofstream outTree("coverage_tree.csv");
        for(int y= 0; y < SIZEGR; y++)
            for(int x= 0; x < SIZEGR; x++){
                outTree << coverage[y][x];
                if(x == SIZEGR-1){
                    outTree << "\n";
                }else{
                    outTree << ",";
                }
            }
        outTree.close();

        std::ofstream outImgHost("coverage_img_host.csv");
        for(int y= 0; y < SIZEGR; y++)
            for(int x= 0; x < SIZEGR; x++){
                outImgHost << coverageImg[y][x];
                if(x == SIZEGR-1){
                    outImgHost << "\n";
                }else{
                    outImgHost << ",";
                }
            }
        outImgHost.close();

        std::ofstream outImgDevice("coverage_img_device.csv");
        for(int y= 0; y < SIZEGR; y++)
            for(int x= 0; x < SIZEGR; x++){
                outImgDevice << coverageImgDebug[y][x];
                if(x == SIZEGR-1){
                    outImgDevice << "\n";
                }else{
                    outImgDevice << ",";
                }
            }
        outImgDevice.close();

    }


}







