#include "ar_grid_first_stage.h"

ARGridFirstStage::ARGridFirstStage()
{

}

void ARGridFirstStage::initAR(cl::Context context){
    initCL(context);
}

void ARGridFirstStage::initCL(cl::Context context){

    size_t globalSize  = getGridDim().dx();
    size_t localSize   = getGridDim().dy();
    addMacro("X_DIM_GLOBAL",(cl_uint)globalSize);
    setup(context);
    CreateProgram("kernels/grid_stage_0.cl");


    ///
    NumGlobalThreads(ARSize(globalSize));
    NumLocalThreads(ARSize(localSize));

    ///
    CLMemory treeMem           = getInput("treeMem");
    CLMemory treeLeavesSizes = getInput("treeLeavesSizes");
    CLMemory treeDirectLeaves  = getInput("treeDirectLeaves");
    CLMemory imgDebug          = getInput("imgDebug");

    ///
    mPrefixSum = new ARPrefixSum(treeLeavesSizes,ARSize(globalSize),ARPrefixSum::SumSizes);
    mPrefixSum->initAR(context);

    ///
    mClusterGroups = new ARClusterGroups(treeLeavesSizes,256);
    mClusterGroups->initAR(context);
    CLMemory groupIntervalMem = mClusterGroups->getCLOutput();

    getTimer() << mPrefixSum->getTimer();
    getTimer() << mClusterGroups->getTimer();

    ///
    CLKernel* packTreeKernel = CreateKernel("kernel_find_neighbors");
    packTreeKernel->arg(treeMem);
    packTreeKernel->arg(treeLeavesSizes);
    packTreeKernel->arg(treeDirectLeaves);
    packTreeKernel->arg(groupIntervalMem);
    packTreeKernel->arg((cl_uint)0);
    packTreeKernel->arg(imgDebug);


}

size_t ARGridFirstStage::numActiveLeaves(){
    return mNumActiveLeaves;
}

void ARGridFirstStage::runCL(){

    mPrefixSum->runCL();
    mClusterGroups->runCL();
    mNumActiveLeaves = mPrefixSum->getTotalSum();
    printf("num leaves: %d\n", mNumActiveLeaves);
    size_t workGroupSize = getGridDim().dz();
    size_t globalSize = workGroupSize*(mClusterGroups->numGroupsUsed());

    GetKernel("kernel_find_neighbors")->threadDimension(ARSize(globalSize),ARSize(workGroupSize));
    GetKernel("kernel_find_neighbors")->setArgument(4,(cl_uint)mNumActiveLeaves);
    GetKernel("kernel_find_neighbors")->run(true);

}


