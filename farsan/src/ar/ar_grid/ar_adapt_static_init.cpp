#include "ar_adapt_static_init.h"

ARAdaptStaticInit::ARAdaptStaticInit(CLMemory treeMetaMem, CLMemory treeMem, CLMemory treeNumLeaves){
    mTreeMetaMem = treeMetaMem;
    mTreeMem = treeMem;
    mNumLeavesMem = treeNumLeaves;
}

void ARAdaptStaticInit::initAR(cl::Context context){
    initCL(context);
}

void ARAdaptStaticInit::initCL(cl::Context context){

    setup(context);
    CreateProgram("kernels/adaptive_grid.cl");

    /// Allocate Memory
    GLuint outputTexture = GLHelper::Texture::create2DTexture(getGridDim().dx(),getGridDim().dy(),NULL,GL_R32F,GL_RGBA);
    setGLOutputTexture(outputTexture);

    ImageOptions optRGBA(cl::ImageFormat(CL_RGBA,CL_FLOAT),getGridDim());
    CLMemory imageOut = CreateImageFromGL(outputTexture,optRGBA);

    CLMemory original1 = CreateImage(optRGBA);
    CLMemory original2 = CreateImage(optRGBA);
    CLMemory debugMem  = CreateImage(optRGBA);

    CLMemory treeNodesMetaMemNext = getInput("treeNodesMetaMemNext");
    CLMemory treeDirectLeaves     = getInput("treeDirectLeaves");
    /// Set cl output
    setCLOutput(debugMem);

    /// Create Kernels
    NumGlobalThreads(getGridDim());
    NumLocalThreads(ARSize(16,16));

    const char * updateLabel = "update";
    CLKernel* update   = CreateKernel(updateLabel);
    CLKernel* ppUpdate = CreateKernel(updateLabel);
    update->arg(original1);
    ppUpdate->arg(original2);
    update->createPingPong(ppUpdate);
    update->arg(0.f);
    AddKernel(update);

    const char * laplacianLabel = "laplacian";
    CLKernel* laplacianKernel = CreateKernel(laplacianLabel);
    CLKernel* ppTemp          = CreateKernel(laplacianLabel);

    laplacianKernel->createPingPong(ppTemp,original1,original2);
    AddKernel(laplacianKernel);

    const char * initTreeLabel = "init_tree_layer";
    CLKernel * initTreeLayerKernel = CreateKernel(initTreeLabel);
    CLKernel * ppInitTree = CreateKernel(initTreeLabel);
    initTreeLayerKernel->createPingPong(ppInitTree,original2,original1);
    initTreeLayerKernel->arg(debugMem);
    initTreeLayerKernel->arg(mTreeMem);
    initTreeLayerKernel->arg(mTreeMetaMem);
    initTreeLayerKernel->arg(mNumLeavesMem);
    initTreeLayerKernel->arg(treeNodesMetaMemNext);
    initTreeLayerKernel->arg(treeDirectLeaves);

    AddKernel(initTreeLayerKernel);

}


void ARAdaptStaticInit::runCL(){
    RunKernels(true);
    sync();
}

