#ifndef _AR_CLUSTER_GROUPS_H
#define _AR_CLUSTER_GROUPS_H

#include "grid_instance_base.h"
#include "ar_instance_base.h"
#include "ar_prefix_sum.h"

class ARClusterGroups: public ARInstanceBase, public GridInstanceBase
{

public:

	ARClusterGroups(CLMemory data, cl_uint itemsToCover);

	void initCL(cl::Context context);
	void runCL();
	void initAR(cl::Context context);
    size_t numGroupsUsed();

protected:

private:

	void createClusterKernel();
    void createPackKernel();
    ARPrefixSum* mPrefixSum;

    CLMemory mGroupIntervalMem1;
    CLMemory mGroupIntervalMem2;
    CLMemory mIntervalSizeMem;
    CLMemory mIntervalMaskMem;
    cl_uint mItemsToCover;
};
#endif
