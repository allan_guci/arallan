#include "ar_adaptive_grid.h"
#include <unistd.h>
#include <math.h>
#include "ar_adapt_second_stage.h"
#include "ar_timer.h"
ARAdaptiveGrid::ARAdaptiveGrid(){
    time = 0.0f;
}

void ARAdaptiveGrid::initAR(cl::Context context){
    initGL();
    initCL(context);
}

void ARAdaptiveGrid::initCL(cl::Context context){

	setup(context);

    CreateProgram("kernels/adaptive_grid.cl");

    size_t workGroupSize = 256;
    size_t localTreeSize = 341;
    size_t numGroups = getGridDim().dSize()/workGroupSize;
    size_t globalTreeSize = localTreeSize*numGroups;

    CLMemory treeMemSparse     = CreateBuffer<NodeType>(globalTreeSize);
    CLMemory treeMemDense      = CreateBuffer<NodeType>(globalTreeSize);
    CLMemory treeDirectLeaves  = CreateBuffer<cl_uint>(globalTreeSize);
    CLMemory treeNodesMetaMem  = CreateBuffer<int>(numGroups);
    CLMemory treeLeavesMetaMem = CreateBuffer<int>(numGroups);
    CLMemory treeNodesMetaMemNext = CreateBuffer<int>(numGroups);

    ImageOptions optR(cl::ImageFormat(CL_R,CL_FLOAT),ARSize(SIZEGR,SIZEGR));
    CLMemory imgDebug = CreateImage(optR);

    setInput(treeNodesMetaMem);

    ARAdaptStaticInit adaptStaticInit(treeNodesMetaMem,treeMemSparse,treeLeavesMetaMem);
    adaptStaticInit.setGridDimension(getGridDim());
    adaptStaticInit.setInput(treeNodesMetaMemNext);
    adaptStaticInit.setInput(treeDirectLeaves);
    adaptStaticInit.initAR(context);


    ARAdaptFirstStage adaptFirstStage(341,"NodeType");
    adaptFirstStage.setInput(treeNodesMetaMem);
    adaptFirstStage.setInput(treeMemSparse);
    adaptFirstStage.setInput(treeMemDense);
    adaptFirstStage.setInput(treeNodesMetaMemNext);


    size_t workGroupSizeAdjusted = workGroupSize;
    if(numGroups < workGroupSizeAdjusted) workGroupSizeAdjusted = numGroups;
    adaptFirstStage.setGridDimension(ARSize(numGroups,workGroupSizeAdjusted,workGroupSize)); // FIXME
    adaptFirstStage.initAR(context);

    ARAdaptSecondStage adaptSecondStage;
    adaptSecondStage.setInput(treeMemDense);
    adaptSecondStage.setInput(treeLeavesMetaMem);
    adaptSecondStage.setInput(treeNodesMetaMem);
    adaptSecondStage.setInput(treeDirectLeaves);
    adaptSecondStage.setInput(imgDebug);
    adaptSecondStage.setGridDimension(ARSize(numGroups,workGroupSizeAdjusted,workGroupSize)); // FIXME
    adaptSecondStage.initAR(context);

    getTimer() << adaptStaticInit.getTimer();
    getTimer() << adaptFirstStage.getTimer();
    getTimer() << adaptSecondStage.getTimer();

    ARTimer stageTimer("all stages");


    if(true){

        adaptStaticInit.runCL();
        treeDirectLeaves.toFile<cl_uint>("direct_leaves.csv");
        treeLeavesMetaMem.toFile<cl_uint>("num_leaves_per_groud.csv");
        stageTimer.start();
        adaptFirstStage.runCL();
        adaptSecondStage.runCL();

        stageTimer.lap();

        getTimer().print();
        stageTimer.print();
        checkResult(treeNodesMetaMem, treeMemDense, adaptStaticInit.getCLOutput(),imgDebug);
        exit(-1);
    }
}



void ARAdaptiveGrid::runCL()
{
    RunKernels();

    time += 0.001f;
    GetKernel("update")->setArgument(1,time);

}

void ARAdaptiveGrid::writeNodeData(NodeType& node,OUTTYPE dataPtr[SIZEGR][SIZEGR]){
    cl_ushort4 bbSquare = node.bbSquare;

    int xstart = bbSquare.s[0];
    int xstop  = bbSquare.s[2];
    int ystart = bbSquare.s[1];
    int ystop  = bbSquare.s[3];
    for(int y = ystart; y < ystop; y++)
        for(int x = xstart; x < xstop; x++){
            dataPtr[y][x] = node.value;
        }

}


void ARAdaptiveGrid::checkResult(CLMemory metaMem, CLMemory treeMem, CLMemory img, CLMemory imgDevice)
{
    bool writeToFile = true;

    if( img.size()/4 != (SIZEGR*SIZEGR) ){
        printf("checkResult: invalid image size");
        exit(-1);
    }

    float * imgPtr = img.toHost<cl_float>();
    auto coverageImg = new OUTTYPE[SIZEGR][SIZEGR];
    for( int i = 0; i < (int)img.size(); i += 4){
        int  iflat = i / 4;
        int x = iflat % SIZEGR;
        int y = iflat / SIZEGR;
        coverageImg[y][x] = imgPtr[i];

    }

    float * imgPtrDevice = imgDevice.toHost<cl_float>();
    auto coverageImgDebug = new OUTTYPE[SIZEGR][SIZEGR];
    for( int i = 0; i < (int)imgDevice.size(); i ++){
        int  iflat = i;
        int x = iflat % SIZEGR;
        int y = iflat / SIZEGR;
        coverageImgDebug[y][x] = imgPtrDevice[i];
    }

    auto coverage = new OUTTYPE[SIZEGR][SIZEGR];
    for(int y = 0; y < SIZEGR; y++)
        for(int x = 0; x < SIZEGR; x++)
            coverage[y][x] = 0;

    int* groupSizes = metaMem.toHost<int>();
    NodeType* treePtr = treeMem.toHost<NodeType>();
    int totNumLeaves = 0;
    int numGroups = SIZEGR*SIZEGR/256;
    for(int group = 0; group < numGroups; group++){
        int o =  (group > 0) * groupSizes[group-1];
        NodeType subRoot = treePtr[o];
        totNumLeaves += subRoot.num_leaves;
        for(int n = 0; n < subRoot.num_leaves; n++){
            int searchedLeafNumber = n+1;
            NodeType node = subRoot;

            for(int lvl = 0; lvl < 5; lvl++){
                int NL = 0;
                if(node.is_leaf){
                    writeNodeData(node,coverage);
                    continue;
                }
                for(int c = 0; c < 4; c ++){
                    int childIdx = o+node.children[c];
                    NodeType child = treePtr[childIdx];
                    NL += child.num_leaves;
                    if(searchedLeafNumber <= NL ){
                        searchedLeafNumber -=(NL-child.num_leaves);
                        node = child;
                        break;
                    }
                }
                if ( lvl == 4)
                    printf("missed leaf\n");
            }

        }
    }
    OUTTYPE errSum1 = 0;
    OUTTYPE errSum2 = 0;
    for(int y= 0; y < SIZEGR; y++)
        for(int x= 0; x < SIZEGR; x++){
            errSum1 += coverage[y][x] - coverageImg[y][x];
            errSum2 += coverageImgDebug[y][x] - coverageImg[y][x];

    }
    printf("check result: errsum host = %f\n",errSum1);
    printf("check result: errsum device = %f\n",errSum2);
    printf("Total number of leaves: %d\n",totNumLeaves);


    if(writeToFile){
        std::ofstream outTree("coverage_tree.csv");
        for(int y= 0; y < SIZEGR; y++)
            for(int x= 0; x < SIZEGR; x++){
                outTree << coverage[y][x];
                if(x == SIZEGR-1){
                    outTree << "\n";
                }else{
                    outTree << ",";
                }
            }
        outTree.close();

        std::ofstream outImgHost("coverage_img_host.csv");
        for(int y= 0; y < SIZEGR; y++)
            for(int x= 0; x < SIZEGR; x++){
                outImgHost << coverageImg[y][x];
                if(x == SIZEGR-1){
                    outImgHost << "\n";
                }else{
                    outImgHost << ",";
                }
            }
        outImgHost.close();


        std::ofstream outImgDevice("coverage_img_device.csv");
        for(int y= 0; y < SIZEGR; y++)
            for(int x= 0; x < SIZEGR; x++){
                outImgDevice << coverageImgDebug[y][x];
                if(x == SIZEGR-1){
                    outImgDevice << "\n";
                }else{
                    outImgDevice << ",";
                }
            }
        outImgDevice.close();

    }


}







