#ifndef _AR_GRID_STATIC_INIT_H
#define _AR_GRID_STATIC_INIT_H

#include "grid_instance_base.h"
#include "ar_instance_base.h"


class ARGridStaticInit: public ARInstanceBase, public GridInstanceBase
{

public:

    ARGridStaticInit();
    void initalRun();
	void runCL();

	void initCL(cl::Context context);

	void initAR(cl::Context context);

    void gief(CLMemory& mem1, CLMemory& mem2);

protected:

private:
     CLMemory original1;
    CLMemory original2;
};
#endif
