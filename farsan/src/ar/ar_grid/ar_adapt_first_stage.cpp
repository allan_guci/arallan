#include "ar_adapt_first_stage.h"

ARAdaptFirstStage::ARAdaptFirstStage(cl_uint stride, std::string type)
:mStride(stride), mType(type)
{

}

void ARAdaptFirstStage::initAR(cl::Context context){
    initCL(context);
}

void ARAdaptFirstStage::initCL(cl::Context context){
    setup(context);
    addMacro("NUM_ITER",2);
    std::string typeMacro = "-DPACK_TYPE=";
    typeMacro += mType + " ";
    setOptions(typeMacro);
    CreateProgram("kernels/adaptive_stage_1.cl");

    size_t globalSize  = getGridDim().dx();
    size_t localSize   = getGridDim().dy();

    NumGlobalThreads(ARSize(globalSize));
    NumLocalThreads(ARSize(localSize));

    CLMemory treeNodesMetaMem     = getInput("treeNodesMetaMem");
    CLMemory treeNodesMetaMemNext = getInput("treeNodesMetaMemNext");
    CLMemory treeSparseMem        = getInput("treeMemSparse");
    CLMemory treeDenseMem         = getInput("treeMemDense");



    ///
    CLMemory groupUsedMem = CreateBuffer<cl_int>(1);
    mTreeReadOffset = CreateBuffer<cl_uint>(treeSparseMem.size());

    ///
    mPrefixSum = new ARPrefixSum(treeNodesMetaMem,ARSize(globalSize),ARPrefixSum::SumSizes);
    mPrefixSum->initAR(context);

    ///
    mClusterGroups = new ARClusterGroups(treeNodesMetaMem,mStride);
    mClusterGroups->initAR(context);
    mGroupIntervalMem = mClusterGroups->getCLOutput();

    getTimer() << mPrefixSum->getTimer();
    getTimer() << mClusterGroups->getTimer();

    ///
    CLKernel* packTreeKernel = CreateKernel("kernel_pack_tree");
    packTreeKernel->arg(treeSparseMem);
    packTreeKernel->arg(treeDenseMem);
    packTreeKernel->arg(treeNodesMetaMem);
    packTreeKernel->arg(treeNodesMetaMemNext);
    packTreeKernel->arg(mGroupIntervalMem);
    packTreeKernel->arg((cl_uint)0);

}

size_t ARAdaptFirstStage::numActiveNodes(){
    return mNumActiveNodes;
}

void ARAdaptFirstStage::runCL(){

    mPrefixSum->runCL();
    mClusterGroups->runCL();

    mNumActiveNodes = mPrefixSum->getTotalSum();
    printf("num nodes: %d\n", mNumActiveNodes);
    size_t workGroupSize = getGridDim().dz();
    size_t globalSize = workGroupSize*(mClusterGroups->numGroupsUsed());
    GetKernel("kernel_pack_tree")->threadDimension(ARSize(globalSize),ARSize(workGroupSize));
    GetKernel("kernel_pack_tree")->setArgument(5,(cl_uint)mNumActiveNodes);
    GetKernel("kernel_pack_tree")->run(true);

}


