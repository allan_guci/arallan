#ifndef _AR_ADAPT_SECOND_STAGE_H
#define _AR_ADAPT_SECOND_STAGE_H

#include "grid_instance_base.h"
#include "ar_instance_base.h"
#include "ar_prefix_sum.h"
#include "ar_cluster_groups.h"
class ARAdaptSecondStage: public ARInstanceBase, public GridInstanceBase
{

public:

	ARAdaptSecondStage();

	void initCL(cl::Context context);

	void runCL();

	void initAR(cl::Context context);

protected:

private:
    ARPrefixSum* mPrefixSum;
    ARClusterGroups* mClusterGroups;
};
#endif
