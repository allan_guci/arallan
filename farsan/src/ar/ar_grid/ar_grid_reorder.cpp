#include "ar_grid_reorder.h"

ARGridReorder::ARGridReorder()
{

}

void ARGridReorder::initAR(cl::Context context){
    initCL(context);
}

void ARGridReorder::initCL(cl::Context context){
    setup(context);
    CreateProgram("kernels/grid_stage_1.cl");

    size_t globalSize  = getGridDim().dx();
    size_t localSize   = getGridDim().dy();

    ///
    NumGlobalThreads(ARSize(globalSize));
    NumLocalThreads(ARSize(localSize));

    ///
    CLMemory treeMem           = getInput("treeMem");
    CLMemory treeLeavesSizes   = getInput("treeLeavesSizes");
    CLMemory treeDirectLeaves  = getInput("treeDirectLeaves");
    CLMemory imgDebug          = getInput("imgDebug");
    CLMemory mem1              = getInput("mem1");
    CLMemory mem2              = getInput("mem2");

    ///
    CLKernel* reorderKernel = CreateKernel("reorder");
    CLKernel* ppKern        = CreateKernel("reorder");

    reorderKernel->createPingPong(ppKern,mem1,mem2);
    reorderKernel->arg(treeMem);
    reorderKernel->arg(treeLeavesSizes);
    reorderKernel->arg(treeDirectLeaves);
    reorderKernel->arg(imgDebug);


}

void ARGridReorder::runCL(){
    GetKernel("reorder")->run(true);
}


