#include "ar_cluster_groups.h"

ARClusterGroups::ARClusterGroups(CLMemory data, cl_uint itemsToCover) : mItemsToCover(itemsToCover)
{
    setCLInput(data);
}

void ARClusterGroups::initAR(cl::Context context){
    cl_uint stop = getCLInput().size();
    if ( stop > 256 )
        stop = 256;

    addMacro("STOP_",stop);
    addMacro("ITEMS_TO_COVER_",mItemsToCover);
    setup(context);
    initCL(context);
}

void ARClusterGroups::createClusterKernel(){
    size_t localSize = 256;
    if (getCLInput().size() < localSize) localSize = getCLInput().size();
    CLKernel *clusterKernel = CreateKernel("kernel_cluster_groups",ARSize(getCLInput().size()),ARSize(localSize));
    CLMemory inputMem = getCLInput();
    clusterKernel->arg(inputMem);
    clusterKernel->arg(mGroupIntervalMem1);
    clusterKernel->arg(mIntervalSizeMem);
    clusterKernel->arg(mIntervalMaskMem);
    AddKernel(clusterKernel);
}

size_t ARClusterGroups::numGroupsUsed(){
    return mPrefixSum->getTotalSum();
}

void ARClusterGroups::createPackKernel(){
    mGroupIntervalMem2 = CreateBuffer<cl_uint>(mGroupIntervalMem1.size());
    CLKernel * packKernel = CreateKernel("kernel_copy_data", ARSize(mIntervalSizeMem.size()), ARSize(0));
    packKernel->arg(mGroupIntervalMem1);
    packKernel->arg(mGroupIntervalMem2);
    packKernel->arg(mIntervalSizeMem);
    packKernel->arg(mIntervalMaskMem);
    AddKernel(packKernel);
}


void ARClusterGroups::initCL(cl::Context context){
    CLMemory inputMem = getCLInput();
    if (inputMem.size() < 256 )
        addMacro("STOP_",inputMem.size());
    else
        addMacro("STOP_",256);

    CreateProgram("kernels/cluster_groups.cl");


    mGroupIntervalMem1 = CreateBuffer<cl_uint>(2*inputMem.size());
    mIntervalSizeMem = CreateBuffer<cl_uint>(inputMem.size());
    mIntervalMaskMem = CreateBuffer<cl_char>(inputMem.size());

    createClusterKernel();

    /// Compute number of groups that are used
    mPrefixSum = new ARPrefixSum(mIntervalSizeMem, mIntervalSizeMem.size(), ARPrefixSum::SumOffsets);
    mPrefixSum->initAR(context);
    getTimer() << mPrefixSum->getTimer();
    if( (getCLInput().size() >> 8) > 1) {
        /// Number groups larger than local size need extra packing stage
        createPackKernel();
        setCLOutput(mGroupIntervalMem2);
    }else {
        /// No extra packing required
        setCLOutput(mGroupIntervalMem1);
    }


}

void ARClusterGroups::runCL(){

    GetKernel("kernel_cluster_groups")->run(true);
    mPrefixSum->runCL();

    if( GetKernel("kernel_copy_data") != NULL)
        GetKernel("kernel_copy_data")->run(true);

}
