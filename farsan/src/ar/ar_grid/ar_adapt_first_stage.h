#ifndef _AR_ADAPT_FIRST_STAGE_H
#define _AR_ADAPT_FIRST_STAGE_H

#include "ar_instance_base.h"
#include "grid_instance_base.h"
#include "ar_prefix_sum.h"
#include "ar_cluster_groups.h"


class ARAdaptFirstStage: public ARInstanceBase , public GridInstanceBase
{

public:

	ARAdaptFirstStage(cl_uint stride, std::string type);

	void initAR(cl::Context context);

	void runCL();

	void initCL(cl::Context context);

	size_t numActiveNodes();


protected:

private:

    CLMemory mGroupIntervalMem;
    CLMemory mTreeReadOffset;
    ARPrefixSum * mPrefixSum;
    ARClusterGroups* mClusterGroups;
    cl_uint mNumActiveNodes;
    cl_uint mStride;
    std::string mType;

};
#endif
