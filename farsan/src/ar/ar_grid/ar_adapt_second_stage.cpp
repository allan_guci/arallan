#include "ar_adapt_second_stage.h"

ARAdaptSecondStage::ARAdaptSecondStage()
{

}

void ARAdaptSecondStage::initAR(cl::Context context){
    setup(context);
    initCL(context);
}

void ARAdaptSecondStage::initCL(cl::Context context){

    addMacro("NUM_ITER",1);
    CreateProgram("kernels/adaptive_stage_2.cl");

    CLMemory treeMemDense   = getInput("treeMemDense");
    CLMemory treeLeavesMeta = getInput("treeLeavesMetaMem");
    CLMemory treeNodesMeta  = getInput("treeNodesMetaMem");
    CLMemory verifyImge     = getInput("treeNodesMetaMem");
    CLMemory treeDirectLeaves = getInput("treeDirectLeaves");
    CLMemory img            = getInput("imgDebug");

    size_t globalSizePrefix  = getGridDim().dx();
    mPrefixSum = new ARPrefixSum(treeLeavesMeta,ARSize(globalSizePrefix),ARPrefixSum::SumSizes);
    mPrefixSum->initAR(context);

    mClusterGroups = new ARClusterGroups(treeLeavesMeta,256);
    mClusterGroups->initAR(context);
    CLMemory groupIntervalMem = mClusterGroups->getCLOutput();

    getTimer() << mPrefixSum->getTimer();
    getTimer() << mClusterGroups->getTimer();

    CLKernel* findNeighboursKernel = CreateKernel("kernel_find_neighbors",ARSize(256),ARSize(0));
    findNeighboursKernel->arg(treeMemDense);
    findNeighboursKernel->arg(treeNodesMeta);
    findNeighboursKernel->arg(treeLeavesMeta);
    findNeighboursKernel->arg(groupIntervalMem);
    findNeighboursKernel->arg((cl_uint)0);
    findNeighboursKernel->arg(img);

}

void ARAdaptSecondStage::runCL(){

    mPrefixSum->runCL();
    mClusterGroups->runCL();

    size_t mNumActiveLeaves = mPrefixSum->getTotalSum();

    printf("num leaves: %d\n", mNumActiveLeaves);
    printf("leaves - groups used: %d\n", mClusterGroups->numGroupsUsed());

    size_t workGroupSize = getGridDim().dz();
    size_t globalSize    = workGroupSize*mClusterGroups->numGroupsUsed();

    GetKernel("kernel_find_neighbors")->setArgument(4,(cl_uint)mNumActiveLeaves);
    GetKernel("kernel_find_neighbors")->threadDimension(ARSize(globalSize),ARSize(workGroupSize));
    GetKernel("kernel_find_neighbors")->run(true);

}



