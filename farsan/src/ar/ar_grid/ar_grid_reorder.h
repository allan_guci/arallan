#ifndef _AR_GRID_REORDER_H
#define _AR_GRID_REORDER_H

#include "ar_instance_base.h"
#include "grid_instance_base.h"

class ARGridReorder: public ARInstanceBase , public GridInstanceBase
{

public:

	ARGridReorder();
	void initAR(cl::Context context);
	void runCL();
	void initCL(cl::Context context);

protected:
private:

};
#endif
