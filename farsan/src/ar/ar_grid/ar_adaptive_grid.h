#ifndef _AR_ADAPTIVE_GRID_H
#define _AR_ADAPTIVE_GRID_H

#include "ar_instance_base.h"
#include "grid_instance_base.h"
#include "ar_adapt_first_stage.h"
#include "ar_adapt_static_init.h"

#define SIZEGR 64
#define OUTTYPE float

class ARAdaptiveGrid: public ARInstanceBase, public GridInstanceBase
{
public:
    typedef struct{
            cl_uchar  is_leaf;
            cl_uchar  level;
            cl_uchar  parent;
            cl_uchar  pad;
            cl_ushort  num_leaves;
            cl_ushort children[4];
            cl_ushort4  bbSquare;
            cl_float value;
    }NodeType;


	ARAdaptiveGrid();
	void writeNodeData(NodeType& node,OUTTYPE dataPtr[SIZEGR][SIZEGR]);
    void initAR(cl::Context context);
    void initCL(cl::Context context);
    void runCL();

private:
	    void checkResult(CLMemory metaMem, CLMemory treeMem, CLMemory img, CLMemory imgDevice);
	    void debugPsum();
	    float time;
};

#endif

