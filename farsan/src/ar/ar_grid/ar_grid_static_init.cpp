#include "ar_grid_static_init.h"

ARGridStaticInit::ARGridStaticInit()
{
}

void ARGridStaticInit::initAR(cl::Context context){
    initCL(context);
}

void ARGridStaticInit::initCL(cl::Context context){

    setup(context);
    CreateProgram("kernels/grid_init.cl");

    /// Allocate Memory
    GLuint outputTexture = GLHelper::Texture::create2DTexture(getGridDim().dx(),getGridDim().dy(),NULL,GL_R32F,GL_RGBA);
    setGLOutputTexture(outputTexture);

    ImageOptions optRGBA(cl::ImageFormat(CL_RGBA,CL_FLOAT),getGridDim());
    CLMemory imageOut = CreateImageFromGL(outputTexture,optRGBA);

    original1 = CreateImage(optRGBA);
    original2 = CreateImage(optRGBA);
    CLMemory debugMem  = CreateImage(optRGBA);

    CLMemory treeMem              = getInput("treeMem");
    CLMemory treeLeavesSizes = getInput("treeLeavesSizes");
    CLMemory treeDirectLeaves     = getInput("treeDirectLeaves");
    /// Set cl output
    setCLOutput(debugMem);

    /// Create Kernels
    NumGlobalThreads(getGridDim());
    NumLocalThreads(ARSize(16,16));

    const char * updateLabel = "update";
    CLKernel* update   = CreateKernel(updateLabel);
    CLKernel* ppUpdate = CreateKernel(updateLabel);
    update->arg(original1);
    ppUpdate->arg(original2);
    update->createPingPong(ppUpdate);
    update->arg(0.f);
    AddKernel(update);

    const char * laplacianLabel = "laplacian";
    CLKernel* laplacianKernel = CreateKernel(laplacianLabel);
    CLKernel* ppTemp          = CreateKernel(laplacianLabel);

    laplacianKernel->createPingPong(ppTemp,original1,original2);
    AddKernel(laplacianKernel);

    const char * initTreeLabel = "init_tree_layer";
    CLKernel * initTreeLayerKernel = CreateKernel(initTreeLabel);
    CLKernel * ppInitTree = CreateKernel(initTreeLabel);
    initTreeLayerKernel->createPingPong(ppInitTree,original2,original1);
    initTreeLayerKernel->arg(debugMem);
    initTreeLayerKernel->arg(treeMem);
    initTreeLayerKernel->arg(treeLeavesSizes);
    initTreeLayerKernel->arg(treeDirectLeaves);

    AddKernel(initTreeLayerKernel);

}

void ARGridStaticInit::gief(CLMemory& mem1, CLMemory& mem2){
    mem1 = original1;
    mem2 = original2;
}
void ARGridStaticInit::initalRun(){
    RunKernels(true);
    sync();
}
void ARGridStaticInit::runCL(){
    GetKernel("update")->run(true);
    GetKernel("laplacian")->run(true);
}

