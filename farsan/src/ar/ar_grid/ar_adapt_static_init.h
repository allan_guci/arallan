#ifndef _AR_ADAPT_STATIC_INIT_H
#define _AR_ADAPT_STATIC_INIT_H

#include "grid_instance_base.h"
#include "ar_instance_base.h"


class ARAdaptStaticInit: public ARInstanceBase, public GridInstanceBase
{

public:

    ARAdaptStaticInit(CLMemory treeMetaMem, CLMemory treeMem, CLMemory treeNumLeaves);

	void runCL();

	void initCL(cl::Context context);

	void initAR(cl::Context context);

protected:

private:
    CLMemory mTreeMetaMem;
    CLMemory mTreeMem;
    CLMemory mNumLeavesMem;
};
#endif
