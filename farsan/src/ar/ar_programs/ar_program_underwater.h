#ifndef _AR_PROGRAM_UNDERWATER_H
#define _AR_PROGRAM_UNDERWATER_H

#include "ar_program.h"
#include "ar_water_surface.h"
#include "ar_framebuffer.h"
#include "ar_texture_to_screen.h"
#include "ar_felja.h"
#include "ar_light_perspective.h"
#include "ar_water_surface_refraction.h"
class ARProgramUnderWater : public ARProgram
{
public:
	void init(cl::Context& context);
	void drawGL(UserCamera * uc);
    protected:
	private:
	    ARFrameBuffer* mFrameBuffer;
};

#endif
