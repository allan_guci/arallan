#include "ar_program.h"


void ARProgram::addAR(ARInstanceBase* ar){
    mARInstances.push_back(ar);
}

void ARProgram::runCL(){
    runCLAll();
}

void ARProgram::drawGL(UserCamera * uc){
    drawGLAll(uc);
}

void ARProgram::runCLIndex(int idx){
    mARInstances[idx]->runCL();
}

void ARProgram::drawGLIndex(int idx,UserCamera * uc){
    mARInstances[idx]->drawGL(uc);
}

void ARProgram::runCLAll(){
    size_t numInstances = mARInstances.size();
    for(int i = 0; i < numInstances; i ++ )
        mARInstances[i]->runCL();
}
void ARProgram::drawGLAll(UserCamera * uc){
    size_t numInstances = mARInstances.size();
    for(int i = 0; i < numInstances; i ++ )
        mARInstances[i]->drawGL(uc);

}

void ARProgram::release(){
    size_t numInstances = mARInstances.size();
    for(int i = 0; i < numInstances; i ++ )
        mARInstances[i]->runCL();
}
