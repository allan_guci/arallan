#include "ar_program_debug.h"

void ARProgramDebug::init(cl::Context& context){
/*
    size_t sizeGrid = 64;
	ARSize gridDim(sizeGrid,sizeGrid,sizeGrid);
	ARSize gridOffset(0.0f,0.0f,0.0f);
	ARSize gridScale(1.0f,1.0f,1.0f);

    ARSmokePlain * smoke = new ARSmokePlain();

    smoke->setGridDimension(gridDim);
	smoke->setGridOffset(gridOffset);
	smoke->setGridScale(gridScale);
    smoke->initAR(context);

    addAR(smoke);*/

    size_t sizeGrid = SIZEGR;
	ARSize gridDim(sizeGrid,sizeGrid);


    ARGrid*  adaptivegrid = new ARGrid();
    adaptivegrid->setGridDimension(gridDim);
    adaptivegrid->initAR(context);
    addAR(adaptivegrid);

    ARTextureToScreen* texturetoscreen = new ARTextureToScreen();
    texturetoscreen->setGLInputTexture(adaptivegrid->getGLOutputTexture());
    texturetoscreen->initAR(context);
    addAR(texturetoscreen);


}
