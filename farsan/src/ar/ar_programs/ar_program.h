#ifndef _AR_PROGRAM_H
#define _AR_PROGRAM_H
#include "gui_db.h"
#include "user_camera.h"
#include "ar_instance_base.h"

class ARProgram {
public:

	virtual void init(cl::Context& context) {};
    virtual void runCL();
	virtual void drawGL(UserCamera* uc);

    void release();
    void setGUI(GUIDB* gui){mGUI = gui;}
    protected:

        void addAR(ARInstanceBase* ar);
        template<class T>
        T * getAR(int index){
            return reinterpret_cast<T*>(mARInstances[index]);
        }

        void runCLAll();
        void runCLIndex(int idx);

        void drawGLAll(UserCamera * uc);
        void drawGLIndex(int idx,UserCamera * uc);


        GUIDB* mGUI;



	private:
        std::vector<ARInstanceBase*> mARInstances;

};

#endif
