#ifndef _AR_PROGRAM_CLOTH_H
#define _AR_PROGRAM_CLOTH_H

#include "ar_program.h"
#include "ar_cloth.h"
#include "ar_meshcropper.h"
#include "ar_meshconverter.h"
#include "ar_meshcollapser.h"
#include "ar_meshdrawer.h"
class ARProgramCloth : public ARProgram
{
public:
	void init(cl::Context& context);
    protected:

    private:

};



#endif
