#include "ar_program_cloth.h"
#include <unistd.h>
void ARProgramCloth::init(cl::Context& context){


    ARMeshConverter* meshconverter = new ARMeshConverter();
  //  meshconverter->setGLInputBuffer(getDeviceInput());
    meshconverter->initAR(context);
/*
    ARMeshCollapser* meshcollapser = new ARMeshCollapser();
    meshcollapser->setInputMesh(meshconverter->getOutputMesh());
    meshcollapser->setOriginalDimension(ARSize(512,424));
    meshcollapser->initAR(context);
 //   addAR(meshcollapser);
    meshcollapser->runCL();

    ARMeshCropper* meshcropper = new ARMeshCropper();
    meshcropper->setInputMesh(meshconverter->getOutputMesh());
    meshcropper->initAR(context);
  //  addAR(meshcropper);
    meshcropper->runCL();
*/
    ARCollision * collision = new ARCollision();
    collision->setInputMesh(meshconverter->getOutputMesh());
    collision->initCL(context);
    collision->staticAlloc();
    collision->runCL();
   // addAR(collision);

    ARMeshDrawer * meshdrawer = new ARMeshDrawer();
    meshdrawer->setGLInputBuffer(meshconverter->getGLOutputBuffer());
   // meshdrawer->setGLInputTexture(getDeviceInputTexture());
    meshdrawer->setInputMesh(meshconverter->getOutputMesh());
    meshdrawer->initAR(context);
    addAR(meshdrawer);
    meshdrawer->runCL();


    ARCloth* cloth = new ARCloth();
    cloth->setCollidable(collision->getCollidable());
    cloth->initAR(context);
    addAR(cloth);


   // runCLAll();
}
