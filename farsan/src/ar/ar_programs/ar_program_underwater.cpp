#include "ar_program_underwater.h"
#include "ar_wall.h"
void ARProgramUnderWater::init(cl::Context& context){
/*
    size_t sizeGrid = 64;
	ARSize gridDim(sizeGrid,sizeGrid,sizeGrid);
	ARSize gridOffset(0.0f,0.0f,0.0f);
	ARSize gridScale(1.0f,1.0f,1.0f);

    ARSmokePlain * smoke = new ARSmokePlain();

    smoke->setGridDimension(gridDim);
	smoke->setGridOffset(gridOffset);
	smoke->setGridScale(gridScale);
    smoke->initAR(context);

    addAR(smoke);*/
    ARSize light(1.0f, -1.0f, 0.0f);
    float sizeXZ = 8.0f;
    ARSize waterScale(sizeXZ,1.0f,sizeXZ);
    ARSize waterOffset(-sizeXZ/2.0f,1.0f,-sizeXZ/2.0f);
    ARSize waterGridDim(512,512);
    ARSize screenSize(512,512);
    ARSize screenSizeCaustics(1024,1024);


    mFrameBuffer = new ARFrameBuffer(RenderTargetType::COLOR,screenSizeCaustics);
    mFrameBuffer->initAR(context);

    ARFrameBuffer* resultBuffer = new ARFrameBuffer(RenderTargetType::COLOR_DEPTH,screenSizeCaustics);
    resultBuffer->initAR(context);


    ARWaterSurface* watersurface = new ARWaterSurface();
    watersurface->setGridDimension(waterGridDim);
    watersurface->setGridScale(waterScale);
    watersurface->setGridOffset(waterOffset);
    watersurface->setLight(light);
    watersurface->setGLInputTexture(mFrameBuffer->getColorTexture(),"COLOR");
    watersurface->initAR(context);

    ARLightPerspective* lightperspective = new ARLightPerspective();
    lightperspective->setGridDimension(screenSize);
    lightperspective->setGLInputBuffer(watersurface->getGLOutputBuffer());
    lightperspective->setLight(light);
    lightperspective->initAR(context);

    ARWaterSurfaceRefraction* surfaceRefraction = new ARWaterSurfaceRefraction();
    surfaceRefraction->setGridDimension(screenSize);
    surfaceRefraction->setGLInputBuffer(watersurface->getGLOutputBuffer());
    surfaceRefraction->initAR(context);

    ARFelja * bottom = new ARFelja();
    bottom->setLight(light);
    bottom->setGridDimension(screenSize);
    bottom->setGLInputTexture(surfaceRefraction->getColorTexture());
    bottom->setExternalCamera(surfaceRefraction->getLightCamera());
    bottom->initAR(context);

    ARTextureToScreen* textureToScreen = new ARTextureToScreen();
    textureToScreen->setGLInputTexture(resultBuffer->getColorTexture(),"COLOR");
    textureToScreen->setGLInputTexture(resultBuffer->getDepthTexture(),"DEPTH");
    textureToScreen->initAR(context);

    ARWall * wall = new ARWall();
    wall->initAR(context);

     ARWall * wall2 = new ARWall();
    wall2->initAR(context);

    addAR(watersurface);
    addAR(mFrameBuffer);
    addAR(lightperspective);
    addAR(bottom);
    addAR(textureToScreen);
    addAR(surfaceRefraction);
    addAR(resultBuffer);
    addAR(wall);
    addAR(wall2);


}


void ARProgramUnderWater::drawGL(UserCamera* uc){

  // getAR<ARLightPerspective>(2)->drawGL(uc);
   getAR<ARWaterSurfaceRefraction>(5)->drawGL(uc);
   getAR<ARFrameBuffer>(1)->enable();
   getAR<ARFelja>(3)->drawGL(uc);
   getAR<ARWall>(7)->drawGL(uc);
   getAR<ARFrameBuffer>(1)->disable(uc);

   getAR<ARFrameBuffer>(6)->enable();
        getAR<ARWaterSurface>(0)->drawGL(uc);
        getAR<ARWall>(8)->drawGL(uc);
   getAR<ARFrameBuffer>(6)->disable(uc);
   glDisable(GL_BLEND);
   getAR<ARTextureToScreen>(4)->drawGL(uc);

}
