#include "ar_program_smoke.h"
#include "ar_wall.h"
void ARProgramSmoke::init(cl::Context& context){

	ARSize gridDim(96,96,96);
	ARSize gridDimCollision(32,32,32);
	ARSize gridOffset(0.2f,-0.2f,1.5f);
	ARSize gridScale(1.0f,1.0f,1.0f);
/*

	//ARDummyMesh* meshdrawer =  new ARDummyMesh();
	//meshdrawer->initAR(context);

    ARMeshConverter* meshconverter = new ARMeshConverter();
  //  meshconverter->setGLInputBuffer(getDeviceInput());
    meshconverter->initAR(context);

    ARMeshCollapser* meshcollapser = new ARMeshCollapser();
    meshcollapser->setInputMesh(meshconverter->getOutputMesh());
    meshcollapser->setOriginalDimension(ARSize(512,424));
    meshcollapser->initAR(context);
    addAR(meshcollapser);

    ARMeshCropper* meshcropper = new ARMeshCropper();
    meshcropper->setInputMesh(meshcollapser->getOutputMesh());
    meshcropper->initAR(context);
    addAR(meshcropper);

    ARMeshDrawer * meshdrawer = new ARMeshDrawer();
    meshdrawer->setGLInputBuffer(meshcropper->getGLOutputBuffer());
   // meshdrawer->setGLInputTexture(getDeviceInputTexture());
    meshdrawer->setInputMesh(meshcropper->getOutputMesh());
    meshdrawer->initAR(context);
    addAR(meshdrawer);

    ARDepthMapper* depthMapper = new ARDepthMapper();
    depthMapper->setGLInputBuffer(meshcropper->getGLOutputBuffer()); //getDeviceInput());
    depthMapper->initAR(context);
    addAR(depthMapper);

    ARGridifier* gridifier = new ARGridifier();
    gridifier->setGLInputTexture(depthMapper->getGLOutputTexture());
	gridifier->setCLInput(meshcropper->getCLOutput());
	gridifier->setDepthMapSize(512,424);
	gridifier->setGridDimension(gridDimCollision);
	gridifier->setGridOffset(gridOffset);
	gridifier->setGridScale(gridScale);
    gridifier->initAR(context);
    addAR(gridifier);

    ARSmoke * smoke = new ARSmoke();
    smoke->setCLInput(gridifier->getCLOutput());
    smoke->setGLInputTexture(depthMapper->getGLOutputTexture());
    smoke->setGridDimension(gridDim);
	smoke->setGridOffset(gridOffset);
	smoke->setGridScale(gridScale);
	smoke->initAR(context);
    addAR(smoke);
*/
/*
 ARWall * wall = new ARWall();
    wall->initAR(context);

    addAR(wall);*/
    ARSmokePlain * smoke = new ARSmokePlain();

    smoke->setGridDimension(gridDim);
	smoke->setGridOffset(gridOffset);
	smoke->setGridScale(gridScale);
    smoke->initAR(context);

    addAR(smoke);


}
