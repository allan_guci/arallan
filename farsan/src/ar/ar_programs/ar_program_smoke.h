#ifndef _AR_PROGRAM_SMOKE_H
#define _AR_PROGRAM_SMOKE_H

#include "ar_program.h"
#include "ar_smoke_plain.h"
#include "ar_smoke.h"
#include "ar_gridifier.h"
#include "ar_dummy_mesh.h"
#include "ar_depth_mapper.h"
#include "ar_cloth.h"
#include "ar_meshcropper.h"
#include "ar_meshconverter.h"
#include "ar_meshcollapser.h"
#include "ar_meshdrawer.h"
class ARProgramSmoke : public ARProgram
{
public:
	void init(cl::Context& context);
//	void drawGL(UserCamera* uc);
    protected:
	private:

};

#endif
