#ifndef _AR_PROGRAM_TEMPLATE_H
#define _AR_PROGRAM_TEMPLATE_H

#include "ar_program.h"
class ARProgramTemplate {
public:
	void init(cl::Context& context);
    void runCL();
	void drawGL(UserCamera* uc);

    protected:

	private:

};

#endif
