#ifndef _AR_PROGRAM_DEBUG_H
#define _AR_PROGRAM_DEBUG_H
#include "ar_program.h"
#include "ar_smoke_plain.h"
#include "ar_adaptive_grid.h"
#include "ar_texture_to_screen.h"
#include "ar_grid.h"
class ARProgramDebug : public ARProgram
{
public:
	void init(cl::Context& context);
    protected:
	private:
};

#endif
