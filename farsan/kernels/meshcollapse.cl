



__kernel void collapse4(
	__global float4* vertex_buffer_in,
	__global float4* vertex_buffer_out,
	const uint width,
	const uint collapse_factor
	
){
	
	int x_new = get_global_id(0);
	int y_new = get_global_id(1);
	
	int x_old = x_new*collapse_factor;
	int y_old = y_new*collapse_factor;
	
	int index_new = y_new*width+x_new;
	int index_old = y_old*width*collapse_factor+x_old;
	
	float4 out = vertex_buffer_in[index_old];
	if(length(out) < 0.001f)
		out+=10.0f;
	vertex_buffer_out[index_new] = out;
	
}

__kernel void collapse2(
	__global float2* vertex_buffer_in,
	__global float2* vertex_buffer_out,
	const uint width,
	const uint collapse_factor
	
){
	
	int x_new = get_global_id(0);
	int y_new = get_global_id(1);
	
	int x_old = x_new*collapse_factor;
	int y_old = y_new*collapse_factor;
	
	int index_new = y_new*width+x_new;
	int index_old = y_old*width*collapse_factor+x_old;
	
	vertex_buffer_out[index_new] = vertex_buffer_in[index_old];
	
}
#define FLAT_INDEX(x,y,w)  ((y)*w+x)
__kernel void collapse_tri(
	__global int* index_out,
	const uint width
){
	
	int x = get_global_id(0);
	int y = get_global_id(1);
	
	int w = width;
	
	int i0 = FLAT_INDEX(x,y,w);
	
	int index_new = 6*(y*(w-1)+x);
	
	
	index_out[index_new++] = i0;
	index_out[index_new++] = FLAT_INDEX(x+1,y+1,w);
	index_out[index_new++] = FLAT_INDEX(x,y+1,w);
	
	
	
	index_out[index_new++] = i0;
	index_out[index_new++] = FLAT_INDEX(x+1,y,w);
	index_out[index_new++] = FLAT_INDEX(x+1,y+1,w);
	
	
	
}