

__kernel void main(__global float3* buf,
							  const uint w,
							  const uint h,
							  const float radius,
							  const float scaleXY,
							  const float time){
	int x = get_global_id(0);
	int y = get_global_id(1);
	int i = (x + y*w);
	
	
	float2 posXY = ((float2)((float)x/(float)w, (float)y/(float)h) - 0.5f) * 2.0f * scaleXY;
	float zf = -1.0;
	if( length(posXY) < radius ) 
		zf += 2*(1.0-length(posXY)/radius);
		
	buf[i] = (float3)(posXY,zf);	

}