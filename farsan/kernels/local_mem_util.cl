#ifndef LOCAL_MEM_UTIL
#define LOCAL_MEM_UTIL


#define LOCAL_SIZE 18
int4 get_extra(){


	int2 extra_l = -1;
	int2 extra_g = 0;
	
	int2 l_id 	  = (int2) (get_local_id(0),get_local_id(1));
	int2 l_size   = (int2)(get_local_size(0),get_local_size(1));
	int2 g_id     = (int2) (get_global_id(0),get_global_id(1));
	int2 g_size   = (int2)(get_global_size(0),get_global_size(1));
	int2 group_id = (int2) (get_group_id(0),get_group_id(1));
	
	int i = l_id.y*l_size.x + l_id.x;		
	int offset = i % LOCAL_SIZE;
	
	int2 offsets[4] = {
						{offset,0},
						{offset,LOCAL_SIZE-1},
						{0,offset},
						{LOCAL_SIZE-1,offset}
					  };
					  
		  
	for(int n = 0; n < 4; n++){
		if( i >= LOCAL_SIZE*n && i <LOCAL_SIZE*(n+1)){
			extra_l = offsets[n];
			break;
		}
	}
	
	int2 g_base = group_id*l_size;
	extra_g = max(min(g_base + extra_l-1,g_size-1),0);
	
	if(extra_l.x == -1 || extra_l.y == -1) { 
		extra_g = g_id;
		extra_l = l_id+1;
	}
	
	return (int4)(extra_g,extra_l);
}

#define INIT_2D_KERNEL \
	int4 tmp; \
	int2 g_id 	  = (int2) (get_global_id(0),get_global_id(1));\
	int2 l_id 	  = (int2) (get_local_id(0),get_local_id(1)); \
	int2 l_id_mem = l_id+1;

#define IMG2D_TO_LOCAL_MEM(L_MEM_NAME,_IMG,G_ID,L_ID) \
	tmp = get_extra();	  \
	__local float L_MEM_NAME[LOCAL_SIZE][LOCAL_SIZE];	\
	L_MEM_NAME[L_ID.y][L_ID.x]         		 = READ_IMG(_IMG,G_ID).x; \
	L_MEM_NAME[tmp.w][tmp.z] = READ_IMG(_IMG,tmp.xy).x;  \
	barrier (CLK_LOCAL_MEM_FENCE); 

#endif