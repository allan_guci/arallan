#include "adaptive_util.cl"

#ifndef PACK_TYPE
#error define PACK_TYPE before compiling adaptive_stage_1.cl
#endif



    
__kernel void kernel_pack_tree(
    __global PACK_TYPE* tree_sparse,
    __global PACK_TYPE* tree_dense,
    __global uint * write_offset,
    __global uint * read_offset,
    __global uint2 * group_interval,    
    const  uint num_tree_nodes    
    )
{
    uint gid = get_global_id(0);
    uint lid = get_local_id(0);
    uint group  = get_group_id(0);
    
    
    INIT_PARTITIONING(meta, group_interval, read_offset, write_offset);
    
    PARTITION_START(meta, current_root, read_index , lid)
   
        uint write_index = meta->write_offset + lid;
        
        // Check valid ranges
        if(write_index >= num_tree_nodes || read_index == -1)
                return;    
        
        // write packed tree nodes
        tree_dense[write_index] = tree_sparse[read_index]; 
                
    PARTITION_END(lid)
    
    
}