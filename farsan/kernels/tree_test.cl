

__kernel void init(__global float * buf){
	int i = get_global_id(0);
	int s = get_global_size(0);
	
	
	if(i > s/4 && i < 3*s/4){
		float t = 10.0f*(float)(i-s/4)/(float)s;
		buf[i]= sin(t)*cos(4.0f*t);
	}else{
		buf[i] = 0.0;
	}
}


__kernel void copy(__global float* src,__global float* dst){
	int i = get_global_id(0);
	dst[i] = src[i]+0.0f;
}

__kernel void laplacian(__global float* src,__global float* dst){
	
	int i = get_global_id(0);
	int s = get_global_size(0);
	
	if(i == 0 || i == (s-1)){
		dst[i] = 0;
		return;
	}
	
	dst[i] = (src[i-1]+src[i+1]+-2*src[i]);

}

__kernel void tree_resolution(__global float* src,__global float* dst){
	int i = get_global_id(0);
	int s = get_global_size(0);
	
	if(i == 0 || i == (s-1)){
		dst[i] = 0;
		return;
	}
	float tmp = (src[i-1]+src[i+1]+src[i])*0.33333f;
	
	int resolution = ceil(10*tmp);
	dst[i] = resolution;
}

/*
struct OctreeNode{
    cl_uint  pos;
    cl_uint  parent;
    cl_uint  child[2];
	cl_uint  flag;
	cl_float data;
};


__kernel void tree_build(__global *OctreeNodes nodes,__global float* data){

	int i 	   = get_global_id(0);
	int nItems = get_global_size(0);
	
	
	int offs = 0;
	
	for(int level = 0 ; level < (nLevels-1); level++){
			
		int split  = nItems >> level;
		
		if(i % split == 0){
		
			int nIdx   = offs + i/split; 
	
			nodes[nIdx].pos = i;
			nodes[nIdx].parent = (nIdx-1) >> 1;
	
			nodes[nIdx].child[0] = (nIdx << 1)+1;
			nodes[nIdx].child[1] = (nIdx << 1)+2;
		}
		
		offs += 1 << level;
	}
	
	// Assign leaves
	int nIdx = nItems+i; 
	nodes[nIdx].pos = i;
	nodes[nIdx].data = data[i];
	nodes[nIdx].parent = (nIdx-1) >> 1;

	
	
	


}
*/
