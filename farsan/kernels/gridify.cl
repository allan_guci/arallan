#pragma OPENCL EXTENSION cl_khr_3d_image_writes : enable

__kernel void gridify(__global float3* buf,
					  __write_only image3d_t img_out,
					  const uint2 depth_map_dim,
					  const float4 grid_scale,
					  const float4 grid_offset,
					  const uint4 grid_dim
					){
	int x = get_global_id(0);
	int y = get_global_id(1);
	int i = (x + y*depth_map_dim.x);
	
	float4 scale  = grid_scale*2.0f;
	float4 offset = convert_float4(grid_offset);
	float4 dim 	  = convert_float4(grid_dim);
	
	float3 vertex = buf[i];
	/*
	if(vertex.z < 0.4)
		return;*/
	
	float4 grid_pos_norm = (((float4)(vertex,0) - offset)/scale + 0.5f);
	
	int4 idx = convert_int4((floor(grid_pos_norm*dim + 0.5f)));

	if( 0 < grid_pos_norm.x  && grid_pos_norm.x < 1 &&
		0 < grid_pos_norm.y  && grid_pos_norm.y < 1 &&
		0 < grid_pos_norm.z  && grid_pos_norm.z < 1 )
		{
			write_imagef(img_out,idx,1);
		}		
}

__kernel void clear_grid( __write_only image3d_t img_out,
						 const uint4 grid_dim
						 ){

	int4 idx = (int4)(get_global_id(0),get_global_id(1),get_global_id(2),0);
	float out_val = 0.0f;
	if(idx.x == 0 || idx.x == (grid_dim.x-1) ||
	   idx.y == 0 || idx.y == (grid_dim.y-1) ||
	   idx.z == 0 || idx.z == (grid_dim.z-1))
	   out_val = 0.2f;
	write_imagef(img_out,idx,out_val);
}
