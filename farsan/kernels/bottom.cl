const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE | //Natural coordinates
						 CLK_ADDRESS_REPEAT | //Clamp to zeros
						 CLK_FILTER_NEAREST; //Don't interpolate			

const sampler_t sampler_linear = CLK_NORMALIZED_COORDS_FALSE | //Natural coordinates
						 CLK_ADDRESS_REPEAT | //Clamp to zeros
						 CLK_FILTER_LINEAR; //Don't interpolate							 
#define CHECK_BOUNDS(p,s,o) \
if(p < o || p > (s-2-o)) return

#define KERNEL_SIZE 1
__kernel void sobel_x(
		__read_only image2d_t in,
		__write_only image2d_t out
){
	int x = get_global_id(0);
	int y = get_global_id(1);
	int2 xy = (int2)(x,y);
	CHECK_BOUNDS(x,get_global_size(0),KERNEL_SIZE);
	CHECK_BOUNDS(y,get_global_size(1),KERNEL_SIZE);
		
	float kern[9] = {-1,0,1,-2,0,2,-1,0,1};
	
	float res = 0.0f;
	for( int yi = -KERNEL_SIZE; yi < (KERNEL_SIZE+1); yi ++)
		for( int xi = -KERNEL_SIZE; xi < (KERNEL_SIZE+1); xi ++){
				int i = yi*KERNEL_SIZE +xi;
				res += kern[i]*read_imagef(in, sampler,xy+(int2)(xi,yi)).x;
		}
	float tmp = read_imagef(in, sampler,xy).x;
	write_imagef(out,xy,tmp);
	
}


__kernel void clear(
		__write_only image2d_t out,
		__global int* in_light
){
	int x = get_global_id(0);
	int y = get_global_id(1);
	int2 xy = (int2)(x,y);	
	int i = y*get_global_size(0) + x;
	
	in_light[i] = 0;
	write_imagef(out,xy,0);
}

__kernel void sum_light(
		__read_only image2d_t in,
		__global int* out
){
	int x = get_global_id(0);
	int y = get_global_id(1);
	int2 xy = (int2)(x,y);	
	
	int i = y*get_global_size(0) + x;
	
	float2 xy_out = read_imagef(in, sampler_linear,xy).xy;
	int2 xy_out_i  = convert_int2(max(min((xy_out/12 + 0.5f),1.0f),0.0f)*(get_global_size(0)-1));
	
	int i_out = xy_out_i.y*get_global_size(0) + xy_out_i.x;
	
	int out_old;
	out[i_out]++;
	/*do{
		out_old = out[i_out];
	}while(atomic_inc(&(out[i_out])) == out_old);
	*/
}

__kernel void commit(
		__global int* in,
		__write_only image2d_t out
		)
{
	int x = get_global_id(0);
	int y = get_global_id(1);
	int i = y*get_global_size(0) + x;
	int2 xy = (int2)(x,y);	
	
	float res = 0.0f;
	for(int yy = -1; yy <= 1; yy++)
		for(int xx = -1; xx <= 1; xx++){
			i = (y+yy)*get_global_size(0) + (x+xx);
			res += in[i];
		}
	
	write_imagef(out,xy,res);

}
		

		
		
__kernel void debug(
		__read_only image2d_t in,
		__write_only image2d_t out
){
	int x = get_global_id(0);
	int y = get_global_id(1);
	int2 xy = (int2)(x,y);	
	float4 tmp = read_imagef(in, sampler_linear,xy);
	#define DEBUG
	
	#ifdef DEBUG
	if(tmp.w < -5.5f)
		write_imagef(out,xy,0); 
	else{
		float2 idx_f1 = max(min((tmp.xy/12 + 0.5f),1.0f),0.0f); //*(get_global_size(0)-1);
		write_imagef(out,xy,(float4)(tmp)); //); 
		}
	#else
	if(tmp.w < 0.5f)
		write_imagef(out,xy,0); 
	else{
		
		float2 idx_f1 = max(min((tmp.xy/6 + 0.5f),1.0f),0.0f)*(get_global_size(0)-1);
		int2 idx_f2 = convert_int2(round(idx_f1));
		
		float2 st = idx_f1-convert_float2(idx_f2);
		float st_len = length(st)/sqrt(2.0f);
		
		write_imagef(out,idx_f2+(int2)(0,0),1);//-st_len); 
	/*	write_imagef(out,idx_f2+(int2)(1,0),1);//st.x); 
		write_imagef(out,idx_f2+(int2)(0,1),1); //,st.y); 
		write_imagef(out,idx_f2+(int2)(1,1),1); //st_len); */
/*
		int2 out_xy = convert_int2(idx_f1);
		write_imagef(out,out_xy,1); */
	}
	#endif
	

}

