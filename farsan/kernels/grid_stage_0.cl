#ifndef X_DIM_GLOBAL
#error define X_DIM_GLOBAL before compiling grid_stage_0.cl
#endif

#define X_DIM_LOCAL 16

#include "local_mem_util.cl"
#include "adaptive_util.cl"
#include "image_util.cl"
int2 gid_to_2d(uint i){    
    return (int2)(i % X_DIM_GLOBAL, i / X_DIM_GLOBAL);
}
int2 lid_to_2d(uint i){    
    return (int2)(i % X_DIM_LOCAL, i / X_DIM_LOCAL);
}

void grad_to_lvl(__local float * buf[18], int2 lid){
    float gradient = clamp(1*log2(ceil(buf[lid.y][lid.x]*80.0f)),0.0f,4.0f);
    buf[lid.y][lid.x] = gradient;
}

void populate_children(NodeType* parent, __local NodeType* tree, uint parent_index) {

    int bb_half_w  = node_width(*parent) >> 1;
    int bb_half_h  = node_height(*parent) >> 1;
    
    int2 offsets[4] = { {-bb_half_h, -bb_half_w}, {-bb_half_h, 0}, {0, -bb_half_w}, {0, 0} };    
        
    uchar next_level = parent->level + 1;
    
    int2 center = node_center(*parent);    
    int pi = center.x - bb_half_w;
    int pj = center.y - bb_half_h;
    
    ASSING_CHILD_INDICES((*parent), pi, pj, parent->level);                
    
    for(int c = 0; c < 4; c++){
        
        NodeType child;        
        // Assign child properties
        child.is_leaf = 1;        
        child.level = next_level;        
        child.parent = parent_index;                        
        int2 ij = center + offsets[c];
        uint i = ij.x; uint j = ij.y;        
        child.center = (i << 3) | j;
        child.bbSquare = get_range(i, j, next_level);        
        child.value = parent->value;        
        
        // Store child in tree
        uint c_idx = parent->children[c];        
        tree[c_idx] = child;   
        
    }
}

__kernel void reorder(
							__read_only image2d_t in_grad_img,
							__read_only image2d_t in_img,
							__global NodeType* tree_in,
							__global uint* tree_num_leaves,
                            __global uint* tree_direct_leaves
							)
{
    uint lid = get_local_id(0);
    uint gid = get_global_id(0);
    uint group = get_global_id(0);
    
    int2 lid2 = lid_to_2d(lid);
    int2 gid2 = gid_to_2d(gid);
    
    INIT_2D_KERNEL;
	IMG2D_TO_LOCAL_MEM(levels, in_grad_img, gid2, lid2);
	IMG2D_TO_LOCAL_MEM(values, in_img     , gid2, lid2);
    
    grad_to_lvl(levels, lid2);
    
    // Tree data
    __local NodeType tree[TREE_DATA_SIZE];
    uint num_leaves = tree_num_leaves[group];
    
    // Leaf data
    __local short tree_leaves[TREE_DATA_SIZE];
	
    uint tree_stride = group*TREE_DATA_SIZE;
    uint last_leaf = tree_direct_leaves[tree_stride] % TREE_DATA_SIZE;       
    uint lid_tmp = lid;
    for(int k = 0; k < 2; k++){
        if(lid_tmp < last_leaf){
            tree[lid_tmp] = tree_in[tree_stride + lid_tmp];        
        }
        lid_tmp += 256;
    }
    
    
    if( lid < num_leaves){      
        uint leaf_nr = tree_direct_leaves[tree_stride + lid];        
        NodeType  node  = tree_in[leaf_nr];       
        
        int2 center = node_center(node);
        
        int i = center.x;
        int j = center.y;
        
        CREATE_TREE_INDICES(tree_idx, j, i);
        CREATE_TREE_INDICES_OFFSETS(nb_tree_idx, j, i); 
        CLEAR_TREE(tree_leaves,tree_idx,i,j);  
        
        char current_level  = node.level;          
        float next_value   = values[j][i];
        char next_level    = convert_char(levels[j][i]);                
        char resulting_level = max(current_level - (next_level < current_level), 0);
        
        uint node_index = tree_idx[resulting_level];        
        node.value = next_value;
        node.level   = next_level;              
        node.is_leaf = next_level <= current_level;
        tree[node_index] = node;
        
        int num_iter = next_level > current_level;
        for(int level = 0; level <  1; level ++){
            populate_children(&node, tree, node_index);            
        }             
        TRAV_DOWN(i,j)
            if(lvl <= next_level){
                uchar max_lvl  = 0;
                float tmp_val = 0.0f;
                NODE_ACTIVE_TRAV_DOWN
                // Max level and mean for children of parent at lvl-1
                for(int yy = 0; yy < 2; yy++)
                    for(int xx = 0; xx < 2; xx++){
                        max_lvl  = max(tree[nb_tree_idx[lvl][yy][xx]].level,max_lvl);
                        tmp_val += tree[nb_tree_idx[lvl][yy][xx]].value;		
                    }			
                // Assign level and data to parent
                int idx = tree_idx[lvl-1];
                tree[idx].level = max_lvl;
                tree[idx].value = max_lvl;                               
                NODE_ACTIVE_TRAV_DOWN_END
            }
            // Syncronize all local writing before entering next level
            barrier(CLK_LOCAL_MEM_FENCE);
        TRAV_DOWN_END  

        uint last_node_idx = 0;
        uint n_leaves = 0;
        if(lid == 0){			
            uint is_leaf = tree[0].is_leaf;
            tree_leaves[n_leaves] = 0;            
            n_leaves += is_leaf;        
            for(int n = 1; n < TREE_DATA_SIZE; n ++ ){           
                uint is_leaf = tree[n].is_leaf;
                tree_leaves[n] += is_leaf*n_leaves;            
                n_leaves += is_leaf;
            }		
        }		
        
        for(int k = 0; k < 2; k++){
            int idx = lid + k*256;
            if( idx < 341){
                    // Read private copy before packing indices
                    NodeType n = tree[idx];
                    // Write node to output
                    tree_in[tree_stride + idx] = n;
                    if(n.is_leaf)
                        tree_direct_leaves[tree_stride+tree_leaves[idx]] = tree_stride+idx;                        
            }
        }
    }	
}