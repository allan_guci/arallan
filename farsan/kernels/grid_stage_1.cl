
#include "image_util.cl"
#include "grid_util.cl"



__kernel void kernel_find_neighbors(
    __global NodeType* tree,   
    __global uint * tree_num_leaves,  
    __global uint * tree_direct_leaves,   
    __global uint2 * group_interval,    
      const  uint num_leaves,        
    __write_only image2d_t out
    )
{
    uint lid = get_local_id(0);
    uint gid = get_global_id(0);
    
    __local AccessMetaType meta[1];     
    
    init(lid, meta, group_interval, tree_num_leaves);
     
    int leaf_nr = -1; int tree_nr = -1;
    find_tree_and_leaf(lid, meta, &leaf_nr, &tree_nr);
    
    if(leaf_nr == -1 )
        return;
    
    NodeType node;
    get_node(leaf_nr, tree_nr, tree, tree_direct_leaves, &node);
  
    ushort4 bbSquare  = node.bbSquare;
    uint xstart = bbSquare.s0;  uint xstop  = bbSquare.s2;
    uint ystart = bbSquare.s1;  uint ystop  = bbSquare.s3;
    for(int y = ystart; y < ystop; y++)
        for(int x = xstart; x < xstop; x++)
            write_imagef(out,(int2)(x,y), node.value);

    
}