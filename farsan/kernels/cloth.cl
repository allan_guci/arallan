#pragma OPENCL EXTENSION cl_khr_3d_image_writes : enable

const int2 spring_offsets[12] = {
	// Struct
	{1 ,0},
	{0,1},
	{-1,0},
	{0,-1},
	//Shear
	{1,1},
	{-1,1},
	{-1,-1},
	{1,-1},
	//Bend
	{2,0},
	{0,2},
	{-2,0},
	{0,-2}
 };

/* STRUCTURAL SRPINGS:  */
#define RIGHT_STRUCTURAL_SPRING   0x1
#define UP_STRUCTURAL_SPRING      0x2
#define LEFT_STRUCTURAL_SPRING    0x4
#define DOWN_STRUCTURAL_SPRING    0x8

/*  SHEAR SPRINGS: */
#define UP_RIGHT_SHEAR_SPRING     0x10
#define UP_LEFT_SHEAR_SPRING      0x20
#define DOWN_LEFT_SHEAR_SPRING    0x40
#define DOWN_RIGHT_SHEAR_SPRING   0x80 

/*  BEND SPRINGS: */
#define RIGHT_BEND_SPRING         0x100
#define UP_BEND_SPRING            0x200
#define LEFT_BEND_SPRING          0x400
#define DOWN_BEND_SPRING          0x800
  

#define CLOTH_SIZE 			  2.0f
  
#define SPRING_STRUCT_CONSTANT 	  40.75f  
#define SPRING_SHEAR_CONSTANT 	  40.75f  
#define SPRING_BEND_CONSTANT 	  10.75f  

#define SPRING_DAMP_CONSTANT  -0.15f

#define SYSTEN_DAMP_CONSTANT  -0.20f
//00525f

#define GRAVITY 			  -0.010981f
#define DELTA_TIME 			  (30*0.0016666f)
#define PARICLE_RADIUS 	  		0.07f

#define FLAT_INDEX(v,w) (v.x + v.y*w)
__kernel void init(__global float4* next_position,
				   __global float4* position,
				   __global float4* previous_position,
				   __global short* spring,
				   const float4 offset,
				   const uint width
					){
	int x = get_global_id(0);
	int y = get_global_id(1);
	int i = (x + y*width);
	
	float scale = CLOTH_SIZE/convert_float(width-1.0f);
	
	float wf_half = convert_float(width)/2.0f;
	float xf = (x-wf_half)*scale;
	float yf = (y-wf_half)*scale;	
	float zf = CLOTH_SIZE; //.0f; 
	
	float4 pos = offset+(float4)(xf,zf,yf,0)+(float4)(0,2.2f,-0.2f,0.0f);
	
	position[i] 		 = pos;
	next_position[i]	 = pos; 
	previous_position[i] = pos;
	
	short spr 			 = 0;

	int2 xy = {x,y};
	for( int n = 0; n < 12; n ++){
		int2 t_xy = xy+spring_offsets[n];		
		if( t_xy.x >= 0 && t_xy.x < width && t_xy.y >= 0  && t_xy.y < width)
			spr |= 1 << n;
	}

	spring[i] = spr;
}
float2 get_spring_info(int n,float w){
	float delta   = CLOTH_SIZE/(w-1.0f);
	
	float lstruct  = delta;	
	float lshear = length((float2)lstruct);	
	float lbend   = 2.0f*lstruct;	
	
	if( n < 4)
		return (float2)(lstruct,SPRING_STRUCT_CONSTANT);
	if(n < 8)
		return (float2)(lshear,SPRING_SHEAR_CONSTANT);
	if( n < 12)
		return (float2)(lbend,SPRING_BEND_CONSTANT);
	
	return -11111;
}

float4 get_velocity(float4 p , float4 p_prev){
	return (p-p_prev)/DELTA_TIME;
}

__kernel void update(
					 __global float4* next_position,
					 __global float4* position,
					 __global float4* previous_position,
					 __global short* spring,
					 const uint width
				   ){

	int x = get_global_id(0);
	int y = get_global_id(1);
	int2 xy = {x,y};
	
	int i = (x + y*width);
	
	float mass_inv = 0.01f*convert_float(width);
	
	short spr = spring[i];
	
	float4 p1  		= position[i];
	float4 p1_prev  = previous_position[i];
	float4 v1  		= get_velocity(p1,p1_prev);
	
	float4 spring_force_tot  = 0;
	float4 dp_sum = 0;
	for( int n = 0; n < 12; n ++){
		if( ((1 << n) & spr) > 0 ) {
			
			float2 spring_info = get_spring_info(n,convert_float(width));
			float rest_len = spring_info.x;
			float spring_k = spring_info.y;
			
			int2  index_offset  = spring_offsets[n];
			int2 tmp = xy + index_offset;
			int j = (tmp.x + tmp.y*width); 
			
			float4 p2 	   = position[j];
			float4 p2_prev = previous_position[j];
			float4 v2 = get_velocity(p2,p2_prev);
			
			float4 dp = (p1-p2);
			float4 dv = v1-v2;
			

			float len = length(dp.xyz)+0.000001f;
			float4 dir = dp/len;
			
			dp_sum += (float4)(rest_len,len,0,0);
			
			float spring_force  = -spring_k*(len - rest_len);
			float spring_damp   = SPRING_DAMP_CONSTANT*dot(dir.xyz,dv.xyz); 
			spring_force_tot += dir*(spring_force+spring_damp);
			
		}		
	}
	
	float4 acceleration = 0;
	
	acceleration += (float4)(0,GRAVITY,0,0);
	
	acceleration += v1*SYSTEN_DAMP_CONSTANT*mass_inv;
	
	acceleration += spring_force_tot*mass_inv;
	
	float4 p1_next = 2.0f*p1 - p1_prev  + DELTA_TIME * DELTA_TIME * acceleration;	
	if( y > (width-2)  &&  x % 10 > 4)
		p1_next = p1;
		
/*
	float r = 1.0f;
	float4 c = (float4)(0,-1.5f,0 ,0);
	float4 sp_dir  = p1_next-c;
	float sp_dist  = length(sp_dir.xyz);
	sp_dir /= (sp_dist+0.001f);
	if(sp_dist < r){
		p1_next += sp_dir*(r-sp_dist);
	}
*/
	next_position[i] = p1_next; 
	

}

bool collideTriangle(uint triangle, __global const uint* vertex_index, __global const float4* vertex_pos, float4* pos_out, float4 vel)
{
	
    float4 V0 = vertex_pos[vertex_index[triangle+0]];
    float4 V1 = vertex_pos[vertex_index[triangle+1]];
    float4 V2 = vertex_pos[vertex_index[triangle+2]];
    V0.w = 0;
    V1.w = 0;
    V2.w = 0;
	float4  pos = *pos_out;
	pos.w = 0.0f;

	
	
    float4 u = V1 - V0;
    float4 v = V2 - V0;
    float4 n = cross(u,v);
    if (dot(n,n) == 0)
    {
        return false;

    }

	 float4 w0 = pos - V0;
    float4 displacement = vel*DELTA_TIME;

    float a = dot(n,w0);
    if (dot(n,w0) < 0)
    {
        return false; // on the other side of the triangle
    }

    float b = dot(n,displacement);
    /*if (fabs(b) < 0.000001f) 
    {     
        // ray is parallel to triangle plane
        if (a == 0)             // ray lies in triangle plane
            return false;
        else 
            return false;   // ray disjoint from plane
    }*/
	
    float r = -a/b;
    if (r < 0.0f)
    { 
        return false;       // ray goes away from triangle
    }


    float4 I = pos + r * displacement;   // intersect point of ray and plane

    // is I inside T?
    float uu = dot(u,u);
    float uv = dot(u,v);
    float vv = dot(v,v);
    float4 w = I - V0;
    float wu = dot(w,u);
    float wv = dot(w,v);
    float D = uv * uv - uu * vv;

    // get and test parametric coords
    float s = (uv * wv - vv * wu) / D;
    if (s < 0.0f || s > 1.0f)        // I is outside T
        return false;
    float t = (uv * wu - uu * wv) / D;
    if (t < 0.0f || (s + t) > 1.0f)  // I is outside T
        return false;
	
	//return true;
	
    n = normalize(n);

    float4 Vc = pos	+ displacement - V0;
    float dist = 1.0*dot(Vc,n);
    float depth = PARICLE_RADIUS - dist;
		
    // particle penetrates surface 
    if (depth > 0)
    {
        float4 dPos = normalize(displacement)*dot(n,displacement)*dist;
		*pos_out   += 4.0*dPos;
		
        return true;
    }

    return false;
}



int intersectBox(float4 r_o, float4 r_d, float4 boxmin, float4 boxmax, float *tnear, float *tfar)
{
    // compute intersection of ray with all six bbox planes
    float4 invR = (float4)(1.0f,1.0f,1.0f,1.0f) / normalize(r_d);
    float4 tbot = invR * (boxmin - r_o);
    float4 ttop = invR * (boxmax - r_o);

    // re-order intersections to find smallest and largest on each axis
    float4 tmin = min(ttop, tbot);
    float4 tmax = max(ttop, tbot);

    // find the largest tmin and the smallest tmax
    float largest_tmin = max(max(tmin.x, tmin.y), max(tmin.x, tmin.z));
    float smallest_tmax = min(min(tmax.x, tmax.y), min(tmax.x, tmax.z));

    *tnear = largest_tmin;
    *tfar = smallest_tmax;

    return smallest_tmax > largest_tmin;
}

typedef struct 
{
        float4 bbMin;
        float4 bbMax;
        int left;
        int right;
        uint bit;
        uint trav;
} BVHNode;


bool collideAABB(__global const BVHNode* node, float4 pos, float4 vel)
{
    float near;
    float far;
    if (intersectBox(pos, vel, node->bbMin, node->bbMax, &near, &far))
    {
        return near < max(PARICLE_RADIUS, fast_length(vel));
    }
    return false;
}
#define QUEUE_SIZE 256
__kernel void collision(
                           __global float4* position, 
						   __global float4* previous_position, 						   
						   __global const float4* vertex_pos, 
						   __global const uint* vertex_index, 
                           __global const BVHNode* bvh, 
                           __global uint* bvhRootPtr						   
						)
{
    uint particle = get_global_id(0);
	uint bvhRoot = bvhRootPtr[0];
    float4 pos  = position[particle];
	float4 prev_pos = previous_position[particle];
	float4 vel = get_velocity(pos,prev_pos);

    uint queue[QUEUE_SIZE];
    uint head = 0;
    uint tail = 1;
    queue[0] = bvhRoot;
    while (head != tail)
    {
        uint iter = queue[head];
        head = (head+1)%QUEUE_SIZE;
        uint left = bvh[iter].left;
        uint right = bvh[iter].right;
        if (left == right)
        {

    	//	position[particle] = prev_pos;                
		//	break;			
		
            // leaf .. intersect
			
            if (collideTriangle(bvh[iter].bit*3, vertex_index, vertex_pos, &pos, vel))
            {
				position[particle] = prev_pos; //-vel*DELTA_TIME;                
                break;
            }
        }
        else
        {
            if (collideAABB(&bvh[left], prev_pos, vel))
            {
                queue[tail] = left;
                tail = (tail+1)%QUEUE_SIZE;
            }
            if (collideAABB(&bvh[right], prev_pos, vel))
            {
                queue[tail] = right;
                tail = (tail+1)%QUEUE_SIZE;
            }
        }
    }
	
}

__kernel void compute_normal(__global float4* position,
							  __global float4* normal,
							  const uint width
							  ){

	int x = get_global_id(0);
	int y = get_global_id(1);
	int i = (x + y*width);
	float w = width;
	
	int2 xy[6] = {
			{x+1,y},
			{x,y+1},
			{x-1,y+1},
			{x-1,y},
			{x,y-1},
			{x+1,y-1}
	};	
					
	float4 center = position[i];
	float4 delta[6];
	for ( int n = 0; n < 6; n ++){
		int j = FLAT_INDEX(clamp(xy[n],0,width),w);
		delta[n] = center - position[j];
	}
	
	float3 norm = 0.0f;
	for(int n = 0; n < 6; n++){
		int i1 = n;
		int i2 = (n+1)%6;
		norm += normalize(cross(delta[i1].xyz,delta[i2].xyz));
	}
	norm /= 6.0f;
	normal[i].xyz = norm;
	
}
