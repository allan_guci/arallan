#ifndef IMAGE_UTIL
#define IMAGE_UTIL
#include "local_mem_util.cl"
const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE | //Natural coordinates
						 CLK_ADDRESS_REPEAT | //Clamp to zeros
						 CLK_FILTER_NEAREST; //Don't interpolate						 
#define CHECK_BOUNDS(p,s,o) \
if(p < o || p > (s-2-o)) return

#define FLAT_ID get_global_id(1)*get_global_size(0) + get_global_id(0)

#define READ_IMG(img,idx) read_imagef(img, sampler,idx)
#define WRITE_IMG(img,idx)
#define FILTER_KERNELS  \
		float kern_lapl[9] = { 0.5f,1.0f ,0.5f,1.0f,-6.0f,1.0f,0.5f,1.0f,0.5f}; \
		float kern_lapl2[9] = { 0.5f,1.0f ,0.5f,1.0f,-6.0f,1.0f,0.5f,1.0f,0.5f}; \
		float kern_sobx[9] = {-1,0,1,-1,0,1,-1,0,1}; \
		float kern_soby[9] = { 1,1,1,0,0,0,-1,-1,-1};		
		
		
__kernel void laplacian(
		__read_only image2d_t in,
		__write_only image2d_t out
){
	int4 tmp;
	int2 g_id 	  = (int2) (get_global_id(0),get_global_id(1));
	int2 l_id 	  = (int2) (get_local_id(0),get_local_id(1))+1; 
	IMG2D_TO_LOCAL_MEM(lbuffer,in,g_id,l_id);
	
	FILTER_KERNELS; 
	
	float cache[9];
	int i = 0;
	int size_kern = 1;
	for( int yi = -size_kern; yi <= size_kern; yi ++)
		for( int xi = -size_kern; xi <=size_kern; xi ++){			
				int2 idx2 = l_id+(int2)(xi,yi);
				cache[i] = lbuffer[idx2.y][idx2.x];
				i++;		
	}
	float res_lapl = 0.0f;
	float resx     = 0.0f;
	float resy     = 0.0f;
	
	for(int n = 0; n < 9; n++){
				res_lapl += kern_lapl[n]*cache[n];
				resx 	 += kern_sobx[n]*cache[n];
				resy 	 += kern_soby[n]*cache[n];
	}
	float res = max(res_lapl+length((float2)(resx,resy)),0.0f);
	
	write_imagef(out,g_id,res);
	//write_imagef(out,g_id,READ_IMG(in,g_id).x);
}
#endif