const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE | //Natural coordinates
						 CLK_ADDRESS_REPEAT | //Clamp to zeros
						 CLK_FILTER_NEAREST; //Don't interpolate						 
const sampler_t samplerLinear = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_REPEAT | CLK_FILTER_LINEAR; 

typedef struct {
	float4 offset;
	float4 scale;
	float4 camoffset;
	float frequency;
	float amplitude;
	float ncompontents;
	float time;
}SurfacePropertyType;

__kernel void init_pos(
		__global float4* vertex_position,
		__global float2* vertex_texcoord,
		__global SurfaceDataType* surface_data,
		__global SurfaceDataType* surface_data_next,
		const SurfacePropertyType properties
){
	int i = get_global_id(0);
	float x = (float)(i % SURF_SIZE_X)/SURF_SIZE_X;
	float y = 0.0f;
	float z = (float)(i / SURF_SIZE_X)/SURF_SIZE_X;
	
	//vertex_position[i] = vertex_position[i].xyzw*properties.scale + properties.offset;
	vertex_position[i] = (float4)(x,y,z,0.0f)*properties.scale + properties.offset;
	vertex_texcoord[i] = (float2)(x,z);
	surface_data[i]. = (float2)(x,z);
	vertex_texcoord[i] = (float2)(x,z);
}


float2 wrap_xz(float2 v, float2 max_size){
	max_size = (float2)(1024,1024);
	 float2 tmp = max_size;
	if(v.x >= tmp.x) v.x -= tmp.x; //max_size.x;  
	if(v.x < -tmp.x) v.x += tmp.x;//max_size.x;
	if(v.y >= tmp.y) v.y -= tmp.y; //max_size.y;
	if(v.y < -tmp.y) v.y += tmp.y; //max_size.y;
	return v;
}
#define FLAT_INDEX(v,w) (v.x + v.y*w)
__kernel void update(
		__global float4* vertex_position,
		__read_only image2d_t height_map,
		const SurfacePropertyType properties
){
	int x = get_global_id(0);
	int y = get_global_id(1);
	int i =  y*get_global_size(0) + x; 
	
	float2 posXZ = vertex_position[i].xz;
	float2 halfScaleInv = 0.5f/properties.scale.xz;

	float rad_att = pow(1.3f*length(posXZ*halfScaleInv),4.0f);
	
		float amplitude[5] = {
		0.23f,0.1f,0.03f,0.08f,0.04f
	};
	
	float speed[5] = {
		5.043f,2.01f,8.013f,14.008f,-8.01f
	};
	float2 direction[5] = {
		{1.707f,0.235f},
		{-1.72307f,1.707f},
		{-1.72307f,1.72307f},
		{1.72307f,-0.5234f},
		{0.707f,-0.707f}
	};
	float z = 0;
	int NUM_WAVES = 2;

	float2 tmp_pos = (float2)(x,y)+ 4.0f*properties.camoffset.xz;

	
	for(int n = 0; n < NUM_WAVES; n++){
	
		float2 dir = normalize(direction[n]);
		float freq = 0.01f*speed[n]; 
		float phi   = 0.1f*speed[n];
		float tmp_inner = dot(dir,tmp_pos)*freq + properties.time*phi;
		float tmp_sin = (sin(tmp_inner) + 1.0f)*0.5f;
		float tmp_cos = cos(tmp_inner);
		float k = n+2;
		float CORR = 0.4f;
	//	z += CORR*amplitude[n] * pow(tmp_sin,k*1.5f); //+rand;
				
		float u = dot(dir,tmp_pos) + properties.time*speed[n];
		float2 uv = (float2)(u,tmp_pos.y);

		z += amplitude[n]*(read_imagef(height_map, samplerLinear,uv).x-0.5f);
		
		
	}

	z+= properties.offset.y - rad_att;
	vertex_position[i] = (float4)(posXZ.x,z,posXZ.y,0); 

}
#define CHECK_BOUNDS(p,s,o) \
(p < o || p > (s-2-o));

typedef struct {
	float prev_y_pos;
	float curr_y_pos;
}SurfaceDataType;

__kernel void update2(
		__global float4* vertex_position,
		__global SurfaceDataType* surface_data,
		__global SurfaceDataType* surface_data_next,
		const SurfacePropertyType properties
){
	int x = get_global_id(0);
	int y = get_global_id(1);
	int i =  y*get_global_size(0) + x; 
	
	char mask = CHECK_BOUNDS(x,get_global_size(0),1);
	mask     &= CHECK_BOUNDS(y,get_global_size(1),1);
	
	// Read indata
	float prev_pos = surface_data.prev_pos;
	float curr_pos = surface_data.curr_pos;		
	float default_y = properties.offset.y;
	float force_y 	= 0;
	
	
	float time = properties.time;
	float ext_force_time = fmod(time,5.5);
	
	if(mask != 1){
	
		float y_neigh = 0.0f;
		
		int2 offset[4] = {
							{-1,0},
							{1,0},
							{0,-1},
							{0,1}
						 };
						 
		// Read neighbour particle heights
		for(int n = 0; n < 4; n++)
		{
			int idx = (y+offset[n].y)*get_global_size(0) + (x+offset[n].x);
			y_neigh += vertex_position[idx].y;		
		}
			
		// Compute force
		float diff_y_neigh   = (y_neigh*0.25f - center_y);
		float diff_y_default = (properties.offset.y - center_y);	
		force_y    			 = center_y + 0.4f*diff_y_neigh + 0.000000f*diff_y_default; 
	
	}
	
	// Add external force
	float2 radius = convert_float2((int2)(x,y))/convert_float2((int2)(get_global_size(0),get_global_size(1)));
	float len = length(radius-0.5f);
	float circle_size = 0.02f;
	
	
	if(len < circle_size) 
		force_y = default_y +  0.3f*cos(1.1f*properties.time);
		
	
	
	float dt = 0.1f;
	float vel_y = (curr_pos - prev_pos)/dt;
	
	// Integrate
	float next_y_pos = 2.0f*curr_pos - prev_pos  + pow(dt,2.0f)*force_y;
	
	// Write output
	surface_data_next.prev_pos = curr_pos;
	surface_data_next.curr_pos = next_y_pos;
	
	vertex_data[i].y = next_y_pos; 
}

__kernel void commit_update(
		__global float4* vertex_position,
		__global float* vertex_y
){
	int x = get_global_id(0);
	int y = get_global_id(1);
	int i =  y*get_global_size(0) + x; 
	vertex_position[i].y = vertex_y[i];
}
__kernel void compute_normal(__global float4* position,
							  __global float4* normal
							  ){

	int x = get_global_id(0);
	int y = get_global_id(1);
	int i = y*get_global_size(0) + x;
	int w = get_global_size(0);
	
	int2 xy[6] = {
			{x+1,y},
			{x,y+1},
			{x-1,y+1},
			{x-1,y},
			{x,y-1},
			{x+1,y-1}
	};	
					
	float4 center = position[i];
	float4 delta[6];
	for ( int n = 0; n < 6; n ++){
		int j = FLAT_INDEX(clamp(xy[n],0,w-1),w);
		delta[n] = center - position[j];
	}
	
	float3 norm = 0.0f;
	for(int n = 0; n < 6; n++){
		int i1 = n;
		int i2 = (n+1)%6;
		norm += normalize(cross(delta[i1].xyz,delta[i2].xyz));
	}
	norm /= 6.0f;
	normal[i].xyz = norm;
	
}
