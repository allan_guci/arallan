#include "adaptive_util.cl"
typedef struct{
    uint num_interval;
    uint leaf_stride[256];
    uint tree_nr[256];
}AccessMetaType;



void init(uint lid, __local AccessMetaType* META, __global uint2* group_interval, __global uint* subtree_num_leaves){
    
    if(lid == 0){
        
        uint2 interval = group_interval[get_group_id(0)];
        uint start = interval.x; uint stop = interval.y;
        uint first_offset =  subtree_num_leaves[start-1] * ZERO_CHECK(start);                   
                         
        META->num_interval   = (interval.y - interval.x)+1;            
        META->tree_nr[0]     = start;
        META->leaf_stride[0] = 0;
        
        uint n = 1;
        for (uint i = start; i <= stop; i ++ ){
                META->leaf_stride[n] = subtree_num_leaves[i] - first_offset;
                META->tree_nr[n] = i+1;
                n++;
        }
    }
    barrier(CLK_LOCAL_MEM_FENCE); 
}    

void find_tree_and_leaf(uint lid, __local AccessMetaType* META,int* leaf_nr, int* tree_nr){
   
    for(uint i = 1; i <= META->num_interval; i++ ){        
        if(lid <  META->leaf_stride[i]){
            *leaf_nr = lid - META->leaf_stride[i-1];
            *tree_nr = META->tree_nr[i-1];     
            break;
        }
    }
}

void get_node(uint leaf_nr, uint tree_nr, __global NodeType* tree, __global uint* tree_direct_leaves, NodeType* node){
    uint root_idx = tree_nr * 341;
    uint leaf_idx = leaf_nr + root_idx;
    uint tree_idx = tree_direct_leaves[leaf_idx];
    *node = tree[tree_idx];
}