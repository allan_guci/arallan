
#include "adaptive_util.cl"
#include "image_util.cl"


__kernel void kernel_find_neighbors(
    __global NodeType* tree,
    __global uint * tree_stride_nodes,    
    __global uint * tree_stride_leaves,   
    __global uint2 * group_interval,    
      const  uint num_leaves,        
    __write_only image2d_t out
    )
{
    uint gid = get_global_id(0);
    uint lid = get_local_id(0);
    uint group  = get_group_id(0);
    
     
    INIT_PARTITIONING(meta, group_interval, tree_stride_nodes, tree_stride_leaves);
    PARTITION_START(meta, current_root, leaf_nr, lid)        
        
        
        
        uint write_idx = gid; //tree_offsets[current_root] + lid;         
       
        if( leaf_nr == -1 ){
            return;
        }
        leaf_nr++;
        uint node_start = meta->read_offsets[current_root];               
        
        NodeType node = tree[node_start];

        for( int lvl = 0; lvl < 5; lvl ++ ){
            if(node.is_leaf){
            
                ushort4 bbSquare  = node.bbSquare;
                uint xstart = bbSquare.s0;  uint xstop  = bbSquare.s2;
                uint ystart = bbSquare.s1;  uint ystop  = bbSquare.s3;
                for(int y = ystart; y < ystop; y++)
                    for(int x = xstart; x < xstop; x++)
                        write_imagef(out,(int2)(x,y), node.value);
       
                return;
            }            
            uint lvl_leaves = 0;           
            
            for( int c = 0; c < 4; c++ ){
                
                uint child_idx = node_start + node.children[c];
                NodeType child = tree[child_idx];
                lvl_leaves += child.num_leaves;
                
                if( leaf_nr <=  lvl_leaves ){
                    leaf_nr -= (lvl_leaves - child.num_leaves);
                    node = child;
                    c = 4;                  
                }                   
            }               
        }

    PARTITION_END(lid)


}