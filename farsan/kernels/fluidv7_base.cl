#ifndef cl_khr_3d_image_writes
#error buhu
#endif


#pragma OPENCL EXTENSION cl_khr_3d_image_writes : enable

const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE | //Natural coordinates
						 CLK_ADDRESS_CLAMP | //Clamp to zeros
						 CLK_FILTER_NEAREST; //Don't interpolate						 
const sampler_t samplerLinear = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP | CLK_FILTER_LINEAR; 

#define B_MAX (float)(EDGE_MAX)
#define B_MIN (float)(EDGE_MIN)

/** Global id and grid position **/
#define GLOBAL_ID_DECL int4 G_ID = (int4)(get_global_id(0), get_global_id(1), get_global_id(2),0)+1
#define WGROUP_ID_DECL int4 W_ID = (int4)(get_group_id (0), get_group_id (1), get_group_id (2),0)
#define LOCAL_ID_DECL int4 L_ID = (int4)(get_local_id(0), get_local_id(1), get_local_id(2),0)
#define GRID_POS (float4)(G_ID.x,G_ID.y,G_ID.z,0)
#define CLAMP_TO_GRID(p) clamp(p,(float4)(B_MIN,B_MIN,B_MIN,B_MIN),(float4)(_G_SIZE_X-B_MAX,_G_SIZE_Y-B_MAX,_G_SIZE_Z-B_MAX,1))
#define FLAT_LOCAL = L_ID.x + _L_SIZE_X*(L_ID.y + L_ID.z*_L_SIZE_Y)
/** Math functions **/
#define GRADIENT(v) (v.s1357-v.s0246)*0.5f
#define SAFE_NORMALIZE(v)  v = v/(length(v)+0.0001f)
#define NAIVE_NOISE(s) fmod(length(s)*0.32332f,0.34544f)
#define RAD_ATTENUATION(dr) min((dr)*10.0f,1.0f)

/** Read from image3d_t **/
#define READ_IMG(img) read_imagef(img, sampler,G_ID)
#define READ_IMG_POS(img,pos) read_imagef(img, sampler,pos)
#define READ_IMG_LINEAR(img,pos) read_imagef(img, samplerLinear,pos)
#define READ_IMG_OFFSET(img,offs) read_imagef(img, sampler,G_ID+offs)
#define VALID_NEIGHBOUR_FLAGS  (float8)(  \
									G_ID.x > EDGE_MIN , \
									G_ID.x < (_G_SIZE_X - EDGE_MAX)  , \
									G_ID.y > EDGE_MIN  , \
									G_ID.y < (_G_SIZE_Y - EDGE_MAX) , \
									G_ID.z > EDGE_MIN , \
									G_ID.z < (_G_SIZE_Z - EDGE_MAX) , \
									0,\
									0) 
									
#define READ_NEIGHBOURS(img,EW,DU,SN)  (float8)(  \
										READ_IMG_OFFSET(img,(int4)(-1,0,0,0)).EW, READ_IMG_OFFSET(img,(int4)(1,0,0,0)).EW, \
										READ_IMG_OFFSET(img,(int4)(0,-1,0,0)).DU, READ_IMG_OFFSET(img,(int4)(0,1,0,0)).DU, \
										READ_IMG_OFFSET(img,(int4)(0,0,-1,0)).SN, READ_IMG_OFFSET(img,(int4)(0,0,1,0)).SN, \
										0.0f, 0.0f)
	
										
/** Write to image3d_t **/
#define WRITE_IMG(img,val) write_imagef(img,G_ID,val)
#define WRITE_IMG_OFFS(img,val,OFFS) write_imagef(img,G_ID+OFFS,val)

/** Constants **/
#define SOURCE_POSITION_X  _G_SIZE_X/4.0f 
#define SOURCE_POSITION_Y  _G_SIZE_Y/4.0f
#define SOURCE_POSITION_Z  _G_SIZE_Z/2.0f + (offset+0.5f)*2.0f
#define SOURCE_POSITION (float3)(SOURCE_POSITION_X, SOURCE_POSITION_Y ,SOURCE_POSITION_Z)


/** Add source constants **/
#define SOURCE_SCALE_XY 0.2f
#define SOURCE_SCALE_Z 1.5f
#define NAIVE_NOISE(s) fmod(length(s)*0.22332f,0.74544f)
/** Buoyancy constants **/
#define TEMP_0 0.04f
#define SMOKE_WEIGHT 0.00125f
#define BUOY_DIRECTION (float4)(1,0, 0, 0)
#define BOUY_NOISE_SCALE 0.0f

/** Confinement constants **/
#define CONFINE_SCALE 2.0f


typedef struct {
		float2 value;
        float4 position;
        float  radius;
        float4 dissipation;
}SourceMetaType;

float4 get_interpolated_img( __read_only image3d_t img_source,float4 pos)
{

	
	pos = clamp(pos,(float4)(1,1,1,1),(float4)(_G_SIZE_X-2,_G_SIZE_Y-2,_G_SIZE_Z-2,1));
	int3 idx0 = { (int)pos.x, (int)pos.y, (int)pos.z};
	
	int3 idx1;
	idx1.x = min(idx0.x+1,_G_SIZE_X-2);
	idx1.y = min(idx0.y+1,_G_SIZE_Y-2);
	idx1.z = min(idx0.z+1,_G_SIZE_Z-2);

	float3 t = { pos.x - idx0.x, pos.y - idx0.y, pos.z - idx0.z};

	float4 f0,f1;
 
	f0 = read_imagef(img_source, sampler,(int4)(idx0.x,idx1.y,idx0.z,0)); f1 = read_imagef(img_source, sampler,(int4)(idx1.x,idx1.y,idx0.z,0));
	float4 FrontTop = mix(f0, f1, t.x);

	f0 = read_imagef(img_source, sampler,(int4)(idx0.x,idx0.y,idx0.z,0)); f1 = read_imagef(img_source, sampler,(int4)(idx1.x,idx0.y,idx0.z,0));
	float4 FrontBot = mix(f0, f1, t.x);

	f0 = read_imagef(img_source, sampler,(int4)(idx0.x,idx1.y,idx1.z,0)); f1 = read_imagef(img_source, sampler,(int4)(idx1.x,idx1.y,idx1.z,0));
	float4 BackTop = mix(f0, f1, t.x);

	f0 = read_imagef(img_source, sampler,(int4)(idx0.x,idx0.y,idx1.z,0)); f1 = read_imagef(img_source, sampler,(int4)(idx1.x,idx0.y,idx1.z,0));
	float4 BackBot = mix(f0, f1, t.x);

	f0 = mix(FrontBot, FrontTop,t.y);
	f1 = mix(BackBot, BackTop, t.y);

	return mix(f0, f1, t.z);


}

float4 clamp_neighbors( __read_only image3d_t img_source,float4 pos,float4 src)
{

	int3 idx0 = (int3)(pos.x, pos.y, pos.z);
	
	idx0.x = clamp(idx0.x, 1, (int)get_global_size(0) - 3);
	idx0.y = clamp(idx0.y, 1, (int)get_global_size(1) - 3);
	idx0.z = clamp(idx0.z, 1, (int)get_global_size(2) - 3);
	
	float4 f0,f1,f2,f3,f4,f5,f6,f7;
	f0 = read_imagef(img_source, sampler,(int4)(idx0.x,idx0.y,idx0.z,0));
	f1 = read_imagef(img_source, sampler,(int4)(idx0.x+1,idx0.y,idx0.z,0));
	f2 = read_imagef(img_source, sampler,(int4)(idx0.x,idx0.y+1,idx0.z,0)); 
	f3 = read_imagef(img_source, sampler,(int4)(idx0.x+1,idx0.y+1,idx0.z,0));
	f4 = read_imagef(img_source, sampler,(int4)(idx0.x,idx0.y,idx0.z+1,0)); 
	f5 = read_imagef(img_source, sampler,(int4)(idx0.x+1,idx0.y,idx0.z+1,0));
	f6 = read_imagef(img_source, sampler,(int4)(idx0.x,idx0.y+1,idx0.z+1,0)); 
	f7 = read_imagef(img_source, sampler,(int4)(idx0.x+1,idx0.y+1,idx0.z+1,0));
	
	
	float4 src_min = min(min(min(min(min(min(min(f0, f1), f2), f3), f4), f5), f6), f7);
	float4 src_max = max(max(max(max(max(max(max(f0, f1), f2), f3), f4), f5), f6), f7);

	return clamp(src,src_min,src_max);


}


__kernel void advect_phase1(
						__read_only image3d_t img_in_velocity ,
						__read_only image3d_t img_source_in, 
						__write_only image3d_t img_source_out
					)
{
	GLOBAL_ID_DECL;

	float4 vel = READ_IMG(img_in_velocity);
	float4 pos = GRID_POS; 
	pos 	  -= _DT*vel;
	
	float4 v = READ_IMG_LINEAR(img_source_in,pos+0.5f); //+0.5f); //get_interpolated_img(img_source_in,pos); 
	WRITE_IMG(img_source_out,v);	
	

	
}

__kernel void advect_phase2(
						__read_only image3d_t img_in_velocity,
						__read_only image3d_t img_source_in, 
						__read_only image3d_t  img_src_forw, 
						__write_only image3d_t img_source_out
					)
{
	
	GLOBAL_ID_DECL;
	
	float4 vel = READ_IMG(img_in_velocity);
	float4 pos = GRID_POS;
	pos 	  +=  _DT*vel;
	
	float4 src_forw_back =  READ_IMG_LINEAR(img_source_in,pos+0.5f); // get_interpolated_img(img_src_forw,pos); 
	
	pos  = GRID_POS;
	
	
	float4 src_forw = READ_IMG(img_src_forw); //get_interpolated_img(img_src_forw,pos); //+get_interpolated_img(img_src_forw,pos); //READ_IMG_LINEAR(img_src_forw,pos+TEMP_OFFSET);
	pos -= _DT*vel;
	float4 src_mid  = READ_IMG(img_source_in);
	
	float4 src_mc	= src_forw + 0.5f * 1.0f * (src_mid - src_forw_back);
	float4 src_out  = clamp_neighbors(img_source_in,pos,src_mc);
	
	WRITE_IMG(img_source_out,src_out);
					
}
            
__kernel void add_velocity(	__read_only image3d_t  img_in_vel, 
							__write_only image3d_t  img_out_vel, 
							__read_only image3d_t  img_props, 
						    const SourceMetaType meta					  
					)             
{ 
	GLOBAL_ID_DECL;
	
	float4 vel = READ_IMG(img_in_vel);
	float2 P   = READ_IMG(img_props).xy;
	
	float D = P.x;
	float T = P.y;
	float4 dissipation = meta.dissipation;
	if(D > 0){
		vel += (_DT * (T-TEMP_0) + D * SMOKE_WEIGHT ) * (BUOY_DIRECTION);
	}
	WRITE_IMG(img_out_vel,vel*dissipation);
	
}

__kernel void add_properties(
						  __read_only  image3d_t  img_in_source, 
						  __write_only image3d_t img_out_source,
						  const SourceMetaType meta					  
					)             
{ 
	GLOBAL_ID_DECL;

	float4 pos = GRID_POS;
	float4 s   = READ_IMG(img_in_source);
	s.xy *= meta.dissipation.xy;
	
	const float  radius   = meta.radius; 
	const float4 position = meta.position;
	float3 sp  = 	(float3)(G_ID.x*4.0f,G_ID.y,G_ID.z) - position.xyz; sp.xy *= sp.xy*SOURCE_SCALE_XY;	sp.z  *= SOURCE_SCALE_Z;
	float r  = length(sp);
	
	if( r < radius ){
		float n = 0.0f; 
		float d = RAD_ATTENUATION(radius-r)-n;
		s.xy = meta.value*d; 
	}
	
	WRITE_IMG(img_out_source,s);
	
}    


__kernel void fluid_divergence(   
	__read_only image3d_t img_velocity, 
	__write_only image3d_t img_divergence
)             
{ 
	GLOBAL_ID_DECL;
	
	float8 n_flags  = VALID_NEIGHBOUR_FLAGS;		
	float8 n_values = READ_NEIGHBOURS(img_velocity,x,y,z)*n_flags; 
	
	float divergence = ((n_values.s1-n_values.s0) + (n_values.s3-n_values.s2) + (n_values.s5-n_values.s4))*0.5f;

	WRITE_IMG(img_divergence,divergence);  
}	


__kernel void fluid_jacobi(                    
	__read_only image3d_t img_pressure, 
	__write_only image3d_t out_pressure,  
	__read_only image3d_t img_divergace 
)             
{ 

	GLOBAL_ID_DECL;
	float c_value    = READ_IMG(img_pressure).x;
	
	float8 n_flags   = VALID_NEIGHBOUR_FLAGS;
	
	float8 n_values  = READ_NEIGHBOURS(img_pressure,x,x,x)*n_flags+(1.0f-n_flags)*c_value;
	
	float divergence = READ_IMG(img_divergace).x;
	
	float p  = ((n_values.s0 + n_values.s1 + n_values.s2 + n_values.s3 + n_values.s4 + n_values.s5) - divergence)*0.16666666f;
	
	WRITE_IMG(out_pressure,p);  
}	

__kernel void clear_buffer(__write_only image3d_t out){
	GLOBAL_ID_DECL;
	WRITE_IMG(out,0); 
}

__kernel void fluid_projection(   
	__read_only image3d_t img_pressure,                  
	__read_only image3d_t in_velocity, 
	__write_only image3d_t out_velocity
	
)             
{ 
	GLOBAL_ID_DECL;
	float c_value    = READ_IMG(img_pressure).x;
	
	float8 n_flags   = VALID_NEIGHBOUR_FLAGS;
	float8 n_values  = READ_NEIGHBOURS(img_pressure,x,x,x) * n_flags + (1.0f-n_flags) * c_value;
	
	float4 mask = n_flags.s0246 * n_flags.s1357;
	
	float4 grad = GRADIENT(n_values);

	float4 c_vel = READ_IMG(in_velocity);
	float4 next_vel = mask*(c_vel-grad);
	
	WRITE_IMG(out_velocity,next_vel);  
	
}	
				
__kernel void vortify(
						__read_only image3d_t in_velocity, 
						__write_only image3d_t out_omega,
						__write_only image3d_t out_curl
						)
						
{
	GLOBAL_ID_DECL;
	
	float4 VC = READ_IMG(in_velocity);
	float4 VW = READ_IMG_OFFSET(in_velocity, (int4)(-1,0,0,0));	float4 VE = READ_IMG_OFFSET(in_velocity, (int4)(1,0,0,0));	
	float4 VD = READ_IMG_OFFSET(in_velocity, (int4)(0,-1,0,0));	float4 VU = READ_IMG_OFFSET(in_velocity, (int4)(0,1,0,0));	
	float4 VS = READ_IMG_OFFSET(in_velocity, (int4)(0,0,-1,0));	float4 VN = READ_IMG_OFFSET(in_velocity,(int4)(0,0,1,0));

	float4 omega = 0.5f*(float4)(
						    ((VU.z-VD.z) - (VN.y-VS.y)),
							((VN.x-VS.x) - (VE.z-VW.z)),
							((VE.y-VW.y) - (VU.x-VD.x)),
							0);
						
	float curl = length(omega);
	
	WRITE_IMG(out_omega,omega);  
	WRITE_IMG(out_curl,curl);  
}


						
__kernel void confine(
						__read_only image3d_t in_velocity, 
						__write_only image3d_t out_velocity,
						__read_only image3d_t in_omega,
						__read_only image3d_t in_curl
					)
{
	GLOBAL_ID_DECL;
	float8 n_values  = READ_NEIGHBOURS(in_curl,x,x,x);
	
	float4 eta  =  GRADIENT(n_values);
	SAFE_NORMALIZE(eta);
	
	float4 omega = READ_IMG(in_omega);
	
	float4 force = cross(eta,omega);
	
	float4 c_vel = READ_IMG(in_velocity);
	
	float4 next_vel = c_vel + CONFINE_SCALE*_DT*force; 
	
	WRITE_IMG(out_velocity,next_vel);  

}

