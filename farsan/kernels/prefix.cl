	// Sum previous indices ( prefix sum)
#define PREF_OFFSET_UP							\
if( local_id == 0){ 							\
	int offset = indices_local[0]; 				\
	indices_local[0] = 0; 						\
	for(int n = 1; n < LOCAL_MEM_SIZE; n ++ ){ 	\
		int tmp = indices_local[n];	 			\
		indices_local[n] = offset;				\
		offset += tmp;							\
	}											\
}	

#include "prefix_settings.cl"

void p(__local uint * indices_lvl, uint lid){
	if(lid < 4){
    
        const uint2 strides[4] = {{0, 64},
                                  {64, 128},
                                  {128,192},
                                  {192,256}};
        uint2 range = strides[lid];
    
    
    }

}

/*p_up(indices_local, local_id);*/
//#define PARTITION
#ifdef PARTITION
#define PREF_SIZE_UP				\
	p(indices_local,local_id);								
#else
#define PREF_SIZE_UP				\
if( local_id == 0)	 								\
	for(uint n = 1; n < LOCAL_MEM_SIZE; n ++ )		\
		indices_local[n] = indices_local[n-1]+indices_local[n];
#endif

#define PREF_SIZE_DOWN								\
if( local_id == 0){ 								\
	uint group = get_group_id(0);					\
	uint mask = group == 0;							\
	uint mask_inv = 1-mask;							\
	uint read_idx = (group-1) + mask; 				\
	offset[0] = mask_inv*indices_lvl_n_1[read_idx];	\
}	

#define PREF_OFFSET_DOWN							\
if( local_id == 0){									\
	offset[0] = indices_lvl_n_1[get_group_id(0)];	\
}													\


#ifdef OFFSET
	#define PSUM_UP_STRAT    PREF_OFFSET_UP
	#define PSUM_DOWN_STRAT  PREF_OFFSET_DOWN
	#define END_ELEM  LOCAL_SIZE
#else 
	#ifndef SIZE
		#error no prefix sum strategy defined 
	#endif 
	#define PSUM_UP_STRAT  	 PREF_SIZE_UP
	#define PSUM_DOWN_STRAT  PREF_SIZE_DOWN
	#define END_ELEM  LOCAL_SIZE-1
#endif 	


#define MEM_SIZE LOCAL_SIZE << 1

__kernel void trav_up(
						__global uint * indices_lvl_n,
						__global uint * indices_lvl_n_1,
						const uint num_elemements
						)
{

	uint global_id  = get_global_id(0);
	uint local_id   = get_local_id(0);
		
	// No larger global id than num vertices
	global_id = min(global_id, num_elemements);

	__local uint indices_local[DATA_SIZE];	
	
	// Copy indices at level n to local memory
	indices_local[local_id]  = indices_lvl_n[global_id];
	barrier(CLK_LOCAL_MEM_FENCE);
	
	// Sum previous indices ( prefix sum)
	PSUM_UP_STRAT
	barrier(CLK_LOCAL_MEM_FENCE);
	
	// Store new indices for level 
	indices_lvl_n[global_id]  = indices_local[local_id];

	

	
	// Store indices for level n+1 
	if(local_id == (LOCAL_SIZE-1))	{
		
		uint lidx = END_ELEM;
		
		if(get_group_id(0) == (get_num_groups(0)-1)){
			lidx = (global_id % LOCAL_SIZE);
		}
			
		indices_lvl_n_1[get_group_id(0)] = indices_local[lidx];
		
	}
	
}

__kernel void trav_down(
							__global uint * indices_lvl_n,
							__global uint * indices_lvl_n_1,
							const uint num_elemements
							)
{
	uint global_id  = get_global_id(0);
	uint local_id   = get_local_id(0);
	
	// First thread in each work group read offset common for all threads in the same work group
	__local uint offset[1];
	PSUM_DOWN_STRAT
	barrier(CLK_LOCAL_MEM_FENCE);
	
	// Discard threads that are out of range
	if( global_id >= num_elemements )
		return;
		
		
	
	// Store add offset to elements 
	indices_lvl_n[global_id] += offset[0];
	
}
