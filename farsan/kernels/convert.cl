__kernel void convert_f3_to_f4(
	__global float* vertex_position_in,
	__global float4* vertex_position_out
	){
	int i = get_global_id(0);
	int j = i*3;
	float x = vertex_position_in[j];
	float y = vertex_position_in[j+1];
	float z = vertex_position_in[j+2];
	vertex_position_out[i] = (float4)(x,y,z,0);
}