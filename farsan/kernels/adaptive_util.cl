typedef struct{
	uchar   is_leaf;
	uchar   level;
	uchar   parent;
    uchar   center;
	ushort  num_leaves;
	ushort  children[4];
	ushort4 bbSquare;
	float   value;
}NodeType;


int2 node_center(const NodeType node)
{
    return (int2) ( node.center >> 3, node.center & 0xF);
}

uint node_width(const NodeType node)
{
    return node.bbSquare.s2-node.bbSquare.s0;
}

uint node_height(const NodeType node)
{
    return node.bbSquare.s3-node.bbSquare.s1;
}
    


#define LSIZE 16
#define N_LEVELS 5
#define LAST_LEVEL (N_LEVELS-1)
#define TREE_DATA_SIZE 341
#define N_LEVELS_1 8


int get_tree_index1(int i, uchar lvl){	
    int offset 	  = (1 << lvl) -1;			
    int bit_shift = N_LEVELS_1 - lvl;
    int x     	  = i >> bit_shift; 
    return offset + x;	
}

int get_tree_index2(int i,int j, uchar lvl){	
    int offset 	  = ((1 << (lvl << 1) ) - 1)/3;	 // Fin. geometric sum for  4^n		
    int bit_shift = LAST_LEVEL - lvl;
    int x     	  = (i) >> bit_shift; 
    int y 		  = (j) >> bit_shift;
    int side  	  = 1 << lvl;
    return offset + y*side + x;			
}

ushort4 get_range(int i,int j, uchar lvl){
    int w  = (1 << (LAST_LEVEL-lvl));	
	return (ushort4)(i,j,i+w,j+w);
}


typedef struct{
    uint num_interval;
    uint write_offset;
    uint read_offsets[256];
    uint roots[256];
}PartitionMetaType;

#define ZERO_CHECK(x) ((int)x > 0)
    

#define INIT_PARTITIONING(META, GROUP_INTERV, INDEX_OFFS_READ, INDEX_OFFS_WRITE)\
    __local PartitionMetaType META[1];                                          \
    if(lid == 0){                                                               \
        uint2 interval = GROUP_INTERV[get_group_id(0)];                         \
        uint start = interval.x; uint stop = interval.y;                        \
        uint first_offset =  INDEX_OFFS_WRITE[start-1] * ZERO_CHECK(start);     \
        META->num_interval = (interval.y - interval.x)+1;                       \
        META->write_offset = first_offset;                                      \
        uint n = 0;    META->roots[0] = 0;                                      \
        META->roots[n+1]      = INDEX_OFFS_WRITE[start] - first_offset;         \
        META->read_offsets[n] = INDEX_OFFS_READ[start-1] * ZERO_CHECK(start);   \
        n++;                                                                    \
        for (uint i = start+1; i <= stop; i ++ ){                               \
                META->roots[n+1] = INDEX_OFFS_WRITE[i] - first_offset;          \
                META->read_offsets[n] = INDEX_OFFS_READ[i-1];                   \
                n++;                                                            \
        }                                                                       \
    }                                                                           \
    barrier(CLK_LOCAL_MEM_FENCE);                            
    
   
    
#if NUM_ITER > 1
    #define ROOT_IDX(META, LID, ROOT) (LID - META->roots[ROOT]) + META->read_offsets[ROOT]   
    #define ITER_EXTEND(LID)  LID += 256; if( LID >= TREE_DATA_SIZE) return;         
#else
    #define ROOT_IDX(META, LID, ROOT) (LID - META->roots[ROOT])  
    #define ITER_EXTEND(LID)  
#endif
      
    
#define PARTITION_START(META, ROOT, N_INDEX, LID)           \
    uint ROOT = 1;                                          \
    for (uint k = 0; k < NUM_ITER; k++){                    \
        int N_INDEX = -1;                                   \
        for(uint r = ROOT; r <= META->num_interval; r++)    \
            if(LID < META->roots[r]){                       \
                N_INDEX = ROOT_IDX(META, LID, r-1);         \
                ROOT  =  r-1;                               \
                break;                                      \
            }    
#define PARTITION_END(LID) \
       ITER_EXTEND(LID) \
       }


// Modulus checks 1D/2D				
#define MOD_CHECK2(cut,i,j) \
	((i % (cut)) == 0 && (j % (cut)) == 0)
	
#define MOD_CHECK1(cut,i) \
	((i % (cut)) == 0 )
	
// Tree travesesal up 
#define TRAV_UP(i,j) \
	for(uchar lvl = 0; lvl < N_LEVELS; lvl ++){ \
		
#define NODE_ACTIVE_TRAV_UP \
	if(MOD_CHECK2(1 << LAST_LEVEL-lvl,i,j) ) {

#define NODE_ACTIVE_TRAV_UP_END }

#define TRAV_UP_END }

// Tree travesesal down 
#define TRAV_DOWN(i,j) \
	for(uchar lvl = LAST_LEVEL; lvl > 0; lvl--){
    
#define TRAV_DOWN_LVL(i, j, lvl_start) \
	for(uchar lvl = lvl_start; lvl > 0; lvl--){
		
#define NODE_ACTIVE_TRAV_DOWN \
	if(MOD_CHECK2(1 << N_LEVELS-lvl,i,j) ) {

#define NODE_ACTIVE_TRAV_DOWN_END }

#define TRAV_DOWN_END }

// Create Tree indices		
#define CREATE_TREE_INDICES(BUF_NAME,i,j) \
	int BUF_NAME[5];	\
	for(uchar lvl = 0; lvl < N_LEVELS; lvl++)	\
		BUF_NAME[lvl] = get_tree_index2(i,j,lvl)
		
#define CREATE_TREE_INDICES_OFFSETS(BUF_NAME,i,j) \
	int BUF_NAME[5][2][2];	\
	for(uchar lvl = 0; lvl < N_LEVELS; lvl++)	\
		for(int yy = 0; yy < 2; yy++)	\
			for(int xx = 0; xx < 2; xx++){	\
				int shift = LAST_LEVEL - lvl; int ii = xx << shift; int jj = yy << shift;\
				BUF_NAME[lvl][yy][xx] = get_tree_index2(i+ii,j+jj,lvl); \
			}			
			
// CLEAR DATA	
#define CLEAR_TREE(tree,idx,i,j)		\
	for(uchar lvl = 0; lvl < N_LEVELS; lvl++) \
		if(MOD_CHECK2(1 << LAST_LEVEL - lvl,i,j))  \
			tree[idx[lvl]] = 0; \

	
#define CLEAR_TREE_LEAF_MASK(tree,idx,i,j)		\
	for(uchar lvl = 0; lvl < N_LEVELS; lvl++) \
		if(MOD_CHECK2(1 << LAST_LEVEL - lvl,i,j)){  \
			tree[idx[lvl]].is_leaf = 0; \
			tree[idx[lvl]].num_leaves = 0; \
		}			\
	
// COPY DATA	
#define COPY_TREE(tree_src,tree_dst,idx,i,j)		\
	for(uchar lvl = 0; lvl < N_LEVELS; lvl++) \
		if(MOD_CHECK2(1 << LAST_LEVEL - lvl,i,j))  \
			tree_dst[idx[lvl]] = tree_src[idx[lvl]]; 


	
// Assign child indices
#define ASSING_CHILD_INDICES(node,i,j,lvl) 						\
	int child_lvl   = min(lvl+1,LAST_LEVEL); 					\
	int stride 		= 1 << (LAST_LEVEL - child_lvl);			\
	node.children[0] = get_tree_index2(i,j,child_lvl);			\
	node.children[1] = get_tree_index2(i+stride,j,child_lvl);	\
	node.children[2] = get_tree_index2(i,j+stride,child_lvl);	\
	node.children[3] = get_tree_index2(i+stride,j+stride,child_lvl) 	
