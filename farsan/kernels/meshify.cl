#pragma OPENCL EXTENSION cl_khr_global_int32_base_atomics : enable
#pragma OPENCL EXTENSION cl_khr_local_int32_base_atomics : enable
#pragma OPENCL EXTENSION cl_khr_global_int32_extended_atomics : enable
#pragma OPENCL EXTENSION cl_khr_local_int32_extended_atomics : enable


__kernel void convert_f3_to_f4(
	__global float* vertex_position_in,
	__global float4* vertex_position_out){
	int i = get_global_id(0);
	int j = i*3;
	float x = vertex_position_in[j];
	float y = vertex_position_in[j+1];
	float z = vertex_position_in[j+2];
	vertex_position_out[i] = (float4)(x,y,z,0);
}

#define CHECK_BB(vpos,bbMin,bbMax)		\
				(						\
					vpos.x > bbMin.x &&  vpos.y > bbMin.y  && vpos.z > bbMin.z && 	\
					vpos.x < bbMAx.x &&  vpos.y < bbMAx.y  && vpos.z < bbMAx.z		\
				)
	
__kernel void create_mask(
					__global float4* vertex_position,
					__global int* vertex_indices_new,
					__global char* mask_char,
					const uint num_vertices
				  ){
	int i = get_global_id(0);

	if( i >= num_vertices)
		return;
	
	// vertex position
	float4 vpos = vertex_position[i];
	
	// Bounding box coordinates - TODO: set as argument
	float minXY = -5.2f;
	float minZ = -5.0f;
	float4 bbMin = (float4)(-0.1f,-1.4f,0.5f,0.0f);
	float4 bbMAx = (float4)(0.35f,2.0f,1.4f,0.0);
	


	// Check if vertex is inside selected box - set flag to 0 if outside
	int m = 1.0; //CHECK_BB(vpos,bbMin,bbMax);
	//int m = CHECK_BB(vpos,bbMin,bbMax);
	m = length(vpos) < 10.00f; //length(vpos.xyz) < 2.2f && length(vpos.xyz) > 0.5f; // && 
	vertex_indices_new[i] = m;
	mask_char[i] = m;
}

__kernel void create_triangle_mask(
									__global char * mask,
									__global char * tri_mask,
									__global int * triangles_old,
									__global int * triangle_offset_new
									)
{
	int i = get_global_id(0);
	int j = 3*i;
	// Read old indices of the triangle vertices
	int i0_old = triangles_old[j + 0];
	int i1_old = triangles_old[j + 1];
	int i2_old = triangles_old[j + 2];
	
	// Check of any of the vertices is discarded
	char  m = 1;
	m &= mask[i0_old];
	m &= mask[i1_old];
	m &= mask[i2_old];
	
	tri_mask[i] = m;
	triangle_offset_new[i] = convert_int(m);
}


__kernel void trav_up(
						__global int * indices_lvl_n,
						__global int * indices_lvl_n_1,
						const uint num_elemements
						)
{

	int global_id  = get_global_id(0);
	int local_id   = get_local_id(0);
		
	// No larger global id than num vertices
	global_id = min(global_id,(int)num_elemements);

	__local int indices_local[LOCAL_MEM_SIZE];	
	
	// Copy indices at level n to local memory
	indices_local[local_id]  = indices_lvl_n[global_id];
	barrier(CLK_LOCAL_MEM_FENCE);
	
	// Sum previous indices ( prefix sum)
	if( local_id == 0){
		int offset = indices_local[0];
		indices_local[0] = 0;
		for(int n = 1; n < LOCAL_MEM_SIZE; n ++ ){
			int tmp = indices_local[n];	
			indices_local[n] = offset;
			offset += tmp;
			
		}
	}
	barrier(CLK_LOCAL_MEM_FENCE);
	
	// Store new indices for level 
	indices_lvl_n[global_id]  = indices_local[local_id];

	// Store indices for level n+1 
	if(local_id == (LOCAL_SIZE-1))	{
		
		int lidx = LOCAL_SIZE;
		
		if(get_group_id(0) == (get_num_groups(0)-1))
			lidx = (global_id % LOCAL_SIZE);
			
		indices_lvl_n_1[get_group_id(0)] = indices_local[lidx];
	}
	
}

__kernel void trav_down(
							__global int * indices_lvl_n,
							__global int * indices_lvl_n_1,
							const uint num_elemements
							)
{
	int global_id  = get_global_id(0);
	int local_id   = get_local_id(0);
	
	// First thread in each work group read offset common for all threads in the same work group
	__local int offset[1];
	if(local_id == 0)
		offset[0] = indices_lvl_n_1[get_group_id(0)];
	barrier(CLK_LOCAL_MEM_FENCE);
	
	// Discard threads that are out of range
	if( global_id >= num_elemements )
		return;
	
	// Store add offset to elements 
	indices_lvl_n[global_id] += offset[0];
	
}


__kernel void map_vertex_buffer4(
							__global char * mask,
							__global int * indices_lvl_n,
							__global float4 * position_old,
							__global float4 * position_new
						 )
{
	int global_id  = get_global_id(0);
	int m = convert_int(mask[global_id]);
	if( m != 0){
		int idx_new = indices_lvl_n[global_id];
		position_new[idx_new] = position_old[global_id];
	}	
}

__kernel void map_vertex_buffer2(
							__global char * mask,
							__global int * indices_lvl_n,
							__global float2 * tc_old,
							__global float2 * tc_new
						 )
{
	int global_id  = get_global_id(0);
	int m = convert_int(mask[global_id]);
	if( m != 0){
		int idx_new = indices_lvl_n[global_id];
		tc_new[idx_new] = tc_old[global_id];
	}
}



__kernel void map_triangle(
							__global char * tri_mask,
							__global int * indices_new,
							__global int * triangle_offset_new,
							__global int * triangles_old,
							__global int * triangles_new
						 )
{
	int i = get_global_id(0);
	int j = i*3;	
	
	int m = tri_mask[i];
	// Discard triangle
	if( m == 0){
		return;
	}	
	

	// Read old indices of the triangle vertices
	int i0_old = triangles_old[j + 0];
	int i1_old = triangles_old[j + 1];
	int i2_old = triangles_old[j + 2];
	
	// Read new indices of the triangle
	int i0 = indices_new[i0_old];
	int i1 = indices_new[i1_old];
	int i2 = indices_new[i2_old];
	

	// Store new indices of the triangle
	int offs = triangle_offset_new[i]*3;
	triangles_new[offs+0] = i0;
	triangles_new[offs+1] = i1;
	triangles_new[offs+2] = i2;
	

}
__kernel void map_triangle2(
							__global char * mask,
							__global int * indices_new,
							__global int * triangles_old,
							__global int * triangles_new,
							__global int * global_offset
						 )
{
	int i = get_global_id(0);
	int j = 3*i;
	
	// Read old indices of the triangle vertices
	int i0_old = triangles_old[j + 0];
	int i1_old = triangles_old[j + 1];
	int i2_old = triangles_old[j + 2];
	
	// Check of any of the vertices is discarded
	char  m = 1;
	m &= mask[i0_old];
	m &= mask[i1_old];
	m &= mask[i2_old];
	
	// Discard triangle
	if( m == 0){
		return;
	}	
	
	int current_offset = 0;
	int new_offset = 0;
	
	// Compare and change current offset in new triangle array
	do{	
		current_offset = *global_offset;
		new_offset     = current_offset + 3;		
	}while(atom_cmpxchg(global_offset,current_offset,new_offset) != current_offset);
	
	// Read new indices of the triangle
	int i0 = indices_new[i0_old];
	int i1 = indices_new[i1_old];
	int i2 = indices_new[i2_old];
	/*triangles_new[j  + 0] = current_offset;
	triangles_new[j  + 1] = current_offset+1;
	triangles_new[j  + 2] = current_offset+2;*/

	// Store new indices of the triangle
	triangles_new[current_offset + 0] = i0;
	triangles_new[current_offset + 1] = i1;
	triangles_new[current_offset + 2] = i2;
	
	
}
	
