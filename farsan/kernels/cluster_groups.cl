#define LOCAL_SIZE 256

#ifndef STOP_
#error define STOP_ before compiling cluster_group.cl
#endif

#ifndef ITEMS_TO_COVER_
#error define ITEMS_TO_COVER_ before compiling cluster_group.cl
#endif

#define N_PARTITIONS 1


typedef struct{
    uint offsets[LOCAL_SIZE+1];
	uint2 intervals[LOCAL_SIZE];
    uint sync_size[N_PARTITIONS];
    uint sync_sum_size[N_PARTITIONS+1];
	uint groups_used;
}IntervalInfoType;



void cluster(__local IntervalInfoType* interval_info, uint lid, uint gid, __global uint* offsets){
   
    uint group  = get_group_id(0);
     

    
    uint group_offset = group * LOCAL_SIZE;
    
    uint2 ranges[N_PARTITIONS];
    uint stride = 256/N_PARTITIONS;
    for(int i = 0; i < N_PARTITIONS; i++){
        ranges[i] = (uint2)(i,(i+1))*stride;    
    }    
    uint2 interval_cache[256];

    if(lid < N_PARTITIONS){        
       
        uint2 range = ranges[lid];
        uint prev_idx  = range.x;
        
        int prev_offset_index = group_offset - 1 + lid*stride;
        char mask = prev_offset_index > - 1;
        uint prev_offs = offsets[prev_offset_index] * mask;
        
        uint n = 0;
		for( uint i = range.x; i <= range.y; i++) {								
			uint curr_offs = interval_info->offsets[i];
			if((curr_offs-prev_offs) > ITEMS_TO_COVER_ ||i == range.y){
					interval_cache[n] = group_offset + (uint2)(prev_idx,i-1); 
					prev_idx = i;
					prev_offs = interval_info->offsets[i-1];
					n++;				
			}	
		}
        
        interval_info->sync_size[lid] = n;        
       
    }   
    barrier(CLK_LOCAL_MEM_FENCE);
    
    if(lid == 0){ 
        uint offset = interval_info->sync_size[0];
        interval_info->sync_sum_size[0] = 0;
        for(int p = 1; p < N_PARTITIONS; p++){                
                interval_info->sync_sum_size[p] = offset;
                offset  += interval_info->sync_size[p];
        }
        interval_info->sync_sum_size[N_PARTITIONS] = offset;
        interval_info->groups_used = offset;	
    }
    barrier(CLK_LOCAL_MEM_FENCE);
    
    if(lid < N_PARTITIONS) {
        uint n_intervals  = interval_info->sync_size[lid];
        uint offset = interval_info->sync_sum_size[lid];
        for(int i = 0; i < n_intervals; i++)            
            interval_info->intervals[offset+i] = interval_cache[i];                
    
    }
    barrier(CLK_LOCAL_MEM_FENCE);

    
}


__kernel void kernel_cluster_groups(
					__global uint * index_offset_in,
					__global uint2 * group_interval_out,
					__global uint * groups_used_offs,
					__global char * groups_used_mask 						
				)
{
	uint lid = get_local_id(0);
	uint gid = get_global_id(0);
	
    
	__local IntervalInfoType interval_info[1];
    
    // Copy input offset data
	interval_info->offsets[lid] = index_offset_in[gid];	
	barrier(CLK_LOCAL_MEM_FENCE);	
   
    // Group offsets into interval clusters  
	cluster(interval_info, lid, gid, index_offset_in);
	
    // Read result from local mem and write to output
	uint2 out = 0; uint  mask = 0;	
	if( (lid % LOCAL_SIZE) < interval_info->groups_used ){
			out = interval_info->intervals[lid];
			mask = 1;
	}
	
	groups_used_mask[gid] 	= mask; 
	groups_used_offs[gid] 	= mask;	
	group_interval_out[gid] = out; 

}

__kernel void kernel_copy_data(
					__global uint2 * group_interval_in,
					__global uint2 * group_interval_out,
					__global uint * groups_used_offs,
					__global char * groups_used_mask
					)
{
	uint gid = get_global_id(0);
	
	char mask = groups_used_mask[gid];
	uint idx  = groups_used_offs[gid];
	
	if(mask){
		group_interval_out[idx] = group_interval_in[gid];
	}


}