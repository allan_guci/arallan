
#pragma OPENCL EXTENSION cl_khr_global_int32_base_atomics : enable
#pragma OPENCL EXTENSION cl_khr_local_int32_base_atomics : enable
#pragma OPENCL EXTENSION cl_khr_global_int32_extended_atomics : enable
#pragma OPENCL EXTENSION cl_khr_local_int32_extended_atomics : enable


__kernel void init(__global float* position,
					const uint num_verts
				  ){

	int i = get_global_id(0);
	if( i >= num_verts) 
		return;

	position[i*3+1] -= 4.0;
	position[i*3+0] -= 0.0;
	
}

__kernel void copy(__global float* position,
				   __global float4* position4,
					const uint num_verts
				  ){

	int i = get_global_id(0);
	if( i >= num_verts) 
		return;

	int j = i*3;
	float4 p = (float4)(position[j],position[j+1],position[j+2],0);
		
	position4[i] = p;
	
}