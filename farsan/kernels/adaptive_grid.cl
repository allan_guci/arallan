#include "local_mem_util.cl"
#include "adaptive_util.cl"
#include "image_util.cl"

__kernel void update(
		__write_only image2d_t out,
		const float time
){

	int x = get_global_id(0);
	int y = get_global_id(1);

	int2 xy = (int2)(x,y);
	
	float xf = (float)x/(float)get_global_size(0);
	float yf = (float)y/(float)get_global_size(1);
	
	float scale = 2.0f;
	
	float res = 0.4f*cos(xf*scale+time)*sin(yf*scale*4+time*4.0f)+0.4f;
	
	write_imagef(out,xy,res);
}




__kernel void init_tree_layer(
							__read_only image2d_t in_grad_img,
							__read_only image2d_t in_img,
							__write_only image2d_t out_img,
							__global NodeType* tree_out,
							__global uint* local_tree_sizes,
							__global uint* local_tree_num_leaves,
                            __global uint* local_tree_sizes_next,
                            __global uint* tree_direct_leaves
							)
{
	INIT_2D_KERNEL;
	IMG2D_TO_LOCAL_MEM(l_img_buf,in_img,g_id,l_id_mem);
	IMG2D_TO_LOCAL_MEM(l_in_grad_img,in_grad_img,g_id,l_id_mem);
	
	int i = l_id.x; int j = l_id.y;
	int gi = g_id.x; int gj = g_id.y;
	int n_flat = j*get_local_size(0) + i;
	
	CREATE_TREE_INDICES(tree_idx,i,j);
	CREATE_TREE_INDICES_OFFSETS(nb_tree_idx,i,j);
	
	__local char tree_lvls[TREE_DATA_SIZE];
	tree_lvls[tree_idx[LAST_LEVEL]] =  convert_char(clamp(1*log2(ceil(l_in_grad_img[l_id_mem.y][l_id_mem.x]*80.0f)),0.0f,4.0f));
	
	__local float value_cache[TREE_DATA_SIZE];
	value_cache[tree_idx[LAST_LEVEL]]= l_img_buf[l_id_mem.y][l_id_mem.x];
	
	barrier(CLK_LOCAL_MEM_FENCE);
	
	// Determine LOD
	TRAV_DOWN(i,j)
			char max_lvl  = 0;
			float tmp_val = 0.0f;
		NODE_ACTIVE_TRAV_DOWN
			// Max level and mean for children of parent at lvl-1
			for(int yy = 0; yy < 2; yy++)
				for(int xx = 0; xx < 2; xx++){
					max_lvl  = max(tree_lvls[nb_tree_idx[lvl][yy][xx]],max_lvl);
					tmp_val += value_cache[nb_tree_idx[lvl][yy][xx]];		
			}			
			// Assign level and data to parent
			int idx = tree_idx[lvl-1];
			tree_lvls[idx]  = max_lvl;
			value_cache[idx] = tmp_val*0.25f;		
			
		NODE_ACTIVE_TRAV_DOWN_END
		// Syncronize all local writing before entering next level
		barrier(CLK_LOCAL_MEM_FENCE);
	TRAV_DOWN_END
	
	// Allocate leaf mask and local tree
	__local int tree_mask[TREE_DATA_SIZE];	
	__local NodeType tree[TREE_DATA_SIZE];
    __local short tree_leaves[TREE_DATA_SIZE];
	CLEAR_TREE(tree_mask,tree_idx,i,j);
	CLEAR_TREE(tree_leaves,tree_idx,i,j);
    CLEAR_TREE_LEAF_MASK(tree,tree_idx,i,j);
    
	barrier(CLK_LOCAL_MEM_FENCE);
	
	// Find leaves 
	float out_val = 0.0f;
	
	TRAV_UP(i,j)
	
	int idx = tree_idx[lvl];	
	uchar is_leaf = tree_lvls[idx] <= lvl;
	
		NODE_ACTIVE_TRAV_UP		
			// Mark node as used
			tree_mask[idx]        = 1; 		
			//Assign node data				
			tree[idx].bbSquare    = get_range(gi,gj,lvl);
			tree[idx].is_leaf  	  = is_leaf;
			tree[idx].level    	  = lvl;
			tree[idx].parent   	  = tree_idx[max(lvl-1,0)];
			ASSING_CHILD_INDICES(tree[idx],i,j,lvl);
			tree[idx].value       = value_cache[idx];
			
		NODE_ACTIVE_TRAV_UP_END
		// If leaf found, break. No further R/W to children of the actual node		
		if(is_leaf){
			tree[idx].num_leaves = 1;	
			out_val = value_cache[idx]; 
			break;
		}
	TRAV_UP_END	

	barrier(CLK_LOCAL_MEM_FENCE);	
	
	// Create new array for pack active nodes tight
	__local int tree_idx_dense[TREE_DATA_SIZE];
   
	int i_wg   = get_num_groups(0)*get_group_id(1) + get_group_id(0);
    uint n_leaves = 0;
	
	if(n_flat == 0){			
		int offset = tree_mask[0];		
		tree_idx_dense[0] = 0;
		tree_idx_dense[1] = tree_mask[1];
        
        
        uint is_leaf = tree[0].is_leaf;
        tree_leaves[n_leaves] = 0;            
        n_leaves += is_leaf;
        
		for(int n = 1; n < TREE_DATA_SIZE; n ++ ){
			int tmp = tree_mask[n];	
			tree_idx_dense[n] = offset;
            
            uint is_leaf = tree[n].is_leaf;
            tree_leaves[n] += is_leaf*n_leaves;            
            n_leaves += is_leaf;
            
			offset += tmp;
            
		}
		local_tree_sizes[i_wg] = offset; 
		
	}		
	
	// Sum leaves for all underlying nodes, for each active node
	TRAV_DOWN(i,j)
		NODE_ACTIVE_TRAV_DOWN				
			int num_sub_leaves = 0;
			for(int yy = 0; yy < 2; yy++)
				for(int xx = 0; xx < 2; xx++){
					num_sub_leaves += tree[nb_tree_idx[lvl][yy][xx]].num_leaves;
				}					
			int idx = tree_idx[lvl-1];	// read idx for next level
			tree[idx].num_leaves += num_sub_leaves; 			
		NODE_ACTIVE_TRAV_DOWN_END	
		// Syncronize all local writing before entering next level
		barrier(CLK_LOCAL_MEM_FENCE);	
	TRAV_DOWN_END
	
	
	if(n_flat == 0){			
		local_tree_num_leaves[i_wg] = n_leaves; 
        local_tree_sizes_next[i_wg] = (i_wg+1)*TREE_DATA_SIZE;
	}
	// Pack nodes/leaves tight in the output buffer
	int stride = TREE_DATA_SIZE;	
	int tree_ofs = stride*i_wg;	
	
    for(int k = 0; k < 2; k++){
        int idx = n_flat + k*256;
        if(tree_mask[idx] && idx < 341){
				// Read private copy before packing indices
				NodeType n = tree[idx];
				// Pack children indices 
				for(int c = 0; c < 4; c++)
					n.children[c] = tree_idx_dense[n.children[c]];
				// Write node to output
				tree_out[tree_ofs+tree_idx_dense[idx]] = n;
                if(n.is_leaf)
                    tree_direct_leaves[tree_ofs+tree_leaves[idx]] = tree_idx_dense[idx];                        
        }
        
        
    }
	
	barrier(CLK_LOCAL_MEM_FENCE);	
	write_imagef(out_img,g_id,out_val);
	
}



__kernel void commit(
				__read_only image2d_t in,
				__write_only image2d_t out
				)
{
	int2 g_id = (int2) (get_global_id(0), get_global_id(1));	
	write_imagef(out,g_id,READ_IMG(in,g_id).x);
}