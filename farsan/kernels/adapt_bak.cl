#include "local_mem_util.cl"
const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE | //Natural coordinates
						 CLK_ADDRESS_REPEAT | //Clamp to zeros
						 CLK_FILTER_NEAREST; //Don't interpolate						 
#define CHECK_BOUNDS(p,s,o) \
if(p < o || p > (s-2-o)) return

#define FLAT_ID get_global_id(1)*get_global_size(0) + get_global_id(0)

#define READ_IMG(img,idx) read_imagef(img, sampler,idx)
#define WRITE_IMG(img,idx)
#define FILTER_KERNELS  \
		float kern_lapl[9] = { 0.5f,1.0f ,0.5f,1.0f,-6.0f,1.0f,0.5f,1.0f,0.5f}; \
		float kern_lapl2[9] = { 0.5f,1.0f ,0.5f,1.0f,-6.0f,1.0f,0.5f,1.0f,0.5f}; \
		float kern_sobx[9] = {-1,0,1,-1,0,1,-1,0,1}; \
		float kern_soby[9] = { 1,1,1,0,0,0,-1,-1,-1};		

__kernel void update(
		__write_only image2d_t out,
		const float time
){

	int x = get_global_id(0);
	int y = get_global_id(1);

	int2 xy = (int2)(x,y);
	
	float xf = (float)x/(float)get_global_size(0);
	float yf = (float)y/(float)get_global_size(1);
	
	float scale = 2.0f;
	
	float res = 0.4*cos(xf*scale+time)*sin(yf*scale*4+time*4.0f)+0.4f;
	
	write_imagef(out,xy,res);
}

__kernel void laplacian(
		__read_only image2d_t in,
		__write_only image2d_t out
){
	int4 tmp;
	int2 g_id 	  = (int2) (get_global_id(0),get_global_id(1));
	int2 l_id 	  = (int2) (get_local_id(0),get_local_id(1))+1; 
	IMG2D_TO_LOCAL_MEM(lbuffer,in,g_id,l_id);
	
	FILTER_KERNELS; 
	
	float cache[9];
	int i = 0;
	int size_kern = 1;
	for( int yi = -size_kern; yi <= size_kern; yi ++)
		for( int xi = -size_kern; xi <=size_kern; xi ++){			
				int2 idx2 = l_id+(int2)(xi,yi);
				cache[i] = lbuffer[idx2.y][idx2.x];
				i++;		
	}
	float res_lapl = 0.0f;
	float resx     = 0.0f;
	float resy     = 0.0f;
	
	for(int n = 0; n < 9; n++){
				res_lapl += kern_lapl[n]*cache[n];
				resx 	 += kern_sobx[n]*cache[n];
				resy 	 += kern_soby[n]*cache[n];
	}
	float res = max(res_lapl+length((float2)(resx,resy)),0.0f);
	
	write_imagef(out,g_id,res);
	//write_imagef(out,g_id,READ_IMG(in,g_id).x);
}
/*
__kernel void adapt_stage_1(
							__read_only image2d_t in,
							__write_only image2d_t out
							)
{
	
	int2 g_id     = 16*(int2) (get_global_id(0),get_global_id(1));
	int2 id_s[
	int2 id_left = g_id + (int2)(-1,0);
	int2 id_up = g_id + (int2)(0,1);
	int2 id_left = g_id + (int2)(-1,0);
	int2 id_up = g_id + (int2)(0,1);
	
	
	lbuffer[local_id.y][local_id.x] = floor(lbuffer[local_id.y][local_id.x]*10.0f);
	BARRIER(CLK_LOCAL_MEM_FENCE);
	
	if(local_id.x == 0 && local_id.y == 0){
		for(int i = 0; i < 16; i +=2)
			for(int j = 0; j < 16; j +=2){
				
				float r = lbuffer[i][j+1];
				float u = lbuffer[i+1][j];
				float ur = lbuffer[i+1][j+1];
				float level = max(max(r,u),ur);
				
				lbuffer[i+1][j]   = level;
				lbuffer[i][j+1]   = level;
				lbuffer[i+1][j+1] = level;
					
			}
	
	
	}
	BARRIER(CLK_LOCAL_MEM_FENCE);
	
	write_imagef(out,g_id,lbuffer[local_id.y][local_id.x]);
	
	
}
*/

typedef struct{
	char num_children;
	int2 idx;
	float4 pos;
	float value;
}NodeType;
#define LSIZE 16
#define N_LEVELS 5
#define LAST_LEVEL (N_LEVELS-1)
#define TREE_DATA_SIZE 341


int get_tree_index(int i,int j, int lvl){

		
		int offset 	  = ((1 << (lvl << 1) ) - 1)/3;			
		int bit_shift = LAST_LEVEL - lvl;
		int x     	  = i >> bit_shift; 
		int y 		  = j >> bit_shift;
		int side  	  = 1 << lvl;
		return offset + y*side + x;	
		
}
#define CREATE_TREE_INDICES(BUF_NAME,i,j) \
	int BUF_NAME[5];	\
	for(int lvl = 0; lvl < N_LEVELS; lvl++)	\
		BUF_NAME[lvl] = get_tree_index(i,j,lvl)
		
#define CREATE_TREE_INDICES_OFFSETS(BUF_NAME,i,j) \
	int BUF_NAME[5][2][2];	\
	for(int yy = 0; yy < N_LEVELS; yy++)	\
		for(int xx = 0; xx < N_LEVELS; xx++)	\
			for(int lvl = 0; lvl < N_LEVELS; lvl++)	\
				BUF_NAME[lvl] = get_tree_index(i++xx,j+yy,lvl)

__kernel void adapt_stage_1(

							__read_only image2d_t in_grad_img,
							__read_only image2d_t in_img,
							__write_only image2d_t out_img
						
							
							)
{
	INIT_2D_KERNEL;
	IMG2D_TO_LOCAL_MEM(l_img_buf,in_img,g_id,l_id_mem);
	IMG2D_TO_LOCAL_MEM(l_in_grad_img,in_grad_img,g_id,l_id_mem);
	
	CREATE_TREE_INDICES(tree_idx,i,j);
	CREATE_TREE_INDICES(nb_tree_idx,i,j);
	
	int i = l_id.x; int j = l_id.y;
	
	__local char tree_cache[N_LEVELS][LSIZE][LSIZE];
	tree_cache[LAST_LEVEL][l_id.y][l_id.x] = convert_char(clamp(log2(ceil(l_in_grad_img[l_id_mem.y][l_id_mem.x]*2.0f)),0.0f,4.0f));
	
	__local float value_cache[N_LEVELS][LSIZE][LSIZE];
	value_cache[LAST_LEVEL][l_id.y][l_id.x] = l_img_buf[l_id_mem.y][l_id_mem.x];
	
	barrier(CLK_LOCAL_MEM_FENCE);
	

	int cut = 2;
	
	bool found = false;
	
	
	for(int lvl = LAST_LEVEL; lvl > 0; lvl--){
		if(!found){
			if( (i % cut) != 0 || (j % cut) != 0){
				found = true;
			}else{
				int bit_shift = (LAST_LEVEL - lvl);
				int x = i >> bit_shift; int y = j >> bit_shift;

				char max_lvl = 0;
				float tmp_val = 0.0f;
				for(int yy = 0; yy < 2; yy++)
					for(int xx = 0; xx < 2; xx++){
						max_lvl = max(tree_cache[lvl][y+yy][x+xx],max_lvl);
						tmp_val += value_cache[lvl][y+yy][x+xx];
						
					}			
				bit_shift++;
				x = i >> bit_shift;	y = j >> bit_shift;
				tree_cache[lvl-1][y][x]  = max_lvl;
				value_cache[lvl-1][y][x] = tmp_val*0.25f;	
				cut = cut << 1;
			}
		}
		barrier(CLK_LOCAL_MEM_FENCE);
	}
	
	int node_lvl = -1;
	float out_val = -10.0f;
	
	
	
	for(int lvl = 0; lvl < N_LEVELS; lvl ++){
	
		int bit_shift = (LAST_LEVEL - lvl);
		int x = i >> bit_shift; int y = j >> bit_shift;
		
		node_lvl = tree_cache[lvl][y][x];
		if(node_lvl <= (lvl)){
			out_val = value_cache[lvl][y][x];
			node_lvl = lvl;
		
			break;
		}
		
	}
	
	/*int itree = -1;
	__local char tree_mask[TREE_DATA_SIZE];
	for(int lvl = 0; lvl < N_LEVELS; lvl ++){
	
		int bit_shift = (LAST_LEVEL - lvl);
		int x = i >> bit_shift; int y = j >> bit_shift;
		
		node_lvl = tree_cache[lvl][y][x];
		char mask = 0.0f;
		
		if(node_lvl <= (lvl)){	
			mask = 1.0f;
		}
		
		int cut_ = 1 << bit_shift;
		if( (i % cut_) == 0 && (j % cut_) == 0){
				itree =	tree_idx[lvl]; 
			//	tree_mask[itree] = mask;
		}	
	}*/
	barrier(CLK_LOCAL_MEM_FENCE);
	
	
	write_imagef(out_img,g_id,node_lvl); //-READ_IMG(in_img,g_id)); //value_cache[2][l_id.y][l_id.x]);
	//write_imagef(out_img,g_id,READ_IMG(in_grad_img,g_id)); //value_cache[2][l_id.y][l_id.x]);
	//write_imagef(out_img,g_id,(float4)(xout,yout,0,0)); //raw[l_id.y][l_id.x]);
    //write_imagef(out_img,g_id,value_cache[3][l_id.y][l_id.x]);
	
}


__kernel void commit(
				__read_only image2d_t in,
				__write_only image2d_t out
				)
{
	int2 g_id = (int2) (get_global_id(0),get_global_id(1));	
	write_imagef(out,g_id,READ_IMG(in,g_id).x);
}


	barrier(CLK_LOCAL_MEM_FENCE);
	itree = 0;
	__local char tree_idx_dense[TREE_DATA_SIZE];
	COPY_TREE(tree_mask,tree_idx_dense,tree_idx,i,j);
	int flat = j*16 + i;
	itree = get_tree_index1(flat,7);

	int tmp_idx = -1;
	int TMPTMP = 6;
	bool outside_data = (flat << 1) < TREE_DATA_SIZE;
	
	for(int lvl = TMPTMP; lvl > (TMPTMP-1); lvl--){
	
		if(outside_data){
		
			if(MOD_CHECK1(1 << (N_LEVELS_1-lvl),flat)){
			
				int lvl_n1 = lvl+1;
				int flat_tmp = flat >> (N_LEVELS_1-lvl_n1);
				
				//if(flat < ( (1 << lvl_n1)  - 1)){
					int res = tree_idx_dense[tmp_idx]+tree_idx_dense[tmp_idx+1];
					tmp_idx = flat_tmp; //get_tree_index1(flat,lvl+1);
					//tree_idx_dense[tmp_idx] = res;
					node_lvl = tmp_idx;
				//}
			
			}
		
		}
		barrier(CLK_LOCAL_MEM_FENCE);*/
	}

		/*
	for(int lvl = 1; lvl < 9; lvl++){
		if( (flat << 1) < TREE_DATA_SIZE){
			if(flat % (1 << lvl) == 0){
				int tmp_idx = get_tree_index1(flat,lvl-1);
				int res = tree_mask[tmp_idx];
				tmp_idx = get_tree_index1(flat,lvl);
				tree_mask[tmp_idx] = tmp_idx;
			}
		
		}
	}*/

	
	//out_val = convert_float(value_cache[tree_idx[4]]);
	int lvl = 8;
	write_imagef(out_img,g_id,tmp_idx);//get_tree_index1(flat,8)]);
	
	//write_imagef(out_img,g_id,convert_float(tree_mask[tree_idx[4]])); //nb_tree_idx[4][0][0]); //-READ_IMG(in_img,g_id)); //value_cache[2][l_id.y][l_id.x]);
	//write_imagef(out_img,g_id,READ_IMG(in_grad_img,g_id)); //value_cache[2][l_id.y][l_id.x]);
	//write_imagef(out_img,g_id,(float4)(xout,yout,0,0)); //raw[l_id.y][l_id.x]);
    //write_imagef(out_img,g_id,value_cache[3][l_id.y][l_id.x]);
	__kernel void kernel_group_tree(
					__global int * index_offset_in,
					__global int2 * group_interval_out,
					__global int * groups_used,
					const int groups_used_prev
				)
{
	int lid = get_local_id(0);
	int gid = get_global_id(0);
	
	int group  = get_group_id(0);
	
	__local int locmem[256];
	__local int locindex_offs[257];
	__local int2 group_interval[256];
	
	int global_size = get_global_size(0);
	int last_elem = global_size - 1;
	int in_index  = index_offset_in[gid+1];
	locindex_offs[gid] =  index_offset_in[gid+1];
	locmem[gid] = /*gid/last_elem + */in_index / TREE_DATA_SIZE; 
	barrier(CLK_LOCAL_MEM_FENCE);
	
	int stop = get_global_size(0) % 257;
	int prev_bin = locmem[0]; 
	int curr_bin = 0;
	int prev_index = 0;
	int extra_bins = 0;
	int prev_offs = 0; //locindex_offs[0]; 
	if(lid == 0){
		int n = 0;
		locindex_offs[stop] = index_offset_in[stop];
		locmem[stop] = locmem[stop-1] + 1; // force to end last interval
		for( int i = 0; i < stop; i++) {						
			int index_offs = locindex_offs[i];
			char need_extra_interval = (index_offs-prev_offs) > TREE_DATA_SIZE;
		
			if(need_extra_interval){
					group_interval[n] = (int2)(prev_offs,index_offs); //(int2)(prev_index,i-1);
					prev_index = i;
					n++;
					prev_offs = locindex_offs[i];
					extra_bins++;
			}	
			/*curr_bin = locmem[i+1];
			
			if(curr_bin > prev_bin ){
			
				group_interval[n] = (int2)(prev_offs,index_offs); //(int2)(prev_index,i);
				prev_index = i+1;
				prev_bin = curr_bin;
				
				prev_offs = index_offs;
				n++;
			}*/
		}
	    groups_used[0]  = prev_bin;	
	}
	
	

	group_interval_out[gid] = group_interval[gid]; //(int2)(locmem[gid],locindex_offs[gid]);

}

int gid = get_global_id(0);
	int lid = get_local_id(0);
	int group  = get_group_id(0);
	
	__local int read_offset[10];
	__local int roots[10];
	__local int local_id_to_read_offset[10];
	__local int num_interval_loc[1];
	__local int write_index_offset[1];

	if(lid == 0){
		int2 interval = group_interval[group];
		roots[0] = 0;
		num_interval_loc[0] = (interval.y - interval.x)+1;
		int start = interval.x; int stop = interval.y;
	    int n = 0;
		write_index_offset[0] = index_offset[interval.x];
		int first_offset =  index_offset[start];
		for(int i = start; i <= stop; i ++ ){
				roots[n+1] = index_offset[i+1] - first_offset;
				read_offset[n] = i*TREE_DATA_SIZE; 
				n++;
		}
	}
	barrier(CLK_LOCAL_MEM_FENCE);
	
	int read_index = -1;
	int next_root = 0;
	for(int i = 1; i <= num_interval_loc[0]; i++){
		if(lid < roots[i]){
			read_index = lid + read_offset[i-1];
			next_root  =  i;
			break;
		}	
	}	
	int write_index = write_index_offset[0] + lid;
	
	if(write_index >= (int)num_tree_nodes || read_index == -1)
		return;
	
	debug_out[write_index] =  read_index; //read_offset[n]; //read_index;
	
	
	lid += 256;
	if( lid >= TREE_DATA_SIZE) 
		return;
		
	
	read_index = -1;	
	for(int i = next_root; i <= num_interval_loc[0]; i++){
		if(lid < roots[i]){
			read_index = lid + read_offset[i-1];
			break;
		}		
	}	
	
	write_index = write_index_offset[0] + lid;
	
	if(write_index >= (int)num_tree_nodes || read_index == -1)
		return;
	
	debug_out[write_index] =  read_index; //read_offset[n]; 

	//tree_dense[write_index] = tree_sparse[read_index];