const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE | //Natural coordinates
						 CLK_ADDRESS_REPEAT | //Clamp to zeros
						 CLK_FILTER_NEAREST; //Don't interpolate						 
const sampler_t samplerLinear = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_REPEAT | CLK_FILTER_LINEAR; 

typedef struct {
	float4 offset;
	float4 scale;
	float4 camoffset;
	float frequency;
	float amplitude;
	float ncompontents;
	float time;
}SurfacePropertyType;

typedef struct {
	float prev_pos;
	float curr_pos;
}SurfaceDataType;

__kernel void init_pos(
		__global float4* vertex_position,
		__global float2* vertex_texcoord,
		__global SurfaceDataType* surface_data,
		__global SurfaceDataType* surface_data_next,
		const SurfacePropertyType properties
){
	int i = get_global_id(0);
	float x = (float)(i % SURF_SIZE_X)/SURF_SIZE_X;
	float y = 0.0f;
	float z = (float)(i / SURF_SIZE_X)/SURF_SIZE_X;
	
	float4 p = (float4)(x,y,z,0.0f)*properties.scale + properties.offset;
	
	vertex_position[i] = p;
	vertex_texcoord[i] = (float2)(x,z);
	
	SurfaceDataType d;
	d.curr_pos = p.y;
	d.prev_pos = p.y;
	surface_data[i] = d;
	surface_data_next[i] = d;
}
#define FLAT_INDEX(v,w) (v.x + v.y*w)


#define CHECK_BOUNDS(p,s,o) \
(p < o || p > (s-1-o));



__kernel void update(
		__global SurfaceDataType* surface_data,
		__global SurfaceDataType* surface_data_next,
		__global float4* vertex_position,
		const SurfacePropertyType properties
){
	int x = get_global_id(0);
	int y = get_global_id(1);
	int i =  y*get_global_size(0) + x; 
	
	char mask = CHECK_BOUNDS(x,get_global_size(0),1);
	mask     |= CHECK_BOUNDS(y,get_global_size(1),1);
	
	// Read indata
	float prev_pos = surface_data[i].prev_pos;
	float curr_pos = surface_data[i].curr_pos;		
	float default_y = properties.offset.y;
	float force_y 	= 0;
	
	
	float time = properties.time;
	float ext_force_time = fmod(time,5.5f);
	
	

	float y_neigh = 0.0f;
	
	int2 offset[4] = {
						{-1,0},
						{1,0},
						{0,-1},
						{0,1}
					 };
					 
	// Read neighbour particle heights
	for(int n = 0; n < 4; n++)
	{
		int idx = (y+offset[n].y)*get_global_size(0) + (x+offset[n].x);
		y_neigh += vertex_position[idx].y;		
	}
		
	// Compute force
	float diff_y_neigh   = (y_neigh*0.25f - curr_pos);
	float diff_y_default = (properties.offset.y - curr_pos);	
	force_y    			 =  1.1f*diff_y_neigh + 0.001f*diff_y_default; 

	
	// Add external force
	float2 n_pos = convert_float2((int2)(x,y))/convert_float2((int2)(get_global_size(0),get_global_size(1)));
	float ampl  = 0.25f;
	float freq  = 6.4f;
	float circle_size = 0.048f;
	float circle_offset = 0.5f;
	

	float2 source_position[4];
	float speed = 0.4f;
	source_position[0] = 0.5f + 0.4f*(float2)(cos(speed*properties.time),sin(-speed*properties.time));
	source_position[1] = 0.5f + 0.4f*(float2)(cos(-speed*properties.time+0.8f),sin(speed*properties.time+0.6f));
	source_position[2] = 0.5f + 0.4f*(float2)(cos(speed*properties.time+0.1f),sin(-speed*properties.time+0.1f));
	source_position[3] = 0.5f + 0.4*(float2)(cos(speed*properties.time),sin(speed*properties.time+4.2f));
	

	float att = 0.0f;
	for(int n = 0; n < 2; n ++){
		float radius = length(n_pos - source_position[n]); 
		if(radius < circle_size) {
			att = -(cos(20.0f))*0.005f*radius/circle_size;
			//next_y_pos = default_y - att*amplitude[n]; //*pow(cos(frequency[n]*properties.time),2.0);	
			break;
		}
	}
	att *= 0.25f;
	
	
	
	
	// System damping
	float dt = 0.06f;
	float vel_y = (curr_pos - prev_pos)/dt;
	force_y -= vel_y*0.00055f;
	
	// Integrate
	float next_y_pos = 2.0f*curr_pos - prev_pos  + pow(dt,2.0f)*(force_y+ att);
	
	
	


		
	if(mask == 1)
		next_y_pos = curr_pos;
	
	// Write output
	SurfaceDataType d;
	d.prev_pos = curr_pos;
	d.curr_pos = next_y_pos;
	surface_data_next[i] = d;
	
	
	vertex_position[i].y = next_y_pos; 
}

__kernel void compute_normal(__global float4* position,
							  __global float4* normal
							  ){

	int x = get_global_id(0);
	int y = get_global_id(1);
	int i = y*get_global_size(0) + x;
	int w = get_global_size(0);
	
	int2 xy[6] = {
			{x+1,y},
			{x,y+1},
			{x-1,y+1},
			{x-1,y},
			{x,y-1},
			{x+1,y-1}
	};	
					
	float4 center = position[i];
	float4 delta[6];
	for ( int n = 0; n < 6; n ++){
		int j = FLAT_INDEX(clamp(xy[n],0,w-1),w);
		delta[n] = center - position[j];
	}
	
	float3 norm = 0.0f;
	for(int n = 0; n < 6; n++){
		int i1 = n;
		int i2 = (n+1)%6;
		norm += normalize(cross(delta[i1].xyz,delta[i2].xyz));
	}
	norm /= 6.0f;
	normal[i].xyz = norm;
	
}
