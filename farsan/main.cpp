#define __CL_ENABLE_EXCEPTIONS
#define GL_SHARING_EXTENSION "cl_khr_gl_sharing"


#include "farsan_thread.h"
/// GUI include
#include "gui_db.h"

#ifdef _WIN32
#include <windows.h>
#include "GL_utilities.h"
#include "GL/freeglut.h"

#include <Wingdi.h>
#include <direct.h>

#include <CL/cl.h>
#include <CL/cl_gl.h>



#else /// UNIX

#include "GL_utilities.h"
//#include <thread>
#include <GL/glut.h>
#include <GL/gl.h>
#include <CL/cl_gl.h>
#include <GL/glx.h>



#endif

#include "ar_handler.h"


/// Standard
#include <iostream>
#include <stdlib.h>
#include <stdio.h>


/// TSBK-utils
#include "user_camera.h"

#include <CL/cl.hpp>



/// VIDEO
/*extern "C" {
    #define __STDC_CONSTANT_MACROS
    #include <libavcodec/avcodec.h>
    #include <libavformat/avformat.h>
    #include <libswscale/swscale.h>
}

*/

UserCamera *userCamera;
/// CL Globals
cl::Context clContext;

/// GUI objects
FarsanThread guiThread;
GUIDB * guiDB;

ARHandler * arHandler;

#define SCREEN_SIZE  512

void reshape(GLsizei w, GLsizei h)
{
	userCamera->setScreenSize(w,h);
}

void keyboard (unsigned char key, int x, int y)
{
    userCamera->keyPress(key, 0, 0);
/*
    if(key = 'n')
        startVideo();
    if(key = 'm')
        stopVideo();
*/
}




void mouseClick (int button, int state, int x, int y)
{
    if (state == GLUT_UP)
        userCamera->mouseUp();
}

void mouse (int x, int y)
{
    userCamera->mouseHandle(x, y);
}


void OnTimer(int value)
{
	glutPostRedisplay();
	glutTimerFunc(1, &OnTimer, value);
}

void display(){

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glClearColor(0.0, 0.4, 0.4, 0.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    /*
	if(!farsanDispatcher.update())
	{
		std::cout<<"display: fARsanObject finished"<<std::endl;
		guiThread.join();
		//farsanDispatcher.~FARsanDispatcher();
		exit(1);
	}

	*/

	arHandler->runCL();
	arHandler->drawGL(userCamera);

	glutSwapBuffers();

}

cl::Device getValidGLCLInteropDevice(cl::Platform platform, cl_context_properties* properties)
{
		cl::Device displayDevice;
		cl_device_id interopDeviceId;
		int status;
		size_t deviceSize = 0;
		clGetGLContextInfoKHR_fn glGetGLContextInfo_func = (clGetGLContextInfoKHR_fn)clGetExtensionFunctionAddress("clGetGLContextInfoKHR");
		status = glGetGLContextInfo_func( properties,
	                                    CL_CURRENT_DEVICE_FOR_GL_CONTEXT_KHR,
	                                    sizeof(cl_device_id),
	                                    &interopDeviceId,
	                                    &deviceSize);
		if(deviceSize == 0)
	        throw cl::Error(1,"No GLGL devices found for current platform");
		if(status != CL_SUCCESS)
			throw cl::Error(1, "Could not get CLGL interop device for the current platform. Failure occured during call to clGetGLContextInfoKHR.");
		return cl::Device(interopDeviceId);
}


void clInit()
{
    cl_int err = CL_SUCCESS;

    std::vector<cl::Platform> platforms;
    if(cl::Platform::get(&platforms) != CL_SUCCESS)
    {
        std::cerr << "error while getting opencl platforms." << std::endl;
        exit(-1);
    }
    if(platforms.empty())
    {
        std::cerr << "no opencl platforms found." << std::endl;
        exit(-1);
    }

    #ifdef WIN32
    cl_context_properties properties[] = {
        CL_GL_CONTEXT_KHR, (cl_context_properties)wglGetCurrentContext(), // WGL Context
        CL_WGL_HDC_KHR, (cl_context_properties)wglGetCurrentDC(), // WGL HDC
        CL_CONTEXT_PLATFORM, (cl_context_properties)platforms.at(0)(), // OpenCL platform
        0
    };
    #else // UNIWX
    cl_context_properties properties[] = {
        CL_GL_CONTEXT_KHR, (cl_context_properties)glXGetCurrentContext(), // WGL Context
        CL_GLX_DISPLAY_KHR, (cl_context_properties)glXGetCurrentDisplay(), // WGL HDC
        CL_CONTEXT_PLATFORM, (cl_context_properties)platforms.at(0)(), // OpenCL platform
        0
    };

    #endif
        //cl_int err;

        cl::Device device = getValidGLCLInteropDevice(platforms[0], properties);


        //std::cout <<		device.getInfo<CL_DEVICE_EXTENSIONS>() << std::endl;

    try
    {
        clContext = cl::Context(device,properties,NULL,NULL,&err);
    }
    catch(const cl::Error &err)
    {
      std::cerr << "ERROR: " << err.what() << " (" << err.err() << ")" << std::endl;
      throw err;
      exit(-1);
    }

    if(err != CL_SUCCESS){
            printf("Failed to create CL context");
            exit(-1);
    }
}



THREAD_RET_VAL threadGUI( THREAD_ARG_TYPE data ){

    guiDB->startGUI();

	return NULL;


}

void glInit(int argc, char** argv){

		glutInit(&argc, argv);
		glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH);
	 	glutInitWindowSize (1280, 720);
 		glutCreateWindow ("fARsan");

        glutInitContextVersion(4, 4);

		glutReshapeFunc(reshape);
		glutMouseFunc(mouseClick);
		glutMotionFunc(mouse);
        glutKeyboardFunc(keyboard);
        glutDisplayFunc(display);
		glutTimerFunc(1, &OnTimer, 0);

        GLenum err = glewInit();
        if (GLEW_OK != err)
        {
          fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
        }
        if (glewIsSupported("GL_VERSION_1_4  GL_ARB_point_sprite"))
        {
            fprintf(stdout, "Status: GL_VERSION 1_4");
        }
        fprintf(stdout, "Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));

    printError("ROLF");
}

int main(int argc,char** argv){
	userCamera = new UserCamera();
	userCamera->init();
	glInit(argc,argv);

	clInit();
	arHandler = new ARHandler();
	arHandler->init(clContext);
    guiDB = new GUIDB();
    arHandler->setGUI(guiDB);
	guiThread.spawn(threadGUI,NULL);
	int n = 1;
    for(int i = 0; i < 90000000; i++)
        n = i*i;

    printf("hej %d",n);

	glutMainLoop();



	return 1;
}
